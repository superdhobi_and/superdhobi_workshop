package com.superdhobi.supperdhobi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.superdhobi.AdapterPackage.Address_recycler_adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by PCIS-ANDROID on 14-04-2016.
 */
public class Choose_time extends AppCompatActivity {

    private String[] dateArray;
    private String[] dayArray;
    private String[] monthArray;
    private String[] week_dayArray;
    RecyclerView timendate;

    static String date;

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_timendate);
            timendate= (RecyclerView) findViewById(R.id.recycler_view_men);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_Pr);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // for 1st day
        Date now = new Date();
        Date alsoNow = Calendar.getInstance().getTime();
        date = new SimpleDateFormat("dd-MMM-yyyy").format(now);
        int month = now.getMonth();
        int d = now.getDate();
        int yr = now.getYear();
        String date_element1=alsoNow.toString();

        String dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", alsoNow);//Thursday
        String stringMonth = (String) android.text.format.DateFormat.format("MMM", alsoNow); //Jun
        String intMonth = (String) android.text.format.DateFormat.format("MM", alsoNow); //06
        String year = (String) android.text.format.DateFormat.format("yyyy", alsoNow); //2013
        String day = (String) android.text.format.DateFormat.format("dd", alsoNow); //20


        // for 2nd day

        final Calendar c = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        c.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = c.getTime();
        String date_element2=tomorrow.toString();

        String dayOfTheWeek_t = (String) android.text.format.DateFormat.format("EEEE", tomorrow);//Thursday
        String stringMonth_t = (String) android.text.format.DateFormat.format("MMM", tomorrow); //Jun
        String intMonth_t = (String) android.text.format.DateFormat.format("MM", tomorrow); //06
        String year_t = (String) android.text.format.DateFormat.format("yyyy", tomorrow); //2013
        String day_t = (String) android.text.format.DateFormat.format("dd", tomorrow); //20

        // // for 3rd day

        final Calendar c1 = Calendar.getInstance();
        DateFormat dateFormat_3 = new SimpleDateFormat("dd-MM-yyyy");
        c1.add(Calendar.DAY_OF_YEAR, 2);
        Date tomorrow_1 = c1.getTime();
        String date_element3=tomorrow_1.toString();

        String dayOfTheWeek_th = (String) android.text.format.DateFormat.format("EEE", tomorrow_1);//Thursday
        String stringMonth_th = (String) android.text.format.DateFormat.format("MMM", tomorrow_1); //Jun
        String intMonth_th = (String) android.text.format.DateFormat.format("MM", tomorrow_1); //06
        String year_th = (String) android.text.format.DateFormat.format("yyyy", tomorrow_1); //2013
        String day_th = (String) android.text.format.DateFormat.format("dd", tomorrow_1); //20

        //for 4th day
        final Calendar c2 = Calendar.getInstance();
        DateFormat dateFormat_4 = new SimpleDateFormat("dd-MM-yyyy");
        c2.add(Calendar.DAY_OF_YEAR, 3);
        Date tomorrow_2 = c2.getTime();
        String date_element4=tomorrow_2.toString();

        String dayOfTheWeek_fr = (String) android.text.format.DateFormat.format("EEE", tomorrow_2);//Thursday
        String stringMonth_fr = (String) android.text.format.DateFormat.format("MMM", tomorrow_2); //Jun
        String intMonth_tfr = (String) android.text.format.DateFormat.format("MM", tomorrow_2); //06
        String year_tfr = (String) android.text.format.DateFormat.format("yyyy", tomorrow_2); //2013
        String day_fr = (String) android.text.format.DateFormat.format("dd", tomorrow_2); //20

//for 5th day
        final Calendar c3 = Calendar.getInstance();
        DateFormat dateFormat_5 = new SimpleDateFormat("dd-MM-yyyy");
        c3.add(Calendar.DAY_OF_YEAR, 4);
        Date tomorrow_3 = c3.getTime();
        String date_element5=tomorrow_3.toString();

        String dayOfTheWeek_fv = (String) android.text.format.DateFormat.format("EEE", tomorrow_3);//Thursday
        String stringMonth_fv= (String) android.text.format.DateFormat.format("MMM", tomorrow_3); //Jun
        String intMonth_fv = (String) android.text.format.DateFormat.format("MM", tomorrow_3); //06
        String year_fv = (String) android.text.format.DateFormat.format("yyyy", tomorrow_3); //2013
        String day_fv = (String) android.text.format.DateFormat.format("dd", tomorrow_3); //20

//for 6th day
        final Calendar c4 = Calendar.getInstance();
        DateFormat dateFormat_6 = new SimpleDateFormat("dd-MM-yyyy");
        c4.add(Calendar.DAY_OF_YEAR, 5);
        Date tomorrow_4 = c4.getTime();
        String date_element6=tomorrow_4.toString();

        String dayOfTheWeek_sx = (String) android.text.format.DateFormat.format("EEE", tomorrow_4);//Thursday
        String stringMonth_sx = (String) android.text.format.DateFormat.format("MMM", tomorrow_4); //Jun
        String intMonth_sx = (String) android.text.format.DateFormat.format("MM", tomorrow_4); //06
        String year_sx = (String) android.text.format.DateFormat.format("yyyy", tomorrow_4); //2013
        String day_sx = (String) android.text.format.DateFormat.format("dd", tomorrow_4); //20

//for 7th day
        final Calendar c5 = Calendar.getInstance();
        DateFormat dateFormat_7 = new SimpleDateFormat("dd-MM-yyyy");
        c5.add(Calendar.DAY_OF_YEAR, 6);
        Date tomorrow_5 = c5.getTime();
        String date_element7=tomorrow_5.toString();

        String dayOfTheWeek_sv = (String) android.text.format.DateFormat.format("EEE", tomorrow_5);//Thursday
        String stringMonth_sv = (String) android.text.format.DateFormat.format("MMM", tomorrow_5); //Jun
        String intMonth_sv = (String) android.text.format.DateFormat.format("MM", tomorrow_5); //06
        String year_sv = (String) android.text.format.DateFormat.format("yyyy", tomorrow_5); //2013
        String day_sv = (String) android.text.format.DateFormat.format("dd", tomorrow_5); //20

        this.dateArray = new String[] {date_element1,date_element2,date_element3,
                                       date_element4,date_element5,date_element6,date_element7};

        this.dayArray = new String[] {day,day_t,day_th,
                                       day_fr,day_fv,day_sx,day_sv};

        this.monthArray = new String[] {stringMonth,stringMonth_t,stringMonth_th,
                                        stringMonth_fr,stringMonth_fv,stringMonth_sx,stringMonth_sv};

        this.week_dayArray = new String[] {dayOfTheWeek,dayOfTheWeek_t,dayOfTheWeek_th,
                dayOfTheWeek_fr,dayOfTheWeek_fv,dayOfTheWeek_sx,dayOfTheWeek_sv};



            Address_recycler_adapter address_recycler_adapter= new Address_recycler_adapter(Choose_time.this,tag);


// Setup layout manager for items
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
// Control orientation of the items
// also supports LinearLayoutManager.HORIZONTAL
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
// Optionally customize the position you want to default scroll to
            layoutManager.scrollToPosition(0);
// Attach layout manager to the RecyclerView
            addr_tag.setLayoutManager(layoutManager);
            addr_tag.setAdapter(address_recycler_adapter);// set adapter
    }
}

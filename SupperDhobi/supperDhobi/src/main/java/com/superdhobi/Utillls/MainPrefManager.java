package com.superdhobi.Utillls;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Abhishek on 4/19/2016.
 */
public class MainPrefManager {




    SharedPreferences pref;

    SharedPreferences.Editor editor;

    Context context ;


    public MainPrefManager(Context context, String PREF_NAME){

        this.context = context;

        pref = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);



    }


    public void storeCount(int position, String KEY_COUNT1){


        editor = pref.edit();
        editor.putInt(KEY_COUNT1,position);
        Log.d("TAG",String.valueOf(position));
        editor.commit();


    }


    public int getCount(String KEY_COUNT1){

        int count = 0 ;

        if(pref.contains(KEY_COUNT1)){

            count = pref.getInt(KEY_COUNT1,0);
            return count;
        }else {

            return 0 ;
        }

    }

}

package com.superdhobi.Utillls;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by abhis on 4/14/2016.
 */
public class Prefmanager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context ;
    public Prefmanager(Context context,String PREF_NAME){
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
    }
    public void storeCount(int position, String KEY_COUNT1){
        editor = pref.edit();
        editor.putInt(KEY_COUNT1,position);
        Log.d("TAG",String.valueOf(position));
        editor.commit();
    }
    public int getPosition1(String KEY_COUNT1){
        int pos = 0 ;
        if(pref.contains(KEY_COUNT1)){
            pos = pref.getInt(KEY_COUNT1,0);
            return pos;
        }else {
            return 0 ;
        }
    }
    public void setPrice(String price , String key_name){
        float value = Float.valueOf(price);
        editor = pref.edit();
        editor.putFloat(key_name,value);
        Log.d("TAG",String.valueOf(value));
        editor.commit();
    }

    public float getPrice(String key_name){
        float value   ;

        if(pref.contains(key_name)){
            value = pref.getFloat(key_name,0);
            return value;
        }else {
            return 0;
        }
    }
    public void storeTotal(int total,String sub){
        editor = pref.edit();
        editor.putInt(sub,total);
        Log.d("TOTAL",String.valueOf(total));
        editor.commit();
    }
    public int getTotal(String sub){
        int pos = 0 ;
        if(pref.contains(sub)){
            pos = pref.getInt(sub,0);
            Log.d("GET_TOTAL",String.valueOf(pos));
            return pos;
        }else {
            return 0 ;
        }
    }


    public void clear_all(){

        pref.edit().clear();
    }
}

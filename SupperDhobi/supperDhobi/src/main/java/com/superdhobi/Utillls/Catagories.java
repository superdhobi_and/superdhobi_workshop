package com.superdhobi.Utillls;

/**
 * Created by Abhishek on 4/28/2016.
 */
public class Catagories  {

    int id;
    String cloth_name ;
    String wash_type ;
    int    total;
    float price;

    String clothId;

    public String getWashId() {
        return washId;
    }

    public void setWashId(String washId) {
        this.washId = washId;
    }

    String washId;
    public String getClothId() {
        return clothId;
    }

    public void setClothId(String clothId) {
        this.clothId = clothId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCloth_name() {
        return cloth_name;
    }

    public void setCloth_name(String cloth_name) {
        this.cloth_name = cloth_name;
    }

    public String getWash_type() {
        return wash_type;
    }

    public void setWash_type(String wash_type) {
        this.wash_type = wash_type;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}

package com.superdhobi.Utillls;

import java.io.Serializable;

/**
 * Created by HI on 6/18/2016.
 */
public class SpinnerData implements Serializable {
    String id,name;

    public SpinnerData(){}
    public SpinnerData(String id,String name){
        this.id=id;
        this.name=name;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return name;
    }
}

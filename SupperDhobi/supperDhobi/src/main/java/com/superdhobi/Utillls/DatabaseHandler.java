package com.superdhobi.Utillls;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Abhishek on 4/28/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int database_version = 1 ;
    private static final String DATABASE_NAME = "SUMMARY";
    private static final String TABLE_CART = "catagory";

    private static final String KEY_ID = "id";
    private static final String KEY_CLOTH = "cloth";
    private static final String KEY_WASHTYPE = "washtype";
    private static final String KEY_WASHID = "washid";
    private static final String KEY_TOTAL_NO = "total";
    private static final String KEY_PRICE = "price" ;
    private static final String KEY_CLOTHID = "clothid" ;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, database_version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_ORDER_TABLE = "CREATE TABLE " + TABLE_CART + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_CLOTH + " TEXT,"
                + KEY_WASHTYPE + " TEXT," + KEY_WASHID + " TEXT," + KEY_TOTAL_NO + " TEXT," + KEY_PRICE + " TEXT," + KEY_CLOTHID + " TEXT"+" )";

        db.execSQL(CREATE_ORDER_TABLE);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        onCreate(db);

    }
    public void addCatagory(Catagories catagory){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_CLOTH,catagory.getCloth_name());
        values.put(KEY_WASHTYPE,catagory.getWash_type());
        values.put(KEY_WASHID,catagory.getWashId());
        values.put(KEY_TOTAL_NO,catagory.getTotal());
        values.put(KEY_PRICE,catagory.getPrice());
        values.put(KEY_CLOTHID,catagory.getClothId());

        db.insert(TABLE_CART, null, values);
        db.close();
    }

    public void updateCatagory(Catagories catagory){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_CLOTH,catagory.getCloth_name());
        values.put(KEY_WASHTYPE,catagory.getWash_type());
        values.put(KEY_WASHID,catagory.getWashId());
        values.put(KEY_TOTAL_NO,catagory.getTotal());
        values.put(KEY_PRICE, catagory.getPrice());
        values.put(KEY_CLOTHID, catagory.getClothId());

        db.update(TABLE_CART, values, KEY_CLOTH + "=?" + " AND " + KEY_WASHTYPE + "=?" + " AND " + KEY_WASHID + "=?",
                new String[]{String.valueOf(catagory.getCloth_name()), String.valueOf(catagory.getWash_type())});

    }

    public boolean isExists(Catagories catagory){

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CART + " WHERE  " + KEY_CLOTH + "=?" +" AND " + KEY_WASHTYPE + "=?" +" AND " + KEY_WASHID+ "=?" + " AND " + KEY_CLOTHID + "=?";
        Cursor cursor = db.rawQuery(query,new  String[]{catagory.getCloth_name(),catagory.getWash_type()}) ;

        if (cursor.getCount()>0){
            cursor.close();
            return true;
        }else {
            cursor.close();
            return false;
        }

    }
    public ArrayList<Catagories> getAllcatagory(){
        ArrayList<Catagories> catList = new ArrayList<Catagories>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CART + " ORDER BY "+KEY_ID+", "+KEY_CLOTHID;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Catagories catagory = new Catagories();
                catagory.setId(Integer.parseInt(cursor.getString(0)));
                catagory.setCloth_name(cursor.getString(1));
                catagory.setWash_type(cursor.getString(2));
                catagory.setWashId(cursor.getString(3));
                catagory.setTotal(Integer.parseInt(cursor.getString(4)));
                catagory.setPrice(Float.parseFloat(cursor.getString(5)));
                catagory.setClothId(cursor.getString(6));
                // Adding contact to list
                catList.add(catagory);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return catList ;
    }
    public void deleteAllCategory(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CART, null, null);
    }
    public void deleteCategoryByClothId(String clothId){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_CART + " where clothid='" + clothId + "'");
    }
    public int isEmptyTableEvents(){
        String query="select * from "+ TABLE_CART;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(query, null);
        return cursor.getCount();
    }
}

package com.superdhobi.vo;

/**
 * Created by harneet on 04/07/16.
 */
public class LoginResponseDataVO {

    private String userid;
    private String name;
    private String mobile;
    private String mobile_varified;
    private String email;
    private String ref_code;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile_varified() {
        return mobile_varified;
    }

    public void setMobile_varified(String mobile_varified) {
        this.mobile_varified = mobile_varified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRef_code() {
        return ref_code;
    }

    public void setRef_code(String ref_code) {
        this.ref_code = ref_code;
    }

    public LoginResponseDataVO()
    {}
    public LoginResponseDataVO(String userid, String name, String mobile, String mobile_varified, String email, String ref_code) {
        this.userid = userid;
        this.name = name;
        this.mobile = mobile;
        this.mobile_varified = mobile_varified;
        this.email = email;
        this.ref_code = ref_code;
    }

    @Override
    public String toString() {
        return "LoginResponseDataVO{" +
                "userid='" + userid + '\'' +
                ", name='" + name + '\'' +
                ", mobile='" + mobile + '\'' +
                ", mobile_varified='" + mobile_varified + '\'' +
                ", email='" + email + '\'' +
                ", ref_code='" + ref_code + '\'' +
                '}';
    }
}

package com.superdhobi.vo;

import org.json.JSONObject;

/**
 * Created by harneet on 30/06/16.
 */
public class LoginResponseVO {

    private Double status;

    private JSONObject data;

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }


    public Double getStatus() {
        return status;
    }

    public void setStatus(Double status) {
        this.status = status;
    }

    public LoginResponseVO(Double status, JSONObject data) {
        this.status = status;
        this.data = data;
    }

    @Override
    public String toString() {
        return "LoginResponseVO{" +
                "status=" + status +
                ", data=" + data +
                '}';
    }
}

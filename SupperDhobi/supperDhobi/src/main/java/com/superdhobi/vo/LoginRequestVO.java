package com.superdhobi.vo;

/**
 * Created by harneet on 30/06/16.
 */
public class LoginRequestVO {

    private String name;
    private String email;
    private String mobile;
    private String password;
    private String ref_by;
    private String login_type;
    private String google_id;
    private String fb_id;
    private String gcm_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRef_by() {
        return ref_by;
    }

    public void setRef_by(String ref_by) {
        this.ref_by = ref_by;
    }

    public String getLogin_type() {
        return login_type;
    }

    public void setLogin_type(String login_type) {
        this.login_type = login_type;
    }

    public String getGoogle_id() {
        return google_id;
    }

    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public String getGcm_id() {
        return gcm_id;
    }

    public void setGcm_id(String gcm_id) {
        this.gcm_id = gcm_id;
    }

    public LoginRequestVO(){}

    public LoginRequestVO(String name, String email, String mobile, String password, String ref_by, String login_type, String google_id, String fb_id, String gcm_id) {
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.password = password;
        this.ref_by = ref_by;
        this.login_type = login_type;
        this.google_id = google_id;
        this.fb_id = fb_id;
        this.gcm_id = gcm_id;
    }

    @Override
    public String toString() {
        return "LoginRequestVO{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", password='" + password + '\'' +
                ", ref_by='" + ref_by + '\'' +
                ", login_type='" + login_type + '\'' +
                ", google_id='" + google_id + '\'' +
                ", fb_id='" + fb_id + '\'' +
                ", gcm_id='" + gcm_id + '\'' +
                '}';
    }
}

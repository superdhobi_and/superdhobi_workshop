package com.superdhobi.supperdhobi;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by PCIS-ANDROID on 05-05-2016.
 */
public class Pick_second_adapt extends RecyclerView.Adapter<Pick_second_adapt.MyViewHolder> {
    private String[] tag;

    private int mSelectedPosition = 0;
    Context context;


    public Pick_second_adapt( Context context,String[] tag) {
        this.tag = tag;
        this.context = context;
    }



    @Override
    public Pick_second_adapt.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.tag_custom, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final Pick_second_adapt.MyViewHolder holder, final int position) {
        holder.tag_name.setText(tag[position]);
        //int selectedItem=

        holder.addr_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.choose_address_custom, null, false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setContentView(view);

                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                dialog.setTitle("Address..");
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                TextView tag_name= (TextView) dialog.findViewById(R.id.tag_name);
                TextView addr_back= (TextView) dialog.findViewById(R.id.addr_back);
                tag_name.setText(tag[position]);

                ImageView edit = (ImageView) dialog.findViewById(R.id.addr_edit);
                // if decline button is clicked, close the custom dialog
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                addr_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                ImageView delet = (ImageView) dialog.findViewById(R.id.addr_del);

                dialog.show();
            }
        });

    }
    @Override
    public int getItemCount() {
        return tag.length;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tag_name;
        public ImageView addr_view;
        public LinearLayout tag_lay;


        public MyViewHolder(View view1) {
            super(view1);
            tag_name = (TextView) view1.findViewById(R.id.tag_name_pick);
            addr_view = (ImageView) view1.findViewById(R.id.view);
            tag_lay = (LinearLayout) view1.findViewById(R.id.tag_lay);
        }
    }
}



package com.superdhobi.supperdhobi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Rating_activity extends AppCompatActivity {

    RatingBar rating_bar;
    Spinner spinner;
    Button submit;
    String ratedValue;
    ArrayAdapter adapter;
    EditText reason;

    String[] arr = {"Your Reason Here 10" ,"Your Reason Here 20","Your Reason Here 30"};
    String[] arr1 = {"Your Reason Here !" ,"Your Reason Here @ ","Your Reason Here #"};
    String[] arr2 = {"Your Reason Here one" ,"Your Reason Here two","Your Reason Here three"};
    String[] arr3 = {"Your Reason Here 4" ,"Your Reason Here 5","Your Reason Here 6"};

    String rating_input,user_input,orderid_input,condition_input,reason_input;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        submit = (Button) findViewById(R.id.submit_feed);
        rating_bar= (RatingBar) findViewById(R.id.ratingBar);
        spinner = (Spinner) findViewById(R.id.spinner);
        reason=(EditText)findViewById(R.id.reason);
        spinner.setVisibility(View.GONE);

        rating_bar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                ratedValue = String.valueOf(ratingBar.getRating());

                if (rating == 4.0) {

                    spinner.setVisibility(View.VISIBLE);
                    adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_dropdown_item_1line, arr);
                    spinner.setAdapter(adapter);

                } else if (rating == 3.0) {
                    spinner.setVisibility(View.VISIBLE);
                    adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_dropdown_item_1line, arr1);
                    spinner.setAdapter(adapter);

                } else if (rating == 2.0) {
                    spinner.setVisibility(View.VISIBLE);
                    adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_dropdown_item_1line, arr2);
                    spinner.setAdapter(adapter);


                } else if (rating == 1.0) {

                    spinner.setVisibility(View.VISIBLE);
                    adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_dropdown_item_1line, arr3);
                    spinner.setAdapter(adapter);
                } else if (rating == 5.0) {

                    spinner.setVisibility(View.GONE);

                }
            }
        });



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(rating_bar.getRating()<1){

                    Toast.makeText(getBaseContext(),"Please rate your wash before submittion",Toast.LENGTH_LONG).show();

                }else {
                    String OrderID = getIntent().getStringExtra("ORDERID");
                    String UserId = getIntent().getStringExtra("userId");

                    user_input=UserId;
                    orderid_input=OrderID;
                   if(rating_bar.getRating() == 5.0){
                       condition_input ="";

                   }else {

                       condition_input=spinner.getSelectedItem().toString();
                   }
                    rating_input = String.valueOf(rating_bar.getRating());
                    reason_input=reason.getText().toString();

                    new SubmitFeedback().execute(ServerLinks.submit_feedback);
                }

            }
        });




    }
    private class SubmitFeedback extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {
            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", user_input);
                jsonObject.accumulate("order_id", orderid_input);
                jsonObject.accumulate("conditions", condition_input);
                jsonObject.accumulate("rating", rating_input);
                jsonObject.accumulate("reason", reason_input);
				/*
				 * jsonObject.accumulate("mobile", Mobile);
				 * jsonObject.accumulate("password", Password);
				 */
                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                if (!result.equals("")) {
                    JSONObject jObject = new JSONObject(result);
                    int status = jObject.getInt("status");
                    if (status == 1) {
                        Toast.makeText(Rating_activity.this,"Feedback Saved Successfully", Toast.LENGTH_LONG).show();
                        finish();

                    }
                } else {
                    //Intent i = new Intent(Rating_activity.this, NointernetActivity.class);
                    //startActivity(i);

                }
            } catch (Exception e) {

            }

            //progressBar.setVisibility(View.GONE);

            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


}

package com.superdhobi.supperdhobi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.MainActivity;
import com.superdhobi.navigation.New_sd_home;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

/**
 * Created by User on 5/26/2016.
 */
public class Otp_Activity extends AppCompatActivity  {
    Button submit,resen;
    EditText otp;
    TextView tim;
    String otp_mobile;
    String _userid="";
    private SharedPreferences shp;
    SharedPreferenceClass sharedPreferenceClass;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_layout);
        shp = this.getSharedPreferences("AuthToken", Context.MODE_PRIVATE);
        submit = (Button) findViewById(R.id.submit);
        resen = (Button) findViewById(R.id.resend);
        otp = (EditText) findViewById(R.id.otp_text);
        tim = (TextView) findViewById(R.id.timer);

        _userid=shp.getString("USER_UID", null);
        countDownTimer.start();
        resen.setVisibility(View.GONE);

        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());

        submit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            otp_mobile = otp.getText().toString();
            if (otp_mobile.equalsIgnoreCase("")) {
                Toast.makeText(Otp_Activity.this, " Enter your OTP ", Toast.LENGTH_LONG).show();
            } else {
                Intent i = new Intent(Otp_Activity.this, MainActivity.class);
                startActivity(i);
                new otp_verification_HttpAsyncTask().execute(ServerLinks.otpverify);
                finish();
            }
        }
        });


        resen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit.setClickable(true);
                submit.setBackgroundColor(Color.parseColor("#1A235E"));
                tim.setVisibility(View.VISIBLE);
                countDownTimer.cancel();
                countDownTimer.start();
                resen.setVisibility(View.GONE);
            }
        });

        //Service Call here
        new SubmitOtp().execute(ServerLinks.sendotp);

    }
    private class SubmitOtp extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", _userid);

                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {


            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.optString("status");
                    if (status!=null) {
                        if(status.equals("1")) {
                            Toast.makeText(Otp_Activity.this, "Send Successfully", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Otp_Activity.this, New_sd_home.class);
                            startActivity(i);
                            sharedPreferenceClass.setValue_string("MobileVerify", "1");

                        }else if(status.equals("4")) {

                            Toast.makeText(getBaseContext(),"INVALID OTP",Toast.LENGTH_LONG).show();

                        }
                    } else {

					/*Toast.makeText(Addlocationnew.this,
							"Something is going wrong!!!.", Toast.LENGTH_LONG).show();*/
                    }
                }

            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progress.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
    private class otp_verification_HttpAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", _userid);
                jsonObject.accumulate("otp", otp_mobile);

                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString1(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {


            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.optString("status");
                    if (status!=null) {
                        if(status.equals("1")) {
                            Toast.makeText(Otp_Activity.this, "successfully verified", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Otp_Activity.this, New_sd_home.class);
                            startActivity(i);
                        }
                    } else {

					/*Toast.makeText(Addlocationnew.this,
							"Something is going wrong!!!.", Toast.LENGTH_LONG).show();*/
                    }
                }

            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progress.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }
    private static String convertInputStreamToString1(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
    CountDownTimer countDownTimer = new CountDownTimer(120000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);


            if (minutes > 0){

                seconds = seconds - 60 ;

                tim.setText("" + minutes + ":" + seconds);
            }else {

                tim.setText("" + minutes + ":" + seconds);
            }



            //tim.setText("" + millisUntilFinished / 1000);


            //here you can have your logic to set text to edittext
        }

        @Override
        public void onFinish() {
            submit.setClickable(false);
            submit.setBackgroundColor(Color.parseColor("#5e658e"));
            tim.setVisibility(View.GONE);
            resen.setVisibility(View.VISIBLE);


        }
    };
}

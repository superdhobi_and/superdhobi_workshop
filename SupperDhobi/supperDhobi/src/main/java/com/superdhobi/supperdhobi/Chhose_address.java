package com.superdhobi.supperdhobi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by PCIS-ANDROID on 14-04-2016.
 */
public class Chhose_address extends AppCompatActivity {
    TextView next;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_address);
        next= (TextView) findViewById(R.id.wallet_statement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_Pr);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n= new Intent(Chhose_address.this, Choose_time.class);
                startActivity(n);
            }
        });
    }
}

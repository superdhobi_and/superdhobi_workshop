package com.superdhobi.supperdhobi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.superdhobi.Utillls.ProfileAddress;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by User on 4/18/2016.
 */
public class Feedback_CustomListAdapter extends ArrayAdapter<String>{
    private final Activity context;
    private String[] orderid;
    private String[] address;
    private String[] orderdate;
    private int [] orderStatus;
    String userId;
    Spinner spinner;
    String order;
    TextView textView;


LinearLayout linearLayout;
    public Feedback_CustomListAdapter(Activity activity, String[] orderid,int[] orderStatus,String[] address,String[] orderdate,String userId) {
        super(activity,R.layout.custom_new_feedback,orderid);

        this.context=activity;
        this.orderid=orderid;
        this.orderStatus=orderStatus;
        this.address=address;
        this.orderdate=orderdate;
        this.userId=userId;
    }
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        final View rowView = inflater.inflate(R.layout.custom_new_feedback, null, true);
        final TextView tv = (TextView) rowView.findViewById(R.id.list_textview);
        final TextView tv1 = (TextView) rowView.findViewById(R.id.status);
        final TextView tv2 = (TextView) rowView.findViewById(R.id.order_addr);
        final TextView tv3 = (TextView) rowView.findViewById(R.id.date_nnnn);
        textView = (TextView) rowView.findViewById(R.id.feedback);

        tv1.setText(ServerLinks.statusArray[orderStatus[position]]);
        tv.setText(orderid[position]);
        tv2.setText(address[position]);
        tv3.setText(orderdate[position]);
        tv1.setTextColor(Color.parseColor("#ff9000"));
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new getFeedBack(orderid[position]).execute(ServerLinks.check_feedback);
            }
        });
       return rowView;
    }

    private class getFeedBack extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String orderId;
        getFeedBack (String orderid){
            this.orderId=orderid;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {
            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                //jsonObject.accumulate("user_id",hm_userid);
                jsonObject.accumulate("order_id",orderId);
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {


            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.optString("status");
                    if (status.equals("3")) {
                        Intent i = new Intent(getContext(),Rating_activity.class);
                        i.putExtra("ORDERID",orderId);
                        i.putExtra("userId",userId);
                        context.startActivity(i);
                        context.finish();
                    } else if (status.equals("1")) {
                        String jsonrating    = jsonObject.optString("rating");
                        String jsoncondition = jsonObject.optString("condition");
                        final JSONArray jArr = jsonObject.getJSONArray("data");

                        Intent i = new Intent(getContext(),second_feedback.class);
                        i.putExtra("ORDERID",orderId);
                        i.putExtra("userId",userId);
                        i.putExtra("rating",jsonrating);
                        i.putExtra("condition",jsoncondition);
                        i.putExtra("pos", jArr.toString());
                        context.startActivity(i);
                        context.finish();
                    }

                }

            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progress.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

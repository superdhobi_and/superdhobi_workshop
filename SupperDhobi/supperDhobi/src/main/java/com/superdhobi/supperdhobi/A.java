package com.superdhobi.supperdhobi;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by PCIS-ANDROID on 22-03-2016.
 */
public class A extends AppCompatActivity {
    ScrollView main;
    private ArrayList<String> pProductArrayList;
    private ArrayList<String> pProductArraySub_List;
    private ArrayList<String> pProductArrayWash_type;
    private ArrayList<String> pProductArrayPrice;
    LinearLayout linear_listview;
    boolean isFirstViewClick=false;
    boolean isSecondViewClick=false;
    static int count=Count_varriable.washtype_count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a);
        main=(ScrollView) findViewById(R.id.lo);
        linear_listview=(LinearLayout) findViewById(R.id.linear_listview);
        pProductArrayList=new ArrayList<String>();
        pProductArraySub_List=new ArrayList<String>();
        pProductArrayWash_type=new ArrayList<String>();
        pProductArrayPrice=new ArrayList<String>();

        pProductArrayList.add("Shirt");
        pProductArrayList.add("Pant");
        pProductArrayList.add("Ethinic");

        pProductArraySub_List.add("Cotton Shirt");
        pProductArraySub_List.add("Kurta");
        pProductArraySub_List.add("T-shirt");

        pProductArrayWash_type.add("Normal Wash");
        pProductArrayWash_type.add("Dry Clean");
        pProductArrayWash_type.add("Super Wash");
        pProductArrayWash_type.add("Iron");

        pProductArrayPrice.add("₹5");
        pProductArrayPrice.add("₹8");
        pProductArrayPrice.add("₹10");
        pProductArrayPrice.add("₹3");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        for (int i=0;i<pProductArrayList.size();i++){

            LayoutInflater inflate =null;
            inflate = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearView= inflate.inflate(R.layout.aa,null);
            final LinearLayout mLinearFirst=(LinearLayout)mLinearView.findViewById(R.id.Only);
            final LinearLayout linear_scroll_sub_list=(LinearLayout)mLinearView.findViewById(R.id.linear_scroll_sub_list);
            final TextView mProductName=(TextView) mLinearView.findViewById(R.id.item_list);
            final String name_=pProductArrayList.get(i);
            mProductName.setText(name_);


            for (int j=0;j<pProductArraySub_List.size();j++){
                LayoutInflater inflate2 =null;
                inflate2 = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mLinearView2 = inflate2.inflate(R.layout.aaa,null);
                final LinearLayout mLinearSecond=(LinearLayout)mLinearView2.findViewById(R.id.Only_subitem);
                final LinearLayout mLinearThird=(LinearLayout)mLinearView2.findViewById(R.id.linear_scroll_wash_type);
                final TextView mSub_ProductName = (TextView) mLinearView2.findViewById(R.id.sub_item_list);
                final String sub_name_ = pProductArraySub_List.get(j);
                mSub_ProductName.setText(sub_name_);
                mSub_ProductName.setTag(j);
               if(isFirstViewClick==false){
                   linear_scroll_sub_list.setVisibility(View.GONE);
                    // mImageArrowFirst.setBackgroundResource(R.drawable.down);

                }

                else{
                   linear_scroll_sub_list.setVisibility(View.VISIBLE);
                    //mImageArrowFirst.setBackgroundResource(R.drawable.up);

                }

                mLinearFirst.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (isFirstViewClick == false) {
                            isFirstViewClick = true;
                            // mImageArrowFirst.setBackgroundResource(R.drawable.up);
                            //if (mLinearSecond.getBackground()=R.drawable.big_menu_act )
                            mLinearFirst.setBackgroundResource(R.drawable.big_menu_act);
                            linear_scroll_sub_list.setVisibility(View.VISIBLE);
                        } else {
                            isFirstViewClick = false;
                            // mImageArrowFirst.setBackgroundResource(R.drawable.down);
                            mLinearFirst.setBackgroundResource(R.drawable.big_menu);
                            linear_scroll_sub_list.setVisibility(View.GONE);
                        }
                    }
                });

                for (int k=0;k<pProductArrayWash_type.size();k++){
                    LayoutInflater inflate3 =null;
                    inflate3 = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View mLinearView3 = inflate3.inflate(R.layout.aaaa,null);
                    final LinearLayout mLinearwash=(LinearLayout)mLinearView3.findViewById(R.id.wash);
                    final TextView wash_type = (TextView) mLinearView3.findViewById(R.id.wash_type);
                    final TextView wash_price = (TextView) mLinearView3.findViewById(R.id.wash_price);
                    final TextView count_item = (TextView) mLinearView3.findViewById(R.id.count_item);
                    final ImageView increment = (ImageView) mLinearView3.findViewById(R.id.increment);

                    final String wash_name_ = pProductArrayWash_type.get(k);
                    final String wash_price_ = pProductArrayPrice.get(k);

                    wash_type.setTag(k);
                    wash_type.setText(wash_name_);
                    wash_price.setText(wash_price_);

                    increment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int pos= (int) wash_type.getTag();
                            int pos1= (int)mSub_ProductName.getTag();
                            Toast.makeText(A.this, String.valueOf(pos) + String.valueOf(pos1), Toast.LENGTH_SHORT).show();

                            count++;
                            count_item.setText(String.valueOf(count));
                        }
                    });


                    if(isSecondViewClick==false){
                        mLinearThird.setVisibility(View.GONE);
                    }

                    else{
                        mLinearThird.setVisibility(View.VISIBLE);
                        }

                    mLinearSecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (isSecondViewClick == false) {
                                isSecondViewClick = true;
                                // mImageArrowFirst.setBackgroundResource(R.drawable.up);
                                mLinearThird.setVisibility(View.VISIBLE);
                            } else {
                                isSecondViewClick = false;
                                // mImageArrowFirst.setBackgroundResource(R.drawable.down);
                                mLinearThird.setVisibility(View.GONE);
                            }
                        }
                    });
                    mLinearThird.addView(mLinearView3);

                }
                linear_scroll_sub_list.addView(mLinearView2);


            }

            linear_listview.addView(mLinearView);

        }



    }
}

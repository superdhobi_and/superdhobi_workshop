package com.superdhobi.supperdhobi;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.Txn_recycler_adapter;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * Created by PCIS-ANDROID on 13-04-2016.
 */
public class New_wallet extends AppCompatActivity implements AsynkTaskCommunicationInterface {
    TextView w_amount;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    String wallet_amount;
    LinearLayout w_statement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_wallet);
        w_statement= (LinearLayout) findViewById(R.id.wallet_statement);
        w_amount= (TextView) findViewById(R.id.walet_amnt);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_Pr);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String userId = getIntent().getStringExtra("userId");
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("user_id", userId);
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.walletdtls);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(New_wallet.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);

    }
    //======
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if(jsonObject!=null){
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status=jsonObject.getInt("staus");
                if(status==0){
                    Toast.makeText(New_wallet.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                }
                else if(status==1){

                    wallet_amount=jsonObject.getString("walletamount");

                    final JSONArray jArr = jsonObject.getJSONArray("data");
                    if (jArr!=null){

                        w_statement.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent st=new Intent(New_wallet.this,Wallet_statement.class);

                                st.putExtra("pos", jArr.toString());
                                st.putExtra("amount",wallet_amount);
                                startActivity(st);

                            }
                        });

                    }
                    w_amount.setText("Rs."+wallet_amount);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Intent i = new Intent(New_wallet.this, NointernetActivity.class);
            startActivity(i);
            New_wallet.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
            finish();
        }
    }
}

package com.superdhobi.supperdhobi;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Sd_Order extends AppCompatActivity implements AsynkTaskCommunicationInterface {

    Boolean isInternetPresent = false;
    static String resumeStatus;
    ViewPager viewPager;
    TabLayout tabLayout;
    TabpagerAdapter pageAdapter;
    FloatingActionButton next;
    String userId = "";
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resumeStatus="";
        setContentView(R.layout.sd_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Schedule Your Order");
        //toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        next = (FloatingActionButton) findViewById(R.id.fab);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tab);
        pageAdapter = new TabpagerAdapter(getSupportFragmentManager());
        userId = getIntent().getStringExtra("userId");

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Sd_Order.this, Pick_address.class);
                i.putExtra("userId", userId);
                startActivity(i);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                resumeStatus="";
            }
        });
        HashMap<String, String> parameters = new HashMap<String, String>();
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.tabarray);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Sd_Order.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);

        /*viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        if (!resumeStatus.equals("")) {
            HashMap<String, String> parameters = new HashMap<String, String>();
            HashMap<String, String> hashMapLink = new HashMap<String, String>();
            hashMapLink.put(AllStaticVariables.link, ServerLinks.tabarray);
            asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Sd_Order.this);
            asynkTaskForServerCommunication.execute(parameters, hashMapLink);*//*

            //pageAdapter.

            //resumeStatus = "";
       }
    }*/

    //======
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if (jsonObject != null) {
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status = jsonObject.getInt("status");
                if (status == 0) {
                    Toast.makeText(Sd_Order.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                } else if (status == 1) {

                    final JSONArray jArr = jsonObject.getJSONArray("data");
                    if (jArr != null) {
                        for (int i = 0; i < jArr.length(); i++) {
                            JSONObject json1 = jArr.getJSONObject(i);
                            String parentName = json1.getString("Parent");
                            String parentId   = json1.getString("parent_id");
                            pageAdapter.addFrag(new MenFrag(json1, parentName, userId), parentName);

                        }
                        viewPager.setAdapter(pageAdapter);
                        tabLayout.setupWithViewPager(viewPager);
                    } else {
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Intent i = new Intent(Sd_Order.this, NointernetActivity.class);
            startActivity(i);
            Sd_Order.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
            finish();
        }


    }

    Runnable tab_config = new Runnable() {
        @Override
        public void run() {

            if (tabLayout.getWidth() < Sd_Order.this.getResources().getDisplayMetrics().widthPixels) {

                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                ViewGroup.LayoutParams params = tabLayout.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                tabLayout.setLayoutParams(params);
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            } else {
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}

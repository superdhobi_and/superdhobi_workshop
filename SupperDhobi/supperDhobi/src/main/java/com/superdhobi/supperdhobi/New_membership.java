package com.superdhobi.supperdhobi;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.superdhobi.navigation.Membership_Allplan_frag;
import com.superdhobi.navigation.Membership_myplan;
import com.superdhobi.navigation.Membership_order_frag;
/**
 * Created by PCIS-ANDROID on 26-04-2016.
 */
public class New_membership  extends AppCompatActivity {
    ViewPager viewPager ;
    TabLayout tabLayout;
    TabpagerAdapter pageAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.membership_tab);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Membership");
        //toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tab);
        pageAdapter = new TabpagerAdapter(getSupportFragmentManager());
        pageAdapter.addFrag(new Membership_Allplan_frag(), "All Plan");
        pageAdapter.addFrag(new Membership_myplan(), "My Plan");
        pageAdapter.addFrag(new Membership_order_frag(), "Order History");
        viewPager.setAdapter(pageAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
}

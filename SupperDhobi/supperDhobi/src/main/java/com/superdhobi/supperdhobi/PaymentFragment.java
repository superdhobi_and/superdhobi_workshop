package com.superdhobi.supperdhobi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.superdhobi.AdapterPackage.CustomQuery;

/**
 * Created by User on 4/22/2016.
 */
public class PaymentFragment  extends Fragment {
    ListView listView;
    public static String[] Question = {"Question:","Question:"};
    public static String[] Question_query = {"How can we Pickup and Deliver the Clothes and Iron the clothes?","How can I get Payments ?"};
    public static String[] Answer = {"Answer :","Answer :"};
    public static String[] Answer_Query = {"By Ordering the Clothes and just click on the order we will just arrive and pick your orders","After Delivering the Clothes to us we will make you update through banking Mode or else Deliveryboy through cash Mode"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.laundry_page,null);
        View rootView = inflater.inflate(R.layout.list_view, container, false);
        listView = (ListView) rootView.findViewById(R.id.mainListView);
        final CustomQuery adapter = new CustomQuery(getActivity(),Question,Question_query,Answer,Answer_Query);
        listView.invalidate();
        listView.setAdapter(adapter);

        return rootView;

    }
}


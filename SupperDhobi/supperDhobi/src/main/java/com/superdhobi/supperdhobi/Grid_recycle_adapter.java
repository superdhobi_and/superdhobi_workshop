package com.superdhobi.supperdhobi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Abhishek on 6/23/2016.
 */
public class Grid_recycle_adapter extends RecyclerView.Adapter<Grid_recycle_adapter.viewHolder> {

    Context context;
    private String[] bgColors;
    int width;
    String userId;
    private  String[] jArr1, jArr2, jArr3, jArr4;;

    public Grid_recycle_adapter(Context baseContext, int width, String[] jArr1, String[] jArr2, String[] jArr3, String[] jArr4, String userId) {
        this.context = baseContext;
        this.width = width;
        this.jArr1 = jArr1;
        this.jArr2 = jArr2;
        this.jArr3 = jArr3;
        this.jArr4 = jArr4;
        this.userId=userId;
        bgColors = context.getApplicationContext().getResources().getStringArray(R.array.color_array);
    }

    @Override
    public Grid_recycle_adapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.staggered_ayout, parent, false);
        viewHolder holder = new viewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Grid_recycle_adapter.viewHolder holder, final int position) {

            holder.heading.setText(jArr2[position]);
            holder.ruppe.setText("RS. "+ jArr3[position]);
            String id=jArr4[position];

        final String color = bgColors[position % bgColors.length];

        if (position % 2 == 0){
            holder.image.setLayoutParams(new RelativeLayout.LayoutParams(width/3, width/2));
        }else {
            holder.image.setLayoutParams(new RelativeLayout.LayoutParams(width / 3, (width / 2)));
        }

        holder.heading.setTextColor(Color.parseColor(color));
        holder.ruppe_lay.setBackgroundColor(Color.parseColor(color));

        if(id.equals("1")){
            holder.tick.setVisibility(View.VISIBLE);
        }else if(id.equals("0")){
            holder.tick.setVisibility(View.INVISIBLE);
        }

        holder.each.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Detail_member.class);
                intent.putExtra("planId",jArr1[position]);
                intent.putExtra("userId",userId);
                intent.putExtra("colorname",color);
                intent.putExtra("used",jArr4[position]);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jArr1.length;
    }

    class viewHolder extends RecyclerView.ViewHolder {

        ImageView image,tick;
        RelativeLayout each, ruppe_lay;
        TextView ruppe, heading;

        public viewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.image_view);
            each = (RelativeLayout) itemView.findViewById(R.id.each);
            ruppe = (TextView) itemView.findViewById(R.id.ruppe);
            heading = (TextView) itemView.findViewById(R.id.heading);
            ruppe_lay = (RelativeLayout) itemView.findViewById(R.id.ruppe_lay);
            tick = (ImageView) itemView.findViewById(R.id.tick);
        }
    }
}

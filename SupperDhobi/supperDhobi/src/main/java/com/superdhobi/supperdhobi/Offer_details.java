package com.superdhobi.supperdhobi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by User on 4/14/2016.
 */
public class Offer_details extends AppCompatActivity implements AsynkTaskCommunicationInterface {
    LinearLayout linearLayout;
    ImageView imageView;
    String offer_name,offer_descrp,UserType,TimesUser,MinPrice,LastExpDate,MainCouponCode,Applicable1,Applicable2,TermsandCond;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    String json_min_amt,json_date,json_wash,json_Max,json_min,json_discount,json_Coupo_code,json_min_balance,json_coupon_title,json_description,json_cust_type;
    TextView  min_amt,coupon_title,Coupon_desc,User_type,min_price,wash,day,last_expiry_date,main_coupon_code,min_rs,max_rs,discount_perc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offer_view);
        //imageView = (ImageView) findViewById(R.id.image_offer);
        coupon_title = (TextView) findViewById(R.id.coupon_title);
          Coupon_desc = (TextView) findViewById(R.id.coupon_desc);
         User_type = (TextView) findViewById(R.id.user_type);
         discount_perc = (TextView) findViewById(R.id.discount_perc);
         min_price = (TextView) findViewById(R.id.min_price_rupee);
         last_expiry_date = (TextView) findViewById(R.id.expire_last_date);
        main_coupon_code = (TextView) findViewById(R.id.coupon_code);
         wash = (TextView) findViewById(R.id.applicable_1);
         day = (TextView) findViewById(R.id.applicable_2);
        min_rs= (TextView) findViewById(R.id.min_rs);
        max_rs= (TextView) findViewById(R.id.max_rs);
        min_amt= (TextView) findViewById(R.id.min_amt);

        //TextView Terms_condition = (TextView) findViewById(R.id.terms_and_cond_descr);




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //offer_name = getIntent().getExtras().getString("OFFER_NAME");
        //offer_descrp = getIntent().getExtras().getString("OFFER_DESC");
        //byte[] byteArray = getIntent().getByteArrayExtra("IMAGES");
        //Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        //imageView.setImageBitmap(bmp);
        //coupon_title.setText(offer_name);
        //Coupon_desc.setText(offer_descrp);
        linearLayout = (LinearLayout) findViewById(R.id.anim);
        overridePendingTransition(R.anim.left_in, R.anim.right_out);

        String Code = getIntent().getExtras().getString("CODE");
        String CouponTitle = getIntent().getExtras().getString("TITLE");
        String ExpiredDeate = getIntent().getExtras().getString("EXPIRE");

        coupon_title.setText(CouponTitle);
        main_coupon_code.setText(Code);
        last_expiry_date.setText(ExpiredDeate);



        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("coupon_id",Code);
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.couponDetails);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Offer_details.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);
    }


    @Override
    public void doInBackgroundForComun(int progress) {

    }

    @Override
    public void doPostExecuteForCommu(JSONObject jsonOb) {
        try {
            if (!jsonOb.equals("")){
                //JSONObject jObject = new JSONObject(jsonOb);
                int status = jsonOb.getInt("status");
                String data=jsonOb.getString("data");
                //Create a jsonObject of String data
                JSONObject dataObject = new JSONObject(data);

                if (status == 0) {
                    //Toast.makeText(getBaseContext(), "Please Enter valid user id and password!!", Toast.LENGTH_SHORT).show();
                } else if (status == 1) {

                    json_Coupo_code=dataObject.getString("coupon_code");
                    json_coupon_title=dataObject.getString("coupon_name");
                    json_cust_type=dataObject.getString("customer_type");
                    json_description=dataObject.getString("description");
                    json_Max=dataObject.getString("max_discount");
                    json_min=dataObject.getString("min_discount");
                    json_min_balance=dataObject.getString("min_apply_amt");
                    json_wash=dataObject.getString("wash");
                    json_date=dataObject.getString("days");
                    json_discount=dataObject.getString("discount");
                    json_min_amt=dataObject.getString("min_amt");




                    coupon_title.setText(json_coupon_title);
                    User_type.setText(json_cust_type);
                    wash.setText(json_wash);
                    day.setText(json_date);
                    min_price.setText(json_min_balance);
                    max_rs.setText(json_Max);
                    min_rs.setText(json_min);
                    main_coupon_code.setText(json_Coupo_code);
                    Coupon_desc.setText(json_description);
                    discount_perc.setText(json_discount);
                    min_amt.setText(json_min_amt);



                    //Toast.makeText(getApplication(), "You Are Logged In...", Toast.LENGTH_SHORT).show();


                }
            }
            else{
                //This part is call when internet connection not found..
                Intent i = new Intent(Offer_details.this,NointernetActivity.class);
                startActivity(i);
                Offer_details.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
                finish();
            }
        } catch (Exception e) {

        }
    }




}

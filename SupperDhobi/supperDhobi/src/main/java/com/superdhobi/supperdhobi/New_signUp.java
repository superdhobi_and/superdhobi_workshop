package com.superdhobi.supperdhobi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by PCIS-ANDROID on 26-02-2016.
 */
public class New_signUp extends Activity {
    TextView login;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.v2_signup);
        login= (TextView) findViewById(R.id.link_login);
        back= (ImageView) findViewById(R.id.back_new);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(New_signUp.this, New_login.class);
                startActivity(s);
                overridePendingTransition(R.anim.left_in, R.anim.right_out);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent b = new Intent(New_signUp.this, New_login.class);
                startActivity(b);
                overridePendingTransition(R.anim.left_in, R.anim.right_out);
                finish();
            }
        });
    }
}

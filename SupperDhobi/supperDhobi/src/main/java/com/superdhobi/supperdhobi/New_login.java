package com.superdhobi.supperdhobi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by PCIS-ANDROID on 26-02-2016.
 */
public class New_login extends Activity {
    TextView signup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.v2_login);
        signup= (TextView) findViewById(R.id.new_user);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s= new Intent(New_login.this,New_signUp.class);
                startActivity(s);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                finish();
            }
        });
    }
}

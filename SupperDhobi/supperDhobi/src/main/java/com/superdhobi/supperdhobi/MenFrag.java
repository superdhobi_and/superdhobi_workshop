package com.superdhobi.supperdhobi;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.superdhobi.AdapterPackage.MenAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenFrag extends Fragment {

    RecyclerView recyclerView;
    MenAdapter menAdapter;
    int columnWidth;
    JSONArray childArr;
    String parentName,userId;

    public MenFrag(){

    }

    public MenFrag(JSONObject jsonObj, String parentName, String userId){
        try {
            childArr = jsonObj.getJSONArray("clothes");
            this.parentName=parentName;
            this.userId=userId;
        }
        catch (Exception ex){

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_men, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.menRECview);
        recyclerView.addItemDecoration(new SpacesItemDecoration(1));
        //list2 = Arrays.asList(itemName);
        new MyAsync().execute();
        return view;
    }

    @Override
    public void onResume() {
        try {
            if (!Sd_Order.resumeStatus.equals("")&&childArr!=null) {
                for(int i=0; i<childArr.length(); i++){
                    JSONObject jObj = childArr.getJSONObject(i);
                    jObj.remove("totalCount");
                    childArr.put(i,jObj);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 6) {
            if (resultCode == Activity.RESULT_OK) {
                int pos = data.getIntExtra("NUM_POSITION", 0);
                int total=data.getIntExtra("totalCount", 0);
                try {
                    JSONObject jObj = childArr.getJSONObject(pos);
                    jObj.put("totalCount",total);
                    childArr.put(pos,jObj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                menAdapter.notifyItemChanged(pos);
            }
        }
    }

    public class MyAsync extends AsyncTask<Void,Void,Void>{

        @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
        @Override
        protected Void doInBackground(Void... params) {

            WindowManager wm = (WindowManager) getActivity().getBaseContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            final Point point = new Point();
            try{
                display.getSize(point);
            }catch (NoSuchMethodError e){
                point.x = display.getWidth();
                point.y = display.getHeight();
            }
            columnWidth=point.x;

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            DisplayMetrics displayMetrics1 =new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics1);
            final int height = displayMetrics1.heightPixels;
            final int width = displayMetrics1.widthPixels;

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            menAdapter = new MenAdapter(getActivity().getBaseContext(),childArr);
            recyclerView.setAdapter(menAdapter);

            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {

                    String clothId="",clothName="",clothImage="";
                    int totalCount = 0;
                    try {
                        clothId=childArr.getJSONObject(position).getString("id");
                        clothName=childArr.getJSONObject(position).getString("cloth");
                        totalCount = childArr.getJSONObject(position).optInt("totalCount",0);
                        clothImage=childArr.getJSONObject(position).getString("img");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(getActivity(), FooddetailActivity.class);
                    intent.putExtra("pos", position);
                    intent.putExtra("jsonclothId",clothId);
                    intent.putExtra("jsonclothName",clothName);
                    intent.putExtra("jsonheading",parentName);
                    intent.putExtra("userId",userId);
                    intent.putExtra("totalCount",totalCount);
                    intent.putExtra("jsonclothImg",clothImage);

                    View rView = (View) view.findViewById(R.id.Rview);
                    View txt1, txt2;

                    txt1 = (TextView) view.findViewById(R.id.textView);
                    txt2 = (TextView) view.findViewById(R.id.textView2);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                        Pair<View, String> pair1 = Pair.create(rView, rView.getTransitionName());
                        Pair<View, String> pair2 = Pair.create(txt1, txt1.getTransitionName());
                        Pair<View, String> pair3 = Pair.create(txt2, txt2.getTransitionName());
                        ActivityOptionsCompat options
                                = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pair1, pair2, pair3);
                        startActivityForResult(intent,6,options.toBundle());

                    } else {
                        startActivityForResult(intent,6);
                    }
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
        }
    }
    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {

            outRect.bottom = space+1;
            // Add top margin only for the first item to avoid double space between items
            if(parent.getChildPosition(view) == 0)
                outRect.top = space;
        }
    }
}

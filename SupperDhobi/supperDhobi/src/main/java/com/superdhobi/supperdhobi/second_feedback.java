package com.superdhobi.supperdhobi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.second_feed_back_adapter;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class second_feedback extends AppCompatActivity {
    RecyclerView recyclerView ;
    second_feed_back_adapter adapter ;

    TextView order_text_view,condition_text;
    EditText reason;
    Button submit;
    RatingBar rating_bar;
    String rating_input,user_input,orderid_input,condition_input,reason_input;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_feedback);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        reason=(EditText)findViewById(R.id.reason);
        submit=(Button)findViewById(R.id.submit);

        Intent intent=getIntent();
        String rating=intent.getStringExtra("rating");
        String orderid=intent.getStringExtra("ORDERID");
        String condition=intent.getStringExtra("condition");
        String userId=intent.getStringExtra("userId");


        user_input=userId;
        orderid_input=orderid;
        rating_input=rating;
        condition_input=condition;





        String feedback= intent.getStringExtra("pos");
        JSONArray jarr=null;
        try {
            jarr=new JSONArray(feedback);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        order_text_view = (TextView) findViewById(R.id.feed_oredr_id);
        order_text_view.setText(orderid);
        rating_bar = (RatingBar) findViewById(R.id.feed_rating_bar);
        rating_bar.setRating(Float.valueOf(rating));
        rating_bar.setClickable(false);

        condition_text = (TextView) findViewById(R.id.reason_fedd_text);
        condition_text.setText(condition);

        recyclerView = (RecyclerView) findViewById(R.id.feedbacjk_recycle);
        adapter = new second_feed_back_adapter(getBaseContext(),jarr);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new SpacesItemDecoration(2));

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reason_input=reason.getText().toString();
                new SubmitFeedback().execute(ServerLinks.submit_feedback);

            }
        });
    }
    private class SubmitFeedback extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {
            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", user_input);
                jsonObject.accumulate("order_id", orderid_input);
                jsonObject.accumulate("conditions", "");
                jsonObject.accumulate("rating", "");
                jsonObject.accumulate("reason", reason_input);
				/*
				 * jsonObject.accumulate("mobile", Mobile);
				 * jsonObject.accumulate("password", Password);
				 */
                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                if (!result.equals("")) {
                    JSONObject jObject = new JSONObject(result);
                    int status = jObject.getInt("status");
                    if (status == 1) {
                        Toast.makeText(second_feedback.this, "Feedback Saved Successfully", Toast.LENGTH_LONG).show();
                        finish();

                    }
                } else {
                    //Intent i = new Intent(Rating_activity.this, NointernetActivity.class);
                    //startActivity(i);

                }
            } catch (Exception e) {

            }

            //progressBar.setVisibility(View.GONE);

            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;
        public SpacesItemDecoration(int space) {
            this.space = space;
        }
        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {

            outRect.bottom = space+1;
            // Add top margin only for the first item to avoid double space between items
            if(parent.getChildPosition(view) == 0)
                outRect.top = space;
        }
    }

}

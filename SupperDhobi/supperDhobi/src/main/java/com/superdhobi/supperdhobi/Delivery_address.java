package com.superdhobi.supperdhobi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.PickAddress_recycler_adapter;
import com.superdhobi.AdapterPackage.PickSlot_recycler_adapter;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by PCIS-ANDROID on 23-04-2016.
 */
public class Delivery_address extends AppCompatActivity implements AsynkTaskCommunicationInterface {
    static String date;
    TextView month1,day1,date1,month2,day2,date2,month3,day3,date3,
            month4,day4,date4,month5,day5,date5,month6,day6,date6,
            month7,day7,date7;

    RelativeLayout next ;
    ArrayList<String> dateArray,dayArray,monthArray;
    HorizontalScrollView main,tag_main;
    private ArrayList<String> slotArrayList;
    private ArrayList<String> tagArrayList;
    LinearLayout slotlayout,taglayout;
    boolean isFirstViewClick=false;
    boolean isTagViewClick=false;
    View lastSelectedView,lastAddressView;
    View lastTimeView;
    DatePicker lastDatePicker;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    String[] addr_id,addr_head;
    String[] slot_id,slot_head,slot_head1;

    String day,day_t;
    TextView mSlotName;
    TextView mTagName;
    String callType;

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_adr_time);

        month1= (TextView) findViewById(R.id.month1);
        day1= (TextView) findViewById(R.id.day1);
        date1= (TextView) findViewById(R.id.date1);
        month2= (TextView) findViewById(R.id.month2);
        day2= (TextView) findViewById(R.id.day2);
        date2= (TextView) findViewById(R.id.date2);
        month3= (TextView) findViewById(R.id.month3);
        day3= (TextView) findViewById(R.id.day3);
        date3= (TextView) findViewById(R.id.date3);

        month4= (TextView) findViewById(R.id.month4);
        day4= (TextView) findViewById(R.id.day4);
        date4= (TextView) findViewById(R.id.date4);
        month5= (TextView) findViewById(R.id.month5);
        day5= (TextView) findViewById(R.id.day5);
        date5= (TextView) findViewById(R.id.date5);
        month6= (TextView) findViewById(R.id.month6);
        day6= (TextView) findViewById(R.id.day6);
        date6= (TextView) findViewById(R.id.date6);
        month7= (TextView) findViewById(R.id.month7);
        day7= (TextView) findViewById(R.id.day7);
        date7= (TextView) findViewById(R.id.date7);
        next= (RelativeLayout) findViewById(R.id.next);

        main=(HorizontalScrollView) findViewById(R.id.slot_hori);
        slotlayout=(LinearLayout) findViewById(R.id.slot);
        tag_main=(HorizontalScrollView) findViewById(R.id.tag_hori);
        taglayout=(LinearLayout) findViewById(R.id.tag);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.left_in, R.anim.right_out);
            }
        });


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        layoutManager1.scrollToPosition(0);

        dateArray = new ArrayList<String>();
        dayArray = new ArrayList<String>();
        monthArray = new ArrayList<String>();

        final String userId = getIntent().getStringExtra("userId");
        final String pickup_date = getIntent().getStringExtra("pickup_date");
        final String pickup_addrid = getIntent().getStringExtra("pickup_addrid");
        final String pickup_slot = getIntent().getStringExtra("pickup_slot");

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastAddressView==null) {
                    Toast toast = Toast.makeText(Delivery_address.this,"Choose Address Slot", Toast.LENGTH_LONG);
                    toast.show();
                }
                else if (lastSelectedView==null) {
                    Toast toast = Toast.makeText(Delivery_address.this,"Choose Time Slot", Toast.LENGTH_LONG);
                    toast.show();
                }

                else {
                    String deliv_date = getSelectedDate();
                    String deliv_addrid = (String) lastAddressView.getTag();
                    String deliv_slotid = (String) lastSelectedView.getTag();
                    Intent summ_intent = new Intent(Delivery_address.this, Summary_order.class);
                    summ_intent.putExtra("delivery_date", deliv_date);
                    summ_intent.putExtra("delivery_addrid", deliv_addrid);
                    summ_intent.putExtra("delivery_slot", deliv_slotid);
                    summ_intent.putExtra("userId", userId);
                    summ_intent.putExtra("pickup_date", pickup_date);
                    summ_intent.putExtra("pickup_addrid", pickup_addrid);
                    summ_intent.putExtra("pickup_slot", pickup_slot);

                    startActivity(summ_intent);
                    finish();
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            }
        });

        Date now = new Date();
        Date alsoNow = Calendar.getInstance().getTime();
        date = new SimpleDateFormat("dd-MMM-yyyy").format(now);
        int month = now.getMonth();
        int d = now.getDate();
        int yr = now.getYear();

        String dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", alsoNow);//Thursday
        String stringMonth = (String) android.text.format.DateFormat.format("MMM", alsoNow); //Jun
        String intMonth = (String) android.text.format.DateFormat.format("MM", alsoNow); //06
        String year = (String) android.text.format.DateFormat.format("yyyy", alsoNow); //2013
        String day = (String) android.text.format.DateFormat.format("dd", alsoNow); //20

        month1.setText(stringMonth);
        day1.setText("Today");
        date1.setText(day);

        String dateStr = new SimpleDateFormat("dd-MMM-yyyy").format(now);
        final DatePicker dtp = new DatePicker(date1, month1, dateStr);
        date1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeDateSelection(v, dtp);
                new TimeslotAsyncTask().execute(ServerLinks.timeslot);

            }
        });

        // for 2nd day

        final Calendar c = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        c.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = c.getTime();

        String dayOfTheWeek_t = (String) android.text.format.DateFormat.format("EEEE", tomorrow);//Thursday
        String stringMonth_t = (String) android.text.format.DateFormat.format("MMM", tomorrow); //Jun
        String intMonth_t = (String) android.text.format.DateFormat.format("MM", tomorrow); //06
        String year_t = (String) android.text.format.DateFormat.format("yyyy", tomorrow); //2013
        String day_t = (String) android.text.format.DateFormat.format("dd", tomorrow); //20

        month2.setText(stringMonth_t);
        day2.setText("Tommorrow");
        date2.setText(day_t);

        String dateStr2 = dateFormat.format(tomorrow);
        final DatePicker dtp2 = new DatePicker(date2, month2, dateStr2);
        date2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeDateSelection(v, dtp2);
                new TimeslotAsyncTask().execute(ServerLinks.timeslot);

            }
        });

        // // for 3rd day

        final Calendar c1 = Calendar.getInstance();
        DateFormat dateFormat_3 = new SimpleDateFormat("dd-MM-yyyy");
        c1.add(Calendar.DAY_OF_YEAR, 2);
        Date tomorrow_1 = c1.getTime();

        String dayOfTheWeek_th = (String) android.text.format.DateFormat.format("EEE", tomorrow_1);//Thursday
        String stringMonth_th = (String) android.text.format.DateFormat.format("MMM", tomorrow_1); //Jun
        String intMonth_th = (String) android.text.format.DateFormat.format("MM", tomorrow_1); //06
        String year_th = (String) android.text.format.DateFormat.format("yyyy", tomorrow_1); //2013
        String day_th = (String) android.text.format.DateFormat.format("dd", tomorrow_1); //20

        month3.setText(stringMonth_th);
        day3.setText(dayOfTheWeek_th);
        date3.setText(day_th);

        String dateStr3 = dateFormat.format(tomorrow_1);
        final DatePicker dtp3 = new DatePicker(date3, month3, dateStr3);
        date3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeDateSelection(v, dtp3);
                new TimeslotAsyncTask().execute(ServerLinks.timeslot);

            }
        });

        //for 4th day

        final Calendar c2 = Calendar.getInstance();
        DateFormat dateFormat_4 = new SimpleDateFormat("dd-MM-yyyy");
        c2.add(Calendar.DAY_OF_YEAR, 3);
        Date tomorrow_2 = c2.getTime();

        String dayOfTheWeek_fr = (String) android.text.format.DateFormat.format("EEE", tomorrow_2);//Thursday
        String stringMonth_fr = (String) android.text.format.DateFormat.format("MMM", tomorrow_2); //Jun
        String intMonth_tfr = (String) android.text.format.DateFormat.format("MM", tomorrow_2); //06
        String year_tfr = (String) android.text.format.DateFormat.format("yyyy", tomorrow_2); //2013
        String day_fr = (String) android.text.format.DateFormat.format("dd", tomorrow_2); //20

        month4.setText(stringMonth_fr);
        day4.setText(dayOfTheWeek_fr);
        date4.setText(day_fr);

        String dateStr4 = dateFormat.format(tomorrow_2);
        final DatePicker dtp4 = new DatePicker(date4, month4, dateStr4);
        date4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeDateSelection(v, dtp4);
                new TimeslotAsyncTask().execute(ServerLinks.timeslot);

            }
        });

//for 5th day
        final Calendar c3 = Calendar.getInstance();
        DateFormat dateFormat_5 = new SimpleDateFormat("dd-MM-yyyy");
        c3.add(Calendar.DAY_OF_YEAR, 4);
        Date tomorrow_3 = c3.getTime();

        String dayOfTheWeek_fv = (String) android.text.format.DateFormat.format("EEE", tomorrow_3);//Thursday
        String stringMonth_fv= (String) android.text.format.DateFormat.format("MMM", tomorrow_3); //Jun
        String intMonth_fv = (String) android.text.format.DateFormat.format("MM", tomorrow_3); //06
        String year_fv = (String) android.text.format.DateFormat.format("yyyy", tomorrow_3); //2013
        String day_fv = (String) android.text.format.DateFormat.format("dd", tomorrow_3); //20

        month5.setText(stringMonth_fv);
        day5.setText(dayOfTheWeek_fv);
        date5.setText(day_fv);
        String dateStr5 = dateFormat.format(tomorrow_3);
        final DatePicker dtp5 = new DatePicker(date5, month5, dateStr5);
        date5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeDateSelection(v, dtp5);
                new TimeslotAsyncTask().execute(ServerLinks.timeslot);

            }
        });

//for 6th day
        final Calendar c4 = Calendar.getInstance();
        DateFormat dateFormat_6 = new SimpleDateFormat("dd-MM-yyyy");
        c4.add(Calendar.DAY_OF_YEAR, 5);
        Date tomorrow_4 = c4.getTime();

        String dayOfTheWeek_sx = (String) android.text.format.DateFormat.format("EEE", tomorrow_4);//Thursday
        String stringMonth_sx = (String) android.text.format.DateFormat.format("MMM", tomorrow_4); //Jun
        String intMonth_sx = (String) android.text.format.DateFormat.format("MM", tomorrow_4); //06
        String year_sx = (String) android.text.format.DateFormat.format("yyyy", tomorrow_4); //2013
        String day_sx = (String) android.text.format.DateFormat.format("dd", tomorrow_4); //20

        month6.setText(stringMonth_sx);
        day6.setText(dayOfTheWeek_sx);
        date6.setText(day_sx);

        String dateStr6 = dateFormat.format(tomorrow_4);
        final DatePicker dtp6 = new DatePicker(date6, month6, dateStr6);
        date6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeDateSelection(v, dtp6);
                new TimeslotAsyncTask().execute(ServerLinks.timeslot);

            }
        });

//for 7th day
        final Calendar c5 = Calendar.getInstance();
        DateFormat dateFormat_7 = new SimpleDateFormat("dd-MM-yyyy");
        c5.add(Calendar.DAY_OF_YEAR, 6);
        Date tomorrow_5 = c5.getTime();

        String dayOfTheWeek_sv = (String) android.text.format.DateFormat.format("EEE", tomorrow_5);//Thursday
        String stringMonth_sv = (String) android.text.format.DateFormat.format("MMM", tomorrow_5); //Jun
        String intMonth_sv = (String) android.text.format.DateFormat.format("MM", tomorrow_5); //06
        String year_sv = (String) android.text.format.DateFormat.format("yyyy", tomorrow_5); //2013
        String day_sv = (String) android.text.format.DateFormat.format("dd", tomorrow_5); //20

        month7.setText(stringMonth_sv);
        day7.setText(dayOfTheWeek_sv);
        date7.setText(day_sv);

        String dateStr7 = dateFormat.format(tomorrow_5);
        final DatePicker dtp7 = new DatePicker(date7, month7, dateStr7);
        date7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeDateSelection(v, dtp7);
                new TimeslotAsyncTask().execute(ServerLinks.timeslot);

            }
        });

        slotArrayList=new ArrayList<String>();

        slotArrayList.add("9.00am-10.00am");
        slotArrayList.add("11.00am-12.00pm");
        slotArrayList.add("1.00pm-2.00pm");
        slotArrayList.add("3.00pm-4.00pm");
        slotArrayList.add("5.00pm-6.00pm");
        //selectSlot();

        date2.callOnClick();

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("user_id", userId);
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.user_address);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Delivery_address.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);
    }
    private class TimeslotAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        private Activity context;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("date", getSelectedDate());
                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {

            try {
                if (!result.equals("")){
                    JSONObject jsonResponse;
                    JSONObject jObject = new JSONObject(result);
                    int status = jObject.getInt("status");
                    //Create a jsonObject of String data

                    if (status == 0) {
                        Toast.makeText(getBaseContext(), "Timeslot not assigned for this date!!", Toast.LENGTH_SHORT).show();
                    } else if (status == 1) {

                        final JSONArray jArr = jObject.getJSONArray("data");
                        if (jArr!=null){
                            selectSlot(jArr);
                        }

                    }
                }
                else{
                    //This part is call when internet connection not found..
                    Intent i = new Intent(Delivery_address.this,NointernetActivity.class);
                    startActivity(i);
                    Delivery_address.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
                    finish();
                }
            } catch (Exception e) {

            }
            //progress.setVisibility(View.GONE);
            super.onPostExecute(result);

        }



    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    //======
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if(jsonObject!=null){
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status=jsonObject.getInt("status");
                if(status==0){
                    Toast.makeText(Delivery_address.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                }
                else if(status==1){
                    if(callType=="addressDtls"){
                        JSONObject jobj= jsonObject.getJSONObject("data");
                        showAddressDialog(jobj);

                    }
                    else {

                        final JSONArray jArr = jsonObject.getJSONArray("data");
                        if (jArr != null) {
                            selectAddress_tag(jArr);
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private void changeDateSelection(View v, DatePicker dtp){
        ((TextView)v).setTextColor(getResources().getColor(R.color.white));
        ((TextView)v).setBackgroundResource(R.drawable.round2);
        if(lastTimeView!=null){
            ((TextView)lastTimeView).setTextColor(getResources().getColor(R.color.blacktxt));
            ((TextView)lastTimeView).setBackgroundResource(R.drawable.round3);
        }
        lastTimeView=v;
        lastDatePicker = dtp;
    }
    private String getSelectedDate(){
        String date = lastDatePicker.dateStr;
        return date;
    }
    private void selectAddress_tag(final JSONArray addr_head) {
        taglayout.removeAllViews();
        for (int i=0;i<addr_head.length();i++) {
            String addr_id="",addr_tag="";
            try {
                addr_id=addr_head.getJSONObject(i).getString("address_id");
                addr_tag=addr_head.getJSONObject(i).getString("address_heading");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            LayoutInflater inflate1 =null;
            inflate1 = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearView1= inflate1.inflate(R.layout.tag_custom1,null);
            TextView mTagName=(TextView)mLinearView1.findViewById(R.id.tag_name_pick);
            ImageView viewTag=(ImageView) mLinearView1.findViewById(R.id.view);
            LinearLayout tag_lay=(LinearLayout) mLinearView1.findViewById(R.id.tag_lay);
            //final String name_=tagArrayList.get(i);
            mTagName.setText(addr_tag);
            isTagViewClick = false;
            tag_lay.setTag(addr_id);
            //textview of inflate layoput on click
            tag_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //textview bcgrnd color chnge
                    v.setBackgroundColor(Color.parseColor("#00A0AE"));

                    TextView txtV=(TextView)v.findViewById(R.id.tag_name_pick);
                    ImageView imgV=(ImageView)v.findViewById(R.id.view);
                    txtV.setTextColor(Color.parseColor("#ffffff"));
                    imgV.setImageResource(R.drawable.viewactive);
                    if(lastAddressView!=null && !v.equals(lastAddressView)) {
                        lastAddressView.setBackgroundColor(Color.parseColor("#ffffff"));
                        TextView txtV1=(TextView)lastAddressView.findViewById(R.id.tag_name_pick);
                        ImageView imgV1=(ImageView)lastAddressView.findViewById(R.id.view);
                        txtV1.setTextColor(Color.parseColor("#000000"));
                        imgV1.setImageResource(R.drawable.view_new);
                    }
                    lastAddressView = v;
                }
            });
            final String addrName = addr_tag;
            viewTag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String addrId=(String)((LinearLayout)v.getParent()).getTag();
                    HashMap<String, String> parameters = new HashMap<String, String>();
                    parameters.put("address_id", addrId);
                    HashMap<String, String> hashMapLink = new HashMap<String, String>();
                    hashMapLink.put(AllStaticVariables.link, ServerLinks.addrdtls);
                    asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Delivery_address.this);
                    asynkTaskForServerCommunication.execute(parameters, hashMapLink);
                    callType="addressDtls";
                }
            });
            taglayout.addView(mLinearView1);
        }
    }
    private void selectSlot(final JSONArray slot_head) {
        slotlayout.removeAllViews();
        for (int i=0;i<slot_head.length();i++) {
            String slot_id="",slot_tag="";
            try {
                slot_id=slot_head.getJSONObject(i).getString("slot_id");
                slot_tag=slot_head.getJSONObject(i).getString("from_to");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            LayoutInflater inflate =null;
            inflate = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearView= inflate.inflate(R.layout.custom_slot_time_lay,null);

            final TextView mSlotName=(TextView) mLinearView.findViewById(R.id.time1);
            mSlotName.setText(slot_tag);
            mSlotName.setTag(slot_id);
            isFirstViewClick = false;
            //textview of inflate layoput on click
            mSlotName.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {

                                                 if (lastSelectedView == null) {
                                                     //textview bcgrnd color chnge
                                                     v.setBackgroundResource(R.drawable.bg_select_slot);
                                                     ((TextView)v).setTextColor(Color.parseColor("#ffffff"));
                                                     lastSelectedView = v;
                                                 } else if(!v.equals(lastSelectedView)) {
                                                     v.setBackgroundResource(R.drawable.bg_select_slot);
                                                     ((TextView)v).setTextColor(Color.parseColor("#ffffff"));
                                                     //and here reset the bgcolor and textcolor of lastSelectedView
                                                     lastSelectedView.setBackgroundResource(R.drawable.bg_slot);
                                                     ((TextView)lastSelectedView).setTextColor(Color.parseColor("#000000"));
                                                     lastSelectedView = v;
                                                 }
                                             }
                                         }

            );
            slotlayout.addView(mLinearView);
        }
    }
    class DatePicker{
        TextView date, month;
        String dateStr;
        DatePicker(TextView date, TextView month, String dateStr){
            this.date = date;
            this.month = month;
            this.dateStr = dateStr;
        }
    }
    private void showAddressDialog(JSONObject jobj){
        String addrHead="",city="",location="",centre="",landmark="";

        try {
            addrHead=jobj.getString("address_heading");
            city=jobj.getString("city");
            location=jobj.getString("location");
            centre=jobj.getString("circle");
            landmark=jobj.getString("landmark");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Dialog dialog = new Dialog(Delivery_address.this);
        LayoutInflater inflater = (LayoutInflater)Delivery_address.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.choose_address_custom, null, false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(view);

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setTitle("Address..");
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        TextView tag_name= (TextView) dialog.findViewById(R.id.tag_name);
        TextView addr_back= (TextView) dialog.findViewById(R.id.addr_back);
        LinearLayout back=(LinearLayout)dialog.findViewById(R.id.btnBack);

        TextView city_name= (TextView) dialog.findViewById(R.id.city);
        TextView land_name= (TextView) dialog.findViewById(R.id.landmark);
        TextView center_name= (TextView) dialog.findViewById(R.id.center);
        TextView location_name= (TextView) dialog.findViewById(R.id.location);

        tag_name.setText(addrHead);
        city_name.setText(city);
        land_name.setText(landmark);
        center_name.setText(centre);
        location_name.setText(location);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ImageView delet = (ImageView) dialog.findViewById(R.id.addr_del);
        dialog.show();

    }
}


package com.superdhobi.supperdhobi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by PCIS-ANDROID on 03-06-2016.
 */
public class Pickup_Location extends Activity {

    Spinner city, centre, circle, location;
    EditText tag, addresss, landmark;
    static String tagg, addrs, lndmrk;
    static String cityy, cntr, crcl, locn;
    Button submit;
    static String tagn,addr_id;
    private SharedPreferences shp;
    static String hm_userid;


    public static String newc_name, newc_id, newcntr_name, newcntr_id,
            newcrcl_name, newcrcl_id, newlocn_name, newlocn_id;

    ArrayList<String> city_name, city_id, center_name, center_id, circle_name,
            circle_id, location_name, location_id;

    public final static int REQUEST_CODE = 1;
    FrameLayout progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pickup_location);
        progress = (FrameLayout) findViewById(R.id.frame_progress);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        new c_cityAsyncTask().execute(ServerLinks.choose_city);

        shp = Pickup_Location.this.getSharedPreferences("AuthToken",
                Context.MODE_PRIVATE);


        city = (Spinner) findViewById(R.id.spin_city);
        centre = (Spinner) findViewById(R.id.spin_cntr);
        circle = (Spinner) findViewById(R.id.spin_crcl);
        location = (Spinner) findViewById(R.id.spin_locn);

        tag = (EditText) findViewById(R.id.et_tagg);
        addresss = (EditText) findViewById(R.id.et_addrs);
        landmark = (EditText) findViewById(R.id.et_lndmk);

        hm_userid = shp.getString("USER_UID", null);
        tagn = getIntent().getStringExtra("EDITTAG");

        addr_id = getIntent().getStringExtra("ADDID");
        if (tagn== null){

        }
        else{
            tag.setText(tagn);
        }

        submit = (Button) findViewById(R.id.btn_sub);
        /*submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                tagg = tag.getText().toString();
                addrs = addresss.getText().toString();
                lndmrk = landmark.getText().toString();

                if (tagg.equalsIgnoreCase("") || addrs.equalsIgnoreCase("")
                        || lndmrk.equalsIgnoreCase("")) {

                    Toast toast = Toast.makeText(Pickup_Location.this,
                            "Fill all the field", Toast.LENGTH_LONG);
                    toast.show();
                } else {

                    new submit_addAsyncTask()
                            .execute(ServerLinks.submit_address);
					*//*
					 * int resultCode = 3; setResult(resultCode); finish();
					 *//*
                    Uri uri = Uri.parse(hm_userid);
                    Intent resultIntent = new Intent();
                    resultIntent.setData(uri);
                    //setResult(Activity.RESULT_OK,resultIntent);
                    int resultCode = 6;
                    setResult(resultCode, resultIntent);
                    finish();





					*//*
					 * Uri uri=Uri.parse(tagg); Intent resultIntent=new
					 * Intent(); resultIntent.setData(uri);
					 * resultIntent.putExtra("TAGG", tagg);
					 * resultIntent.putExtra("ADDRS", addrs);
					 * resultIntent.putExtra("LANDD", lndmrk);
					 * setResult(Activity.RESULT_OK,resultIntent);
					 * Toast.makeText(Addlocationnew.this, "Successfully Saved",
					 * 1).show(); finish();
					 *//*

                }
            }
        });*/
		/*
		 * tag.setText(tagg); addresss.setText(addrs); landmark.setText(lndmrk);
		 */

    }

    private class c_cityAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                // jsonObject.accumulate("p_date",date);
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            city_name = new ArrayList<String>();
            city_id = new ArrayList<String>();
            // String OutputData = "";
            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);

                    String status = jsonObject.optString("status");

                    if (status.equals("1")) {
                        JSONArray jArr = jsonObject.getJSONArray("data");
                        if (jArr != null) {

                            city_name.add("Select Your City");
                            city_id.add("");
                            for (int i = 0; i < jArr.length(); i++) {
                                jsonResponse = jArr.getJSONObject(i);
                                String c_id = jsonResponse.optString("id")
                                        .toString();
                                String c_name = jsonResponse.optString("city")
                                        .toString();
                                city_id.add(c_id);
                                city_name.add(c_name);
                            }
                            loadSpinnerData();

                        } else {

                        }
                    } else {
                        city_name.add("Select Your City");
                        city_id.add("l");
                        loadSpinnerData();
					/*Toast.makeText(Addlocationnew.this,
							"Something is going wrong!!!.", Toast.LENGTH_LONG).show();*/
                    }
                }
                else{
                    Toast.makeText( Pickup_Location.this, "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            progress.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }

    // ==============
    private class c_centerAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("location_id", newc_id);
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString1(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            center_name = new ArrayList<String>();
            center_id = new ArrayList<String>();
            // String OutputData = "";
            JSONObject jsonResponse;

            try {
                if (!result.equals("")){
                    JSONObject jsonObject = new JSONObject(result);

                    String status = jsonObject.optString("status");

                    if (status.equals("1")) {
                        JSONArray jArr = jsonObject.getJSONArray("data");

                        if (jArr != null) {

                            center_name.add("Select Your Center");
                            center_id.add("");
                            for (int i = 0; i < jArr.length(); i++) {
                                jsonResponse = jArr.getJSONObject(i);
                                String cntr_id = jsonResponse.optString("id")
                                        .toString();
                                String cntr_name = jsonResponse.optString(
                                        "location_name").toString();
                                center_id.add(cntr_id);
                                center_name.add(cntr_name);
                            }
                            loadSpinnerData1();

                        } else {

                        }
                    } else {
                        center_name.add("Select Your Center");
                        center_id.add("");
                        loadSpinnerData1();

                    }

                }
                else{
                    Toast.makeText( Pickup_Location.this, "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            // progressDialog.dismiss();

            super.onPostExecute(result);
        }

    }

    // ==============
    private class c_circleAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("location_id", newcntr_id);
                //jsonObject.accumulate("centerid", newcntr_id);
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString2(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            circle_name = new ArrayList<String>();
            circle_id = new ArrayList<String>();
            // String OutputData = "";
            JSONObject jsonResponse;

            try {
                if (!result.equals("")){
                    JSONObject jsonObject = new JSONObject(result);

                    String status = jsonObject.optString("status");

                    if (status.equals("1")) {
                        JSONArray jArr = jsonObject.getJSONArray("data");

                        if (jArr != null) {

                            circle_name.add("Select Your Circle");
                            circle_id.add("");
                            for (int i = 0; i < jArr.length(); i++) {
                                jsonResponse = jArr.getJSONObject(i);
                                String crcl_id = jsonResponse.optString("id")
                                        .toString();
                                String crcl_name = jsonResponse.optString(
                                        "location_name").toString();
                                circle_id.add(crcl_id);
                                circle_name.add(crcl_name);
                            }
                            loadSpinnerData2();

                        } else {

                        }
                    } else {
                        circle_name.add("Select Your Circle");
                        circle_id.add("");
                        loadSpinnerData2();

                    }
                }
                else{
                    Toast.makeText( Pickup_Location.this, "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            // progressDialog.dismiss();

            super.onPostExecute(result);
        }

    }

    // ==============
    private class c_locationAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */

        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("location_id", newcrcl_id);
                //jsonObject.accumulate("centerid", newcntr_id);
                //jsonObject.accumulate("circleid", newcrcl_id);
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString3(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            location_name = new ArrayList<String>();
            location_id = new ArrayList<String>();
            // String OutputData = "";
            JSONObject jsonResponse;

            try {
                if (!result.equals("")){
                    JSONObject jsonObject = new JSONObject(result);

                    String status = jsonObject.optString("status");

                    if (status.equals("1")) {
                        JSONArray jArr = jsonObject.getJSONArray("data");

                        if (jArr != null) {

                            location_name.add("Select Your Location");
                            location_id.add("l");
                            for (int i = 0; i < jArr.length(); i++) {
                                jsonResponse = jArr.getJSONObject(i);
                                String locn_id = jsonResponse.optString(
                                        "id").toString();
                                String locn_name = jsonResponse.optString(
                                        "location_name").toString();
                                location_id.add(locn_id);
                                location_name.add(locn_name);
                            }
                            loadSpinnerData3();

                        } else {

                        }
                    } else {
                        location_name.add("Select Your Location");
                        location_id.add("");
                        loadSpinnerData3();

                    }

                }
                else{
                    Toast.makeText( Pickup_Location.this, "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            // progressDialog.dismiss();

            super.onPostExecute(result);
        }

    }

    // =============

    private class submit_addAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this); // Set
			 * progressdialog title
			 * //mProgressDialog.setTitle("Android JSON Parse Tutorial"); // Set
			 * progressdialog message progressDialog.setMessage("Loading...");
			 * progressDialog.setIndeterminate(false); // Show progressdialog
			 * progressDialog.show();
			 */
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("userid", hm_userid);
                jsonObject.accumulate("tag_addressname", tagg);
                jsonObject.accumulate("cityid", newc_id);
                jsonObject.accumulate("centerid", newcntr_id);
                jsonObject.accumulate("circleid", newcrcl_id);
                jsonObject.accumulate("locationid", newlocn_id);
                jsonObject.accumulate("plot_detail", addrs);
                jsonObject.accumulate("landmark_detail", lndmrk);
                jsonObject.accumulate("addressid", addr_id);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString5(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            try {
                if (!result.equals("")){
                    JSONObject jObject = new JSONObject(result);
                    int status = jObject.getInt("status");
                    if (status == 0) {

                        Toast.makeText(getBaseContext(), "Try Again!", Toast.LENGTH_SHORT).show();
                    } else if (status == 1) {
                        tag.setText("");
                        addresss.setText("");
                        landmark.setText("");
                        // submitAddress();
					/*
					 * Toast.makeText(getApplication(), "Successfull",
					 * 0).show(); int resultCode = 3; setResult(resultCode);
					 * finish();
					 */
					/*
					 * Uri uri=Uri.parse(tagg); Intent resultIntent=new
					 * Intent(); resultIntent.setData(uri);
					 * resultIntent.putExtra("TAGG", tagg);
					 * resultIntent.putExtra("ADDRS", addrs);
					 * resultIntent.putExtra("LANDD", lndmrk);
					 * setResult(Activity.RESULT_OK,resultIntent);
					 * Toast.makeText(Addlocationnew.this, "Successfully Saved",
					 * 1).show(); finish();
					 */

					/*
					 * Intent intent4=new
					 * Intent(Addlocationnew.this,MainActivity.class);
					 * startActivity(intent4);
					 */
                    }
                }
                else{
                    Toast.makeText( Pickup_Location.this, "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {

            }
            new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    progress.setVisibility(View.GONE);

                }
            };

            super.onPostExecute(result);

        }

    }

    // ==============
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    private void loadSpinnerData() {
        // TODO Auto-generated method stub

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                Pickup_Location.this, android.R.layout.simple_spinner_item,
                city_name);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        city.setAdapter(dataAdapter);

        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                newc_name = city_name.get(position);
                newc_id = city_id.get(position);
                // then return selectedValue to the web service
                if (newc_name == "Select Your City")
                {
                    new c_centerAsyncTask().execute(ServerLinks.choose_center);
                }
                else
                {
                    new c_centerAsyncTask().execute(ServerLinks.choose_center);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }

        });
    }

    // ==============
    private static String convertInputStreamToString1(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    private void loadSpinnerData1() {
        // TODO Auto-generated method stub

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                Pickup_Location.this, android.R.layout.simple_spinner_item,
                center_name);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        centre.setAdapter(dataAdapter);

        centre.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                newcntr_name = center_name.get(position);
                newcntr_id = center_id.get(position);
                // then return selectedValue to the web service
                if (newcntr_name == "Select Your Center") {
                    new c_circleAsyncTask().execute(ServerLinks.choose_circle);
                } else {
                    new c_circleAsyncTask().execute(ServerLinks.choose_circle);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }

        });
    }

    // ==============
    private static String convertInputStreamToString2(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    private void loadSpinnerData2() {
        // TODO Auto-generated method stub

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                Pickup_Location.this, android.R.layout.simple_spinner_item,
                circle_name);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        circle.setAdapter(dataAdapter);

        circle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                newcrcl_name = circle_name.get(position);
                newcrcl_id = circle_id.get(position);
                // then return selectedValue to the web service
                if (newcrcl_name == "Select Your Circle") {
                    new c_locationAsyncTask()
                            .execute(ServerLinks.choose_location);
                } else {
                    new c_locationAsyncTask()
                            .execute(ServerLinks.choose_location);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }

        });
    }

    // =============

    private static String convertInputStreamToString3(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    private void loadSpinnerData3() {
        // TODO Auto-generated method stub

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                Pickup_Location.this, android.R.layout.simple_spinner_item,
                location_name);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        location.setAdapter(dataAdapter);

        location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                newlocn_name = location_name.get(position);
                newlocn_id = location_id.get(position);
                // then return selectedValue to the web service
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

    }

    // ===========

    private static String convertInputStreamToString5(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

}

package com.superdhobi.supperdhobi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by PCIS-ANDROID on 14-04-2016.
 */
public class Choose_time extends AppCompatActivity {

    private String[] dateArray;
    private String[] dayArray;
    private String[] monthArray;
    private String[] week_dayArray;

    static String date;
    TextView month1,day1,date1,month2,day2,date2,month3,day3,date3,month4,day4,date4,month5,day5,date5,month6,day6,date6,month7,day7,date7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_time);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_Pr);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        month1= (TextView) findViewById(R.id.month1);
        day1= (TextView) findViewById(R.id.day1);
        date1= (TextView) findViewById(R.id.date1);
        month2= (TextView) findViewById(R.id.month2);
        day2= (TextView) findViewById(R.id.day2);
        date2= (TextView) findViewById(R.id.date2);
        month3= (TextView) findViewById(R.id.month3);
        day3= (TextView) findViewById(R.id.day3);
        date3= (TextView) findViewById(R.id.date3);

        month4= (TextView) findViewById(R.id.month4);
        day4= (TextView) findViewById(R.id.day4);
        date4= (TextView) findViewById(R.id.date4);
        month5= (TextView) findViewById(R.id.month5);
        day5= (TextView) findViewById(R.id.day5);
        date5= (TextView) findViewById(R.id.date5);
        month6= (TextView) findViewById(R.id.month6);
        day6= (TextView) findViewById(R.id.day6);
        date6= (TextView) findViewById(R.id.date6);
        month7= (TextView) findViewById(R.id.month7);
        day7= (TextView) findViewById(R.id.day7);
        date7= (TextView) findViewById(R.id.date7);

        Date now = new Date();
        Date alsoNow = Calendar.getInstance().getTime();
        date = new SimpleDateFormat("dd-MMM-yyyy").format(now);
        int month = now.getMonth();
        int d = now.getDate();
        int yr = now.getYear();

        String dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", alsoNow);//Thursday
        String stringMonth = (String) android.text.format.DateFormat.format("MMM", alsoNow); //Jun
        String intMonth = (String) android.text.format.DateFormat.format("MM", alsoNow); //06
        String year = (String) android.text.format.DateFormat.format("yyyy", alsoNow); //2013
        String day = (String) android.text.format.DateFormat.format("dd", alsoNow); //20

        month1.setText(stringMonth);
        day1.setText("Today");
        date1.setText(day);

// for 2nd day
        final Calendar c = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        c.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = c.getTime();

        String dayOfTheWeek_t = (String) android.text.format.DateFormat.format("EEEE", tomorrow);//Thursday
        String stringMonth_t = (String) android.text.format.DateFormat.format("MMM", tomorrow); //Jun
        String intMonth_t = (String) android.text.format.DateFormat.format("MM", tomorrow); //06
        String year_t = (String) android.text.format.DateFormat.format("yyyy", tomorrow); //2013
        String day_t = (String) android.text.format.DateFormat.format("dd", tomorrow); //20

        month2.setText(stringMonth_t);
        day2.setText("Tommorrow");
        date2.setText(day_t);

// for 3rd day
        final Calendar c1 = Calendar.getInstance();
        DateFormat dateFormat_3 = new SimpleDateFormat("dd-MM-yyyy");
        c1.add(Calendar.DAY_OF_YEAR, 2);
        Date tomorrow_1 = c1.getTime();

        String dayOfTheWeek_th = (String) android.text.format.DateFormat.format("EEE", tomorrow_1);//Thursday
        String stringMonth_th = (String) android.text.format.DateFormat.format("MMM", tomorrow_1); //Jun
        String intMonth_th = (String) android.text.format.DateFormat.format("MM", tomorrow_1); //06
        String year_th = (String) android.text.format.DateFormat.format("yyyy", tomorrow_1); //2013
        String day_th = (String) android.text.format.DateFormat.format("dd", tomorrow_1); //20

        month3.setText(stringMonth_th);
        day3.setText(dayOfTheWeek_th);
        date3.setText(day_th);

//for 4th day
        final Calendar c2 = Calendar.getInstance();
        DateFormat dateFormat_4 = new SimpleDateFormat("dd-MM-yyyy");
        c2.add(Calendar.DAY_OF_YEAR, 3);
        Date tomorrow_2 = c2.getTime();

        String dayOfTheWeek_fr = (String) android.text.format.DateFormat.format("EEE", tomorrow_2);//Thursday
        String stringMonth_fr = (String) android.text.format.DateFormat.format("MMM", tomorrow_2); //Jun
        String intMonth_tfr = (String) android.text.format.DateFormat.format("MM", tomorrow_2); //06
        String year_tfr = (String) android.text.format.DateFormat.format("yyyy", tomorrow_2); //2013
        String day_fr = (String) android.text.format.DateFormat.format("dd", tomorrow_2); //20

        month4.setText(stringMonth_fr);
        day4.setText(dayOfTheWeek_fr);
        date4.setText(day_fr);

//for 5th day
        final Calendar c3 = Calendar.getInstance();
        DateFormat dateFormat_5 = new SimpleDateFormat("dd-MM-yyyy");
        c3.add(Calendar.DAY_OF_YEAR, 4);
        Date tomorrow_3 = c3.getTime();

        String dayOfTheWeek_fv = (String) android.text.format.DateFormat.format("EEE", tomorrow_3);//Thursday
        String stringMonth_fv= (String) android.text.format.DateFormat.format("MMM", tomorrow_3); //Jun
        String intMonth_fv = (String) android.text.format.DateFormat.format("MM", tomorrow_3); //06
        String year_fv = (String) android.text.format.DateFormat.format("yyyy", tomorrow_3); //2013
        String day_fv = (String) android.text.format.DateFormat.format("dd", tomorrow_3); //20

        month5.setText(stringMonth_fv);
        day5.setText(dayOfTheWeek_fv);
        date5.setText(day_fv);

//for 6th day
        final Calendar c4 = Calendar.getInstance();
        DateFormat dateFormat_6 = new SimpleDateFormat("dd-MM-yyyy");
        c4.add(Calendar.DAY_OF_YEAR, 5);
        Date tomorrow_4 = c4.getTime();

        String dayOfTheWeek_sx = (String) android.text.format.DateFormat.format("EEE", tomorrow_4);//Thursday
        String stringMonth_sx = (String) android.text.format.DateFormat.format("MMM", tomorrow_4); //Jun
        String intMonth_sx = (String) android.text.format.DateFormat.format("MM", tomorrow_4); //06
        String year_sx = (String) android.text.format.DateFormat.format("yyyy", tomorrow_4); //2013
        String day_sx = (String) android.text.format.DateFormat.format("dd", tomorrow_4); //20

        month6.setText(stringMonth_sx);
        day6.setText(dayOfTheWeek_sx);
        date6.setText(day_sx);

//for 7th day
        final Calendar c5 = Calendar.getInstance();
        DateFormat dateFormat_7 = new SimpleDateFormat("dd-MM-yyyy");
        c5.add(Calendar.DAY_OF_YEAR, 6);
        Date tomorrow_5 = c5.getTime();

        String dayOfTheWeek_sv = (String) android.text.format.DateFormat.format("EEE", tomorrow_5);//Thursday
        String stringMonth_sv = (String) android.text.format.DateFormat.format("MMM", tomorrow_5); //Jun
        String intMonth_sv = (String) android.text.format.DateFormat.format("MM", tomorrow_5); //06
        String year_sv = (String) android.text.format.DateFormat.format("yyyy", tomorrow_5); //2013
        String day_sv = (String) android.text.format.DateFormat.format("dd", tomorrow_5); //20

        month7.setText(stringMonth_sv);
        day7.setText(dayOfTheWeek_sv);
        date7.setText(day_sv);

    }
}

package com.superdhobi.supperdhobi;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by User on 4/18/2016.
 */
public class New_FeedbackForm extends AppCompatActivity {
    Spinner spinner;
    Button submit;
    EditText feedbak,nam,emai;
    String order,name,email,feedback,spin;
    TextView Submit,orderid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_feedback_form);
        orderid = (TextView) findViewById(R.id.order_id);
        nam = (EditText) findViewById(R.id.edittext_name);
        emai = (EditText) findViewById(R.id.edittext_email);
        feedbak = (EditText) findViewById(R.id.edittext_feedback);
        spinner = (Spinner) findViewById(R.id.spinner1);
        submit = (Button) findViewById(R.id.Submit);


        nam.setText("Ashish Kumar");
        nam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nam.setFocusable(true);
                nam.setFocusableInTouchMode(true);

            }
        });
        emai.setText("kbashish160@gmail.com");
        emai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emai.setFocusable(true);
                emai.setFocusableInTouchMode(true);
            }
        });

        String OrderID = getIntent().getStringExtra("ORDERID");
        orderid.setText(OrderID);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        final String[] list ={"Select a Category","General","Complaint","Appreciate"};


        final ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(New_FeedbackForm.this, android.R.layout.simple_spinner_item,
                list);

        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            private boolean mInitialized = false;
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order = orderid.getText().toString();
                name = nam.getText().toString();
                email = emai.getText().toString();
                feedback = feedbak.getText().toString();
                if(name.equals("")){
                    nam.setError("Name Should not be blank");
                }
                else if(email.equals("")){
                    emai.setError("Email Should not be blank");
                }

                else if (spinner.getSelectedItem().equals("Select a Category")){
                    TextView errorText = (TextView)spinner.getSelectedView();
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText("Please select a Feedback Category");
                }

                else if(feedback.equals("")){
                    feedbak.setError("Feedback Should not left blank");
                }


                 else {
                    feedbak.setError(null);
                    nam.setText(name);
                    emai.setText(email);
                    feedbak.setText(feedback);
                    Toast.makeText(New_FeedbackForm.this, "Thank you for your feedback", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }
}

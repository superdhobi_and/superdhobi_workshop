package com.superdhobi.supperdhobi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.support.v4.view.PagerAdapter;

import com.superdhobi.AdapterPackage.MenAdapter;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class super_dhobi_order extends AppCompatActivity  implements AsynkTaskCommunicationInterface {

    ViewPager pager;
   public TabLayout tabLayout;
    public MyViewpagerAdapter myViewPagerAdapter;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;

    ArrayList<JSONObject> childArray = new ArrayList<>();

    MenAdapter menAdapter ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_dhobi_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Schedule Your Order");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        pager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_sd);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



        HashMap<String, String> parameters = new HashMap<String, String>();
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.tabarray);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(super_dhobi_order.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);


        myViewPagerAdapter = new MyViewpagerAdapter();


    }

    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if(jsonObject!=null){
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status=jsonObject.getInt("status");
                if(status==0){
                    Toast.makeText(super_dhobi_order.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                }
                else if(status==1){

                    final JSONArray jArr = jsonObject.getJSONArray("data");

                    Log.d("JARREY" , jArr.toString());
                    if (jArr!=null){
                        for (int i=0;i<jArr.length();i++){
                            JSONObject json1 = jArr.getJSONObject(i);
                            childArray.add(json1);
                            String parentName = json1.getString("Parent");
                            String parentId = json1.getString("parent_id");

                            tabLayout.addTab(tabLayout.newTab().setText(parentName));


                           // pageAdapter.addFrag(new MenFrag(json1,parentName,userId), parentName);
                        }
                        pager.setAdapter(myViewPagerAdapter);

                        tabLayout.setupWithViewPager(pager);
                    }
                    else {}
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Intent i = new Intent(super_dhobi_order.this, NointernetActivity.class);
            startActivity(i);
            super_dhobi_order.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
            finish();
        }




        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {



            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


        }

        @Override
        public void onPageSelected(int position) {




        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    public class MyViewpagerAdapter extends PagerAdapter{

        private LayoutInflater layoutInflater;
        JSONArray childArr = null;
        public MyViewpagerAdapter() {



        }




        @Override
        public int getCount() {
            return childArr.length();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.view_pager_layout, container, false);


            JSONObject jObj = childArray.get(position);
            try {
                childArr = jObj.getJSONArray("clothes");

            } catch (JSONException e) {
                e.printStackTrace();
            }


            RecyclerView view_recycle = (RecyclerView) view.findViewById(R.id.rec_view_pager_);

              menAdapter = new MenAdapter(getBaseContext(),childArr);

            view_recycle.setAdapter(menAdapter);

            container.addView(view);

            return view;


        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }



    private void displayMetaInfo(int position) {

    }


}

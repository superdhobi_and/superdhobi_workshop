package com.superdhobi.supperdhobi;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.Login_Activity;
import com.superdhobi.navigation.MainActivity;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class SignupActivity extends Activity {
	static EditText name, email, mobile, password, lastname,refer, mb_otp,con_password;
	TextView login_link;
	Button  submit_otp;
	ImageView register,back;
	static String Name;
	static String Lastname;
	static String Fullname;
	static String Email,refer_code;
	static String Mobile, otp_mobile;
	static String Password;
	private SharedPreferences shp;
	static String u_userid, mb_id;
	static String f_name, f_mbl, f_uid, f_email,f_referal,f_passowrd;
	FrameLayout progress, otp_f;
	LinearLayout main_layout;
	SharedPreferenceClass sharedPreferenceClass;
	static  String imsiSIM1,imsiSIM2;

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// super.onCreate(savedInstanceState);
		setContentView(R.layout.v2_signup);
		//ButterKnife.bind(this);
		shp = getSharedPreferences("AuthToken", MODE_PRIVATE);
		//registerViews();
		super.onCreate(savedInstanceState);
		sharedPreferenceClass=new SharedPreferenceClass(getApplicationContext());
		main_layout = (LinearLayout) findViewById(R.id.main_signup);
		progress = (FrameLayout) findViewById(R.id.frame_progress);
		otp_f = (FrameLayout) findViewById(R.id.otp_frame);
		//back_sign = (ImageView) findViewById(R.id.back_sign);
		login_link= (TextView) findViewById(R.id.link_login);

		name = (EditText) findViewById(R.id.et_uname);
		email = (EditText) findViewById(R.id.et_uem);
		mobile = (EditText) findViewById(R.id.et_umb);
		password = (EditText) findViewById(R.id.et_up);
		//con_password = (EditText) findViewById(R.id.et_up_c);
		mb_otp = (EditText) findViewById(R.id.et_otp);
		refer = (EditText) findViewById(R.id.et_ref);
		back= (ImageView) findViewById(R.id.back_new);
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent b = new Intent(SignupActivity.this, Login_Activity.class);
				startActivity(b);
				overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();
			}
		});
		/*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});*/

		/*back_sign.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				shp = getSharedPreferences("AuthToken", MODE_PRIVATE);
				String s = shp.getString("start_sign", null);
				*//*if (s.equals("from_first")) {
					Intent bc = new Intent(SignupActivity.this, First.class);
					startActivity(bc);
					finish();

				}*//*
				if (s.equals("from_log")) {
					Intent bc = new Intent(SignupActivity.this, Login_Activity.class);
					startActivity(bc);
					overridePendingTransition(R.anim.left_in, R.anim.right_out);
					finish();
				}


			}
		});*/
		login_link.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent abc = new Intent(SignupActivity.this, Login_Activity.class);
				startActivity(abc);
				overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();
			}
		});

		register = (ImageView) findViewById(R.id.submit);
		submit_otp = (Button) findViewById(R.id.ok_otp);

		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*if (!InputValidation.isEditTextHasvalue(name)) {
					name.setError("Enter your name");
				} else*/ if (!InputValidation.isEditTextHasvalue(email)) {
					email.setError("Enter email id");
				} else if (!InputValidation.isEditTextContainEmail(email)) {
					email.setError("Enter a valid email id");
				} else if (!InputValidation.isEditTextHasvalue(mobile)) {
					mobile.setError("Enter your mobile number");
				} else if (!InputValidation.isNumberselected(mobile)) {
					mobile.setError("Enter a valid mobile number");
				} else if (!InputValidation.isEditTextHasvalue(password)) {
					password.setError("Enter password");
				} else if (!InputValidation.isPasswordLengthCheck(password)) {
					password.setError("Enter a valid password");
				}
				else {
					Name = name.getText().toString();
					//Lastname = lastname.getText().toString();
					Email = email.getText().toString();
					Mobile = mobile.getText().toString();
					Password = password.getText().toString();
					refer_code = refer.getText().toString();
					new User_signup_HttpAsyncTask().execute(ServerLinks.saveUser);
				}
			}
		});

		submit_otp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				otp_mobile = mb_otp.getText().toString();
				if (otp_mobile.equalsIgnoreCase("")) {
					Toast.makeText(SignupActivity.this, " Enter your OTP ", Toast.LENGTH_LONG).show();
				} else {
					//new otp_verification_HttpAsyncTask().execute(ServerLinks.otp);
				}
			}
		});
		imsiSIM1 = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
		/*TelephonyManager manager = (TelephonyManager) getSystemService(this.TELEPHONY_SERVICE);
		Log.i("OmSai ", "Single or Dula Sim "+manager.getPhoneCount());
		Log.i("OmSai ", "Defualt device ID "+manager.getDeviceId());
		Log.i("OmSai ", "Single 1 " + manager.getDeviceId(0));
		Log.i("OmSai ", "Single 2 " + manager.getDeviceId(1));*/
		// rnd
		/*TelephonyManager tm=(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		imsiSIM1=tm.getDeviceId();*/
		// actual code
		/*TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);

		 imsiSIM1 = telephonyInfo.getImsiSIM1();
		 imsiSIM2 = telephonyInfo.getImsiSIM2();

		boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
		boolean isSIM2Ready = telephonyInfo.isSIM2Ready();

		boolean isDualSIM = telephonyInfo.isDualSIM();*/
/*Toast.makeText(this," IME1 : " + imsiSIM1 + "\n" +
		" IME2 : " + imsiSIM2 + "\n" +
		" IS DUAL SIM : " + isDualSIM + "\n" +
		" IS SIM1 READY : " + isSIM1Ready + "\n" +
		" IS SIM2 READY : " + isSIM2Ready + "\n",Toast.LENGTH_LONG).show();*/
		/*tv.setText(" IME1 : " + imsiSIM1 + "\n" +
				" IME2 : " + imsiSIM2 + "\n" +
				" IS DUAL SIM : " + isDualSIM + "\n" +
				" IS SIM1 READY : " + isSIM1Ready + "\n" +
				" IS SIM2 READY : " + isSIM2Ready + "\n");
	*/

	}

	private class User_signup_HttpAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);

		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("name", Name);
				jsonObject.accumulate("email", Email);
				jsonObject.accumulate("mobile", Mobile);
				jsonObject.accumulate("password", Password);
				/*jsonObject.accumulate("deviceid1",imsiSIM1 );
				jsonObject.accumulate("deviceid2", imsiSIM2);*/
				jsonObject.accumulate("ref_by", refer_code);
				/*deviceid1
						deviceid2
				senderreferral*/
				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}
			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {
			progress.setVisibility(View.GONE);
			try {
				if (!result.equals("")){
					JSONObject jObject = new JSONObject(result);
					String status = jObject.optString("status");
					String data = jObject.getString("data");
					if (status.equals("1")) {
						JSONObject dataObject = new JSONObject(data);
						f_name = dataObject.getString("name");
						f_email = dataObject.getString("email");
						f_mbl = dataObject.getString("mobile");
						f_uid = dataObject.getString("userid");
						f_referal = dataObject.getString("ref_code");
						sharedPreferenceClass.setValue_string("Who_Login","USER");
						sharedPreferenceClass.setValue_boolean("USER", true);
						submitForm();
						//Toast.makeText(SignupActivity.this, "Successfully created", Toast.LENGTH_SHORT).show();
						Intent intent4 = new Intent(SignupActivity.this,Otp_Activity.class);
						startActivity(intent4);
						finish();
					}else {
						Toast.makeText(SignupActivity.this, data, Toast.LENGTH_LONG).show();
					}
			}
			else{
				Toast.makeText( SignupActivity.this, "Bad Network Connection !!",
						Toast.LENGTH_SHORT).show();
			}
			} catch (Exception e) {

			}
			// progressDialog.dismiss();
			super.onPostExecute(result);
		}
		private void submitForm() {
			try {

				SharedPreferences.Editor shpEdit = shp.edit();
				shpEdit.putString("AuthKey", f_email);
				shpEdit.putString("USER_UID", f_uid);
				shpEdit.putString("UNAME", f_name);
				shpEdit.putString("MBL_NUM", f_mbl);
				shpEdit.putString("USER_REFERAL", f_referal);
				shpEdit.commit();

			} catch (Exception e) {
				e.printStackTrace();

				finish();

			}
		}
	}

	private class otp_verification_HttpAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("otp", otp_mobile);
				jsonObject.accumulate("mid", mb_id);
				jsonObject.accumulate("mobile", Mobile);
				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {

			// progress.setVisibility(View.GONE);
			try {
				JSONObject jObject = new JSONObject(result);
				int status = jObject.getInt("status");
				if (status == 0) {

					Toast.makeText(getBaseContext(), "Try Again!", Toast.LENGTH_SHORT).show();
				} else if (status == 1) {
					f_name = jObject.getString("name");
					f_email = jObject.getString("email");
					f_mbl = jObject.getString("mobile");
					f_uid = jObject.getString("userid");
					//submitForm();
					Toast.makeText(getApplication(), "Successfull", Toast.LENGTH_SHORT).show();

					Intent intent4 = new Intent(SignupActivity.this, MainActivity.class);
					startActivity(intent4);
					finish();

				} else if (status == 2) {
					Toast.makeText(getApplication(), "OTP did not match.", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {

			}

			// progressDialog.dismiss();
			super.onPostExecute(result);

		}

		/*private void submitForm() {

			try {

				SharedPreferences.Editor shpEdit = shp.edit();
				shpEdit.putString("AuthKey", f_email);
				shpEdit.putString("USER_UID", f_uid);
				shpEdit.putString("UNAME", f_name);
				shpEdit.putString("MBL_NUM", f_mbl);
				shpEdit.commit();
				finish();
			} catch (Exception e) {
				e.printStackTrace();

				finish();

			}
		}*/
	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	@Override
	public void onBackPressed() {
		// Display alert message when back button has been pressed
		backButtonHandler();
		return;
	}

	public void backButtonHandler() {
		shp = getSharedPreferences("AuthToken", MODE_PRIVATE);
		String s = shp.getString("start_sign", null);
		/*if (s.equals("from_first")) {
			Intent bc = new Intent(SignupActivity.this, First.class);
			startActivity(bc);
			finish();

		}*/
		if (s.equals("from_log")) {
			Intent bc = new Intent(SignupActivity.this, Login_Activity.class);
			startActivity(bc);
			overridePendingTransition(R.anim.left_in, R.anim.right_out);
			finish();
		}
	}

}

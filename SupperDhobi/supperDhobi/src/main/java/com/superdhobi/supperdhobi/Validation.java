package com.superdhobi.supperdhobi;


import android.widget.EditText;

import java.util.regex.Pattern;

public class Validation {
 
    // Regular Expression
    // you can change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REGEX = "\\d{3}-\\d{7}";
 
    // Error Messages
    private static final String REQUIRED_MSG = "required";
    private static final String EMAIL_MSG = "invalid email";
   private static final String PASSWORD="password  > 5";
//    private static final String PHONE_MSG = "##########";
    
 
    // call this method when you need to check email validation
    public static boolean isEmailAddress(EditText editText, boolean required) {
        return isValid(editText, EMAIL_REGEX, EMAIL_MSG, required);
    }
    //call this method when you need password validation
    public static boolean isPassword(EditText text,boolean required){
    	return isvalidate(text,PASSWORD,required);
    }
 
    // call this method when you need to check phone number validation
    public static boolean isPhoneNumber(EditText editText, boolean required) {

    	String phoneNo = editText.getText().toString().trim();
        editText.setError(null);
        //validate phone numbers of format "1234567890"
        if (phoneNo.matches("\\d{10}")) return true;
        //validating phone number with -, . or spaces
        else if(phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
        //validating phone number with extension length from 3 to 5
        else if(phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
        //validating phone number where area code is in braces ()
        else if(phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
        //return false if nothing matches the input
        else return false;
         
    }
    // return true if the input field is valid, based on the parameter passed
    public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {
 
        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);
 
        // text required and editText is blank, so return false
        if ( required && !hasText(editText) ) return false;
 
        // pattern doesn't match so returning false
        if (required && !Pattern.matches(regex, text)) {
           
            return false;
        };
        
        return true;
    }
 
    // check the input field has any text or not
    // return true if it contains text otherwise false
   
        public static boolean hasText(EditText editText) {
        	 
            String text = editText.getText().toString().trim();
            editText.setError(null);
     
            // length 0 means there is no text
            if (text.length() == 0) {
                editText.setError(REQUIRED_MSG);
                return false;
            }
 
        return true;
    }
        public static boolean isvalidate(EditText text,String passwor,boolean required){
        	String password=text.getText().toString().trim();
        	text.setError(null);
        	if ( required && !hasText(text) ) return false;
        	
        		if (password != null && password.length() < 5) {
        			
        			return true;
        			
        		}
        			
        		return false;
        	}
        }

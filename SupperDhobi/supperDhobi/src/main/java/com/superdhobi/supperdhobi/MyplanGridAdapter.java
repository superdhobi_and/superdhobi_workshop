package com.superdhobi.supperdhobi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Abhishek on 6/23/2016.
 */
public class MyplanGridAdapter extends RecyclerView.Adapter<MyplanGridAdapter.gridHolder> {

    Context context;
    int width;
    private String[] bgColors;
    private  String[] jArr1, jArr2, jArr3, jArr4;;
    String userId;
    String color;

    public MyplanGridAdapter(Context baseContext, int width, String[] jArr1, String[] jArr2, String[] jArr3, String[] jArr4, String userId) {

        this.context = baseContext;
        this.width = width;
        this.jArr1 = jArr1;
        this.jArr2 = jArr2;
        this.jArr3 = jArr3;
        this.jArr4 = jArr4;
        this.userId=userId;
        bgColors = context.getApplicationContext().getResources().getStringArray(R.array.color_array);
    }

    @Override
    public MyplanGridAdapter.gridHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.myplan_adapter, parent, false);
        gridHolder holder = new gridHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyplanGridAdapter.gridHolder holder, final int position) {
        color = bgColors[position % bgColors.length];
        holder.image.setLayoutParams(new RelativeLayout.LayoutParams(width , 100));


        holder.heading.setTextColor(Color.parseColor(color));
        holder.ruppe_lay.setBackgroundColor(Color.parseColor(color));

        holder.heading.setText(jArr2[position]);
        holder.ruppe.setText("Rs. "+  jArr3[position]);

        holder.each.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Detail_member_user.class);
                intent.putExtra("planId", jArr1[position]);
                intent.putExtra("userId", userId);
                intent.putExtra("colorname", color);
                intent.putExtra("valid", jArr4[position]);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jArr1.length;
    }

    public class gridHolder extends RecyclerView.ViewHolder {

        ImageView image;
        RelativeLayout each, ruppe_lay;
        TextView ruppe, heading;

        public gridHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.image_view);
            each = (RelativeLayout) itemView.findViewById(R.id.each);
            ruppe = (TextView) itemView.findViewById(R.id.ruppe);
            heading = (TextView) itemView.findViewById(R.id.heading);
            ruppe_lay = (RelativeLayout) itemView.findViewById(R.id.ruppe_lay);

        }
    }
}

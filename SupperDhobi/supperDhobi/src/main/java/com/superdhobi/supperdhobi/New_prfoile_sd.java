package com.superdhobi.supperdhobi;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.superdhobi.AdapterPackage.Address_recycler_adapter;
import com.superdhobi.Utillls.ProfileAddress;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by PCIS-ANDROID on 11-04-2016.
 */
public class New_prfoile_sd extends AppCompatActivity implements AsynkTaskCommunicationInterface {

    List<ProfileAddress> profileAddresses;

    RecyclerView addr_tag;
    ImageView edit_profile;
    EditText name, mobile, email;
    Button update;
    private final int SELECT_PHOTO = 1;
    private ImageView pick_image,add_address;

    CircleImageView imageView;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    String json_username,json_name,json_mob,json_email,json_refcode;
    //created by Asish
    String f_name, f_email, f_mbl, f_uid;
    static String p_userid, userid, emailid, new_phn, new_name;
    String userId;
    ProgressBar progressBar;

    File  image  = null;


    SharedPreferences preference ;
    SharedPreferences.Editor editor;


    String[] json_addrHead,json_circle,json_city,json_location,json_landmark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_sd_profile);

        pick_image = (ImageView) findViewById(R.id.pick_image);
        edit_profile= (ImageView) findViewById(R.id.edit_profile);
        add_address= (ImageView) findViewById(R.id.add_profile_address);
        name= (EditText) findViewById(R.id.tv_name);
        mobile= (EditText) findViewById(R.id.tv_mbl);
        email= (EditText) findViewById(R.id.tv_email);
        update= (Button) findViewById(R.id.update);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_Pr);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imageView = (CircleImageView) findViewById(R.id.profile_image);

        progressBar = (ProgressBar) findViewById(R.id.profile_new_prog);
         preference = getSharedPreferences("PROFILE_PIC",MODE_APPEND);

         editor = preference.edit();

        String input = preference.getString("imagePreferance", null);

        if (input == null){

            try {

            }catch (Exception e){

                e.printStackTrace();
            }

        }else {
            Log.e("encoded_bitmap",input);
            Bitmap profile_bit = decodeBase64(input);
            imageView.setImageBitmap(profile_bit);
        }



        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(New_prfoile_sd.this,Addlocationnew.class);
                startActivityForResult(i, 1);
            }
        });




        addr_tag= (RecyclerView) findViewById(R.id.recycler_view_men);

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                update.setVisibility(View.VISIBLE);
                edit_profile.setVisibility(View.GONE);
                name.setFocusable(true);
                name.setFocusableInTouchMode(true);
                mobile.setFocusable(true);
                mobile.setFocusableInTouchMode(true);
                email.setFocusable(true);
                email.setFocusableInTouchMode(true);
                name.setBackground(getResources().getDrawable(R.drawable.bg_profile_edit));
                mobile.setBackground(getResources().getDrawable(R.drawable.bg_profile_edit));
                email.setBackground(getResources().getDrawable(R.drawable.bg_profile_edit));

            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                update.setVisibility(View.GONE);
                edit_profile.setVisibility(View.VISIBLE);
                name.setFocusable(false);
                name.setFocusableInTouchMode(false);
                mobile.setFocusable(false);
                mobile.setFocusableInTouchMode(false);
                email.setFocusable(false);
                email.setFocusableInTouchMode(false);
                name.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                mobile.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                email.setBackgroundColor(getResources().getColor(android.R.color.transparent));

                new_name = name.getText().toString();
                new_phn = mobile.getText().toString();
                emailid = email.getText().toString();

                new Updateprofile().execute(ServerLinks.edit_profile);
            }
        });

        pick_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        userId = getIntent().getStringExtra("userId");

        refresService();

    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }



    private static final int READ_CONTACTS_PERMISSIONS_REQUEST = 1234;

    // Called when the user is performing an action which requires the app to read the
    // user's contacts
    public void getPermissionToReadUserContacts() {
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // The permission is NOT already granted.
            // Check if the user has been asked about this permission already and denied
            // it. If so, we want to give more explanation about why the permission is needed.
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_CONTACTS)) {
                // Show our own UI to explain to the user why we need to read the contacts
                // before actually requesting the permission and showing the default UI
            }

            // Fire off an async request to actually get the permission
            // This will show the standard permission request dialog UI
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},
                    READ_CONTACTS_PERMISSIONS_REQUEST);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_CONTACTS_PERMISSIONS_REQUEST) {
            if (grantResults.length == 2 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, " permission granted to Access Camera , Sd card", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "permission denied to Access Camera , Sd card", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }






    private class Updateprofile extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {
            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", userId);
                jsonObject.accumulate("email", emailid);
                jsonObject.accumulate("name", new_name);
                jsonObject.accumulate("mobile", new_phn);
				/*
				 * jsonObject.accumulate("mobile", Mobile);
				 * jsonObject.accumulate("password", Password);
				 */
                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                if (!result.equals("")) {
                    JSONObject jObject = new JSONObject(result);
                    int status = jObject.getInt("status");
                    if (status == 1) {
                        f_name = jObject.getString("name");
                        f_email = jObject.getString("email");
                        f_mbl = jObject.getString("mobile");
                        f_uid = jObject.getString("user_id");

                        name.setText(f_name);
                        email.setText(f_email);
                        mobile.setText(f_mbl);

                    } else if (status == 2) {

                        Toast.makeText(New_prfoile_sd.this, "Email Id  is already Registered ,Please Use any other emailid ", Toast.LENGTH_SHORT).show();
                    } else if (status == 3) {
                        Toast.makeText(New_prfoile_sd.this, "Mobile number already in use, Please use any other email id", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(New_prfoile_sd.this, "Unable to store. Please try again.", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Intent i = new Intent(New_prfoile_sd.this, NointernetActivity.class);
                    startActivity(i);

                }
            } catch (Exception e) {

            }

            progressBar.setVisibility(View.GONE);

            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if(jsonObject!=null){
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status = jsonObject.getInt("status");
                String data=jsonObject.getString("data");
                //Create a jsonObject of String data
                JSONObject dataObject = new JSONObject(data);
                if(status==0){
                    Toast.makeText(New_prfoile_sd.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                }
                else if(status==1){

                    json_username=dataObject.getString("username");
                    json_name=dataObject.getString("name");
                    json_mob=dataObject.getString("mobile");
                    json_email=dataObject.getString("email");
                    json_refcode=dataObject.getString("ref_code");

                    name.setText(json_name);
                    mobile.setText(json_mob);
                    email.setText(json_email);
                    final JSONArray jArr = dataObject.getJSONArray("address");
                    profileAddresses = new Gson().fromJson(jArr.toString(),new TypeToken<ArrayList<ProfileAddress>>(){}.getType());
                    if(profileAddresses==null)

                    profileAddresses=new ArrayList<ProfileAddress>();
                    Address_recycler_adapter address_recycler_adapter= new Address_recycler_adapter(New_prfoile_sd.this,profileAddresses);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                    layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    layoutManager.scrollToPosition(0);
                    addr_tag.setLayoutManager(layoutManager);
                    addr_tag.setAdapter(address_recycler_adapter);// set adapter

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(New_prfoile_sd.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {


                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imgFilename = "sd_profile" + timeStamp;

                    File nfile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);



                        try {
                            image = File.createTempFile(
                                    imgFilename,  /* prefix */
                                    ".jpg",         /* suffix */
                                    nfile      /* directory */
                            );

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    editor.clear();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
                    startActivityForResult(intent, 3);
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, 2);
                    editor.clear();

                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void refresService(){
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("user_id", userId);
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.user_profile);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(New_prfoile_sd.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode==RESULT_OK){
                refresService();

               // convert_image_bitmap(image);


            }

        }else if (requestCode==2 && resultCode==RESULT_OK){

            Uri uri = data.getData();

           // choose from gallery
            convert_bitmap(uri);

        }

        else if (requestCode == 3 && resultCode == RESULT_OK){

            //take photo
            convert_image_bitmap(image);


        }
    }



    public void convert_bitmap(Uri uri){

        Bitmap bitmap=null;


        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
        } catch (Exception e) {
            e.printStackTrace();
        }

        imageView.setImageBitmap(bitmap);
        save_in_preferrence(bitmap);

    }




    public void convert_image_bitmap(File image){



        Bitmap bitmap=null;


        try {
            bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }

        imageView.setImageBitmap(bitmap);

        save_in_preferrence(bitmap);
    }

    private void save_in_preferrence(Bitmap bitmap) {




        editor.putString("imagePreferance", encodeTobase64(bitmap));
        editor.commit();

    }

    private String encodeTobase64(Bitmap bitmap) {

        Bitmap immage = bitmap;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.d("Image Log:", imageEncoded);
        return imageEncoded;

    }

    @Override
                public boolean onCreateOptionsMenu (Menu menu){
                    return true;
                }





            }


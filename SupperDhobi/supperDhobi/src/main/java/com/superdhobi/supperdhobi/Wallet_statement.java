package com.superdhobi.supperdhobi;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.Txn_recycler_adapter;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * Created by PCIS-ANDROID on 13-04-2016.
 */
public class Wallet_statement extends AppCompatActivity implements AsynkTaskCommunicationInterface  {
    RecyclerView txn_statement;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    String wallet_amount;
    TextView txtCurBal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_statement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_Pr);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        txtCurBal=(TextView)findViewById(R.id.currBal);
        txn_statement= (RecyclerView) findViewById(R.id.recycler_view_state);
        /*Intent intent=getIntent();
        String reason_tag1=intent.getStringExtra("pos");
        JSONArray jarr=null;
        try {
            jarr=new JSONArray(reason_tag1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String walletAmount;
        walletAmount=intent.getStringExtra("amount");
        txtCurBal.setText("Current Balance Rs."+walletAmount);*/




        String userId = getIntent().getStringExtra("userId");
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("user_id", userId);
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.walletdtls);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Wallet_statement.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);

    }
    //======
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if(jsonObject!=null){
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status=jsonObject.getInt("staus");
                if(status==0){
                    Toast.makeText(Wallet_statement.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                }
                else if(status==1){

                    wallet_amount=jsonObject.getString("walletamount");

                    final JSONArray jArr = jsonObject.getJSONArray("data");
                    if (jArr!=null){

                        Txn_recycler_adapter txn_recycler_adapter= new Txn_recycler_adapter(Wallet_statement.this,jArr);
                        txn_statement.setHasFixedSize(true);
                        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(Wallet_statement.this);
                        txn_statement.setLayoutManager(mLayoutManager2);
                        txn_statement.addItemDecoration(new DividerItemDecoration(Wallet_statement.this, LinearLayoutManager.VERTICAL));
                        txn_statement.setItemAnimator(new DefaultItemAnimator());
                        txn_statement.setAdapter(txn_recycler_adapter);

                    }
                    txtCurBal.setText("Current Balance Rs."+wallet_amount);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Intent i = new Intent(Wallet_statement.this, NointernetActivity.class);
            startActivity(i);
            Wallet_statement.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
            finish();
        }
    }
}

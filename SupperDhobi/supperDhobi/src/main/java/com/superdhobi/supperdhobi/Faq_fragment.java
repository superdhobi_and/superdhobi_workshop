package com.superdhobi.supperdhobi;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.superdhobi.AdapterPackage.Faq_adapter;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class Faq_fragment extends Fragment {

    RecyclerView faq_rec;
    JSONArray childArr;
    Faq_adapter faqAdapter;
    int columnWidth;
    public Faq_fragment(JSONObject jsonObj){
        try {
            childArr = jsonObj.getJSONArray("faqlist");
        }
        catch (Exception ex){

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =inflater.inflate(R.layout.fragment_faq_fragment, container, false);

        faq_rec = (RecyclerView) view.findViewById(R.id.faq_rec);
        faq_rec.addItemDecoration(new SpacesItemDecoration(6));
        faq_rec.setLayoutManager(new LinearLayoutManager(getActivity()));
        new MyAsync().execute();
        return view ;
    }
    public class MyAsync extends AsyncTask<Void,Void,Void> {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
        @Override
        protected Void doInBackground(Void... params) {

            WindowManager wm = (WindowManager) getActivity().getBaseContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            final Point point = new Point();
            try{
                display.getSize(point);
            }catch (NoSuchMethodError e){
                point.x = display.getWidth();
                point.y = display.getHeight();
            }
            columnWidth=point.x;

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            DisplayMetrics displayMetrics1 =new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics1);
            final int height = displayMetrics1.heightPixels;
            final int width = displayMetrics1.widthPixels;

            faq_rec.setLayoutManager(new LinearLayoutManager(getActivity()));

            faqAdapter = new Faq_adapter(getActivity().getBaseContext(),childArr);
            faq_rec.setAdapter(faqAdapter);

        }
    }
    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {

            outRect.bottom = space+1;
            // Add top margin only for the first item to avoid double space between items
            if(parent.getChildPosition(view) == 0)
                outRect.top = space;
        }
    }
}

package com.superdhobi.supperdhobi;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.Address_recycler_adapter;
import com.superdhobi.AdapterPackage.Order_history_adapter;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by PCIS-ANDROID on 18-04-2016.
 */
public class New_order_history extends Fragment {
    int[]orderImage;
    RecyclerView order_history;
    FrameLayout progress;
    String[] json_orderId,json_address;
    int[] json_orderstatus;
    String userId;
    ProgressBar progressBar;
    public New_order_history(String userId){
        try {
            this.userId=userId;
        }
        catch (Exception ex){

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.new_order_history, container, false);
        order_history= (RecyclerView) rootView.findViewById(R.id.recycler_view_order);
        progress= (FrameLayout) rootView.findViewById(R.id.frame_progress);
        progressBar = (ProgressBar) rootView.findViewById(R.id.order_new_prog);

        new orderhistory_listAsyntask().execute(ServerLinks.order_data);

        return rootView;
    }
    private class orderhistory_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", userId);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);

            JSONObject jsonResponse;

            try {
                if (!result.equals("")){

                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject!=null){
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            JSONArray jArr = jsonObject.getJSONArray("data");
                            if (jArr != null) {

                                json_orderId = new String[jArr.length()];
                                json_address = new String[jArr.length()];
                                json_orderstatus = new int[jArr.length()];
                                orderImage = new int[jArr.length()];
                                for (int i = 0; i < jArr.length(); i++) {
                                    jsonResponse = jArr.getJSONObject(i);
                                    json_orderId[i] = jsonResponse.getString("order_id");
                                    json_address[i] = jsonResponse.getString("address");
                                    json_orderstatus[i]= jsonResponse.getInt("orderstatus");


                                    if(json_orderstatus[i] >=5 && json_orderstatus[i] !=6 && json_orderstatus[i] < 9){
                                        orderImage[i]=R.drawable.track2;
                                    }else if(json_orderstatus[i] >=9  && json_orderstatus[i] < 11 ){
                                        orderImage[i]=R.drawable.track3;
                                    }else if(json_orderstatus[i] >=11 && json_orderstatus[i] !=12){
                                        orderImage[i]=R.drawable.track4 ;
                                    }else {
                                        orderImage[i]=R.drawable.track1;
                                    }
                                }
                            }

                            Order_history_adapter order_history_adapter= new Order_history_adapter(getActivity(),json_orderId,json_orderstatus,orderImage,json_address);
                            order_history.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getActivity());
                            order_history.setLayoutManager(mLayoutManager2);
                            order_history.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL));
                            order_history.setItemAnimator(new DefaultItemAnimator());

                            order_history.setAdapter(order_history_adapter);// set adapter

                        }
                    }
                    else {
                        Toast.makeText(getActivity(), "NetWork Error",Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(getActivity(), "Bad Network Connection !!",Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            progressBar.setVisibility(View.GONE);
            super.onPostExecute(result);
        }
    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

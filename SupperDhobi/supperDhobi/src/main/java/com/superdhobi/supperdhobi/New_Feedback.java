package com.superdhobi.supperdhobi;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.Order_history_adapter;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
public class New_Feedback extends AppCompatActivity {

    ListView listView;
    String userId;
    String[] json_orderId,json_address,json_orderdate;
    int[] json_orderstatus;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_feedback_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        listView = (ListView) findViewById(R.id.mainListView);
        userId = getIntent().getStringExtra("userId");
        progressBar = (ProgressBar)findViewById(R.id.order_new_prog);
        new orderhistory_listAsyntask().execute(ServerLinks.feedback_data);

    }
    private class orderhistory_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", userId);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);

            JSONObject jsonResponse;

            try {
                if (!result.equals("")){

                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject!=null){
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            JSONArray jArr = jsonObject.getJSONArray("data");
                            if (jArr != null) {

                                json_orderId = new String[jArr.length()];
                                json_address = new String[jArr.length()];
                                json_orderstatus = new int[jArr.length()];
                                json_orderdate = new String[jArr.length()];
                                for (int i = 0; i < jArr.length(); i++) {
                                    jsonResponse = jArr.getJSONObject(i);
                                    json_orderId[i] = jsonResponse.getString("order_id");
                                    json_address[i] = jsonResponse.getString("address");
                                    json_orderstatus[i]= jsonResponse.getInt("orderstatus");
                                    json_orderdate[i]= jsonResponse.getString("orderdate");
                                }
                            }

                            Feedback_CustomListAdapter adapter = new Feedback_CustomListAdapter(New_Feedback.this,json_orderId,json_orderstatus,json_address,json_orderdate,userId);
                            listView.invalidate();
                            listView.setAdapter(adapter);//set adapter

                        }
                    }
                    else {
                        Toast.makeText(New_Feedback.this, "NetWork Error", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(New_Feedback.this, "Bad Network Connection !!",Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            progressBar.setVisibility(View.GONE);
            super.onPostExecute(result);
        }
    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

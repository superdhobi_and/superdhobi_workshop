package com.superdhobi.supperdhobi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.superdhobi.Utillls.ProfileAddress;
import com.superdhobi.Utillls.SpinnerData;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import info.hoang8f.widget.FButton;
public class Addlocationnew extends Activity {

	Spinner citySpinner, circleSpinner, locationSpinner, areaSpinner;
	EditText tag, addresss, landmark,pincode;
	//static String tagg, addrs, lndmrk,pinCode;
	//static String cityy, cntr, crcl, locn;
	FButton submit,update;
	//static String tagn,addr_id;
	private SharedPreferences shp;
	private String hm_userid;
	private ProfileAddress addressObj;


	public static String newc_name, newc_id, newcntr_name, newcntr_id,
			newcrcl_name, newcrcl_id, newlocn_name, newlocn_id;

	//ArrayList<String> city_name, city_id, center_name, center_id, circle_name,
	//		circle_id, location_name, location_id;

	ArrayList<SpinnerData> cityArray, circleArray, locationArray, areaArray;

	public final static int REQUEST_CODE = 1;
	FrameLayout progress;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addressObj=(ProfileAddress)getIntent().getSerializableExtra("address");
		if(addressObj==null){
			addressObj=new ProfileAddress();
		}
		inIt();
		setContentView(R.layout.addlocation1);
		progress = (FrameLayout) findViewById(R.id.frame_progress);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		new c_cityAsyncTask().execute(ServerLinks.choose_city);

		shp = Addlocationnew.this.getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);

		citySpinner = (Spinner) findViewById(R.id.spin_city);
		circleSpinner = (Spinner) findViewById(R.id.spin_cntr);
		locationSpinner = (Spinner) findViewById(R.id.spin_crcl);
		areaSpinner = (Spinner) findViewById(R.id.spin_locn);

		tag = (EditText) findViewById(R.id.et_tagg);
		addresss = (EditText) findViewById(R.id.et_addrs);
		landmark = (EditText) findViewById(R.id.et_lndmk);
		pincode = (EditText) findViewById(R.id.et_pincode);

		hm_userid = shp.getString("USER_UID", null);

		submit = (FButton) findViewById(R.id.btn_sub);
		if(addressObj.getAddr_id()!=null){
			submit.setText("Update");
		}

		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				getValues();

				if (tag.getText().toString().equalsIgnoreCase("") || addresss.getText().toString().equalsIgnoreCase("")
						|| landmark.getText().toString().equalsIgnoreCase("") || pincode.getText().toString().equalsIgnoreCase("")) {

					Toast toast = Toast.makeText(Addlocationnew.this,
							"Fill all the field", Toast.LENGTH_LONG);
					toast.show();
				} else {

					new submit_addAsyncTask().execute(ServerLinks.submit_address);

				}
			}
		});
		/*
		 * tag.setText(tagg); addresss.setText(addrs); landmark.setText(lndmrk);
		 */
		populateData();

	}

	private void inIt(){
		cityArray = new ArrayList<SpinnerData>();
		SpinnerData defaultData = new SpinnerData("0","Select Your City");
		cityArray.add(defaultData);

		circleArray = new ArrayList<SpinnerData>();
		locationArray = new ArrayList<SpinnerData>();
		areaArray = new ArrayList<SpinnerData>();
	}
	private void onGetAddress(){

		Intent i=this.getIntent();
		setResult(RESULT_OK, i);
		finish();

	}
	private void populateData(){
		tag.setText(addressObj.getAdd_heading());
		addresss.setText(addressObj.getAddress());
		landmark.setText(addressObj.getLandmark());
		pincode.setText(addressObj.getPincode());
	}
	private  void getValues(){
		addressObj.setAdd_heading(tag.getText().toString());
		addressObj.setAddress(addresss.getText().toString());
		addressObj.setLandmark(landmark.getText().toString());
		addressObj.setPincode(pincode.getText().toString());
		addressObj.setCity(((SpinnerData)citySpinner.getSelectedItem()).getId());
		addressObj.setCircle(((SpinnerData) circleSpinner.getSelectedItem()).getId());
		addressObj.setLocation(((SpinnerData) locationSpinner.getSelectedItem()).getId());
		addressObj.setArea(((SpinnerData) areaSpinner.getSelectedItem()).getId());
	}

	private class c_cityAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				// jsonObject.accumulate("p_date",date);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			JSONObject jsonResponse;
			int selectedIndex=0;
			try {
				if (!result.equals("")) {

					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.optString("status");

					if (status.equals("1")) {
						JSONArray jArr = jsonObject.getJSONArray("data");
						if (jArr != null) {
							for (int i = 0; i < jArr.length(); i++) {
								jsonResponse = jArr.getJSONObject(i);
								String id = jsonResponse.optString("id").toString();
								String name = jsonResponse.optString("city").toString();
								SpinnerData data = new SpinnerData(id,name);
								cityArray.add(data);

								if(addressObj.getCity() != null && addressObj.getCity().equals(id)){
									selectedIndex=i+1;
								}
							}
						}
					}
					loadSpinnerData(citySpinner,cityArray,selectedIndex,"city");
				}
				else{
					Toast.makeText( Addlocationnew.this, "Bad Network Connection !!",
							Toast.LENGTH_SHORT).show();
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}
			progress.setVisibility(View.GONE);

			super.onPostExecute(result);
		}

	}

	// ==============
	private class c_CircleAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("location_id", newc_id);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString1(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			circleArray.clear();
			locationArray.clear();
			areaArray.clear();
			SpinnerData defaultdata =new SpinnerData("0","Select Your Circle");
			circleArray.add(defaultdata);

			int selectedIndex=0;
			JSONObject jsonResponse;

			try {
				if (!result.equals("")){
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					JSONArray jArr = jsonObject.getJSONArray("data");

					if (jArr != null) {
						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);
							String id = jsonResponse.optString("id").toString();
							String name = jsonResponse.optString("location_name").toString();
							SpinnerData data = new SpinnerData(id,name);
							circleArray.add(data);
							if(addressObj.getCircle()!=null && addressObj.getCircle().equals(id)){
								selectedIndex = i+1;
							}
						}
					}
					loadSpinnerData(circleSpinner,circleArray,selectedIndex,"circle");
				}

			}
			else{
				Toast.makeText( Addlocationnew.this, "Bad Network Connection !!",
						Toast.LENGTH_SHORT).show();
			}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}
			// progressDialog.dismiss();

			super.onPostExecute(result);
		}

	}

	// ==============For Location

	private class c_LocationAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("location_id", newcntr_id);
				//jsonObject.accumulate("centerid", newcntr_id);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString2(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			locationArray.clear();
			areaArray.clear();
			SpinnerData defaultdata =new SpinnerData("0","Select Your Location");
			locationArray.add(defaultdata);

			int selectedIndex=0;
			JSONObject jsonResponse;

			try {
				if (!result.equals("")){
					JSONObject jsonObject = new JSONObject(result);

					String status = jsonObject.optString("status");

					if (status.equals("1")) {
						JSONArray jArr = jsonObject.getJSONArray("data");
						if (jArr != null) {
							for (int i = 0; i < jArr.length(); i++) {
								jsonResponse = jArr.getJSONObject(i);
								String id = jsonResponse.optString("id").toString();
								String name = jsonResponse.optString("location_name").toString();
								SpinnerData data = new SpinnerData(id,name);
								locationArray.add(data);
								if(addressObj.getLocation()!=null && addressObj.getLocation().equals(id)){
									selectedIndex = i+1;
								}
							}
							loadSpinnerData(locationSpinner,locationArray,selectedIndex,"location");

						}
					}
				}
				else{
					Toast.makeText( Addlocationnew.this, "Bad Network Connection !!",
							Toast.LENGTH_SHORT).show();
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}
			// progressDialog.dismiss();

			super.onPostExecute(result);
		}

	}

	// ==============For Area
	private class c_AreaAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */

		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("location_id", newcrcl_id);
				//jsonObject.accumulate("centerid", newcntr_id);
				//jsonObject.accumulate("circleid", newcrcl_id);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString3(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			areaArray.clear();
			SpinnerData defaultdata =new SpinnerData("0","Select Your Area");
			areaArray.add(defaultdata);

			int selectedIndex=0;
			JSONObject jsonResponse;

			try {
				if (!result.equals("")){
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					JSONArray jArr = jsonObject.getJSONArray("data");

					if (jArr != null) {
						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);
							String id = jsonResponse.optString("id").toString();
							String name = jsonResponse.optString("location_name").toString();
							SpinnerData data = new SpinnerData(id,name);
							areaArray.add(data);
							if(addressObj.getArea()!=null && addressObj.getArea().equals(id)){
								selectedIndex = i+1;
							}
						}
						loadSpinnerData(areaSpinner,areaArray,selectedIndex,"area");

					}
				}
			}
			else{
				Toast.makeText( Addlocationnew.this, "Bad Network Connection !!",
						Toast.LENGTH_SHORT).show();
			}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}
			// progressDialog.dismiss();

			super.onPostExecute(result);
		}

	}

	// =============

	private class submit_addAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this); // Set
			 * progressdialog title
			 * //mProgressDialog.setTitle("Android JSON Parse Tutorial"); // Set
			 * progressdialog message progressDialog.setMessage("Loading...");
			 * progressDialog.setIndeterminate(false); // Show progressdialog
			 * progressDialog.show();
			 */
			//progress.setVisibility(View.VISIBLE);
		}
		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("user_id", hm_userid);
				jsonObject.accumulate("id",(addressObj.getAddr_id()==null)?"":addressObj.getAddr_id());
				jsonObject.accumulate("add_heading", addressObj.getAdd_heading());
				jsonObject.accumulate("city", addressObj.getCity());
				jsonObject.accumulate("circle", addressObj.getCircle());
				jsonObject.accumulate("location", addressObj.getLocation());
				jsonObject.accumulate("area", addressObj.getArea());
				jsonObject.accumulate("address", addressObj.getAddress());
				jsonObject.accumulate("landmark", addressObj.getLandmark());
				jsonObject.accumulate("pincode", addressObj.getPincode());

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString5(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {

			try {
				if (!result.equals("")){
				JSONObject jObject = new JSONObject(result);
				int status = jObject.getInt("status");
				if (status == 0) {
					Toast.makeText(getBaseContext(), "Try Again!", Toast.LENGTH_SHORT).show();
				} else if (status == 1) {
					Toast.makeText(getBaseContext(), "Address Saved Successfully", Toast.LENGTH_SHORT).show();
					onGetAddress();
				}
			}
			else{
				Toast.makeText( Addlocationnew.this, "Bad Network Connection !!",
						Toast.LENGTH_SHORT).show();
			}
			} catch (Exception e) {

			}
			new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progress.setVisibility(View.GONE);

				}
			};
			super.onPostExecute(result);
		}

	}

	// ==============
	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	/*private void loadSpinnerData() {
		// TODO Auto-generated method stub

		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				Addlocationnew.this, android.R.layout.simple_spinner_item,
				city_name);

		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		citySpinner.setAdapter(dataAdapter);

		citySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				newc_name = city_name.get(position);
				newc_id = city_id.get(position);
				// then return selectedValue to the web service
				if (newc_name == "Select Your City") 
				{
					new c_CircleAsyncTask().execute(ServerLinks.choose_center);
				} 
				else 
				{
					new c_CircleAsyncTask().execute(ServerLinks.choose_center);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});

		citySpinner.setSelection(city_name.indexOf(addressObj.getCity()));
	}*/

	private void loadSpinnerData(Spinner spinner,final ArrayList<SpinnerData> spinnerItems,int selectedIndex,final String type) {
		// TODO Auto-generated method stub

		// Creating adapter for spinner
		ArrayAdapter<SpinnerData> dataAdapter = new ArrayAdapter<SpinnerData>(
				Addlocationnew.this, android.R.layout.simple_spinner_item,
				spinnerItems);

		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		spinner.setAdapter(dataAdapter);

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
									   View selectedItemView, int position, long id) {
				if(type.equals("city")) {
					newc_id = spinnerItems.get(position).getId();
					new c_CircleAsyncTask().execute(ServerLinks.choose_center);
				}
				else if(type.equals("circle")) {
					newcntr_id = spinnerItems.get(position).getId();
					new c_LocationAsyncTask().execute(ServerLinks.choose_center);
				}
				else if(type.equals("location")) {
					newcrcl_id = spinnerItems.get(position).getId();
					new c_AreaAsyncTask().execute(ServerLinks.choose_center);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});

		if(selectedIndex > 0)
			spinner.setSelection(selectedIndex);
	}

	// ==============
	private static String convertInputStreamToString1(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	/*private void loadSpinnerData1() {
		// TODO Auto-generated method stub

		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				Addlocationnew.this, android.R.layout.simple_spinner_item,
				center_name);

		// Drop down layout style - list view with radio button
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		circleSpinner.setAdapter(dataAdapter);

		circleSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
									   View selectedItemView, int position, long id) {
				newcntr_name = center_name.get(position);
				newcntr_id = center_id.get(position);
				// then return selectedValue to the web service
				if (newcntr_name == "Select Your Center") {
					new c_LocationAsyncTask().execute(ServerLinks.choose_circle);
				} else {
					new c_LocationAsyncTask().execute(ServerLinks.choose_circle);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});
	}*/

	// ==============
	private static String convertInputStreamToString2(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	/*private void loadSpinnerData2() {
		// TODO Auto-generated method stub

		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				Addlocationnew.this, android.R.layout.simple_spinner_item,
				circle_name);

		// Drop down layout style - list view with radio button
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		locationSpinner.setAdapter(dataAdapter);

		locationSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
									   View selectedItemView, int position, long id) {
				newcrcl_name = circle_name.get(position);
				newcrcl_id = circle_id.get(position);
				// then return selectedValue to the web service
				if (newcrcl_name == "Select Your Circle") {
					new c_AreaAsyncTask()
							.execute(ServerLinks.choose_location);
				} else {
					new c_AreaAsyncTask()
							.execute(ServerLinks.choose_location);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});
	}*/

	// =============

	private static String convertInputStreamToString3(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	/*private void loadSpinnerData3() {
		// TODO Auto-generated method stub

		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				Addlocationnew.this, android.R.layout.simple_spinner_item,
				location_name);

		// Drop down layout style - list view with radio button
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		areaSpinner.setAdapter(dataAdapter);

		areaSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
									   View selectedItemView, int position, long id) {
				newlocn_name = location_name.get(position);
				newlocn_id = location_id.get(position);
				// then return selectedValue to the web service
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			}
		});

	}*/

	// ===========

	private static String convertInputStreamToString5(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

}

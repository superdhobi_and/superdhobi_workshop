package com.superdhobi.supperdhobi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by PCIS-ANDROID on 11-04-2016.
 */
public class Offer_recycler_adapter extends RecyclerView.Adapter<Offer_recycler_adapter.MyViewHolder> {
    //nameArray,offerArray,drawableArray
    private String[] nameArray;
    private String[] offerArray,expDtArray,idArray,imgArray;
    private int[] imageid;
    private static String LOG_TAG = "Offer_recycler_adapter";
    String couponId="";
    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,offer,couponcode,expiredt,text_percent;
        public ImageView imageView;
        public LinearLayout linearLayout;

        RelativeLayout img_rel ;

        public MyViewHolder(View view1) {
            super(view1);
            name = (TextView) view1.findViewById(R.id.textViewName);
            offer = (TextView) view1.findViewById(R.id.textViewVersion);
            imageView = (ImageView) view1.findViewById(R.id.imageView);
            linearLayout = (LinearLayout) view1.findViewById(R.id.tag_name);
            couponcode=(TextView)view1.findViewById(R.id.txtCouponCode);
            expiredt=(TextView)view1.findViewById(R.id.txtExpiredt);
            img_rel = (RelativeLayout) itemView.findViewById(R.id.imageView_relative);
            text_percent = (TextView) itemView.findViewById(R.id.text_percent);

        }
    }

    public Offer_recycler_adapter(Context context, String[] idArray,String[] nameArray, String[] offerArray,String[] expDtArray, int[] imageid,String[] imgArray) {
        this.nameArray = nameArray;
        this.offerArray = offerArray;
        this.expDtArray = expDtArray;
        this.idArray=idArray;
        this.context = context;
        this.imageid=imageid;
        this.imgArray=imgArray;

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.custom_offer, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(nameArray[position]);
        holder.couponcode.setText(offerArray[position]);
        //holder.imageView.setImageResource(imageid[position]);
        holder.expiredt.setText(expDtArray[position]);

        Uri myUri = Uri.parse(imgArray[position]);

        if (myUri.equals(null)){

            holder.imageView.setVisibility(View.VISIBLE);
            holder.text_percent.setVisibility(View.GONE);
            Glide.with(context)
                    .load(myUri)
                    .into(holder.imageView);

        }else {
            holder.imageView.setVisibility(View.GONE);
            holder.text_percent.setVisibility(View.VISIBLE);
            holder.text_percent.setText("50%");

        }


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*String Coupon = couponcode.getText().toString();
                String CouponTitle = name.getText().toString();
                String ExpiredDate = expiredt.getText().toString();*/
                Intent i = new Intent(context,Offer_details.class);
                i.putExtra("CODE",idArray[position]);
                i.putExtra("TITLE",nameArray[position]);
                i.putExtra("EXPIRE",expDtArray[position]);
                context.startActivity(i);
            }
        });


    }
    @Override
    public int getItemCount() {
        return nameArray.length;
    }


}
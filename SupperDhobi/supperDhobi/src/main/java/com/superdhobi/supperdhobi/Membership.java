package com.superdhobi.supperdhobi;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.superdhobi.AdapterPackage.tabpagerAdapter;

public class Membership extends AppCompatActivity {

    public static ViewPager viewPager;
    tabpagerAdapter pageAdapter;
    public static TabLayout tabLayout;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.member_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        tabLayout = (TabLayout) findViewById(R.id.tab);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        pageAdapter = new tabpagerAdapter(getSupportFragmentManager());

        userId = getIntent().getStringExtra("userId");

        pageAdapter.addFrag(new Plan_fragment(userId), "All plans");
        pageAdapter.addFrag(new History_fragment(userId), "My Plans");

        viewPager.setAdapter(pageAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }

}

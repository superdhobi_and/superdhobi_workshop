package com.superdhobi.supperdhobi;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Plan_Detail_Activity extends AppCompatActivity {

    Button apply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plan_details1);

        apply = (Button) findViewById(R.id.apply);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_all_plan);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Plan_Detail_Activity.this);
                overridePendingTransition(R.anim.left_in,R.anim.right_out);
                //setting custom layout to dialog
                dialog.setContentView(R.layout.confirmation_plan);
                dialog.setTitle("Super Wash Monthly");

                //adding text dynamically
                TextView wallet_amount = (TextView) dialog.findViewById(R.id.wallet_amount);
                wallet_amount.setText("Wallet Amount:");
                TextView wallet_price = (TextView) dialog.findViewById(R.id.wallet_price);
                wallet_price.setText("Rs.50");
                TextView plan = (TextView) dialog.findViewById(R.id.plan);
                plan.setText("Cloth Limit");
                TextView plan_title = (TextView) dialog.findViewById(R.id.plan_title);
                plan_title.setText("50");
                TextView price = (TextView) dialog.findViewById(R.id.plan_price);
                price.setText("Plan Price");
                TextView plan_cost = (TextView) dialog.findViewById(R.id.plan_cost);
                plan_cost.setText("Rs.250");
                TextView discount_plan = (TextView) dialog.findViewById(R.id.discont_plan);
                discount_plan.setText("Discount");
                TextView discount_price = (TextView) dialog.findViewById(R.id.discount_price);
                discount_price.setText("Rs.50");

                Button proceed = (Button) dialog.findViewById(R.id.proceed);
                proceed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Plan_Detail_Activity.this, "You have Sucessfully applied for this membership plan",
                                Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
                //adding button click event
                Button back = (Button) dialog.findViewById(R.id.back);
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
       }
}

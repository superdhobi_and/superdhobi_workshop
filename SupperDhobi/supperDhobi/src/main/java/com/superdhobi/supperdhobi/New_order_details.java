package com.superdhobi.supperdhobi;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.OrderListRecyclerAdapater;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;

/**
 * Created by PCIS-ANDROID on 20-04-2016.
 */
public class New_order_details extends AppCompatActivity implements AsynkTaskCommunicationInterface {
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    String json_orderdt,json_pickdt,json_delivdt,json_pickadd,json_deladdr,
            json_status,json_paidamnt,json_orderamnt,json_discamnt,json_couponcode;
    TextView orderdt,pickdt,delivdt,pickadd,deivadd,orderid,copupncode,priceid,
            pickadd1,pickadd2,pickadd3,deivadd1,deivadd2,deivadd3;
    String orderId="",orderStatus="";
    RecyclerView recyclerView;
    String[] json_cloth,json_services,json_quantity,json_unitprice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_order_details__);

        recyclerView = (RecyclerView) findViewById(R.id.order_list_dtails);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        orderdt= (TextView) findViewById(R.id.date);
        pickdt= (TextView) findViewById(R.id.datentime);
        delivdt= (TextView) findViewById(R.id.del_datentime);
        pickadd= (TextView) findViewById(R.id.pickaddr);
        pickadd1= (TextView) findViewById(R.id.pickaddr1);
        pickadd2= (TextView) findViewById(R.id.pickaddr2);
        pickadd3= (TextView) findViewById(R.id.pickaddr3);

        deivadd= (TextView) findViewById(R.id.delivaddr);
        deivadd1= (TextView) findViewById(R.id.delivaddr1);
        deivadd2= (TextView) findViewById(R.id.delivaddr2);
        deivadd3= (TextView) findViewById(R.id.delivaddr3);

        orderid= (TextView) findViewById(R.id.orderid);
        copupncode = (TextView) findViewById(R.id.copupn_code);
        priceid =(TextView)findViewById(R.id.price_id);

        orderId = getIntent().getStringExtra("jsonorderId");
        orderStatus = getIntent().getStringExtra("jsonorderStatus");
        getSupportActionBar().setTitle(orderStatus);


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("order_id", orderId);

        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.order_dtls);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(New_order_details.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);
    }

    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if (jsonObject != null) {
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status = jsonObject.getInt("status");
                String data = jsonObject.getString("data");
                //Create a jsonObject of String data
                JSONObject dataObject = new JSONObject(data);
                if (status == 0) {
                    Toast.makeText(New_order_details.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                } else if (status == 1) {
                        json_orderdt = dataObject.getString("order_date");
                        json_pickdt = dataObject.getString("pickup_date");
                        json_delivdt = dataObject.getString("delivery_date");
                        json_pickadd = dataObject.getString("pickupadd");

                        json_deladdr = dataObject.getString("deliveryadd");
                        json_status = dataObject.getString("status");
                        json_paidamnt= dataObject.getString("paid_amount");
                        json_orderamnt= dataObject.getString("order_price");
                        json_discamnt= dataObject.getString("discount_amount");
                        json_couponcode = dataObject.getString("coupon_code");

                        String[] paddrList = json_pickadd.split(",");
                        String pa1 = paddrList[0];
                        String pa2 = paddrList[1];
                        String pa3 = paddrList[2];
                        String pa4 = paddrList[3];
                        String pa5 = paddrList[4];
                        String pa6 = paddrList[5];
                        String pa7 = paddrList[6];

                        String[] daddrList = json_pickadd.split(",");
                        String da1 = daddrList[0];
                        String da2 = daddrList[1];
                        String da3 = daddrList[2];
                        String da4 = daddrList[3];
                        String da5 = daddrList[4];
                        String da6 = daddrList[5];
                        String da7 = daddrList[6];

                        orderdt.setText(json_orderdt);
                        pickdt.setText(json_pickdt);
                        delivdt.setText(json_delivdt);
                        orderid.setText(orderId);
                        pickadd.setText(pa1);
                        pickadd1.setText(pa2);
                        pickadd2.setText(pa3);
                        pickadd3.setText(pa4);
                        deivadd.setText(da1);
                        deivadd1.setText(da2);
                        deivadd2.setText(da3);
                        deivadd3.setText(da4);
                        copupncode.setText(json_couponcode);
                        priceid.setText("Total Amount :"+json_paidamnt);

                     JSONArray jArr = dataObject.getJSONArray("clothdetails");
                        if (jArr != null) {
                        json_cloth = new String[jArr.length()];
                        json_services = new String[jArr.length()];
                        json_quantity = new String[jArr.length()];
                        json_unitprice = new String[jArr.length()];

                        for (int i = 0; i < jArr.length(); i++) {
                            jsonResponse = jArr.getJSONObject(i);
                            json_cloth[i]   = jsonResponse.getString("cloth");
                            json_services[i] = jsonResponse.getString("service");
                            json_quantity[i] = jsonResponse.getString("quantity");
                            json_unitprice[i] = jsonResponse.getString("unitprice");
                        }
                    }
                    OrderListRecyclerAdapater adapter = new OrderListRecyclerAdapater(New_order_details.this, json_cloth, json_services, json_quantity, json_unitprice);
                    recyclerView.setHasFixedSize(true);

                    LinearLayoutManager layoutManager = new LinearLayoutManager(New_order_details.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    layoutManager.scrollToPosition(0);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapter);

                    }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    }
}

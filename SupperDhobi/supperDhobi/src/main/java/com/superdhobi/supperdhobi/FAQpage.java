package com.superdhobi.supperdhobi;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FAQpage extends AppCompatActivity implements AsynkTaskCommunicationInterface {

    TabLayout tab;
    ViewPager faq_pager;
    TabpagerAdapter tabpagerAdapter;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqpage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("FAQs");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        tabpagerAdapter = new TabpagerAdapter(getSupportFragmentManager());

        faq_pager = (ViewPager) findViewById(R.id.view_faq_pager);
        tab = (TabLayout) findViewById(R.id.tab_);

        HashMap<String, String> parameters = new HashMap<String, String>();
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.faqlist);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(FAQpage.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);

    }
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if (jsonObject != null) {
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status = jsonObject.getInt("status");
                if (status == 0) {
                    Toast.makeText(FAQpage.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                } else if (status == 1) {

                    final JSONArray jArr = jsonObject.getJSONArray("data");
                    if (jArr != null) {
                        for (int i = 0; i < jArr.length(); i++) {
                            JSONObject json1 = jArr.getJSONObject(i);
                            String parentName = json1.getString("faqcat");
                            tabpagerAdapter.addFrag(new Faq_fragment(json1), parentName);
                        }
                        faq_pager.setAdapter(tabpagerAdapter);
                        tab.setupWithViewPager(faq_pager);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Intent i = new Intent(FAQpage.this, NointernetActivity.class);
            startActivity(i);
            FAQpage.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
            finish();
        }


    }

}

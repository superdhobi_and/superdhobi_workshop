package com.superdhobi.supperdhobi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.superdhobi.AdapterPackage.subAdapter;
import com.superdhobi.Utillls.Catagories;
import com.superdhobi.Utillls.DatabaseHandler;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FooddetailActivity extends AppCompatActivity implements AsynkTaskCommunicationInterface {

    public static RecyclerView listView;

    ImageView itemImage;
    TextView title, desc;
    RelativeLayout layout;
    subAdapter adapter;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;

    int[] itenList = new int[]{R.drawable.sd_shirt, R.drawable.blazzer, R.drawable.trouser};
    String clothId = "",clothImg;
    JSONArray jArr;
    String userId;
    FloatingActionButton next;
    //String[] washId;

    //MainPrefManager prefmanager;
    int fragmentitemPos;

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fooddetail);

        fragmentitemPos = getIntent().getIntExtra("pos",-1);

        listView = (RecyclerView) findViewById(R.id.detailListView);
        itemImage = (ImageView) findViewById(R.id.imageViewItem);
        title = (TextView) findViewById(R.id.titleView);
        desc = (TextView) findViewById(R.id.decpView);
        layout = (RelativeLayout) findViewById(R.id.relative);
        next = (FloatingActionButton) findViewById(R.id.fab);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Schedule Your Order");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        userId = getIntent().getStringExtra("userId");

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(FooddetailActivity.this, Pick_address.class);
                i.putExtra("userId", userId);
                startActivity(i);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                finish();
            }
        });

        //prefmanager = new MainPrefManager(this, clothId);


        final int pos = getIntent().getIntExtra("pos", -1);
        clothId = getIntent().getStringExtra("jsonclothId");
        clothImg = getIntent().getStringExtra("jsonclothImg");
        final String heading = getIntent().getStringExtra("jsonclothName");

        Uri myUri = Uri.parse(clothImg);
        Glide.with(FooddetailActivity.this)
                .load(myUri)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .thumbnail(0.1f)
                .into(itemImage);

        layout.getBackground().setAlpha(150);
        title.setText(heading);

        listView.setLayoutManager(new LinearLayoutManager(FooddetailActivity.this));
        listView.addItemDecoration(new SpacesItemDecoration(1));


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("parent_cloth", clothId);
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.clothdtlsarray);//Call Service here
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(FooddetailActivity.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);

    }

    @Override
    public void finish() {
        try {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("NUM_POSITION", fragmentitemPos);


            int total = 0;
            subAdapter adapter=(subAdapter)listView.getAdapter();
            JSONArray jsonArray=adapter.getjArr();
            //JSONObject selected = new JSONObject();
            for(int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject =jsonArray.getJSONObject(i);
                total+=jsonObject.optInt("totalCount",0);
                //selected.put(jsonObject.getString("id"),jsonObject.optInt("totalCount",0));
            }
            returnIntent.putExtra("totalCount",total);
            //returnIntent.putExtra("selectedItems",selected.toString());
            setResult(RESULT_OK, returnIntent);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        super.finish();
    }
    //======

   /* @Override
    protected void onResume() {
        super.onResume();
        if(!resumeStatus.equals("")){
            HashMap<String, String> parameters = new HashMap<String, String>();
            parameters.put("parent_cloth", clothId);
            HashMap<String, String> hashMapLink = new HashMap<String, String>();
            hashMapLink.put(AllStaticVariables.link, ServerLinks.clothdtlsarray);//Call Service here
            asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(FooddetailActivity.this);
            asynkTaskForServerCommunication.execute(parameters, hashMapLink);
            resumeStatus="";
        }
    }*/

    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }

    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if (jsonObject != null) {
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status = jsonObject.getInt("status");
                if (status == 0) {
                    Toast.makeText(FooddetailActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                } else if (status == 1) {

                    jArr = jsonObject.getJSONArray("data");
                    if (jArr != null) {
                        /*washId = new String[jArr.length()];
                        for (int i = 0; i < jArr.length(); i++) {
                            jsonResponse = jArr.getJSONObject(i);
                            washId[i] = jsonResponse.getString("id");
                        }*/
                        int selectedCount = getIntent().getIntExtra("totalCount",0);
                        List<Catagories> catagoriesList = new ArrayList<Catagories>();
                        if(selectedCount>0){
                            DatabaseHandler db = new DatabaseHandler(this.getApplicationContext());
                            catagoriesList = db.getAllcatagory();
                            /*for (Catagories cat : catagoriesList){
                                inner:
                                for(int i=0; i<jArr.length(); i++){
                                    JSONObject obj = jArr.getJSONObject(i);
                                    String clothId = obj.getString("id");
                                    if(cat.getClothId().equals(clothId)){
                                        JSONArray serviceArr = obj.getJSONArray("services");
                                        boolean ifAnyChange=false;
                                        int totalWashTypes = 0;
                                        for(int j=0; j<serviceArr.length(); j++){
                                            JSONObject servObj = serviceArr.getJSONObject(j);
                                            String washId = servObj.getString("wash_id");
                                            if(cat.getWashId().equals(washId)){
                                                servObj.put("totalCount", cat.getTotal());
                                                servObj.put("totalPrice", cat.getPrice());
                                                serviceArr.put(j, servObj);
                                                ifAnyChange = true;
                                            }
                                            totalWashTypes+=cat.getTotal();
                                        }
                                        if(ifAnyChange){
                                            obj.put("services", serviceArr);
                                            obj.put("totalCount", totalWashTypes);
                                            jArr.put(i, obj);
                                            break inner;
                                        }
                                    }
                                }
                            }*/

                            for (int i = 0; i < jArr.length(); i++) {
                                JSONObject obj = jArr.getJSONObject(i);
                                String clothId = obj.getString("id");
                                JSONArray serviceArr = obj.getJSONArray("services");
                                boolean ifAnyChange = false;
                                int totalWashTypes = 0;
                                for (int j = 0; j < serviceArr.length(); j++) {
                                    JSONObject servObj = serviceArr.getJSONObject(j);
                                    String washId = servObj.getString("wash_id");
                                    for (Catagories cat : catagoriesList){
                                        if (cat.getClothId().equals(clothId) && cat.getWashId().equals(washId)) {
                                            servObj.put("totalCount", cat.getTotal());
                                            servObj.put("totalPrice", cat.getPrice());
                                            serviceArr.put(j, servObj);
                                            ifAnyChange = true;
                                            totalWashTypes += cat.getTotal();
                                        }
                                    }
                                }
                                if (ifAnyChange) {
                                    obj.put("services", serviceArr);
                                    obj.put("totalCount", totalWashTypes);
                                    jArr.put(i, obj);
                                }
                            }
                        }


                        adapter = new subAdapter(getBaseContext(), jArr);

                        listView.setAdapter(adapter);

                        listView.addOnItemTouchListener(new RecyclerTouchListener(getBaseContext(), listView, new ClickListener() {
                            @Override
                            public void onClick(View view, int position) {
                                try {
                                    JSONArray arr = ((subAdapter) listView.getAdapter()).getjArr();
                                    JSONObject object = arr.getJSONObject(position);
                                    diailogFragment fragment = new diailogFragment(object, position);
                                    fragment.show(getSupportFragmentManager(), "TAG");

                                } catch (Exception ex) {

                                }
                            }

                            @Override
                            public void onLongClick(View view, int position) {

                            }
                        }));

                    } else {
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;

            // Add top margin only for the first item to avoid double space between items
            if (parent.getChildPosition(view) == 0)
                outRect.top = space;
        }
    }

    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}

package com.superdhobi.supperdhobi;

/**
 * Created by PCIS-ANDROID on 31-03-2016.
 */
public class Category {
        private String type;
        private String sub_type;
        private int price;

        public Category(){

        }

        public Category(String i, String d, int p){
            this.type = d;
            this.sub_type = i;
            this.price = p;
        }

        public String gettype() {
            return type;
        }

        public void settype(String type) {
            this.type = type;
        }

        public String getsub_type() {
            return sub_type;
        }

        public void setsub_type(String sub_type) {
            this.sub_type = sub_type;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

    }


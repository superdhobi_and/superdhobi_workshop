package com.superdhobi.supperdhobi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
/**
 * Created by User on 5/24/2016.
 */
public class New_address_Activity extends AppCompatActivity{

    EditText tag_name,address_entry;
    Button Save;
    Spinner city,circle,center,location;
    String Tag,Address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_address_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.Ass_address_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tag_name = (EditText) findViewById(R.id.tag_address);
        address_entry = (EditText) findViewById(R.id.address_entry);
        Save = (Button) findViewById(R.id.new_address_save);
        city = (Spinner) findViewById(R.id.city_spin);
        circle = (Spinner) findViewById(R.id.circle_spin);
        center = (Spinner) findViewById(R.id.center_spin);
        location = (Spinner) findViewById(R.id.location_spin);


        String[] City = {"Bhubaneswar","Hyderabad","Bangalore"};
        String[] Circle={"XXXX","YYY","ZZZ"};
        String[] Center ={"Nayapali","CRP","Cs pur","Baramunda"};
        String[] Location = {"IRC","IRC","IRC"};
        Tag = tag_name.getText().toString();
        Address = address_entry.getText().toString();
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(New_address_Activity.this, android.R.layout.simple_spinner_item,
                City);

        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        city.setAdapter(dataAdapter1);

        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(New_address_Activity.this, android.R.layout.simple_spinner_item,
                Circle);

        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        circle.setAdapter(dataAdapter2);

        circle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(New_address_Activity.this, android.R.layout.simple_spinner_item,
                Center);

        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        center.setAdapter(dataAdapter3);

        center.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(New_address_Activity.this, android.R.layout.simple_spinner_item,
                Location);

        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        location.setAdapter(dataAdapter4);

        location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Tag.equals("")){
                    Snackbar snackbar = Snackbar.make(city, "Please Enter Full details", Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }
                else if (city.getSelectedItem().equals("Select a city")) {
                    Snackbar snackbar = Snackbar.make(city, "Please select a City", Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (circle.getSelectedItem().equals("Select a center")) {
                    Snackbar snackbar = Snackbar.make(circle, "Please select a center", Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                } else if (center.getSelectedItem().equals("Select a center")) {
                    Snackbar snackbar = Snackbar.make(center, "Please Select a center", Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                }else if (location.getSelectedItem().equals("Select Location")) {
                    Snackbar snackbar = Snackbar.make(location, "Please Select a Location", Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                }
                else {
                    Address.equals(null);
                    Intent i = new Intent(New_address_Activity.this, New_prfoile_sd.class);
                    startActivity(i);
                    finish();
                }

            }
        });
    }

}

package com.superdhobi.supperdhobi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * A simple {@link Fragment} subclass.
 */
public class History_fragment extends Fragment {

    RecyclerView history_recycle;
    MyplanGridAdapter gridAdapter;
    ProgressBar progressBar;
    String[] json_1, json_2, json_3, json_4;
    int width;
    String userId;

    public History_fragment(String userId) {
        try {
            this.userId = userId;
        } catch (Exception ex) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.history_fragment, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.offer_new_prog);

        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        int height = metrics.heightPixels;

        history_recycle = (RecyclerView) view.findViewById(R.id.history_recycle);
        new plan_listAsyntask().execute(ServerLinks.membership_user);
        return view;
    }
    private class plan_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", "SDU004512");

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject != null) {
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            JSONArray jArr = jsonObject.getJSONArray("data");
                            if (jArr != null) {
                                json_1 = new String[jArr.length()];
                                json_2 = new String[jArr.length()];
                                json_3 = new String[jArr.length()];
                                json_4 = new String[jArr.length()];

                                for (int i = 0; i < jArr.length(); i++) {
                                    jsonResponse = jArr.getJSONObject(i);
                                    json_1[i] = jsonResponse.getString("id");
                                    json_2[i] = jsonResponse.getString("plan_name");
                                    json_3[i] = jsonResponse.getString("price");
                                    json_4[i] = jsonResponse.getString("valid");

                                }
                            } else {
                                Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                            }
                            history_recycle.setLayoutManager(new LinearLayoutManager(getActivity()));
                            history_recycle.addItemDecoration(new GridSpaceItemDecoration(2, dpToPx(6), true));
                            gridAdapter = new MyplanGridAdapter(getActivity().getBaseContext(), width,json_1, json_2, json_3, json_4,userId);
                            history_recycle.setAdapter(gridAdapter);
                        } else {
                            Toast.makeText(getActivity(), "NetWork Error", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //Toast.makeText(getActivity(), "Bad Network Connection !!",Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getContext(), NointernetActivity.class);
                        startActivity(i);
                    /*getContext().overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
                    finish();*/
                    }
                }

            } catch (Exception e) {
                Log.e("String ", e.toString());
            }
            progressBar.setVisibility(View.GONE);
            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public class GridSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpaceItemDecoration(int spanCount, int spacing, boolean includeEdge) {

            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}

package com.superdhobi.supperdhobi;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.superdhobi.AdapterPackage.BucketAdapter;
import com.superdhobi.Utillls.Catagories;
import com.superdhobi.Utillls.DatabaseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;
import info.hoang8f.widget.FButton;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class diailogFragment extends SupportBlurDialogFragment {

    Dialog dialog ;
    //Prefmanager pref;
    static int total ;
    public static JSONArray childArr;
    //String[] washType,washId,washPrice;
    String clothDesc;
    int position;
    String clothId;

    public diailogFragment(JSONObject jsonObj, int position){
        try {
            childArr = jsonObj.getJSONArray("services");
            this.clothDesc=jsonObj.getString("cloth");
            this.clothId=jsonObj.getString("id");
            this.position=position;
            total=0;
        }
        catch (Exception ex){

        }
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.fragment_diailog);
        RecyclerView bucket_list_view ;

        dialog.setTitle(clothDesc);
        final int pos=position;
        FButton confirm = (FButton) dialog.findViewById(R.id.confirm);
        bucket_list_view = (RecyclerView) dialog.findViewById(R.id.bucket_recycler_view);

        bucket_list_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        //final BucketAdapter adapter = new BucketAdapter(getActivity().getBaseContext(),washId,washType,washPrice,pref_name,clothId,clothDesc);
        final BucketAdapter adapter = new BucketAdapter(getActivity().getBaseContext());
        bucket_list_view.addItemDecoration(new SpacesItemDecoration(10));
        bucket_list_view.setAdapter(adapter);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHandler db = new DatabaseHandler(getActivity());

                try {
                    db.deleteCategoryByClothId(clothId);
                    for (int i = 0; i < childArr.length(); i++) {

                        int totalCount = childArr.getJSONObject(i).optInt("totalCount");
                        if (totalCount > 0) {
                            Catagories catagorie = new Catagories();
                            catagorie.setCloth_name(clothDesc);
                            catagorie.setWash_type(childArr.getJSONObject(i).getString("wash"));
                            catagorie.setWashId(childArr.getJSONObject(i).getString("wash_id"));
                            catagorie.setTotal(totalCount);
                            catagorie.setPrice(Float.valueOf(childArr.getJSONObject(i).optString("totalPrice","0")));
                            catagorie.setClothId(clothId);
                            db.addCatagory(catagorie);
                        }
                    }
                    FooddetailActivity.listView.getAdapter().notifyItemChanged(pos, childArr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });

        return dialog;

    }
    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space+5;

            // Add top margin only for the first item to avoid double space between items
            if(parent.getChildPosition(view) == 0)
                outRect.top = space;
        }
    }
    @Override
    protected boolean isDimmingEnable() {
        // Enable or disable the dimming effect.
        // Disabled by default.
        return true;
    }
    @Override
    protected boolean isRenderScriptEnable() {
        // Enable or disable the use of RenderScript for blurring effect
        // Disabled by default.
        return true;
    }
    @Override
    protected float getDownScaleFactor() {
        return (float) 5.0;
    }
    @Override
    protected int getBlurRadius() {
        return 5;
    }


}

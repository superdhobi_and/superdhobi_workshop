package com.superdhobi.supperdhobi;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdhobi.navigation.Login_Activity;
import com.superdhobi.navigation.MainActivity;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class WelcomeActivity extends Activity {
	Boolean isInternetPresent = false;
	   ConnectionDetector cd;
	//int[] imagelist= {R.drawable.bg2_b, R.drawable.bg1, R.drawable.bg6};
	
	private static final int TIME = 5 * 1000;// 4 seconds
	private SharedPreferences shp;
	//ImageView logo;
	TextView text;
	ImageView image;
	LinearLayout lsplash;
	LinearLayout net;
	SharedPreferenceClass sharedPreferenceClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcomescreen);
		sharedPreferenceClass=new SharedPreferenceClass(this);
        cd = new ConnectionDetector(getApplicationContext());
		net=(LinearLayout)findViewById(R.id.no_int);
		isInternetPresent = cd.isConnectingToInternet();
		  
 if (isInternetPresent) {
        shp = getSharedPreferences("AuthToken", MODE_PRIVATE);

        //text=(TextView) findViewById(R.id.splash_name);
		//logo=(ImageView) findViewById(R.id.marko_logo);
		
		//lsplash=(LinearLayout) findViewById(R.id.lsplash);
		image=(ImageView)findViewById(R.id.splash_image);
	    text=(TextView) findViewById(R.id.splashtext);
		
		final Animation Rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pump_bottom);
		final Animation Rotat = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.right_in);
		final Animation Rotat1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_in);
		final Animation Rt=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoomin);
		//@SuppressWarnings("unused")
		//final Animation Rotat1 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.blink);
		
		//logo.startAnimation(Rotate);
		
		   /* Random random = new Random(System.currentTimeMillis());
		    
		    	int posOfImage = random.nextInt(imagelist.length );
		    	
		        lsplash.setBackgroundResource(imagelist[posOfImage]);*/
		   
		    	
		    
	        /*int posOfImage = random.nextInt(imagelist.length + 1);
	        
	        lsplash.setBackgroundResource(imagelist[posOfImage]);*/
	       text.startAnimation(Rotat);
	       // lsplash.startAnimation(Rt);
	       image.startAnimation(Rotat1);
			
		new Handler().postDelayed(new Runnable() { 
			@Override 
			public void run() {

				String who_login=sharedPreferenceClass.getValue_string("Who_Login");
				boolean login=sharedPreferenceClass.getValue_boolean(who_login);
                    // After 5 seconds redirect to another intent
				if(who_login.equals("USER")&&login==true){
					String mobVerify=sharedPreferenceClass.getValue_string("MobileVerify");
					/*if(mobVerify.equals("0")) {
						Intent intent4 = new Intent(WelcomeActivity.this,Otp_Activity.class);
						startActivity(intent4);
						finish();
					}else {*/
						Intent i = new Intent(getBaseContext(), MainActivity.class);
						startActivity(i);
						overridePendingTransition(R.anim.right_in, R.anim.left_out);
						finish();
					//}
				}
				else{
					Intent i = new Intent(getBaseContext(), Get_starting.class);
					startActivity(i);
					overridePendingTransition(R.anim.right_in, R.anim.left_out);
					finish();
				}
                }
			}, TIME); 
		new Handler().postDelayed(new Runnable() { 
			@Override 
			public void run() { } }, TIME); 
		}

	
	else{
	 //net.setVisibility(View.VISIBLE);
	 Intent i = new Intent(WelcomeActivity.this,NointernetActivity.class);
	 startActivity(i);
	 WelcomeActivity.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
	 finish();
	 /*Snackbar snackbar = Snackbar.make(net, "Please Check your Internet Connection", Snackbar.LENGTH_LONG);
	 snackbar.setActionTextColor(Color.RED);
	 View snackbarView = snackbar.getView();
	 snackbarView.setBackgroundColor(Color.RED);
	 TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
	 textView.setTextColor(Color.YELLOW);
	 snackbar.show();*/

		/*AlertDialog alertDialog = new AlertDialog.Builder(WelcomeActivity.this).create();
		   
	       alertDialog.setTitle("No Network!!");

	       alertDialog.setMessage("Please Connect To Internet.");
	        
	       

	              alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int which) {
	           	finish();
	           }
	       });

	       alertDialog.show();*/
	}

		printKeyHash(this);
    
    }
	public static String printKeyHash(Activity context) {
		PackageInfo packageInfo;
		String key = null;
		try {
			//getting application package name, as defined in manifest
			String packageName = context.getApplicationContext().getPackageName();

			//Retriving package info
			packageInfo = context.getPackageManager().getPackageInfo(packageName,
					PackageManager.GET_SIGNATURES);

			Log.e("Package Name=", context.getApplicationContext().getPackageName());

			for (Signature signature : packageInfo.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				key = new String(Base64.encode(md.digest(), 0));

				// String key = new String(Base64.encodeBytes(md.digest()));
				Log.e("Key Hash=", key);
			}
		} catch (PackageManager.NameNotFoundException e1) {
			Log.e("Name not found", e1.toString());
		}
		catch (NoSuchAlgorithmException e) {
			Log.e("No such an algorithm", e.toString());
		} catch (Exception e) {
			Log.e("Exception", e.toString());
		}

		return key;
	}
}


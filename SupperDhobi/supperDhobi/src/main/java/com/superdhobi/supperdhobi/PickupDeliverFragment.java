package com.superdhobi.supperdhobi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.superdhobi.AdapterPackage.CustomQuery;

/**
 * Created by User on 4/22/2016.
 */
public class PickupDeliverFragment extends Fragment {
    ListView listView;
    public static String[] Question = {"Question:","Question:","Question:"};
    public static String[] Question_query = {"How can we Pickup and Deliver the Clothes and Iron the clothes","How can we Pickup and Deliver the Clothes and Iron the clothes","How can we Pickup and Deliver the Clothes and Iron the clothes"};
    public static String[] Answer = {"Answer :","Answer :","Answer :"};
    public static String[] Answer_Query = {"By Ordering the Clothes and just click on the order we will just arrive and pick your orders","By Ordering the Clothes and just click on the order we will just arrive and pick your orders","By Ordering the Clothes and just click on the order we will just arrive and pick your orders"};

  /*  public PickupDeliverFragment() {
        // Required empty public constructor
    }*/
/*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        //return inflater.inflate(R.layout.pickup_deliver, container, false);
        View rootView = inflater.inflate(R.layout.list_view, container, false);
        listView = (ListView) rootView.findViewById(R.id.mainListView);
        final CustomQuery adapter = new CustomQuery(getActivity(),Question,Question_query,Answer,Answer_Query);
        listView.invalidate();
        listView.setAdapter(adapter);

        return rootView;


    }
}



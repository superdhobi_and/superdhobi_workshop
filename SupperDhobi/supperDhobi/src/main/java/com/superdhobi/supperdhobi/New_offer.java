package com.superdhobi.supperdhobi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.superdhobi.navigation.MainActivity;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class New_offer extends Fragment {

    RecyclerView recycleView;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
   private  RecyclerView.Adapter adapter;
    private static String LOG_TAG = "New_offer";
    //public static String[] nameArray = {"Food Panda","Free Charge","Paytm","Swiggy","Raymonds","Zomato"};
   public static String[] offerArray = {"20% off","50% Cashback","12% cashback","300Rs off","55% CashBack","200 off"};
   public static int[] drawableArray = {R.drawable.panda,R.drawable.freecharge,R.drawable.paym,R.drawable.swiggy,R.drawable.raymond,R.drawable.zomato};
    FrameLayout progress;
    private ProgressDialog pd ;
    ProgressDialog progressDialog;
    ProgressBar progressBar;

    String[] json_couponId,json_couponCode,json_couponName,json_couponExpdt,json_couponImg;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.new_offer, container, false);
        MainActivity.viewIsAtHome=false;
        MainActivity.toolbar.setTitle("SuperDhobi");
        MainActivity.toolbar.setSubtitle("Offer");
        recycleView = (RecyclerView)rootView.findViewById(R.id.my_recycler_view);
        //progress= (FrameLayout) rootView.findViewById(R.id.offer_frame_progress);
        progressBar = (ProgressBar) rootView.findViewById(R.id.offer_new_prog);

        /*Offer_recycler_adapter offer_recycler_adapter = new Offer_recycler_adapter(getActivity(),nameArray,offerArray,drawableArray);
        recycleView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        recycleView.setLayoutManager(layoutManager);

        recycleView.setAdapter(offer_recycler_adapter);
*/
        new offer_listAsyntask().execute(ServerLinks.couponarray);

        return rootView;

    }

    private class offer_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {



            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                //jsonObject.accumulate("customerid", hm_userid);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {



            JSONObject jsonResponse;

            try {
                if (!result.equals("")){

                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject!=null){
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            JSONArray jArr = jsonObject.getJSONArray("data");
                            if (jArr != null) {
                                json_couponId = new String[jArr.length()];
                                json_couponCode = new String[jArr.length()];
                                json_couponName = new String[jArr.length()];
                                json_couponExpdt = new String[jArr.length()];
                                json_couponImg = new String[jArr.length()];

                                for (int i = 0; i < jArr.length(); i++) {
                                    jsonResponse = jArr.getJSONObject(i);
                                    json_couponId[i] = jsonResponse.getString("id");
                                    json_couponCode[i] = jsonResponse.getString("coupon_code");
                                    json_couponName[i] = jsonResponse.getString("coupon_name");
                                    json_couponExpdt[i] = jsonResponse.getString("exp_date");
                                    json_couponImg[i] = jsonResponse.getString("image");
                                }
                            } else {
                                Toast.makeText(getActivity(), "No Offer For This Time", Toast.LENGTH_SHORT).show();
                            }
                            Offer_recycler_adapter offer_recycler_adapter = new Offer_recycler_adapter(getActivity(),json_couponId,json_couponName,json_couponCode,json_couponExpdt,drawableArray,json_couponImg);
                            recycleView.setHasFixedSize(true);

                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            layoutManager.scrollToPosition(0);
                            recycleView.setLayoutManager(layoutManager);

                            recycleView.setAdapter(offer_recycler_adapter);
                        } else {
                            json_couponId = new String[0];
                            json_couponCode = new String[0];
                            json_couponName = new String[0];
                            json_couponExpdt = new String[0];
                            json_couponImg = new String[0];

                            Offer_recycler_adapter offer_recycler_adapter = new Offer_recycler_adapter(getActivity(),json_couponId,json_couponName,json_couponCode,json_couponExpdt,drawableArray,json_couponImg);
                            recycleView.setHasFixedSize(true);

                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            layoutManager.scrollToPosition(0);
                            recycleView.setLayoutManager(layoutManager);

                            recycleView.setAdapter(offer_recycler_adapter);
                            Toast.makeText(getActivity(), "No Offer For This Time",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(getActivity(), "NetWork Error",Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    //Toast.makeText(getActivity(), "Bad Network Connection !!",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(getContext(),NointernetActivity.class);
                    startActivity(i);
                    /*getContext().overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
                    finish();*/
                }
            }
            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            progressBar.setVisibility(View.GONE);
            super.onPostExecute(result);
        }
    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

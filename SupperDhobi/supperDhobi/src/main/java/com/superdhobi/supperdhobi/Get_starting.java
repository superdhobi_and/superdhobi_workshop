package com.superdhobi.supperdhobi;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.superdhobi.navigation.Custom_fragment1;
import com.superdhobi.navigation.Custom_fragment2;
import com.superdhobi.navigation.Custom_fragment3;
import com.superdhobi.navigation.Custom_fragment4;
import com.superdhobi.navigation.Login_Activity;
import com.vlonjatg.android.apptourlibrary.AppTour;
import com.vlonjatg.android.apptourlibrary.MaterialSlide;

/**
 * Created by PCIS-ANDROID on 29-02-2016.
 */
public class Get_starting extends AppTour {
    @Override
    public void init(@Nullable Bundle savedInstanceState) {

        int customSlideColor1 = Color.parseColor("#3997d3");
        int customSlideColor2 = Color.parseColor("#FBB871");
        int thirdColor = Color.parseColor("#4EC4D2");
        int forthColor = Color.parseColor("#F48B8B");
        int customSlideColor = Color.parseColor("#EBA6CA");

       /* //Create pre-created fragments
        Fragment firstSlide = MaterialSlide.newInstance(R.drawable.tour_logo1, "Presentations on the go",
                "Get stuff done with or without an internet connection.", Color.WHITE, Color.WHITE);

        Fragment secondSlide = MaterialSlide.newInstance(R.drawable.tour_logo2, "Share and edit together",
                "Write on your own or invite more people to contribute.", Color.WHITE, Color.WHITE);*/

        /*Fragment thirdSlide = MaterialSlide.newInstance(R.drawable.tour_logo3, "Instance Doctors Appointment\nAt your fingertip",
                "Get instant booking of\nProfessional Physicians", Color.WHITE, Color.WHITE);

        Fragment forthSlide = MaterialSlide.newInstance(R.drawable.tour_logo4, "Instance Diagnostic Services \nAt your doorstep",
                "Experience fastest,Reliable\nDiagnostic Servives at your Doorstep", Color.WHITE, Color.WHITE);*/

        //Add slides
        /*addSlide(new CustomSlide1(), customSlideColor1);
        addSlide(new CustomSlide2(), customSlideColor2);
        addSlide(thirdSlide, thirdColor);
        addSlide(forthSlide, forthColor);*/

        //Custom slide
       // addSlide(new CustomSlide(), customSlideColor);
        addSlide(new Custom_fragment1());
        addSlide(new Custom_fragment2());
        addSlide(new Custom_fragment3());
        addSlide(new Custom_fragment4());

        //Customize tour
        setSkipButtonTextColor(Color.WHITE);
        setNextButtonColorToWhite();
        setDoneButtonTextColor(Color.WHITE);
    }

    @Override
    public void onSkipPressed() {

    }

    @Override
    public void onDonePressed() {
        Intent intent=new Intent(Get_starting.this,Login_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();
    }
}

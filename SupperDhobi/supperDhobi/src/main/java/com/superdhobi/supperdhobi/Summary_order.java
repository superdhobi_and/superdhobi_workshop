package com.superdhobi.supperdhobi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.superdhobi.AdapterPackage.Order_adapter;
import com.superdhobi.Utillls.Catagories;
import com.superdhobi.Utillls.DatabaseHandler;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Summary_order extends AppCompatActivity {

    RecyclerView oreder_list ;
    TextView sum_price;
    Order_adapter order_adapter;
    float sum ;
    Button apply_coupon,back_syummary,proceed_summary;
    TextView message_coupon;
    LinearLayout discount_coupon,grand_total;
    String couponcode;
    DatabaseHandler db = new DatabaseHandler(this);
    List<Catagories> cat;
    ArrayList<Catagories> sync;
    String userId="",pickup_date="",pickup_addrid="",pickup_slot="",delivery_date="",delivery_addrid="",delivery_slot="",
            order_price,total_price;
    String serviceCallMode;
    EditText edtCouponcode;
    TextView coupon_text,coupon_discount_Rs;
    LinearLayout coupon_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbar);

        oreder_list = (RecyclerView) findViewById(R.id.order_list);
        sum_price = (TextView) findViewById(R.id.total_price_sum);
        apply_coupon = (Button) findViewById(R.id.coupon_apply);
        back_syummary = (Button) findViewById(R.id.back_summary);
        proceed_summary = (Button) findViewById(R.id.proceed_summary);
        message_coupon = (TextView) findViewById(R.id.message_text1);
        discount_coupon = (LinearLayout) findViewById(R.id.discount_text);
        grand_total = (LinearLayout) findViewById(R.id.grand);
        edtCouponcode = (EditText) findViewById(R.id.coupon_code_edit);
        coupon_layout = (LinearLayout) findViewById(R.id.coupon_layout);
        coupon_text = (TextView) findViewById(R.id.coupon_text);
        coupon_discount_Rs  = (TextView) findViewById(R.id.coupon_discount_Rs);

        delivery_date=getIntent().getStringExtra("delivery_date");
        delivery_addrid=getIntent().getStringExtra("delivery_addrid");
        delivery_slot=getIntent().getStringExtra("delivery_slot");
        userId=getIntent().getStringExtra("userId");
        pickup_date=getIntent().getStringExtra("pickup_date");
        pickup_addrid=getIntent().getStringExtra("pickup_addrid");
        pickup_slot=getIntent().getStringExtra("pickup_slot");




        apply_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                couponcode = edtCouponcode.getText().toString();
                if (couponcode.equals("")){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Summary_order.this);
                    alertDialogBuilder.setMessage("make Sure you entered a coupon code or not?");

                    alertDialogBuilder.setNegativeButton("ok",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }else {
                    /*edtCouponcode.setError(null);
                    message_coupon.setVisibility(View.VISIBLE);
                    discount_coupon.setVisibility(View.VISIBLE);
                    coupon_layout.setVisibility(View.GONE);*/
                    saveOrder("coupon");

                }
                coupon_text.setText(couponcode);


                final ImageView cancel_promo = (ImageView) findViewById(R.id.coupon_cancel);
                cancel_promo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Summary_order.this);
                        alertDialogBuilder.setMessage("Are You sure want to make remove this coupon code?");
                        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                removeCoupon();
                                /*discount_coupon.setVisibility(View.GONE);
                                coupon_layout.setVisibility(View.VISIBLE);
                                edtCouponcode.setText("");*/
                                //Toast.makeText(Summary_order.this, " yes ", Toast.LENGTH_LONG).show();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Never", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                discount_coupon.setVisibility(View.VISIBLE);
                                dialog.dismiss();
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                });
            }
        });

        List<Catagories> contacts = db.getAllcatagory();
        Gson gson = new Gson();
        Log.d("CATAGORY", "catagory: " + gson.toJson(contacts));

        cat = new ArrayList<Catagories>();
        for (int i =0 ; i< contacts.size();i++){
            if (contacts.get(i).getTotal()==0 && contacts.get(i).getPrice()== 0.0){
            }else {
                cat.add(contacts.get(i));
            }
        }

        for (int i = 0 ; i < cat.size(); i++){
            sum = sum + cat.get(i).getPrice();
        }

        sum_price.setText(String.valueOf(sum));

        //couponCode=coupon_text.getText().toString();

        oreder_list.addItemDecoration(new SpacesItemDecoration(2));
        oreder_list.setLayoutManager(new LinearLayoutManager(this));
        order_adapter = new Order_adapter(this,cat);
        oreder_list.setAdapter(order_adapter);
        Log.e("NEW_CATAGORY", "new catagory:" + gson.toJson(cat));

        back_syummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(Summary_order.this, Sd_Order.class);
                startActivity(n);
                finish();
            }
        });

        proceed_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOrder("proceed");
            }
        });

    }
    private void saveOrder(String callType){
        total_price=sum_price.getText().toString();
        String discountPrice=coupon_discount_Rs.getText().toString();
        if(discountPrice.equals("")){
            order_price=total_price;
            couponcode="";
        }else{
            order_price=""+(Double.parseDouble(total_price) +Double.parseDouble(discountPrice));
            couponcode=coupon_text.getText().toString();
        }
        if (cat==null){
            Toast.makeText(Summary_order.this, "There is no data..", Toast.LENGTH_SHORT).show();
        } else {

            serviceCallMode=callType;
            new SubmitTask().execute((callType.equals("proceed")) ? ServerLinks.saveorder : ServerLinks.coupon);
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {

            outRect.bottom = space+3;

            // Add top margin only for the first item to avoid double space between items
            if(parent.getChildPosition(view) == 0)
                outRect.top = space;
        }
    }

    private class SubmitTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                HashMap<String, Object> dataMap = new HashMap<String, Object>();
                dataMap.put("user_id", userId);
                dataMap.put("pickup_date", pickup_date);
                dataMap.put("pickup_addrid", pickup_addrid);
                dataMap.put("pickup_slotid", pickup_slot);
                dataMap.put("deliv_date", delivery_date);
                dataMap.put("deliv_addrid", delivery_addrid);
                dataMap.put("deliv_slotid", delivery_slot);
                dataMap.put("order_price", order_price);
                dataMap.put("total_price", total_price);
                dataMap.put("coupon_code", couponcode);
                dataMap.put("clothlist", cat);

                json = new Gson().toJson(dataMap);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.optString("status");
                    if (status!=null) {
                        if(serviceCallMode.equals("proceed") && status.equals("1")) {
                            Toast.makeText(Summary_order.this, "Order Placed Successfully", Toast.LENGTH_LONG).show();
                            db.deleteAllCategory();
                            Sd_Order.resumeStatus="summary";
                            /*Intent intent= new Intent(Summary_order.this, Sd_Order.class);
                            startActivity(intent);*/
                            finish();
                        }else if(serviceCallMode.equals("coupon")) {
                            String data=jsonObject.getString("data");
                            if(status.equals("1")){
                                JSONObject obj = new JSONObject(data);
                                String discount_amount=obj.getString("discount_amount");
                                String billing_amount=obj.getString("billing_amount");
                                String total_price=obj.getString("total_price");
                                applyCoupon(Double.parseDouble(discount_amount),Double.parseDouble(billing_amount));
                            }else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(Summary_order.this);
                                builder.setMessage(data)
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                                edtCouponcode.setText("");
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }

                        }
                    } else {

					/*Toast.makeText(Addlocationnew.this,
							"Something is going wrong!!!.", Toast.LENGTH_LONG).show();*/
                    }
                }

            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progress.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }
    private void applyCoupon(double discountAmnt,double billingAmnt){

        coupon_text.setText(edtCouponcode.getText().toString());
        coupon_discount_Rs.setText(discountAmnt +"");
        sum_price.setText(billingAmnt +"");
        discount_coupon.setVisibility(View.VISIBLE);
    }
    private void removeCoupon(){
        String couponDiscount=coupon_discount_Rs.getText().toString();
        String sumPrice=sum_price.getText().toString();
        coupon_text.setText("");

        Double total=Double.parseDouble(couponDiscount) + Double.parseDouble(sumPrice);
        sum_price.setText(total +"");
        coupon_discount_Rs.setText("0");

        discount_coupon.setVisibility(View.GONE);
        coupon_layout.setVisibility(View.VISIBLE);
        edtCouponcode.setText("");
    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

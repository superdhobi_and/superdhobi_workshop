package com.superdhobi.supperdhobi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.Detailed_redycler_adpter_user;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by PCIS-ANDROID on 01-07-2016.
 */
public class Detail_member_user extends AppCompatActivity implements AsynkTaskCommunicationInterface {
    Detailed_redycler_adpter_user adapter ;
    RelativeLayout textLay,valid_lay;
    TextView plan_name , active , days, plan_dtl,term_cnd;

    RecyclerView detailed_recyle;
    int width,height;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    JSONArray jArr;
    String jsodays,jsonplanname,jsoncond;
    LinearLayout head;
    Button buy;
    ImageView arrow ;
    String userId;
    String planId;
    String valid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_member_user);

        term_cnd = (TextView) findViewById(R.id.term_desc);
        textLay = (RelativeLayout) findViewById(R.id.text_lay);
        valid_lay = (RelativeLayout) findViewById(R.id.valid_lay);
        //plan_dtl = (TextView) findViewById(R.id.detail_plan);
        plan_name = (TextView) findViewById(R.id.plan_name);
        active = (TextView) findViewById(R.id.active_);
        days = (TextView) findViewById(R.id.days_validty);
        detailed_recyle = (RecyclerView) findViewById(R.id.plan_det_recycleview);
        arrow = (ImageView) findViewById(R.id.arrow);
        head = (LinearLayout) findViewById(R.id.heading_);


        DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels;

        String colorname = getIntent().getStringExtra("colorname");

        buy = (Button) findViewById(R.id.buy);
        head.setBackgroundColor(Color.parseColor(colorname));
        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        textLay.setBackgroundColor(Color.parseColor(colorname));
        valid_lay.setBackgroundColor(Color.parseColor(colorname));
        active.setTextColor(Color.parseColor(colorname));
        buy.setBackgroundColor(Color.parseColor(colorname));

        userId=getIntent().getStringExtra("userId");
        planId=getIntent().getStringExtra("planId");
        valid=getIntent().getStringExtra("valid");

        if(valid.equals("1")){
            buy.setVisibility(View.INVISIBLE);
        }else{
            buy.setVisibility(View.VISIBLE);
        }

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new plan_buyAsyntask().execute(ServerLinks.buy);
            }
        });

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("id", planId);
        parameters.put("user_id", "SDU004512");
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.membership_userdtls);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Detail_member_user.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);
    }
    private class plan_buyAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", "SDU004512");
                jsonObject.accumulate("id_membership", planId);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject != null) {
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            Toast.makeText(Detail_member_user.this, "Membership has been added successfully", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(Detail_member_user.this, "NetWork Error", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Intent i = new Intent(Detail_member_user.this, NointernetActivity.class);
                        startActivity(i);
                    }
                }

            } catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progressBar.setVisibility(View.GONE);
            super.onPostExecute(result);
        }
    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }

    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if (jsonObject != null) {
            JSONObject jsonResponse;
            try {
                int status = jsonObject.getInt("status");
                String data=jsonObject.getString("data");
                if (status == 0) {
                    Toast.makeText(Detail_member_user.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                } else if (status == 1) {

                    JSONObject dataObject = new JSONObject(data);
                    jsodays=dataObject.getString("duration");
                    jsonplanname=dataObject.getString("plan_name");
                    jsoncond=dataObject.getString("terms");

                    jArr = dataObject.getJSONArray("plan_details");

                    if (jArr != null) {
                        adapter = new Detailed_redycler_adpter_user(getBaseContext(),jArr,width);
                        detailed_recyle.setAdapter(adapter);
                        detailed_recyle.setLayoutManager(new LinearLayoutManager(this));
                        detailed_recyle.addItemDecoration(new SpacesItemDecoration(2));
                    }
                    days.setText(jsodays + " Days");
                    plan_name.setText(jsonplanname);
                    term_cnd.setText(Html.fromHtml(jsoncond));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;
        public SpacesItemDecoration(int space) {
            this.space = space;
        }
        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {

            outRect.bottom = space+1;
            // Add top margin only for the first item to avoid double space between items
            if(parent.getChildPosition(view) == 0)
                outRect.top = space;
        }
    }
}

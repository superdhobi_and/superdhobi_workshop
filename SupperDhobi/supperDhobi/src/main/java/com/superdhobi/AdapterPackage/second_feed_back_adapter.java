package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by PCIS-ANDROID on 20-06-2016.
 */
public class second_feed_back_adapter extends RecyclerView.Adapter<second_feed_back_adapter.MyHolder> {
    JSONArray feedback;

    Context context ;
    public second_feed_back_adapter(Context baseContext,final JSONArray feedback) {
        this.context = baseContext;
        this.feedback=feedback;
    }

    @Override
    public second_feed_back_adapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.second_feed_back_adaptr_lay, parent, false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(second_feed_back_adapter.MyHolder holder, int position) {
        try {
            String feedBy,reason,feedDate;
            JSONObject jobj = feedback.getJSONObject(position);
            /*feedBy=jobj.getString("feed_by");
            reason=jobj.getString("reason");*/
            /*feedDate=jobj.getString("add_date");
            SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat DesiredFormat = new SimpleDateFormat("MM/dd/yyyy");



            Date date = sourceFormat.parse(feedDate);

            String formattedDate = DesiredFormat.format(date.getTime());*/

            holder.date.setText(jobj.getString("add_date"));
            holder.user_feedback.setText(jobj.getString("reason"));
            if(jobj.getString("feed_by").equals("user")){
                holder.feed_by.setText("");
            }else {
                holder.feed_by.setText(jobj.getString("feed_by").toUpperCase());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } /*catch (ParseException e) {
            e.printStackTrace();
        }*/

    }

    @Override
    public int getItemCount() {
        return feedback.length();
    }

    public class MyHolder extends RecyclerView.ViewHolder{

        TextView date,user_feedback,feed_by;



        public MyHolder(View itemView) {
            super(itemView);

            feed_by = (TextView) itemView.findViewById(R.id.user_feedback);
            date = (TextView) itemView.findViewById(R.id.date_feed);
            user_feedback = (TextView) itemView.findViewById(R.id.feedback_text);

        }
    }
}

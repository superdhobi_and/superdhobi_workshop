package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 25-04-2016.
 */
public class PickSlot_recycler_adapter extends RecyclerView.Adapter<PickSlot_recycler_adapter.MyViewHolder> {
    private String[] slot;


    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView time, year, genre;


        public MyViewHolder(View view1) {
            super(view1);
            time = (TextView) view1.findViewById(R.id.time1);


        }
    }

    public PickSlot_recycler_adapter( Context context,String[] slot) {
        this.slot = slot;
        this.context = context;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.custom_time_slot, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.time.setText(slot[position]);

    }
    @Override
    public int getItemCount() {
        return slot.length;
    }
}



package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdhobi.supperdhobi.Offer_details;
import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 11-04-2016.
 */
public class Offer_recycler_adapter extends RecyclerView.Adapter<Offer_recycler_adapter.MyViewHolder> {
    //nameArray,offerArray,drawableArray
    private String[] nameArray;
    private String[] offerArray;
    private int[] imageid;
    private static String LOG_TAG = "Offer_recycler_adapter";
    String offer_name,offer_descrp;
    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,offer;
        public ImageView imageView;
        public LinearLayout linearLayout;


        public MyViewHolder(View view1) {
            super(view1);
            name = (TextView) view1.findViewById(R.id.textViewName);
            offer = (TextView) view1.findViewById(R.id.textViewVersion);
            //imageView = (ImageView) view1.findViewById(R.id.imageView);
            linearLayout = (LinearLayout) view1.findViewById(R.id.tag_name);



            linearLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //offer_name = name.getText().toString();
                    //offer_descrp = offer.getText().toString();
                    Intent i = new Intent(context,Offer_details.class);
                 /*   Bitmap bitmap;
                    bitmap = BitmapFactory.decodeResource(context.getResources(), imageid[getAdapterPosition()]);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();*/

                    //i.putExtra("IMAGES",byteArray);
                    //i.putExtra("OFFER_NAME",offer_name);
                    //i.putExtra("OFFER_DESC",offer_descrp);
                    context.startActivity(i);

                }
            });
        }

    }
    public Offer_recycler_adapter(Context context, String[] nameArray, String[] offerArray, int[] imageid) {
        this.nameArray = nameArray;
        this.offerArray = offerArray;
        this.context = context;
        //this.imageid=imageid;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.custom_offer, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        //holder.name.setText(nameArray[position]);
        //holder.offer.setText(offerArray[position]);
        //holder.imageView.setImageResource(imageid[position]);

    }
    @Override
    public int getItemCount() {
        return nameArray.length;
    }


}
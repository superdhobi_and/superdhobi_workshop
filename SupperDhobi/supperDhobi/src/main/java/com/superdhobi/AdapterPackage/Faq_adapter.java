package com.superdhobi.AdapterPackage;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by PCIS-ANDROID on 01-07-2016.
 */
public class Faq_adapter extends RecyclerView.Adapter<Faq_adapter.Holder> {
    Context context ;
    JSONArray jarr ;

    public Faq_adapter(Context context, JSONArray jarr){

        this.context = context;
        this.jarr = jarr;
    }

    @Override
    public Faq_adapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.faq_layout,parent,false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Faq_adapter.Holder holder, int position) {

        try {
            JSONObject jobj = jarr.getJSONObject(position);
            holder.question.setText(jobj.getString("qn"));
            holder.answer.setText(jobj.getString("ans"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jarr.length();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView question , answer;
        public Holder(View itemView) {
            super(itemView);
            question = (TextView) itemView.findViewById(R.id.question_detail);
            answer = (TextView) itemView.findViewById(R.id.anser_detail);

        }
    }
}

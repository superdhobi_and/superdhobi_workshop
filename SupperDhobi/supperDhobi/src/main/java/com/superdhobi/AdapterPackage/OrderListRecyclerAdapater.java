package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

/**
 * Created by User on 6/10/2016.
 */
public class OrderListRecyclerAdapater extends RecyclerView.Adapter<OrderListRecyclerAdapater.MyViewHolder>{
    private String[] Cloths;
    private String[] Service;
    private String[] Quantity;
    private String[] UnitPrice;
    Context context;
    private static String LOG_TAG = "OrderListRecyclerAdapater";
    public class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout linearLayout;

        public TextView cloths,services,unitprices,quantities;

        public MyViewHolder(View itemView) {
            super(itemView);
            cloths = (TextView) itemView.findViewById(R.id.cloth);
            services = (TextView) itemView.findViewById(R.id.services);
            quantities = (TextView) itemView.findViewById(R.id.quantity);
            unitprices = (TextView) itemView.findViewById(R.id.price_list);
        }
    }


    public OrderListRecyclerAdapater(Context context, String[] json_cloth, String[] json_services, String[] json_quantity, String[] json_unitprice) {

        this.Cloths=json_cloth;
        this.Service=json_services;
        this.Quantity=json_quantity;
        this.UnitPrice=json_unitprice;
        this.context=context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.custom_order_details_adapter, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.cloths.setText(Cloths[position]);
        holder.services.setText(Service[position]);
        holder.quantities.setText(Quantity[position]);
        holder.unitprices.setText(UnitPrice[position]);
     /*   if (position%2==0){
            holder.linearLayout.setBackgroundColor(Color.WHITE);
        }else {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#D8D8D8"));
        }
*/
    }



    @Override
    public int getItemCount() {
        return Cloths.length;
    }


}

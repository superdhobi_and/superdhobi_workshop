package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

import java.util.ArrayList;

/**
 * Created by PCIS-ANDROID on 16-04-2016.
 */
public class Choose_date_adapter extends RecyclerView.Adapter<Choose_date_adapter.MyViewHolder> {

    private ArrayList<String> dateArray;
    private ArrayList<String> dayArray;
    private ArrayList<String> monthArray;
    private ArrayList<String> week_dayArray;


    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date, month, day;


        public MyViewHolder(View view1) {
            super(view1);
            date = (TextView) view1.findViewById(R.id.date1);
            month = (TextView) view1.findViewById(R.id.month1);
            day = (TextView) view1.findViewById(R.id.day1);


        }
    }

    public Choose_date_adapter( Context context,ArrayList<String> dateArray,ArrayList<String> dayArray,ArrayList<String> monthArray) {
        this.dateArray = dateArray;
        this.dayArray = dayArray;
        this.monthArray = monthArray;
        this.context = context;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.choose_date_custom, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.date.setText(dateArray.get(position));
        holder.month.setText(dateArray.get(position));
        holder.day.setText(dateArray.get(position));

    }
    @Override
    public int getItemCount() {
        return dateArray.size();
    }
}


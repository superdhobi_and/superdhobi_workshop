package com.superdhobi.AdapterPackage;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by PCIS-ANDROID on 28-06-2016.
 */
public class Detailed_redycler_adpter extends RecyclerView.Adapter<Detailed_redycler_adpter.Myholder> {
      Context context ;
      JSONArray  jarr ;
      int width ;

    public Detailed_redycler_adpter(Context context , JSONArray jarr , int width){

        this.context = context;
        this.jarr = jarr ;
        this.width = width ;
    }

    @Override
    public Detailed_redycler_adpter.Myholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.detailed_adapter_plan, parent, false);
        Myholder holder = new Myholder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(Detailed_redycler_adpter.Myholder holder, int position) {




        JSONObject jobj = null;
        try {
            jobj = jarr.getJSONObject(position);
            holder.men.setText(jobj.getString("category"));
            holder.service.setText(jobj.getString("service"));
            holder.quantity.setText(jobj.getString("quantity"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return jarr.length();
    }
    public class Myholder extends RecyclerView.ViewHolder{
        TextView men , service , quantity ;
        public Myholder(View itemView) {
            super(itemView);
            men = (TextView) itemView.findViewById(R.id.plan_catagory);
            service = (TextView) itemView.findViewById(R.id.service_plan);
            quantity = (TextView) itemView.findViewById(R.id.qunatity);
        }
    }
}

package com.superdhobi.AdapterPackage;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.Utillls.ProfileAddress;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.Addlocationnew;
import com.superdhobi.supperdhobi.New_prfoile_sd;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import info.hoang8f.widget.FButton;

/**
 * Created by PCIS-ANDROID on 11-04-2016.
 */
public class Address_recycler_adapter extends RecyclerView.Adapter<Address_recycler_adapter.MyViewHolder> {
    //private String[] addr_tag;
    private List<ProfileAddress> profAddr;
    private String hm_userid;
    private SharedPreferences shp;

    //private String[]  location ,city , circle , centre ;
    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tag, year, genre,city,centre,location,landmark;
        ImageView imgdel,imgedit ;
        RelativeLayout adress_layout ;
        public MyViewHolder(View view1) {
            super(view1);
            tag = (TextView) view1.findViewById(R.id.tag_name);
            city = (TextView) view1.findViewById(R.id.city_add);
            centre = (TextView) view1.findViewById(R.id.center_add);
            location = (TextView) view1.findViewById(R.id.location_add);
            landmark = (TextView) view1.findViewById(R.id.landmark_add);
            imgdel=(ImageView)view1.findViewById(R.id.del_addr);
            imgedit=(ImageView)view1.findViewById(R.id.edit_addr);
        }
    }

    public Address_recycler_adapter( Context context,List<ProfileAddress> profAddr) {
        this.profAddr = profAddr;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.adress_custom_grid, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProfileAddress address=profAddr.get(position);
        holder.tag.setText(address.getAdd_heading());
        holder.city.setText(address.getCity_name());
        holder.centre.setText(address.getCircle_name());
        holder.location.setText(address.getLocation_name());
        holder.landmark.setText(address.getLandmark());
        holder.tag.setTag(address);
        holder.imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create_dialogue(position);
                Intent i = new Intent(context, Addlocationnew.class);
                i.putExtra("address", address);
                ((New_prfoile_sd) context).startActivityForResult(i, 1);
            }
        });

        holder.imgdel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Do You Want to Close this App ?");
                String positiveText = "OK";
                builder.setPositiveButton(positiveText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new DeleteAddress(address).execute(ServerLinks.deleteaddr);
                            }
                        });

                String negativeText = context.getString(android.R.string.cancel);

                builder.setNegativeButton(negativeText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });


    }
    @Override
    public int getItemCount() {
        return profAddr.size();
    }

    private class DeleteAddress extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        private ProfileAddress address;
        DeleteAddress (ProfileAddress address){
            this.address=address;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {
            shp = context.getSharedPreferences("AuthToken",
                    Context.MODE_PRIVATE);
            //hm_userid = shp.getString("USER_UID", null);

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                //jsonObject.accumulate("user_id",hm_userid);
                jsonObject.accumulate("addr_id",address.getAddr_id());
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {


            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.optString("status");
                    if (status.equals("1")) {
                        Toast.makeText(context,"Address deleted successfully", Toast.LENGTH_LONG).show();
                        profAddr.remove(address);
                        Address_recycler_adapter.this.notifyDataSetChanged();

                    } else {

                        Toast.makeText(context,"Try Again", Toast.LENGTH_LONG).show();
                    }
                }

            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progress.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

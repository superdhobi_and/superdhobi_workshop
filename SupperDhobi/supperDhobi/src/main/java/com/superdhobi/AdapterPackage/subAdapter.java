package com.superdhobi.AdapterPackage;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.superdhobi.supperdhobi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by abhis on 4/12/2016.
 */
public class subAdapter extends RecyclerView.Adapter<subAdapter.subListHolder> {
    Context context;
    //Prefmanager pref;
    public JSONArray getjArr() {
        return jArr;
    }

    public void setjArr(JSONArray jArr) {
        this.jArr = jArr;
    }

    JSONArray jArr;
    public subAdapter(Context baseContext, JSONArray jArr) {
        this.context = baseContext;
        this.jArr=jArr;
    }
    @Override
    public subListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.each_sub_layout,parent,false);
        subListHolder holder = new subListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(subListHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        try {
            if(payloads!= null && payloads.size() > 0) {
                int total=0;
                JSONArray jsonArray = (JSONArray) payloads.get(0);
                for(int i=0;i<jsonArray.length();i++){
                    total+=jsonArray.getJSONObject(i).optInt("totalCount", 0);
                }
                JSONObject jsonObject =jArr.getJSONObject(position);
                jsonObject.put("services",jsonArray);
                jsonObject.put("totalCount",total);
                jArr.put(position,jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        onBindViewHolder(holder, position);
    }

    @Override
    public void onBindViewHolder(subListHolder holder, int position) {
        try {

            JSONObject jobj = jArr.getJSONObject(position);

            String itemName=jobj.getString("id");
            holder.title.setText(jobj.getString("cloth"));
            holder.layout.getBackground().setAlpha(70);

            String image=jobj.getString("image");
            Uri myUri = Uri.parse(image);
            Glide.with(context)
                    .load(myUri)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .crossFade()
                    .thumbnail(0.1f)
                    .into(holder.subImg);

            //pref = new Prefmanager(context, itemName);

            int total = jobj.optInt("totalCount");

            if (total != 0) {
                holder.sub_notif_lay.setVisibility(View.VISIBLE);
                holder.no_of_items.setText(String.valueOf(total));
            } else {
                holder.sub_notif_lay.setVisibility(View.INVISIBLE);
            }
        }catch(Exception e){

        }
    }

    @Override
    public int getItemCount() {
        return jArr.length();
    }
    public class subListHolder extends RecyclerView.ViewHolder{

        ImageView subImg;
        TextView title,desc,no_of_items;
        RelativeLayout layout,sub_notif_lay;
        public subListHolder(View itemView) {
            super(itemView);
            subImg = (ImageView) itemView.findViewById(R.id.imageView2);
            title = (TextView) itemView.findViewById(R.id.textView);
            desc = (TextView) itemView.findViewById(R.id.textView2);
            layout = (RelativeLayout) itemView.findViewById(R.id.swipeRELATIVE);
            no_of_items = (TextView) itemView.findViewById(R.id.sub_notification);
            sub_notif_lay = (RelativeLayout) itemView.findViewById(R.id.sub_notif_lay);
        }
    }
}

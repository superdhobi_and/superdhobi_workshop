package com.superdhobi.AdapterPackage;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

/**
 * Created by User on 4/22/2016.
 */
public class CustomQuery extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] Question;
    private final String[] Question_Query;
    private final String[] Answer;
    private final String[] Answer_Query;

    public CustomQuery(Activity context, String[] question, String[] question_query, String[] answer, String[] answer_Query) {
        super(context, R.layout.custom_query_list,question);
        this.Question = question;
        this.Question_Query = question_query;
        this.Answer = answer;
        this.Answer_Query = answer_Query;
        this.context = context;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_query_list, null, true);
        final TextView question = (TextView) rowView.findViewById(R.id.question);
        final TextView question_query = (TextView) rowView.findViewById(R.id.question_query);
        final TextView answer = (TextView) rowView.findViewById(R.id.answer);
        final TextView answer_query = (TextView) rowView.findViewById(R.id.answeer_query);

        question.setText(Question[position]);
        question_query.setText(Question_Query[position]);
        answer.setText(Answer[position]);
        answer_query.setText(Answer_Query[position]);

        return rowView;

    }
}

package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;
import com.superdhobi.supperdhobi.diailogFragment;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Abhishek on 4/19/2016.
 */
public class BucketAdapter extends RecyclerView.Adapter<BucketAdapter.BucketHolder> {
    Context baseContext;
    /*int count=0;
    float price=0;*/

    public BucketAdapter(Context baseContext) {
        this.baseContext = baseContext;
    }

    @Override
    public BucketHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(baseContext).inflate(R.layout.bucket_layout, parent, false);
        BucketHolder holder = new BucketHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final BucketHolder holder, final int position) {

        try {
            JSONObject obj = diailogFragment.childArr.getJSONObject(position);
            String washType = obj.getString("wash");
            final String washPrice = obj.getString("price");

            int count = obj.optInt("totalCount", 0);
            String price = obj.optString("totalPrice", "0");

            holder.options.setText(washType);
            holder.rates.setTag(count);
            holder.notif_lay.setTag(price);
            if (count > 0) {
                holder.notif_lay.setVisibility(View.VISIBLE);
                holder.notification.setText(String.valueOf(count));
                holder.rates.setText(String.valueOf(price));
            } else {
                holder.notification.setText("0");
                holder.rates.setText("0");
                holder.notif_lay.setVisibility(View.INVISIBLE);
            }

            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int count = (Integer)holder.rates.getTag();
                    float price = Float.valueOf((String)holder.notif_lay.getTag());

                    count++;
                    price += Float.valueOf(washPrice);

                    holder.notif_lay.setVisibility(View.VISIBLE);
                    holder.notification.setText(String.valueOf(count));
                    holder.rates.setText(String.valueOf(price));
                    holder.rates.setTag(count);
                    holder.notif_lay.setTag(String.valueOf(price));

                    try {
                        JSONObject obj = diailogFragment.childArr.getJSONObject(position);
                        obj.put("totalCount",String.valueOf(count));
                        obj.put("totalPrice", String.valueOf(price));
                        diailogFragment.childArr.put(position, obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            holder.remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int count = (Integer)holder.rates.getTag();
                    float price = Float.valueOf((String)holder.notif_lay.getTag());

                    if(count>0) {
                        count--;
                        price -= Float.valueOf(washPrice);
                    }

                    holder.notification.setText(String.valueOf(count));
                    holder.rates.setText(String.valueOf(price));
                    holder.rates.setTag(count);
                    holder.notif_lay.setTag(String.valueOf(price));
                    if(count>0)
                        holder.notif_lay.setVisibility(View.VISIBLE);
                    else
                        holder.notif_lay.setVisibility(View.INVISIBLE);

                    try {
                        JSONObject obj = diailogFragment.childArr.getJSONObject(position);
                        obj.put("totalCount",String.valueOf(count));
                        obj.put("totalPrice", String.valueOf(price));
                        diailogFragment.childArr.put(position, obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return diailogFragment.childArr.length();
    }

    public class BucketHolder extends RecyclerView.ViewHolder {

        RelativeLayout notif_lay;
        TextView options, rates, notification;
        ImageView remove, add;

        public BucketHolder(View itemView) {
            super(itemView);

            options = (TextView) itemView.findViewById(R.id.textView3);
            rates = (TextView) itemView.findViewById(R.id.textView4);
            notification = (TextView) itemView.findViewById(R.id.notification);
            remove = (ImageView) itemView.findViewById(R.id.remove1);
            add = (ImageView) itemView.findViewById(R.id.add1);
            notif_lay = (RelativeLayout) itemView.findViewById(R.id.notif_lay);
        }
    }
}

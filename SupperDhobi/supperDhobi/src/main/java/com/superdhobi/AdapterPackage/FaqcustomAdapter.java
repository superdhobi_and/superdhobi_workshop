package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

import java.util.ArrayList;

/**
 * Created by Santanu on 24-11-2015.
 */
public class FaqcustomAdapter extends BaseAdapter{
  // private final Activity context;
    Context context;
    ArrayList<String> fid;
    ArrayList<String> question;
    ArrayList<String> answer;
    LayoutInflater layoutInflater;

    public FaqcustomAdapter(Context context, ArrayList<String> fid, ArrayList<String> question, ArrayList<String> answer) {

        this.context = context;
        this.fid = fid;
        this.question = question;
        this.answer = answer;
        layoutInflater = LayoutInflater.from(context);}


    @Override
    public int getCount() {
        return question.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       // LayoutInflater inflater=context.getLayoutInflater();
        //
        convertView= layoutInflater.inflate(R.layout.faqdetails, null, true);
        TextView qstn= (TextView) convertView.findViewById(R.id.qst);
        TextView ans= (TextView) convertView.findViewById(R.id.ans);
        qstn.setText(question.get(position));
        ans.setText(answer.get(position));
        return convertView;
    }
}

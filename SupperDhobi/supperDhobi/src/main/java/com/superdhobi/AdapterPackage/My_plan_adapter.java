package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 27-04-2016.
 */
public class My_plan_adapter extends RecyclerView.Adapter<My_plan_adapter.MyViewHolder> {

    private String[] my_plan_list;
    private String[] dayArray;
    private String[] monthArray;
    private String[] week_dayArray;


    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView plan_name, year, genre;


        public MyViewHolder(View view1) {
            super(view1);
            plan_name = (TextView) view1.findViewById(R.id.custom_plan);


        }
    }

    public My_plan_adapter(Context context, String[] my_plan_list) {
        this.my_plan_list = my_plan_list;
        this.context = context;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.custom_membership_myplan, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.plan_name.setText(my_plan_list[position]);

    }
    @Override
    public int getItemCount() {
        return my_plan_list.length;
    }
}



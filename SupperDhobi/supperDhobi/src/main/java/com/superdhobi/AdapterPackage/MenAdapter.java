package com.superdhobi.AdapterPackage;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.superdhobi.Utillls.Prefmanager;
import com.superdhobi.supperdhobi.R;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by abhis on 4/11/2016.
 */
public class MenAdapter extends RecyclerView.Adapter<MenAdapter.ListHolder> {
    Context context;
    JSONArray childArray;
    Prefmanager pref;
    String id_demo="";

    public MenAdapter(Context baseContext, JSONArray childArray) {
        this.context = baseContext;
        this.childArray= childArray;
    }
    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.eachitem,parent,false);
        ListHolder holder = new ListHolder(view);
        return holder;
    }
    @Override
    public void onBindViewHolder(ListHolder holder, int position) {
        try{
            JSONObject jobj=childArray.getJSONObject(position);

            holder.title.setText(jobj.getString("cloth"));
            holder.desc.setText(jobj.getString("description"));
            id_demo = jobj.getString("id");
            int total = jobj.optInt("totalCount",0);
            if(total>0){
                holder.sub_notif_lay.setVisibility(View.VISIBLE);
                holder.no_of_items.setText(""+total);
            }
            else {
                holder.sub_notif_lay.setVisibility(View.INVISIBLE);
                holder.no_of_items.setText("0");
            }
            Uri myUri = Uri.parse(jobj.getString("img"));
            Glide.with(context)
                    .load(myUri)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .crossFade()
                    .thumbnail(0.1f)
                    .into(holder.imgview);

        }catch (Exception ex){

        }
    }

    @Override
    public int getItemCount() {
        return childArray.length();
    }

    public class ListHolder extends RecyclerView.ViewHolder{

        ImageView imgview;
        TextView title, desc,no_of_items;
        RelativeLayout layout,sub_notif_lay;

        public ListHolder(View itemView) {
            super(itemView);
            layout = (RelativeLayout) itemView.findViewById(R.id.relative);

            imgview = (ImageView) itemView.findViewById(R.id.imageView);
            title = (TextView) itemView.findViewById(R.id.textView);
            desc = (TextView) itemView.findViewById(R.id.textView2);

            sub_notif_lay = (RelativeLayout) itemView.findViewById(R.id.sub_notif_lay);
            no_of_items = (TextView) itemView.findViewById(R.id.sub_notification);
        }
    }
}

package com.superdhobi.AdapterPackage;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

/**
 * Created by Santanu on 06-10-2015.
 */
public class All_plan_order_adapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] plan_orderid;
    private final String[] plan_quant;
    public All_plan_order_adapter(Activity context, String[] plan_orderid,
                            String[] plan_quant) {
        super(context, R.layout.membership_order_listcustom, plan_orderid);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.plan_orderid = plan_orderid;
        this.plan_quant = plan_quant;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.membership_order_listcustom, null,
                true);
        TextView orderno = (TextView) rowView.findViewById(R.id.custom_plan_orderno);
        TextView order_quan = (TextView) rowView.findViewById(R.id.custom_plan_quantity);


        orderno.setText(plan_orderid[position]);
        order_quan.setText(plan_quant[position]);

        return rowView;
    }

}

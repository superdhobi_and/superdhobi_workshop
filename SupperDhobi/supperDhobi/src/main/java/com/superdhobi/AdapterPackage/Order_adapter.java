package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdhobi.Utillls.Catagories;
import com.superdhobi.supperdhobi.R;
import com.superdhobi.supperdhobi.Summary_order;

import java.util.List;

/**
 * Created by PCIS-ANDROID on 02-05-2016.
 */
public class Order_adapter extends RecyclerView.Adapter<Order_adapter.listHolder> {
    Context context;
    List<Catagories> cat;
    public Order_adapter(Context context, List<Catagories> cat) {
        this.context =  context;
        this.cat=cat;
    }

    @Override
    public Order_adapter.listHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.each_summary_layout,parent,false);
        listHolder holder = new listHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(Order_adapter.listHolder holder, int position) {

        holder.cloth.setText(cat.get(position).getCloth_name());
        holder.wash.setText(cat.get(position).getWash_type());
        holder.total.setText(String.valueOf(cat.get(position).getTotal()));
        holder.price.setText(String.valueOf(cat.get(position).getPrice()));

        if (position%2==0){
            holder.linearLayout.setBackgroundColor(Color.WHITE);
        }else {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#D8D8D8"));
        }
    }

    @Override
    public int getItemCount() {
        return cat.size();
    }

    public class listHolder extends RecyclerView.ViewHolder{

        TextView cloth,wash,total,price;
        LinearLayout linearLayout ;

        public listHolder(View itemView) {
            super(itemView);
            cloth = (TextView) itemView.findViewById(R.id.cloth_type);
            wash = (TextView) itemView.findViewById(R.id.wash_type);
            total = (TextView) itemView.findViewById(R.id.total_count);
            price = (TextView) itemView.findViewById(R.id.total_price);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout);

        }
    }
}

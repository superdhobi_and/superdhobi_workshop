package com.superdhobi.AdapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 28-04-2016.
 */
public class Membership_order_adapter extends RecyclerView.Adapter<Membership_order_adapter.MyViewHolder> {

    private String[] my_order_list;
    private String[] dayArray;
    private String[] monthArray;
    private String[] week_dayArray;


    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView orderid, year, genre;


        public MyViewHolder(View view1) {
            super(view1);
            orderid = (TextView) view1.findViewById(R.id.orderid);


        }
    }

    public Membership_order_adapter(Context context, String[] my_order_list) {
        this.my_order_list = my_order_list;
        this.context = context;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.custom_membership_order1, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.orderid.setText(my_order_list[position]);

    }
    @Override
    public int getItemCount() {
        return my_order_list.length;
    }
}




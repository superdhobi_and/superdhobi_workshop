package com.superdhobi.AdapterPackage;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by PCIS-ANDROID on 13-04-2016.
 */
public class Txn_recycler_adapter extends RecyclerView.Adapter<Txn_recycler_adapter.MyViewHolder> {
    JSONArray reason_tag;

    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView reason, year, order,rupee,walletstatus;

        public MyViewHolder(View view1) {
            super(view1);
            reason = (TextView) view1.findViewById(R.id.reason);
            year = (TextView) view1.findViewById(R.id.txtDate);
            order= (TextView) view1.findViewById(R.id.order_id_code);
            rupee= (TextView) view1.findViewById(R.id.text_ruppee);
            walletstatus=(TextView) view1.findViewById(R.id.txtStatus);
        }
    }

    public Txn_recycler_adapter(Context context,final JSONArray reason_tag) {
        this.reason_tag = reason_tag;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.wallet_custom, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        try {
            JSONObject jobj = reason_tag.getJSONObject(position);
            String jsontype=jobj.getString("type");
            String type;
            if(jsontype.equals("1")){

                type="- Rs."+jobj.getString("amount");
                holder.walletstatus.setText("Dr");
            }else{
                type="+ Rs."+jobj.getString("amount");
                holder.walletstatus.setText("Cr");
            }

            String new_order;
            String order_id=jobj.getString("transaction_id");
            new_order="Txn Id "+order_id;

            holder.year.setText(jobj.getString("add_date"));
            holder.order.setText(new_order);
            holder.rupee.setText(type);
            holder.reason.setText(jobj.getString("remark"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public int getItemCount() {
        return reason_tag.length();
    }
}


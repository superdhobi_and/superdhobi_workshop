package com.superdhobi.AdapterPackage;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.New_order_details;
import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 18-04-2016.
 */
public class Order_history_adapter extends RecyclerView.Adapter<Order_history_adapter.MyViewHolder> {
    private String[] orderid;
    private String[] address;
    private int [] orderStatus;
    private int[]orderImage;

    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView o_id, o_status,o_address;
        public ImageView o_pic;
        public LinearLayout order_lay;

        public MyViewHolder(View view1) {
            super(view1);
            o_id = (TextView) view1.findViewById(R.id.orderid);
            o_status = (TextView) view1.findViewById(R.id.order_status);
            o_pic = (ImageView) view1.findViewById(R.id.order_pic);
            order_lay = (LinearLayout) view1.findViewById(R.id.order_lay);
            o_address = (TextView) view1.findViewById(R.id.addressid);
        }
    }

    public Order_history_adapter(Context context,String[] orderid,int[] orderStatus,int[]orderImage,String[] address) {
        this.orderid = orderid;
        this.orderStatus = orderStatus;
        this.orderImage = orderImage;
        this.context = context;
        this.address=address;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.custom_order_history, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.o_id.setText(orderid[position]);
        holder.o_status.setText(ServerLinks.statusArray[orderStatus[position]]);
        holder.o_pic.setImageResource(orderImage[position]);
        holder.o_address.setText(address[position]);

        holder.order_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(context, New_order_details.class);
                i.putExtra("jsonorderId",orderid[position]);
                i.putExtra("jsonorderStatus",ServerLinks.statusArray[orderStatus[position]]);
                context.startActivity(i);
            }
        });
        /*//pending (fa1630)
        picked up (0770ff)
        proccessing (ff9000)
        delivered (02c479)*/

    }
    @Override
    public int getItemCount() {
        return orderid.length;
    }
}


package com.superdhobi.AdapterPackage;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

/**
 * Created by Santanu on 01-10-2015.
 */
public class All_plan_adapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] plan_list;
    //private final String[] plan_list_id;
    // String[] plan_list_id
    public All_plan_adapter(Activity context, String[] plan_list)
    {
        super(context, R.layout.custom_membership_plan, plan_list);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.plan_list = plan_list;
        //this.plan_list_id = plan_list_id;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_membership_plan, null,true);
        TextView plan = (TextView) rowView.findViewById(R.id.custom_plan);


        plan.setText(plan_list[position]);

        return rowView;
    }

    }


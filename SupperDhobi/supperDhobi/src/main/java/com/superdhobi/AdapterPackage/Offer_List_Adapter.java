package com.superdhobi.AdapterPackage;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;

/**
 * Created by Santanu on 30-11-2015.
 */
public class Offer_List_Adapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] promo_name;
    private final String[] eligible;
    private final String[] wash_name;
    private final String[] promo_code;
    private final String[] discount;
    private final String[] max_discunt_allow;
    private final String[] start_date;
    private final String[] end_date;
    private final String[] promo_desc;
    public Offer_List_Adapter(Activity context, String[] promo_name, String[] eligible, String[] wash_name,
                              String[] promo_code, String[] discount, String[] max_discunt_allow,
                              String[] start_date, String[] end_date, String[] promo_desc) {


        super(context, R.layout.offer_custom, promo_name);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.promo_name = promo_name;
        this.eligible = eligible;
        this.wash_name = wash_name;
        this.promo_code = promo_code;
        this.discount = discount;
        this.max_discunt_allow = max_discunt_allow;
        this.start_date = start_date;
        this.end_date = end_date;
        this.promo_desc = promo_desc;
        //this.imageId = imageId;

    }

    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.offer_custom, null, true);
        TextView offer_name= (TextView) rowView.findViewById(R.id.coupon_name);
        TextView offer_code= (TextView) rowView.findViewById(R.id.coupon_code);
        TextView offer_desc= (TextView) rowView.findViewById(R.id.coupon_desc);
        TextView offer_valid= (TextView) rowView.findViewById(R.id.coupon_expiery);

        offer_name.setText(promo_name[position]);
        offer_code.setText(promo_code[position]);
        offer_desc.setText(promo_desc[position]);
        offer_valid.setText("*Valid Upto "+end_date[position]);

        return rowView;
    }
}


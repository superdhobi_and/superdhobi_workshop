package com.superdhobi.navigation;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.FaqcustomAdapter;
import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Santanu on 24-11-2015.
 */
public class Faq extends Activity implements AsynkTaskCommunicationInterface {
    ListView faq_list,faq_list1,faq_list2;
    Button pnd,lnd,pay;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    ArrayList<String> question, answer,fid,question1, answer1,fid1,question2, answer2,fid2;
    LinearLayout back_faq;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq);
        faq_list= (ListView) findViewById(R.id.faqlist);
        faq_list1= (ListView) findViewById(R.id.faqlist1);
        faq_list2= (ListView) findViewById(R.id.faqlist2);
        pnd= (Button) findViewById(R.id.pnd);
        lnd= (Button) findViewById(R.id.lnd);
        pay= (Button) findViewById(R.id.pay);
        //back_faq= (LinearLayout) findViewById(R.id.back_faq);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        pnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pnd.setBackgroundColor(getResources().getColor(R.color.app_color));
                lnd.setBackgroundColor(getResources().getColor(R.color.text));
                pay.setBackgroundColor(getResources().getColor(R.color.text));
                faq_list.setVisibility(View.VISIBLE);
                faq_list1.setVisibility(View.GONE);
                faq_list2.setVisibility(View.GONE);
            }
        });

        lnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pnd.setBackgroundColor(getResources().getColor(R.color.text));
                lnd.setBackgroundColor(getResources().getColor(R.color.app_color));
                pay.setBackgroundColor(getResources().getColor(R.color.text));
                faq_list.setVisibility(View.GONE);
                faq_list1.setVisibility(View.VISIBLE);
                faq_list2.setVisibility(View.GONE);
            }
        });

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pnd.setBackgroundColor(getResources().getColor(R.color.text));
                lnd.setBackgroundColor(getResources().getColor(R.color.text));
                pay.setBackgroundColor(getResources().getColor(R.color.app_color));
                faq_list.setVisibility(View.GONE);
                faq_list1.setVisibility(View.GONE);
                faq_list2.setVisibility(View.VISIBLE);
            }
        });

       /* back_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("customerid", "123456");

        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.faq);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Faq.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);

    }


    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if(jsonObject!=null){
            //{"status":1}
            question = new ArrayList<String>();
            answer = new ArrayList<String>();
            fid = new ArrayList<String>();
            question1 = new ArrayList<String>();
            answer1 = new ArrayList<String>();
            fid1 = new ArrayList<String>();
            question2 = new ArrayList<String>();
            answer2 = new ArrayList<String>();
            fid2 = new ArrayList<String>();
            try {

                int status=jsonObject.getInt("status");
                if(status==0)
                {
                    Toast.makeText(Faq.this, "Please Try Again. Connection slow.", Toast.LENGTH_SHORT).show();

                }
                else if(status==1){
                    //String desc=jsonObject.getString("description");
                    //tnc.setText(desc);
                    JSONArray jArr1 = jsonObject.getJSONArray("pick");
                    JSONArray jArr2 = jsonObject.getJSONArray("lundary");
                    JSONArray jArr3 = jsonObject.getJSONArray("payment");
                    if (jArr1 != null) {

                        for (int i = 0; i < jArr1.length(); i++) {
                            JSONObject  jsonResponse = jArr1.getJSONObject(i);

                            String qstn = jsonResponse.optString("question").toString();
                            String ans = jsonResponse.optString("answer").toString();
                            String id = jsonResponse.optString("id").toString();

                            fid.add(id);
                            question.add(qstn);
                            answer.add(ans);
                        }
                        FaqcustomAdapter faqcustomAdapter=new FaqcustomAdapter(Faq.this,fid,question,answer);
                        faq_list.setAdapter(faqcustomAdapter);
                    }
                    if (jArr2 != null) {

                        for (int i = 0; i < jArr2.length(); i++) {
                            JSONObject  jsonResponse2 = jArr2.getJSONObject(i);

                            String qstn = jsonResponse2.optString("question").toString();
                            String ans = jsonResponse2.optString("answer").toString();
                            String id = jsonResponse2.optString("id").toString();

                            fid1.add(id);
                            question1.add(qstn);
                            answer1.add(ans);
                        }
                        FaqcustomAdapter faqcustomAdapter1=new FaqcustomAdapter(Faq.this,fid1,question1,answer1);
                        faq_list1.setAdapter(faqcustomAdapter1);
                    }
                    if (jArr3 != null) {

                        for (int i = 0; i < jArr3.length(); i++) {
                            JSONObject  jsonResponse3 = jArr3.getJSONObject(i);

                            String qstn = jsonResponse3.optString("question").toString();
                            String ans = jsonResponse3.optString("answer").toString();
                            String id = jsonResponse3.optString("id").toString();

                            fid2.add(id);
                            question2.add(qstn);
                            answer2.add(ans);
                        }
                        FaqcustomAdapter faqcustomAdapter2=new FaqcustomAdapter(Faq.this,fid2,question2,answer2);
                        faq_list2.setAdapter(faqcustomAdapter2);
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }
}


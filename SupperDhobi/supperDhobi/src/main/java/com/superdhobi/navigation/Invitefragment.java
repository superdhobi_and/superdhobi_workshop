package com.superdhobi.navigation;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.supperdhobi.R;

public class Invitefragment extends Fragment{
	
	private SharedPreferences shp;
	Button invite;
	static MainActivity inviteContext;
	TextView refer_code;
	static  String _refercode;
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;




	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		shp=inviteContext.getSharedPreferences("AuthToken",Context.MODE_PRIVATE);
		SharedPreferences.Editor shpEdit = shp.edit();
    	shpEdit.putString("start_home", "from_invite");
    	shpEdit.commit();
		
		View rootView=inflater.inflate(R.layout.new_refer, container, false);
		shp = getActivity().getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);
		_refercode = shp.getString("USER_REFERAL", null);
		invite= (Button) rootView.findViewById(R.id.invite);
		refer_code= (TextView) rootView.findViewById(R.id.refer_code);
		if (_refercode != null){
			refer_code.setText(_refercode);

		}
		else{
			refer_code.setText("------");
		}

		invite.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (_refercode != null){
					Intent sendIntent = new Intent();
					sendIntent.setAction(Intent.ACTION_SEND);
					sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.superdhobi.supperdhobi" + "\n" + "Use this Referal code: " + _refercode);
					sendIntent.setType("text/plain");
					startActivity(sendIntent);

				}
				else{
					Toast.makeText(getActivity(),"You have no refferal code. Please Contact to superDhobi service care.",Toast.LENGTH_LONG).show();
				}

			}
		});
		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		inviteContext=(MainActivity) activity;
		super.onAttach(activity);
	}
	    public void onBackPressed()
	{

		/*mFragmentManager = getFragmentManager();
		mFragmentTransaction = mFragmentManager.beginTransaction();
		mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();*/
		/*FragmentManager fm = getFragmentManager();
		    FragmentTransaction ft = fm.beginTransaction();
		    Homefragment llf = new Homefragment();
		    ft.replace(R.id.containerView, llf);
		    ft.commit();*/
	}

}

package com.superdhobi.navigation;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.Offer_List_Adapter;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Santanu on 30-11-2015.
 */
public class Offer_fragment extends Fragment {

    private SharedPreferences shp;
    ListView offerlist;
    static MainActivity offerContext;
    FrameLayout progress;
  //  ImageView o_home,o_myorder,o_profile,o_price;
    static String hm_userid;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    SharedPreferenceClass sharedPreferenceClass;

    String[] promo_name,eligible,wash_name,promo_code,discount,max_discunt_allow,start_date,end_date,promo_desc;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        shp=offerContext.getSharedPreferences("AuthToken", Context.MODE_PRIVATE);
        SharedPreferences.Editor shpEdit = shp.edit();
        shpEdit.putString("start_home", "from_offer");
        shpEdit.commit();

        View rootView=inflater.inflate(R.layout.offernew, container, false);
        shp = getActivity().getSharedPreferences("AuthToken",
                Context.MODE_PRIVATE);
        hm_userid = shp.getString("USER_UID", null);
        progress= (FrameLayout) rootView.findViewById(R.id.frame_progress);
        offerlist= (ListView) rootView.findViewById(R.id.offer_list);

        /*o_home= (ImageView) rootView.findViewById(R.id.o_home);
        o_myorder= (ImageView) rootView.findViewById(R.id.o_morder);
        o_profile= (ImageView) rootView.findViewById(R.id.o_profile);
        o_price= (ImageView) rootView.findViewById(R.id.o_price);*/

        /*o_home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Fragment fragment = new Homefragment();
                FragmentManager fragmentManager = getActivity().getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        o_myorder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Fragment fragment = new Myorderfragment();
                FragmentManager fragmentManager = getActivity().getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        o_profile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Fragment fragment = new Pricingfragment();
                FragmentManager fragmentManager = getActivity().getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        o_price.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Fragment fragment = new Profilefragment();
                FragmentManager fragmentManager = getActivity().getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/
        new offer_listAsyntask().execute(ServerLinks.promooffer);
       /* HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("customerid", hm_userid);

        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.promooffer);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(getActivity());
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);*/


        return rootView;
    }

    private class offer_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("customerid", hm_userid);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.setVisibility(View.GONE);

            JSONObject jsonResponse;

            try {
                if (!result.equals("")){

                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject!=null){
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    JSONArray jArr = jsonObject.getJSONArray("promo");
                    if (jArr != null) {
                        //promo_name,eligible,wash_name,promo_code,discount,max_discunt_allow,start_date,end_date,promo_desc;
                        /* {"status": "1",
                        "promo":
                        [
                        {"promo_name": "hhjhj",
                                "eligible": "Any Time Order",
                                "wash_name": "Normal wash",
                                "promo_code": "123",
                                "discount": "22",
                                "max_discunt_allow": "200",
                                "start_date": "2015-11-21",
                                "end_date": "2015-11-22",
                                "promo_desc": "rfdfdfdfd"
                        }
                        ]}*/
                        promo_name = new String[jArr.length()];
                        eligible = new String[jArr.length()];
                        wash_name = new String[jArr.length()];
                        promo_code = new String[jArr.length()];
                        discount = new String[jArr.length()];
                        max_discunt_allow = new String[jArr.length()];
                        start_date = new String[jArr.length()];
                        end_date = new String[jArr.length()];
                        promo_desc = new String[jArr.length()];

                        for (int i = 0; i < jArr.length(); i++) {
                            jsonResponse = jArr.getJSONObject(i);

                            promo_name[i] = jsonResponse.getString("promo_name");
                            eligible[i] = jsonResponse.getString("eligible");
                            wash_name[i] = jsonResponse.getString("wash_name");
                            promo_code[i] = jsonResponse.getString("promo_code");
                            discount[i] = jsonResponse.getString("discount");
                            max_discunt_allow[i] = jsonResponse.getString("max_discunt_allow");
                            start_date[i] = jsonResponse.getString("start_date");
                            end_date[i] = jsonResponse.getString("end_date");
                            promo_desc[i] = jsonResponse.getString("promo_desc");


                        }
                    } else {
                        Toast.makeText(getActivity(), "No Offer For This Time",
                                Toast.LENGTH_SHORT).show();
                    }
                    Offer_List_Adapter offer_list_adapter = new Offer_List_Adapter(getActivity(), promo_name, eligible, wash_name,
                            promo_code, discount, max_discunt_allow, start_date, end_date, promo_desc);

                    offerlist.setAdapter(offer_list_adapter);
                } else {
                    promo_name = new String[0];
                    eligible = new String[0];
                    wash_name = new String[0];
                    promo_code = new String[0];
                    discount = new String[0];
                    max_discunt_allow = new String[0];
                    start_date = new String[0];
                    end_date = new String[0];
                    promo_desc = new String[0];

                    Offer_List_Adapter offer_list_adapter = new Offer_List_Adapter(getActivity(), promo_name, eligible, wash_name,
                            promo_code, discount, max_discunt_allow, start_date, end_date, promo_desc);

                    offerlist.setAdapter(offer_list_adapter);
                    Toast.makeText(getActivity(), "No Offer For This Time",
                            Toast.LENGTH_SHORT).show();
                }


            }
                else {
                    Toast.makeText(getActivity(), "NetWork Error",
                            Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(getActivity(), "Bad Network Connection !!",
                        Toast.LENGTH_SHORT).show();
            }
        }
            catch (Exception e) {
                Log.e("String ", e.toString());
            }
        }
    }


    /*@Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        JSONObject  jsonResponse;
        if(jsonObject!=null){
            try {
                int status=jsonObject.getInt("status");
                if (status==1) {
                    JSONArray jArr = jsonObject.getJSONArray("promo");
                    if (jArr != null) {
                        //promo_name,eligible,wash_name,promo_code,discount,max_discunt_allow,start_date,end_date,promo_desc;
                        *//* {"status": "1",
                        "promo":
                        [
                        {"promo_name": "hhjhj",
                                "eligible": "Any Time Order",
                                "wash_name": "Normal wash",
                                "promo_code": "123",
                                "discount": "22",
                                "max_discunt_allow": "200",
                                "start_date": "2015-11-21",
                                "end_date": "2015-11-22",
                                "promo_desc": "rfdfdfdfd"
                        }
                        ]}*//*
                        promo_name = new String[jArr.length()];
                        eligible = new String[jArr.length()];
                        wash_name = new String[jArr.length()];
                        promo_code = new String[jArr.length()];
                        discount = new String[jArr.length()];
                        max_discunt_allow = new String[jArr.length()];
                        start_date = new String[jArr.length()];
                        end_date = new String[jArr.length()];
                        promo_desc = new String[jArr.length()];

                        for (int i = 0; i < jArr.length(); i++) {
                            jsonResponse = jArr.getJSONObject(i);

                            promo_name[i] = jsonResponse.getString("promo_name");
                            eligible[i] = jsonResponse.getString("eligible");
                            wash_name[i] = jsonResponse.getString("wash_name");
                            promo_code[i] = jsonResponse.getString("promo_code");
                            discount[i] = jsonResponse.getString("discount");
                            max_discunt_allow[i] = jsonResponse.getString("max_discunt_allow");
                            start_date[i] = jsonResponse.getString("start_date");
                            end_date[i] = jsonResponse.getString("end_date");
                            promo_desc[i] = jsonResponse.getString("promo_desc");


                        }
                    } else {
                        Toast.makeText(getActivity(), "No Offer For This Time",
                                Toast.LENGTH_SHORT).show();
                    }
                    Offer_List_Adapter offer_list_adapter = new Offer_List_Adapter(getActivity(),promo_name,eligible,wash_name,
                            promo_code,discount,max_discunt_allow,start_date,end_date,promo_desc);

                    offerlist.setAdapter(offer_list_adapter);
                }
                else{
                    promo_name = new String[0];
                    eligible = new String[0];
                    wash_name = new String[0];
                    promo_code = new String[0];
                    discount = new String[0];
                    max_discunt_allow = new String[0];
                    start_date = new String[0];
                    end_date = new String[0];
                    promo_desc = new String[0];

                    Offer_List_Adapter offer_list_adapter = new Offer_List_Adapter(getActivity(),promo_name,eligible,wash_name,
                            promo_code,discount,max_discunt_allow,start_date,end_date,promo_desc);

                    offerlist.setAdapter(offer_list_adapter);
                    Toast.makeText(getActivity(), "No Offer For This Time",
                            Toast.LENGTH_SHORT).show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else {
            Toast.makeText(getActivity(), "NetWork Error",
                    Toast.LENGTH_SHORT).show();
        }

    }*/
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
    @Override
    public void onAttach(Activity activity) {
        offerContext=(MainActivity) activity;
        super.onAttach(activity);
    }
    public void onBackPressed()
    {

       /* mFragmentManager = getFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();*/

        /*FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Homefragment llf = new Homefragment();
        ft.replace(R.id.containerView, llf);
        ft.commit();*/
    }

}

package com.superdhobi.navigation.webservices;

public class ServerLinks {
    public static  String URL="http://superdhobi.in/index.php/api/";
	//pcinfosolutions.
	public static String user_social_Signup = URL+"user/insert_user_social/format/json";

	public static String user_Signup = URL+"user/insert_user/format/json";
	public static String user_invoice = URL+"user/invoiceorder/format/json";
	// {"username":"prand","email":"pra@g.com","mobile":"943977707","password":"123455"}
	// user/get_user/format/json

	//public static String user_Login = URL+"user/get_user/format/json";

	public static String slot_time = URL+"order/shift_display/format/json";

	// order/shift_display/format/json
	// for adres spiner
	//public static String choose_city = URL+"locationapi/city_display/format/json";

	//public static String choose_center = URL+"locationapi/center_display/format/json";

	//public static String choose_circle = URL+"locationapi/circle_display/format/json";

	//public static String choose_location = URL+"locationapi/location_display/format/json";
	// for adres summit
	//public static String submit_address = URL+"locationapi/address_insert/format/json";
	// for delet adress
	public static String delete_address =URL+"locationapi/address_remove/format/json";
	// for adress view
	public static String view_tag = URL+"locationapi/address_display/format/json";
	public static String tag_address = URL+"locationapi/address_display/format/json";
	public static String view_pricelist = URL+"order/order_price/format/json";
	public static String delivery_login = URL+"employeeapi/get_users/format/json";
	// / for order
	public static String wash_order = URL+"order/order_request/format/json";
	// myorder
	public static String my_order = URL+"order/my_order/format/json";
	public static String my_order_details = URL+"order/my_order_detail/format/json";
	// eployee_circleid
	public static String employee_circle_id = URL+"employeeapi/getemployee_circle_detail/format/json";
	public static String employee_location_id = URL+"employeeapi/getemployee_location_detail/format/json";
	// all order request
	public static String all_order_rq = URL+"employeeapi/getemployee_allorder/format/json";
	// order request per loction nd circle
	public static String filter_order_rq = URL+"employeeapi/getpick_location_order/format/json";
	public static String view_order_rq = URL+"employeeapi/view_address/format/json";
	public static String del_frm_wrhs = URL+"employeeapi/getemployee_alldeli/format/json";
	// for today work
	public static String today_work = URL+"employeeapi/getemployee_allorder_deliver/format/json";
	// confirm from warehouse
	public static String confirm_del = URL+"employeeapi/conform_delivery_order/format/json";
	// filter daily ord loc n circle wise
	public static String filter_today_order = URL+"employeeapi/gettoday_location_pick_deli_order/format/json";
    //pickup order details
	public static String pickup_view =URL+"employeeapi/view_address/format/json";
	// take order wash type and cloth
	public static String take_order_type=URL+"order/take_cloth_wash/format/json";
	// for fetching price
	public static String single_price=URL+"order/price_cloth_wash/format/json";
	// for submit order
	public static String submit_order=URL+"order/order_pick_detail/format/json";
	//for otp
	public static String otp=URL+"user/otp_verf/format/json";
	// for fb mobile otp
	public static String f_otp=URL+"user/mobile_social/format/json";
	//forgot pwd
	public static String frgt_pwd = URL+"user/forgot/format/json";
	// for update profile
	public static String update_profile = URL+"user/update_detail/format/json";
	//for feedback
	public static String feedback = URL+"user/feedback/format/json";
	//for number of plans
	public static String num_plan=URL+"order/membership_plan/format/json";
	//for buying membership plan
	public static String buy_membership = URL+"user/member_req/format/json";
	//for profile pic update

	public static String profile_pic=URL+"user/upload_filer/format/json";
	//public static String profile_pic="tryapi/preetish1/format/json";
	//my_plan
	public static String my_plan=URL+"order/member_ship/format/json";
	//membershiporder details
	public static String my_plan_order=URL+"order/membership_orderhistory/format/json";
	// for promo code
	public static String promo_code_=URL+"order/promo_entry/format/json";
	// for check membership
	public static String check_member=URL+"order/find_membership_order/format/json";
// for tnc
	public static String tnc=URL+"user/get_terms/format/json";
	// for faq
	public static String faq=URL+"user/get_faq/format/json";
	//for showing promocode offer
	public static String promooffer=URL+"user/get_promo/format/json";

	//walet amount
	public static String walet_amount=URL+"user/get_wallet/format/json";
	//membership cancel
	public static String member_cancel=URL+"user/member_reqcancel/format/json";


	//New WebService Call Here
	//Created By Smaranika

	/*public static String user_Login ="http://192.168.0.113/superdhobi_beta/Mobileapi/login";
	public static String tabarray = "http://192.168.0.113/superdhobi_beta/Mobileapi/parentclothlist";
	public static  String clothdtlsarray="http://192.168.0.113/superdhobi_beta/Mobileapi/servicedetails";
	public static String couponarray="http://192.168.0.113/superdhobi_beta/Mobileapi/coupondata";
	public static String couponDetails="http://192.168.0.113/superdhobi_beta/Mobileapi/couponDetail";
	public static String choose_city = "http://192.168.0.113/superdhobi_beta/Mobileapi/locationlist";
	public static String choose_center = "http://192.168.0.113/superdhobi_beta/Mobileapi/locationlist";
	public static String choose_circle = "http://192.168.0.113/superdhobi_beta/Mobileapi/locationlist";
	public static String choose_location = "http://192.168.0.113/superdhobi_beta/Mobileapi/locationlist";
	public static String user_address="http://192.168.0.113/superdhobi_beta/Mobileapi/getUserAddress";
	public static String user_profile="http://192.168.0.113/superdhobi_beta/Mobileapi/getUserProfile";
	public static String order_data="http://192.168.0.113/superdhobi_beta/Mobileapi/getOrderData";
	public static String order_dtls="http://192.168.0.113/superdhobi_beta/Mobileapi/getOrderDetail";
	public static String submit_address="http://192.168.0.113/superdhobi_beta/Mobileapi/saveAddress";
	public static String edit_profile="http://192.168.0.113/superdhobi_beta/Mobileapi/editProfile";
	public static String lastorder="http://192.168.0.113/superdhobi_beta/Mobileapi/getlastOrder";
	public static  String saveorder="http://192.168.0.113/superdhobi_beta/Mobileapi/saveOrder";
	public static String timeslot="http://192.168.0.113/superdhobi_beta/Mobileapi/getTimeslot";
	public static String termsnconditn="http://192.168.0.113/superdhobi_beta/Mobileapi/tearmscondition";
	public static String addrdtls="http://192.168.0.113/superdhobi_beta/Mobileapi/getAddressDetail";
	public static String walletdtls="http://192.168.0.113/superdhobi_beta/Mobileapi/walletdetails";//input user_id
	public static String coupon="";*/
	public static String authLogin="http://139.59.12.252/superdhobi/Mobileapi/saveUser";
	public static String user_Login ="http://139.59.12.252/superdhobi/Mobileapi/login";
	public static String tabarray = "http://139.59.12.252/superdhobi/Mobileapi/parentclothlist";
	public static  String clothdtlsarray="http://139.59.12.252/superdhobi/Mobileapi/servicedetails";
	public static String couponarray="http://139.59.12.252/superdhobi/Mobileapi/coupondata";
	public static String couponDetails="http://139.59.12.252/superdhobi/mobileapi/couponDetail";
	public static String choose_city = "http://139.59.12.252/superdhobi/mobileapi/locationlist";
	public static String choose_center = "http://139.59.12.252/superdhobi/mobileapi/locationlist";
	public static String choose_circle = "http://139.59.12.252/superdhobi/mobileapi/locationlist";
	public static String choose_location = "http://139.59.12.252/superdhobi/mobileapi/locationlist";
	public static String user_address="http://139.59.12.252/superdhobi/mobileapi/getUserAddress";
	public static String user_profile="http://139.59.12.252/superdhobi/mobileapi/getUserProfile";
	public static String order_data="http://139.59.12.252/superdhobi/mobileapi/getOrderData";
	public static String order_dtls="http://139.59.12.252/superdhobi/mobileapi/getOrderDetail";
	public static String submit_address="http://139.59.12.252/superdhobi/mobileapi/saveAddress";
	public static String edit_profile="http://139.59.12.252/superdhobi/mobileapi/editProfile";
	public static String lastorder="http://139.59.12.252/superdhobi/mobileapi/getlastOrder";
	public static  String saveorder="http://139.59.12.252/superdhobi/Mobileapi/saveOrder";
	public static String timeslot="http://139.59.12.252/superdhobi/mobileapi/getTimeslot";
	public static String termsnconditn="http://139.59.12.252/superdhobi/Mobileapi/tearmscondition";
	public static String addrdtls="http://139.59.12.252/superdhobi/mobileapi/getAddressDetail";
	public static String walletdtls="http://139.59.12.252/superdhobi/Mobileapi/walletdetails";//input user_id
	public static String coupon="http://139.59.12.252/superdhobi/mobileapi/couponValidate";
	public static String saveUser="http://139.59.12.252/superdhobi/mobileapi/saveUser";
	public static String deleteaddr="http://139.59.12.252/superdhobi/mobileapi/delete_address/format/json";
	public static String sendotp="http://139.59.12.252/superdhobi/mobileapi/sendotp";
	public static String otpverify="http://139.59.12.252/superdhobi/mobileapi/checkotp";
	public static String feedback_data="http://139.59.12.252/superdhobi/mobileapi/getfeedbackorder";
	public static String check_feedback="http://139.59.12.252/superdhobi/mobileapi/checkfeedback";
	public static String submit_feedback="http://139.59.12.252/superdhobi/mobileapi/savefeedback";
	public static String membership_all="http://139.59.12.252/superdhobi/mobileapi/userMembership";
	public static String membership_dtls="http://139.59.12.252/superdhobi/mobileapi/membershipDetail/format/json";
	public static String membership_user="http://139.59.12.252/superdhobi/mobileapi/userplans/format/json";
	public static String membership_userdtls="http://139.59.12.252/superdhobi/mobileapi/userplanDetail/format/json";
	public static String faqlist="http://139.59.12.252/superdhobi/mobileapi/faqlisting/format/json";
	public static String buy="http://139.59.12.252/superdhobi/mobileapi/membershipBuy/format/json";



	public static final String[] statusArray={"","Order Placed","Order Confirmed","Ready To Pickup","Out For Pickup","Picked up","Pickup Reschedule","Intransit To Warehouse","Order Processing","Ready To deliver","Out for Deliver","Delivered","Deliver Reschedule","Cancel"};
}

package com.superdhobi.navigation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.superdhobi.supperdhobi.R;
import com.superdhobi.navigation.webservices.ServerLinks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

public class Forgot_pw extends Activity {

	ImageView back_frg;
	Button submit;
	EditText email;
	static String em;
	FrameLayout progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forget_pw);
		//ButterKnife.bind(this);
		progress = (FrameLayout)findViewById(R.id.frame_progress);
		email = (EditText) findViewById(R.id.et_email);
		//back_frg = (ImageView) findViewById(R.id.back);
		submit = (Button) findViewById(R.id.ok_forget);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		/*back_frg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent bck = new Intent(Forgot_pw.this, Login_Activity.class);
				startActivity(bck);
				overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();
			}
		});*/
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				em = email.getText().toString();
				if (em.equalsIgnoreCase("")) {
					Toast.makeText(Forgot_pw.this, " Enter your email ",
							Toast.LENGTH_LONG).show();
				} else {
					new Forget_pwd_HttpAsyncTask()
							.execute(ServerLinks.frgt_pwd);
				}

			}
		});
	}

	private class Forget_pwd_HttpAsyncTask extends
			AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);

		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("email", em);
				

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {

			progress.setVisibility(View.GONE);
			try {
				JSONObject jObject = new JSONObject(result);
				int status = jObject.getInt("status");
				if (status == 0) {
					Toast.makeText(Forgot_pw.this, "Email Id not exist!", Toast.LENGTH_SHORT).show();
				} else if (status == 1) {
					
					Toast.makeText(Forgot_pw.this, "You Pasword has been sent to your emailId. Please check your email.", Toast.LENGTH_SHORT).show();
					Intent p= new Intent(Forgot_pw.this,Login_Activity.class);
					startActivity(p);
					overridePendingTransition(R.anim.left_in, R.anim.right_out);
					finish();
				}
			} catch (Exception e) {

			}

			// progressDialog.dismiss();

			super.onPostExecute(result);

		}

	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}
	@Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        backButtonHandler();
        return;
    }
 
    public void backButtonHandler() {
    	Intent bc= new Intent(Forgot_pw.this,Login_Activity.class);
    	startActivity(bc);
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
    	finish();
    }

}

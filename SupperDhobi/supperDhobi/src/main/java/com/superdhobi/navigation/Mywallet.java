package com.superdhobi.navigation;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Santanu on 16-11-2015.
 */

public class Mywallet extends Fragment {
    static MainActivity walletContext;
    private SharedPreferences shp;
    TextView waletamount,waletstatus;
    static  String walet_amount;
    static  String custmer_id;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        shp=walletContext.getSharedPreferences("AuthToken",Context.MODE_PRIVATE);
        SharedPreferences.Editor shpEdit = shp.edit();
        shpEdit.putString("start_home", "from_wallet");
        shpEdit.commit();
        View rootView=inflater.inflate(R.layout.walet, container, false);
        shp = getActivity().getSharedPreferences("AuthToken",
                Context.MODE_PRIVATE);
        //progress = (FrameLayout) rootView.findViewById(R.id.frame_progress);
        custmer_id = shp.getString("USER_UID", null);
        waletamount= (TextView) rootView.findViewById(R.id.walet_amnt);
        waletstatus= (TextView) rootView.findViewById(R.id.walet_status);
        new getWallet_amountAsyncTask().execute(ServerLinks.walet_amount);
        return rootView;
    }

    private class getWallet_amountAsyncTask extends AsyncTask<String, Void, String> {
       // ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progress.setVisibility(View.VISIBLE);
            getActivity().getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("customerid", custmer_id);
                //jsonObject.accumulate("membership_plan_ids", m_id);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //progress.setVisibility(View.GONE);
            getActivity().getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            JSONObject jsonResponse;
//{"status":"1","order_id":[{"order":"0","quantityPlaced":"0"}]}
            try {
                JSONObject jsonObject = new JSONObject(result);

                String status = jsonObject.optString("status");

                if (status.equals("1")) {
                    //planlist.setVisibility(View.VISIBLE);
                    walet_amount=jsonObject.optString("wallet_amount");
                    waletamount.setText("Rs."+walet_amount);
                    /*Toast.makeText(getActivity(), "success",
                            Toast.LENGTH_SHORT).show();*/
                    SharedPreferences.Editor shpEdit = shp.edit();
                    shpEdit.putString("WALLET", walet_amount);

                    shpEdit.commit();
                }
                else if (status.equals("0")){
                    Toast.makeText(getActivity(), "please try again !!",
                            Toast.LENGTH_SHORT).show();
                }
                /*else {
                    Toast.makeText(getActivity(), "Incomplete data!!",
                            Toast.LENGTH_SHORT).show();
                }*/
            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }


        }

    }


    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    @Override
    public void onAttach(Activity activity) {
        walletContext = (MainActivity) activity;
        super.onAttach(activity);
    }

    public void onBackPressed() {

       /* mFragmentManager = getFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();*//**/
       /* FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Homefragment llf = new Homefragment();
        ft.replace(R.id.containerView, llf);
        ft.commit();*/
    }
}
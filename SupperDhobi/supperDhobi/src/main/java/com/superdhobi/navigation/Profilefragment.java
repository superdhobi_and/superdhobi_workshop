package com.superdhobi.navigation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinkCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;
import com.superdhobi.supperdhobi.Addlocationnew;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class Profilefragment extends Fragment {
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;
	Toolbar toolbar;
	ImageView profile;
	TextView image_edit;
	static String userid, emailid, name, mobile, usr_nmbr;
	TextView email, uname, phn_no, edit,path_txt;
	EditText newemail, newuname, newph;
	static String crnt_email, crnt_name, crnt_mobile, new_email, new_name,
			new_phn, otp_number;
	static String f_name, f_email, f_mbl, f_uid;
	LinearLayout main, otp;
	EditText et_nmbr, otp_num;
	Button submit,cancel, otp_submit;
	TextView adres1, adres2, adres3, adres4, adres5;
	ArrayList<String> addressid, tag_addressname;
	static String add1, add2, add3, add4, add5;
	static String p_userid;
	static String _addr;
	static String _id;
	static String u_userid;
	TextView tv_show_tag, tv_show_plot, tv_show_landmark, tv_show_location,
			tv_show_circle, tv_show_center, tv_show_city;
	FrameLayout progress, progress1, progress_ad, otp_layout;
	private SharedPreferences shp;
	static  String otp_dlg;
	static MainActivity profileContext;
	String imageFilePath = "";
	AsynkTaskForServerCommunication asynkTaskForServerCommunication=null;
	String file;
	int REQUEST_CAMERA=1;
	int SELECT_FILE=2;
	public final static int REQUEST_CODE = 2;

	ImageView p_home,p_myorder,p_profile,p_price;
SharedPreferenceClass sharedPreferenceClass;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		shp = profileContext.getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor shpEdit = shp.edit();
		shpEdit.putString("start_home", "from_profile");
		shpEdit.commit();



		View rootView = inflater.inflate(R.layout.profile, container, false);

		sharedPreferenceClass=new SharedPreferenceClass(getActivity());
		/*sharedPreferenceClass.setValue_string("NAV", "FRAG");
		sharedPreferenceClass.setValue_boolean("FRAG", false);*/
		shp = getActivity().getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);
		progress = (FrameLayout) rootView.findViewById(R.id.frame_progress);

		/*hpEdit.putString("AuthKey", Email);
		shpEdit.putString("USER_UID", u_userid);
		shpEdit.putString("UNAME", u_name);
		shpEdit.putString("MBL_NUM", u_mobile);*/
		((MainActivity) getActivity()).toolbar.setSubtitle("Profile");
		p_userid = shp.getString("USER_UID", null);
		userid = shp.getString("UID", null);
		emailid = shp.getString("AuthKey", null);
		name = shp.getString("UNAME", null);
		usr_nmbr = shp.getString("MBL_NUM", null);

		image_edit= (TextView) rootView.findViewById(R.id.image_edit);
		main = (LinearLayout) rootView.findViewById(R.id.main_pro);
		email = (TextView) rootView.findViewById(R.id.tv_email);
		uname = (TextView) rootView.findViewById(R.id.tv_name);
		phn_no = (TextView) rootView.findViewById(R.id.tv_phn);
		edit = (TextView) rootView.findViewById(R.id.edit_text);
		newuname = (EditText) rootView.findViewById(R.id.et_name);
		newph = (EditText) rootView.findViewById(R.id.et_phone);
		submit = (Button) rootView.findViewById(R.id.p_btn_sv);
		cancel = (Button) rootView.findViewById(R.id.p_btn_cncl);
		otp_layout = (FrameLayout) rootView.findViewById(R.id.otp_frame_pro);
		otp_num = (EditText) rootView.findViewById(R.id.et_otp_pro);
		otp_submit = (Button) rootView.findViewById(R.id.ok_otp_pro);

		profile = (ImageView) rootView.findViewById(R.id.iv_profile);
		adres1 = (TextView) rootView.findViewById(R.id.profile_add1);
		adres2 = (TextView) rootView.findViewById(R.id.profile_add2);
		adres3 = (TextView) rootView.findViewById(R.id.profile_add3);
		adres4 = (TextView) rootView.findViewById(R.id.profile_add4);
		adres5 = (TextView) rootView.findViewById(R.id.profile_add5);

		p_home= (ImageView) rootView.findViewById(R.id.p_home);
		p_myorder= (ImageView) rootView.findViewById(R.id.p_morder);
		p_profile= (ImageView) rootView.findViewById(R.id.p_profile);
		p_price= (ImageView) rootView.findViewById(R.id.p_price);

		String path=sharedPreferenceClass.getValue_string("User_Image");
if(path.equals("")){
	displayProfileImage();

}
		else{
	Bitmap bm = BitmapFactory.decodeFile(path);

	profile.setImageBitmap(bm);
		}

		new display_tagAsyncTask().execute(ServerLinks.view_tag);

		/*Uri myUri = Uri.parse(imageFilePath);
		Picasso.with(getActivity())
				.load(myUri)
				.placeholder(R.drawable.loading3)
				.error(R.drawable.profile_blank)
				.into(profile);*/
		email.setText(emailid);
		uname.setText(name);
		mobile = phn_no.getText().toString();
		phn_no.setText(usr_nmbr);

		adres1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_addr = adres1.getText().toString();
				int pos = tag_addressname.indexOf(_addr);
				_id = addressid.get(pos);
				Dialog_class();

			}

		});
		adres2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_addr = adres2.getText().toString();
				int pos = tag_addressname.indexOf(_addr);
				_id = addressid.get(pos);
				Dialog_class();

			}

		});

		adres3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_addr = adres3.getText().toString();
				int pos = tag_addressname.indexOf(_addr);
				_id = addressid.get(pos);
				Dialog_class();

			}

		});
		adres4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_addr = adres4.getText().toString();
				int pos = tag_addressname.indexOf(_addr);
				_id = addressid.get(pos);
				Dialog_class();

			}

		});
		adres5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_addr = adres5.getText().toString();
				int pos = tag_addressname.indexOf(_addr);
				_id = addressid.get(pos);
				Dialog_class();

			}

		});
		edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				crnt_name = uname.getText().toString().trim();
				crnt_mobile = phn_no.getText().toString().trim();
				uname.setVisibility(View.GONE);
				newuname.setVisibility(View.VISIBLE);
				newuname.setText(crnt_name);
				newuname.requestFocus();
				// newuname.setFocusableInTouchMode(true);
				phn_no.setVisibility(View.GONE);
				newph.setVisibility(View.VISIBLE);
				newph.setText(crnt_mobile);
				// newph.setFocusableInTouchMode(true);
				submit.setVisibility(View.VISIBLE);
				cancel.setVisibility(View.VISIBLE);

			}
		});
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new_name = newuname.getText().toString().trim();
				new_phn = newph.getText().toString().trim();
				// for profile updating
				new update_profileAsyncTask()
						.execute(ServerLinks.update_profile);

			}
		});
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				uname.setVisibility(View.VISIBLE);
				newuname.setVisibility(View.GONE);

				// newuname.setFocusableInTouchMode(true);
				phn_no.setVisibility(View.VISIBLE);
				newph.setVisibility(View.GONE);

				// newph.setFocusableInTouchMode(true);
				submit.setVisibility(View.GONE);
				cancel.setVisibility(View.GONE);


			}
		});
		/*profile.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				final Dialog dialog = new Dialog(getActivity());
				dialog.setContentView(R.layout.custom_dialog_myphoto);
				dialog.setTitle("Upload Photos");

				path_txt = (TextView) dialog.findViewById(R.id.path_txt);

				Button browse_btn = (Button) dialog.findViewById(R.id.browse_btn);
				Button save_btn = (Button) dialog.findViewById(R.id.btn_ok);
				browse_btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent();
						intent.setType("image*//*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(Intent.createChooser(intent, "Complete action using"), 1);


					}
				});
				save_btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
				dialog.show();
				return false;
			}
		});*/
		image_edit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final Dialog dialog = new Dialog(getActivity());
				LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View view = inflater.inflate(R.layout.custom_dialog_myphoto, null, false);
				//dialog.setContentView(R.layout.custom_dialog_myphoto);

				dialog.setContentView(view);

				dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				dialog.setTitle("Upload Photos");

				path_txt = (TextView) dialog.findViewById(R.id.path_txt);

				Button browse_btn = (Button) dialog.findViewById(R.id.browse_btn);
				Button save_btn = (Button) dialog.findViewById(R.id.btn_ok);
				browse_btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						final CharSequence[] items = { "Take Photo", "Choose from Library",
								"Cancel" };

						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setTitle("Add Photo!");
						builder.setItems(items, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int item) {
								if (items[item].equals("Take Photo")) {
									Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
									startActivityForResult(intent, REQUEST_CAMERA);
								} else if (items[item].equals("Choose from Library")) {
									Intent intent = new Intent(
											Intent.ACTION_PICK,
											MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
									intent.setType("image/*");
									startActivityForResult(
											Intent.createChooser(intent, "Select File"),
											SELECT_FILE);
								} else if (items[item].equals("Cancel")) {
									dialog.dismiss();
								}
							}
						});
						builder.show();


					}
				});
				save_btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.dismiss();
						updateFunction();

					}
				});
				dialog.show();
			}
		});
		/*otp_submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				otp_number = otp_num.getText().toString().trim();

				// for profile updating
				if (otp_number.equalsIgnoreCase("")) {
					Toast.makeText(getActivity(), " Enter your OTP ",
							Toast.LENGTH_LONG).show();
				} else {
					new otp_verification_HttpAsyncTask()
							.execute(ServerLinks.otp);
				}

			}
		});*/

		p_home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();
			}
		});

		p_myorder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Myorderfragment()).commit();
			}
		});
		p_price.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Pricingfragment()).commit();

			}
		});
		p_profile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Profilefragment()).commit();
			}
		});

		return rootView;
	}



	private class update_profileAsyncTask extends
			AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("custmerid", p_userid);
				jsonObject.accumulate("email", emailid);
				jsonObject.accumulate("username", new_name);
				jsonObject.accumulate("mobile", new_phn);
				/*
				 * jsonObject.accumulate("mobile", Mobile);
				 * jsonObject.accumulate("password", Password);
				 */
				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {

			try {
				if (!result.equals("")){
				//{"status":"1",
				// "custmerid":"SDU2015015",
				// "email":"Kumar@gmail.com",
				// "username":"KUMAROJHA",
				// "mobile":"8342978527",
				// "msg":"update mobile no"}
				JSONObject jObject = new JSONObject(result);
				int status = jObject.getInt("status");
				if (status == 1) {
					//u_userid = jObject.getString("mid");
					//Toast.makeText(getActivity(), "Profile detail updated successfully", Toast.LENGTH_SHORT).show();
					f_name = jObject.getString("username");
					f_email = jObject.getString("email");
					f_mbl = jObject.getString("mobile");
					f_uid = jObject.getString("custmerid");
					submitForm();
					newuname.setVisibility(View.GONE);
					uname.setVisibility(View.VISIBLE);
					uname.setText(f_name);

					newph.setVisibility(View.GONE);
					phn_no.setVisibility(View.VISIBLE);
					phn_no.setText(f_mbl);
					// newph.setFocusableInTouchMode(true);
					submit.setVisibility(View.GONE);
					cancel.setVisibility(View.GONE);

					/*main.setVisibility(View.GONE);
					otp_layout.setVisibility(View.VISIBLE);*/
					/*
					 * Intent intent4 = new Intent(getActivity(),
					 * Mobile_num.class); intent4.putExtra("uid", u_userid);
					 * startActivity(intent4);
					 */
					/*final Dialog dialog = new Dialog(getActivity());
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

					// inflate the layout dialog_layout.xml and set it as contentView
					LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View view = inflater.inflate(R.layout.number_dialog, null, false);
					dialog.setCanceledOnTouchOutside(false);
					dialog.setContentView(view);

					dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					//dialog.setContentView(R.layout.newdialog);
					dialog.setTitle("OTP Verification");
					dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
					final EditText otp_d= (EditText) dialog.findViewById(R.id.otp_dlg);
					Button cancel= (Button) dialog.findViewById(R.id.cancel);
					cancel.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
						}
					});
					Button verify= (Button) dialog.findViewById(R.id.submit);
					cancel.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {

								dialog.dismiss();


						}
					});
					verify.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							otp_number= otp_d.getText().toString().trim();
							*//*otp_number = otp_num.getText().toString().trim();*//*

							// for profile updating
							if (otp_number.equalsIgnoreCase("")) {
								Toast.makeText(getActivity(), " Enter your OTP ",
										Toast.LENGTH_LONG).show();
							} else {
								new otp_verification_HttpAsyncTask()
										.execute(ServerLinks.otp);
								dialog.dismiss();
							}

						}
					});


					dialog.show();*/

				}
				else if (status == 2){
					Toast.makeText(getActivity(), "Nothing to be changed. Username and mobile number is same as the previous.", Toast.LENGTH_SHORT).show();
				}
				else if (status == 3){
					Toast.makeText(getActivity(), "Mobile number already in use.", Toast.LENGTH_SHORT).show();
				}
				else {
					Toast.makeText(getActivity(), "Unable to store. Please try again.", Toast.LENGTH_SHORT).show();

				}
				}
				else{
					Toast.makeText( getActivity(), "Bad Network Connection !!",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {

			}

			progress.setVisibility(View.GONE);

			super.onPostExecute(result);

		}

		private void submitForm() {

			try {

				SharedPreferences.Editor shpEdit = shp.edit();
				shpEdit.putString("AuthKey", f_email);
				shpEdit.putString("USER_UID", f_uid);
				shpEdit.putString("UNAME", f_name);
				shpEdit.putString("MBL_NUM", f_mbl);
				shpEdit.commit();

			} catch (Exception e) {
				e.printStackTrace();

			}
		}

	}


	private void displayProfileImage() {
		// TODO Auto-generated method stub

		ImageLoader imageLoader = ImageLoader.getInstance();
		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration
					.createDefault(getActivity()));
		}

		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.defaultimage)
				.showImageForEmptyUri(R.drawable.defaultimage)
				.showImageOnFail(R.drawable.defaultimage).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		imageLoader.displayImage("https://graph.facebook.com/" + userid
				+ "/picture?type=large", profile, options,
				new SimpleImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
					}
				}, new ImageLoadingProgressListener() {
					@Override
					public void onProgressUpdate(String imageUri, View view,
							int current, int total) {
					}
				});

	}

	private class display_tagAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
			/*getActivity().getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);*/
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("userid", p_userid);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
			//getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
			addressid = new ArrayList<String>();
			tag_addressname = new ArrayList<String>();

			JSONObject jsonResponse;

			try {
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					JSONArray jArr = jsonObject.getJSONArray("circle");
					if (jArr != null) {

						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);
							String add_id = jsonResponse.optString("addressid")
									.toString();
							String ad_name = jsonResponse.optString(
									"tag_addressname").toString();
							addressid.add(add_id);
							tag_addressname.add(ad_name);
						}
					}
				} else {
					Toast.makeText(getActivity(), "You have no adress",
							Toast.LENGTH_SHORT).show();
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}

			for (int j = 0; j < tag_addressname.size(); j++) {
				String pos = tag_addressname.get(j);

				if (j == 0) {
					// ethi 1st textview re ad_name set karibu
					adres1.setVisibility(View.VISIBLE);
					adres1.setText(pos);

				} else if (j == 1) {
					// ethi 2nd ta
					adres2.setVisibility(View.VISIBLE);
					adres2.setText(pos);

				} else if (j == 2) {
					// ethi 3rd ta
					adres3.setVisibility(View.VISIBLE);
					adres3.setText(pos);

				} else if (j == 3) {
					adres4.setVisibility(View.VISIBLE);
					adres4.setText(pos);

				} else {
					adres5.setVisibility(View.VISIBLE);
					adres5.setText(pos);

				}

			}
		}

	}

	public void Dialog_class() {

		final Dialog dialog = new Dialog(getActivity());
		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.newdialog, null, false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(view);

		dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		dialog.setTitle("Address..");
		dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
		progress_ad = (FrameLayout) dialog.findViewById(R.id.frame_progress_ad);
		// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		tv_show_tag = (TextView) dialog.findViewById(R.id.tv_show_tag);
		tv_show_plot = (TextView) dialog.findViewById(R.id.tv_show_plot);
		tv_show_landmark = (TextView) dialog.findViewById(R.id.tv_show_lmrk);
		tv_show_location = (TextView) dialog
				.findViewById(R.id.tv_show_location);
		tv_show_circle = (TextView) dialog.findViewById(R.id.tv_show_circle);
		tv_show_center = (TextView) dialog.findViewById(R.id.tv_show_center);
		tv_show_city = (TextView) dialog.findViewById(R.id.tv_show_city);
		// tv_set_add=(TextView) dialog.findViewById(R.id.tv_show_adress);
		new tag_addressAsyncTask().execute(ServerLinks.tag_address);

		LinearLayout edit = (LinearLayout) dialog.findViewById(R.id.edit_ad);
		// if decline button is clicked, close the custom dialog
		edit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				/*Intent d= new Intent(getActivity(),Showaddress.class);
				startActivity(d);*/
				String new_id=_id;
				Intent edit=new Intent(getActivity(),Addlocationnew.class);
				edit.putExtra("EDITTAG", _addr);
				edit.putExtra("ADDID",new_id);
				//startActivity(edit);
				startActivityForResult(edit, REQUEST_CODE);
				dialog.dismiss();
			}
		});
		LinearLayout delet = (LinearLayout) dialog.findViewById(R.id.dl_ad);
		// if decline button is clicked, close the custom dialog
		delet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						getActivity());

				// Setting Dialog Title
				alertDialog.setTitle("Warning!!!");
				// Setting Dialog Message
				alertDialog.setMessage("Are you sure to delete?");
				// Setting Icon to Dialog
				alertDialog.setIcon(R.drawable.warn);
				// Setting Positive "Yes" Button
				alertDialog.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

								new del_addressAsyncTask()
										.execute(ServerLinks.delete_address);

								// new
								// display_tagAsyncTask().execute(ServerLinks.view_tag);

								dialog.dismiss();

							}
						});

				// Setting Negative "NO" Button
				alertDialog.setNegativeButton("NO",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// Write your code here to invoke NO event
								dialog.cancel();
							}
						});
				// Showing Alert Message
				alertDialog.show();
				// Close dialog

				dialog.dismiss();

			}

		});

		LinearLayout save = (LinearLayout) dialog.findViewById(R.id.ok_ad);
		save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*Toast.makeText(getActivity(), "Successfully Set",
						Toast.LENGTH_LONG).show();*/
				dialog.dismiss();
			}
		});
		/* new display_tagAsyncTask().execute(ServerLinks.view_tag); */
		dialog.show();

	}

	private class tag_addressAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress_ad.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("userid", p_userid);
				jsonObject.accumulate("addressid", _id);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("unused")
		@Override
		protected void onPostExecute(String result) {
			/*
			 * super.onPostExecute(result); progressDialog.dismiss();
			 */

			JSONObject jsonResponse;

			try {
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {

					JSONObject jOb = jsonObject.getJSONObject("addressdetai");

					if (jOb != null) {

						String tag = jOb.getString("tag_addressname");
						String city = jOb.getString("cityname");
						String center = jOb.getString("centrename");
						String circle = jOb.getString("circlename");
						String location = jOb.getString("locationname");
						String plot = jOb.getString("plot_detail");
						String landmark = jOb.getString("landmark_detail");
						tv_show_tag.setText(tag);
						tv_show_plot.setText(plot + ",");
						tv_show_landmark.setText("Near:" + landmark);
						tv_show_location.setText(location + ",");
						tv_show_circle.setText(circle);
						tv_show_center.setText(center + ",");
						tv_show_city.setText(city);
						// tv_set_add.setText(plot+","+location+"\n"+circle+","+center+","+city+"\n"+"LandMark:"+landmark);

						// Toast.makeText(getActivity(),
						// "Please Check Your Internet Connection.",
						// Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(getActivity(),
								"Please Check Your Internet Connection.",
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(getActivity(),
							"something is going wrong !!!", Toast.LENGTH_LONG)
							.show();
				}

			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}

			progress_ad.setVisibility(View.GONE);

			super.onPostExecute(result);
		}

	}

	private class del_addressAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("addressid", _id);
				jsonObject.accumulate("userid", p_userid);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject jObject = new JSONObject(result);
				String status = jObject.getString("status");
				if (status.equals("0")) {

					Toast.makeText(getActivity(), "Try Again!",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getActivity(), "Successfully deleted!",
							Toast.LENGTH_LONG).show();
					new display_tagAsyncTask().execute(ServerLinks.view_tag);
					refresh_fragment();

				}
			} catch (Exception e) {

			}
			new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					// progress.setVisibility(View.GONE);
				}
			};

			super.onPostExecute(result);

		}
	}

	private class otp_verification_HttpAsyncTask extends
			AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("otp", otp_number);
				jsonObject.accumulate("mid", u_userid);
				jsonObject.accumulate("mobile", new_phn);
				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {

			//progress.setVisibility(View.GONE);
			try {
				JSONObject jObject = new JSONObject(result);
				int status = jObject.getInt("status");
				if (status == 0) {

					Toast.makeText(getActivity(), "Try Again!", Toast.LENGTH_SHORT).show();
				} else if (status == 1) {
					f_name = jObject.getString("name");
					f_email = jObject.getString("email");
					f_mbl = jObject.getString("mobile");
					f_uid = jObject.getString("userid");
					//submitForm();
					otp_layout.setVisibility(View.GONE);
					main.setVisibility(View.VISIBLE);

					newuname.setVisibility(View.GONE);
					uname.setVisibility(View.VISIBLE);
					uname.setText(f_name);

					newph.setVisibility(View.GONE);
					phn_no.setVisibility(View.VISIBLE);
					phn_no.setText(f_mbl);
					// newph.setFocusableInTouchMode(true);
					submit.setVisibility(View.GONE);

					Toast.makeText(getActivity(), "Successfull", Toast.LENGTH_SHORT).show();

					updateFunction();

				} else if (status == 2) {
					Toast.makeText(getActivity(), "OTP did not match.",  Toast.LENGTH_SHORT)
							.show();
				}
			} catch (Exception e) {

			}

			// progressDialog.dismiss();
			progress.setVisibility(View.GONE);
			super.onPostExecute(result);

		}

		/*private void submitForm() {

			try {

				SharedPreferences.Editor shpEdit = shp.edit();
				shpEdit.putString("AuthKey", f_email);
				shpEdit.putString("USER_UID", f_uid);
				shpEdit.putString("UNAME", f_name);
				shpEdit.putString("MBL_NUM", f_mbl);
				shpEdit.commit();

			} catch (Exception e) {
				e.printStackTrace();

			}
		}*/
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void refresh_fragment() {
		Fragment frg = null;
		frg = getFragmentManager().findFragmentById(R.id.containerView);
		final FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.detach(frg);
		ft.attach(frg);
		ft.commit();
	}


	private void updateFunction() {
		// fetching value
			// hashmap for parameters
			HashMap<String, String> parameters = new HashMap<String, String>();

			parameters.put("image", imageFilePath);

	    	parameters.put("customerid", p_userid);

		    new Member_List().execute(parameters);
			/*HashMap<String, String> hashMapLink = new HashMap<String, String>();
			hashMapLink.put(AllStaticVariables.link, ServerLinks.profile_pic);
			asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(getActivity());
			asynkTaskForServerCommunication.execute(parameters, hashMapLink);*/
	}

	/*@Override
	public void doInBackgroundForComun(int progress) {

	}

	@Override
	public void doPostExecuteForCommu(JSONObject jsonObject) {
		if (jsonObject != null) try {

			int status = jsonObject.getInt("status");
			if (status == 0) {

				Toast.makeText(getActivity(), "Try Again!", Toast.LENGTH_SHORT).show();
			} else if (status == 1) {


			} else if (status == 2) {
				Toast.makeText(getActivity(), "OTP did not match.",  Toast.LENGTH_SHORT)
						.show();
			}
		} catch (Exception e) {

		}
	}*/
	private class Member_List extends
			AsyncTask<HashMap<String, String>, JSONObject, JSONObject> {
		JSONObject object;
		ServerLinkCommunication serverLinkCommunication;

		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading Please Wait...");
			pDialog.setCancelable(false);
			pDialog.show();
			serverLinkCommunication = new ServerLinkCommunication(
					getActivity());
		}

		@Override
		protected JSONObject doInBackground(HashMap<String, String>... params) {
			HashMap<String, String> parameters = params[0];

			HashMap<String, String> hashMapLink = new HashMap<String, String>();
			hashMapLink.put("LINK", ServerLinks.profile_pic);

			object = serverLinkCommunication.GetJsonfromServerFile(parameters,
					hashMapLink);

			return object;
		}

		@Override
		protected void onPostExecute(JSONObject jsonObject) {

			// Log.i(AllStaticVariables.TAG, result.toString());
			pDialog.dismiss();
			if (jsonObject != null)
				try {

				int status = jsonObject.getInt("status");
				if (status == 0) {

					Toast.makeText(getActivity(), "Try Again!", Toast.LENGTH_SHORT).show();
				} else if (status == 1) {


				} else if (status == 2) {
					Toast.makeText(getActivity(), "OTP did not match.",  Toast.LENGTH_SHORT)
							.show();
				}
			} catch (Exception e) {

			}

		}
	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_FILE)
				onSelectFromGalleryResult(data);
			else if (requestCode == REQUEST_CAMERA)
				onCaptureImageResult(data);
		}
		if(resultCode==6 && requestCode == REQUEST_CODE ){
			try {

				new display_tagAsyncTask().execute(ServerLinks.view_tag);
			} catch (Exception e) {



			}

		}


	}
	private void onCaptureImageResult(Intent data) {


		Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

		File destination = new File(Environment.getExternalStorageDirectory(),
				System.currentTimeMillis() + ".jpg");
		imageFilePath = destination.toString();
		sharedPreferenceClass.setValue_string("User_Image",imageFilePath);
		FileOutputStream fo;
		try {
			destination.createNewFile();
			fo = new FileOutputStream(destination);
			fo.write(bytes.toByteArray());
			fo.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		profile.setImageBitmap(thumbnail);
	}

	@SuppressWarnings("deprecation")
	private void onSelectFromGalleryResult(Intent data) {
		Uri selectedImageUri = data.getData();
		String[] projection = { MediaStore.MediaColumns.DATA };
		Cursor cursor = getActivity().managedQuery(selectedImageUri, projection, null, null,
				null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
		cursor.moveToFirst();

		imageFilePath = cursor.getString(column_index).toString();
		sharedPreferenceClass.setValue_string("User_Image",imageFilePath);


		BitmapFactory.decodeFile(imageFilePath);

		Bitmap bm = BitmapFactory.decodeFile(imageFilePath);

		profile.setImageBitmap(bm);
	}
	@Override
	public void onAttach(Activity activity) {
		profileContext = (MainActivity) activity;
		super.onAttach(activity);
	}

	public void onBackPressed() {

		/*mFragmentManager = getFragmentManager();
		mFragmentTransaction = mFragmentManager.beginTransaction();
		mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();*/

		/*FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Homefragment llf = new Homefragment();
		ft.replace(R.id.containerView, llf);
		ft.commit();*/
	}
}

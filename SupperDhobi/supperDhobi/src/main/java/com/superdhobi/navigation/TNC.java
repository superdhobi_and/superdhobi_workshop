package com.superdhobi.navigation;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Santanu on 24-11-2015.
 */
public class TNC extends Activity implements AsynkTaskCommunicationInterface {
    TextView tnc;
    LinearLayout back_tnc;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tnc);
        tnc= (TextView) findViewById(R.id.show_tnc);
        back_tnc= (LinearLayout) findViewById(R.id.back_tnc);
        back_tnc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("customerid", "123456");

        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, ServerLinks.tnc);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(TNC.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);

    }


    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }
    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if(jsonObject!=null){
            //{"status":1}
            try {
                int status=jsonObject.getInt("status");
                //{"admin_status":"1","msg":"welcome admin","sanitary_status":"0","admin_id":"admin","status":"1"}
                if(status==0)
                {
                    Toast.makeText(TNC.this, "Please Try Again. Connection slow.", Toast.LENGTH_SHORT).show();

                }
                else if(status==1){
                    String desc=jsonObject.getString("description");
                    tnc.setText(Html.fromHtml(desc));
                    //about.setText(Html.fromHtml(desc));

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }
}

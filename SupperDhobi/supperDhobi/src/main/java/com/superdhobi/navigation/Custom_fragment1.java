package com.superdhobi.navigation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 29-02-2016.
 */
public class Custom_fragment1 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.custom_slide1, container, false);

        return rootView;
    }
}

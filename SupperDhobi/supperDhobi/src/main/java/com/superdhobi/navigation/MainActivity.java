package com.superdhobi.navigation;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.superdhobi.Utillls.DatabaseHandler;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;
import com.superdhobi.supperdhobi.Chhose_address;
import com.superdhobi.supperdhobi.Membership;
import com.superdhobi.supperdhobi.New_Feedback;
import com.superdhobi.supperdhobi.New_membership;
import com.superdhobi.supperdhobi.New_offer;
import com.superdhobi.supperdhobi.New_order_history;
import com.superdhobi.supperdhobi.New_prfoile_sd;
import com.superdhobi.supperdhobi.New_wallet;
import com.superdhobi.supperdhobi.R;
import com.superdhobi.supperdhobi.RateThisApp;
import com.superdhobi.supperdhobi.Rating_activity;
import com.superdhobi.supperdhobi.TextDrawable;
import com.superdhobi.supperdhobi.Wallet_statement;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Fragment newfragment;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
     public static Toolbar toolbar;
    private SharedPreferences shp;
    static  String emailid,name,mobile;
    SharedPreferenceClass sharedPreferenceClass;
    public static boolean viewIsAtHome;
    DatabaseHandler db;
    String _userid="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferenceClass=new SharedPreferenceClass(getApplicationContext());
        shp = getSharedPreferences("AuthToken", MODE_PRIVATE);
        emailid = shp.getString("AuthKey", null);
        name = shp.getString("UNAME", null);
        mobile=shp.getString("MBL_NUM",null);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String nav=sharedPreferenceClass.getValue_string("NAV");
        // fr=sharedPreferenceClass.getValue_boolean(nav);

      /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView =  navigationView.inflateHeaderView(R.layout.nav_header_main);
        TextView namehead= (TextView)headerView.findViewById(R.id.name_head);
        ImageView image=(ImageView) headerView.findViewById(R.id.imageView);
        TextView mobiletext= (TextView)headerView.findViewById(R.id.mobile_head);
        namehead.setText(name);

        mobiletext.setText(mobile);
        TextDrawable drawable1 = TextDrawable.builder()
                .buildRoundRect("A", Color.parseColor("#ffffff"), 10);

        String first=name.substring(0, 1);

        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .textColor(Color.parseColor("#ffa500"))
                .useFont(Typeface.DEFAULT)
                .fontSize(60) /* size in px */
                .bold()
                .toUpperCase()
                .endConfig()
                .buildRound(first, Color.parseColor("#ffffff"));
        image.setImageDrawable(drawable);

        //TextDrawable drawable = TextDrawable.builder().buildRect("A", Color.RED);
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new New_sd_home()).commit();
        viewIsAtHome = true;

        shp = this.getSharedPreferences("AuthToken", Context.MODE_PRIVATE);

        _userid=shp.getString("USER_UID", null);
        String _username=shp.getString("UNAME", null);
    }

   @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
      /* if (getSupportFragmentManager().getBackStackEntryCount() == 1){
           finish();
       }
       else {
           super.onBackPressed();
       }*/
   /* if (fr== false){
           mFragmentManager = getSupportFragmentManager();
           mFragmentTransaction = mFragmentManager.beginTransaction();
           mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();
           sharedPreferenceClass.setValue_string("NAV", "FRAG");
           sharedPreferenceClass.setValue_boolean("FRAG", true);
       }*/
     if (!viewIsAtHome) { //if the current view is not the News fragment
          // displayView(R.id.nav_order); //display the News fragment
           mFragmentManager = getSupportFragmentManager();
           mFragmentTransaction = mFragmentManager.beginTransaction();
           mFragmentTransaction.replace(R.id.containerView,new New_sd_home()).commit();
         toolbar.setTitle("SuperDhobi");
         toolbar.setSubtitle("Home");
           viewIsAtHome = true;
       }
       else {
         AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
         builder.setMessage("Do You Want to Close this App ?");
         String positiveText = "OK";
         builder.setPositiveButton(positiveText,
                 new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         // positive button logic

                         finish();
                         System.exit(0);
                     }
                 });

         String negativeText = getString(android.R.string.cancel);

         builder.setNegativeButton(negativeText,
                 new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         // negative button logic

                     }
                 });
         AlertDialog dialog = builder.create();
         dialog.show();
          //moveTaskToBack(true);  //If view is in News fragment, exit application
       }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            db = new DatabaseHandler(MainActivity.this);
            //db.delete_data();


            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
            // set title
            alertDialogBuilder.setTitle("Log Out!!");
            // set dialog message
            alertDialogBuilder
                    .setMessage("Do You Want To Log Out?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity
                            sharedPreferenceClass.setValue_boolean("USER", false);
                            Intent intent=new Intent(MainActivity.this,Login_Activity.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton("No",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
            return true;
        }
        if (id == R.id.action_profile){
            Intent intent=new Intent(MainActivity.this,New_prfoile_sd.class);
            intent.putExtra("userId",_userid);
            startActivity(intent);


           // finish();
            return true;
        }
        if (id == R.id.action_rate){
            SharedPreferenceClass sharedPreferenceClass=new SharedPreferenceClass(MainActivity.this);

                RateThisApp.init(new RateThisApp.Config(3, 5));

                RateThisApp.showRateDialog(MainActivity.this);

            return true;
        }
        if (id == R.id.action_share){
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.superdhobi.supperdhobi");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
            return true;
        }
        if (id == R.id.action_feedback){
            Intent intent1=new Intent(MainActivity.this,New_Feedback.class);
            intent1.putExtra("userId",_userid);
            startActivity(intent1);
           // finish();
            return true;
        }
        if (id == R.id.action_help){
            Intent intent1=new Intent(MainActivity.this,HelpnSupport.class);
            startActivity(intent1);
           // finish();
            return true;
        }
        if (id == R.id.action_tnc){
            Intent intent1=new Intent(MainActivity.this,Termsnconditions.class);
            startActivity(intent1);
           // finish();
            return true;
        }
        if (id == R.id.action_wallet){
            Intent intent=new Intent(MainActivity.this,Wallet_statement.class);
            intent.putExtra("userId",_userid);
            startActivity(intent);
           // finish();
            return true;
        } /*if (id == R.id.action_addr){
            Intent intent=new Intent(MainActivity.this,Chhose_address.class);
            startActivity(intent);
           // finish();
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //displayView(item.getItemId());
       if (id == R.id.nav_order) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new New_sd_home()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("Home");
          /* sharedPreferenceClass.setValue_string("NAV", "FRAG");
           sharedPreferenceClass.setValue_boolean("FRAG", true);*/
          viewIsAtHome = true;
            //toolbar.setTitleTextColor();

         //   Toast.makeText(MainActivity.this,"u r clicking camera",Toast.LENGTH_SHORT).show();
            // Handle the camera action
        }
        else if (id == R.id.nav_myorder) {
          /* Intent or=new Intent(MainActivity.this, New_order_history.class);
           startActivity(or);*/
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            //Intent intent=new Intent(MainActivity.this,New_prfoile_sd.class);
            //intent.putExtra("userId",_userid);
            mFragmentTransaction.replace(R.id.containerView,new New_order_history(_userid)).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("MyOrder");
          viewIsAtHome = false;
         //  sharedPreferenceClass.setValue_string("NAV", "FRAG");
           //sharedPreferenceClass.setValue_boolean("FRAG", false);
        }
        /*else if (id == R.id.nav_profile) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new Profilefragment()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("Profile");
           viewIsAtHome = false;
           //sharedPreferenceClass.setValue_string("NAV", "FRAG");
           //sharedPreferenceClass.setValue_boolean("FRAG", false);
        }*/
        /*else if (id == R.id.nav_pricing) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new Pricingfragment()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("Pricing");
           viewIsAtHome = false;
           //sharedPreferenceClass.setValue_string("NAV", "FRAG");
           //sharedPreferenceClass.setValue_boolean("FRAG", false);
        }*/
        else if (id == R.id.nav_membership) {
            /*mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new Washmoneyfragment()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("Membership");
          viewIsAtHome = false;*/
          // sharedPreferenceClass.setValue_string("NAV", "FRAG");
          // sharedPreferenceClass.setValue_boolean("FRAG", false);
           Intent n= new Intent(MainActivity.this, Membership.class);
           startActivity(n);
        }
       /* else if (id == R.id.nav_mywallet) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new Mywallet()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("MyWallet");
            viewIsAtHome = false;
           //sharedPreferenceClass.setValue_string("NAV", "FRAG");
           //sharedPreferenceClass.setValue_boolean("FRAG", false);
        }*/
        else if (id == R.id.nav_invite) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new Invitefragment()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("Invite and Earn");
           viewIsAtHome = false;
           //sharedPreferenceClass.setValue_string("NAV", "FRAG");
           //sharedPreferenceClass.setValue_boolean("FRAG", false);
        }
        else if (id == R.id.nav_offer) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new New_offer()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("Offer");
           viewIsAtHome = false;
          // sharedPreferenceClass.setValue_string("NAV", "FRAG");
           //sharedPreferenceClass.setValue_boolean("FRAG", false);
         }
       /* else if (id == R.id.nav_feedback) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new Feedback_fragment()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("Feedback");
           viewIsAtHome = false;
          // sharedPreferenceClass.setValue_string("NAV", "FRAG");
           //sharedPreferenceClass.setValue_boolean("FRAG", false);
        }*/
       /* else if (id == R.id.nav_help) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new HelpnSupport()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("Help");
           viewIsAtHome = false;
          // sharedPreferenceClass.setValue_string("NAV", "FRAG");
          // sharedPreferenceClass.setValue_boolean("FRAG", false);
        }*/
       /* else if (id == R.id.nav_term) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView, new Termsnconditions()).commit();
            toolbar.setTitle("SuperDhobi");
            toolbar.setSubtitle("Term and Condition");
          viewIsAtHome = false;
          // sharedPreferenceClass.setValue_string("NAV", "FRAG");
           //sharedPreferenceClass.setValue_boolean("FRAG", false);
        }*/
       else if (id == R.id.action_logout) {
           AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);

           // set title
           alertDialogBuilder.setTitle("Log Out!!");

           // set dialog message
           alertDialogBuilder
                   .setMessage("Do You Want To Log Out?")
                   .setCancelable(false)
                   .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog,int id) {
                           // if this button is clicked, close
                           // current activity
                           sharedPreferenceClass.setValue_boolean("USER", false);
                           Intent intent=new Intent(MainActivity.this,Login_Activity.class);
                           startActivity(intent);
                           finish();
                       }
                   })
                   .setNegativeButton("No", new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int id) {
                           // if this button is clicked, just close
                           // the dialog box and do nothing
                           dialog.cancel();
                       }
                   });

           // create alert dialog
           AlertDialog alertDialog = alertDialogBuilder.create();

           // show it
           alertDialog.show();
           return true;
       }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
   /* public void displayView(int viewId) {

        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (viewId) {
            case R.id.nav_order:
                fragment = new Homefragment();
                toolbar.setTitle("SuperDhobi");
                toolbar.setSubtitle("Order");
                viewIsAtHome = true;
                break;
            case R.id.nav_myorder:
                fragment = new Myorderfragment();
                toolbar.setTitle("SuperDhobi");
                toolbar.setSubtitle("MyOrder");
                viewIsAtHome = false;
                break;

        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.containerView, fragment);
            ft.commit();
        }

        // set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

    }*/

}

package com.superdhobi.navigation;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Myorderfragment extends Fragment {
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;
	Toolbar toolbar;
	ListView myorderlist;
	String[] order_ref, pick_date, pick_time, respond, cancel, pick,
			delivery_status;
	private SharedPreferences shp;
	static MainActivity myorderContext;
	static String hm_userid;
FrameLayout progress;
	SharedPreferenceClass sharedPreferenceClass;
	ImageView m_home,m_myorder,m_profile,m_price;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		shp = myorderContext.getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor shpEdit = shp.edit();
		shpEdit.putString("start_home", "from_myorder");
		shpEdit.commit();

		View rootView = inflater.inflate(R.layout.myorder, container, false);
		sharedPreferenceClass=new SharedPreferenceClass(getActivity());

	/*	sharedPreferenceClass.setValue_string("NAV", "FRAG");
		sharedPreferenceClass.setValue_boolean("FRAG", false);*/

		shp = getActivity().getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);
		hm_userid = shp.getString("USER_UID", null);
		progress= (FrameLayout) rootView.findViewById(R.id.frame_progress);
		((MainActivity) getActivity()).toolbar.setSubtitle("MyOrder");
		myorderlist = (ListView) rootView.findViewById(R.id.myorder_list);

		m_home= (ImageView) rootView.findViewById(R.id.m_home);
		m_myorder= (ImageView) rootView.findViewById(R.id.m_morder);
		m_profile= (ImageView) rootView.findViewById(R.id.m_profile);
		m_price= (ImageView) rootView.findViewById(R.id.m_price);

		new order_listAsyntask().execute(ServerLinks.my_order);

		m_home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();
			}
		});

		m_myorder.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Myorderfragment()).commit();
			}
		});
		m_price.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Pricingfragment()).commit();
			}
		});
		m_profile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Profilefragment()).commit();
			}
		});
		return rootView;
	}

	private class order_listAsyntask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("userid", hm_userid);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("unused")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);

			/*
			 * washtype=new ArrayList<String>(); wash_id=new
			 * ArrayList<String>();
			 */

			JSONObject jsonResponse;

			try {
				if (!result.equals("")){
				JSONObject jsonObject = new JSONObject(result);
//{"status":"1",
// "myorder":
// [{"orderre":"SDR061115022",
// "pick_date":"2015-11-06",
// "respond_status":"1",
// "cancel_status":"0",
// "pick_status":"0",
// "delivery_status":"0",
// "time":"6:30 pm-9:00 pm"},
// {"orderre":"SDR061115021","pick_date":"2015-11-06","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"6:30 pm-9:00 pm"},{"orderre":"SDR061115020","pick_date":"2015-11-06","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"6:30 pm-9:00 pm"},{"orderre":"SDR041115011","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"6:30 pm-9:00 pm"},{"orderre":"SDR041115010","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"6:30 pm-9:00 pm"},{"orderre":"SDR04111509","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"6:30 pm-9:00 pm"},{"orderre":"SDR04111508","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"6:30 pm-9:00 pm"},{"orderre":"SDR04111507","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"6:30 pm-9:00 pm"},{"orderre":"SDR04111506","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"6:30 pm-9:00 pm"},{"orderre":"SDR04111505","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"5:00 pm-6:00 pm"},{"orderre":"SDR04111504","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"5:00 pm-6:00 pm"},{"orderre":"SDR04111503","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"5:00 pm-6:00 pm"},{"orderre":"SDR04111502","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"5:00 pm-6:00 pm"},{"orderre":"SDR04111501","pick_date":"2015-11-04","respond_status":"1","cancel_status":"0","pick_status":"0","delivery_status":"0","time":"5:00 pm-6:00 pm"}]}
				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					JSONArray jArr = jsonObject.getJSONArray("myorder");
					if (jArr != null) {
						order_ref = new String[jArr.length()];
						pick_date = new String[jArr.length()];
						pick_time = new String[jArr.length()];
						pick = new String[jArr.length()];
						respond = new String[jArr.length()];
						cancel = new String[jArr.length()];
						delivery_status = new String[jArr.length()];

						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);

							order_ref[i] = jsonResponse.getString("orderre");
							pick_date[i] = jsonResponse.getString("pick_date");
							pick_time[i] = jsonResponse.getString("time");
							pick[i] = jsonResponse.getString("pick_status");
							respond[i] = jsonResponse.getString("respond_status");
							cancel[i] = jsonResponse.getString("cancel_status");
							delivery_status[i] = jsonResponse.getString("delivery_status");

						}
					} else {
						Toast.makeText(getActivity(), "You have no adress",
								Toast.LENGTH_SHORT).show();
					}
					 my_order_adapter adapter = new my_order_adapter(getActivity(),order_ref,pick_date,pick_time,respond,cancel,pick,delivery_status);
					 
						myorderlist.setAdapter(adapter);
				}
				else {
					Toast.makeText(getActivity(),
							"You have no order for today!!!",
							Toast.LENGTH_SHORT).show();
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("Warning!!");
					builder.setIcon(R.drawable.warn);
					builder.setMessage("You have no order for today!!!")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {

											//etrhi
											dialog.dismiss();

										}
									});

					AlertDialog alert = builder.create();
					alert.show();
				}
				}
				else{
					Toast.makeText(getActivity(), "Bad Network Connection !!",
							Toast.LENGTH_SHORT).show();
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}

			/*my_order_adapter adapter = new my_order_adapter(getActivity(),
					order_ref, pick_date, pick_time, respond, cancel, pick,
					delivery_status);

			myorderlist.setAdapter(adapter);*/
			/*
			 * myorderlist.setOnItemClickListener(new
			 * AdapterView.OnItemClickListener() {
			 * 
			 * @Override public void onItemClick(AdapterView<?> parent, View
			 * view, int position, long id) { // TODO Auto-generated method stub
			 * int pos=position; String orderref=order_ref[pos]; String
			 * uid=hm_userid; Intent in=new
			 * Intent(getActivity(),Detail_order.class); in.putExtra("ODR_REF",
			 * orderref); in.putExtra("U_ID_O", uid); startActivity(in);
			 * 
			 * } });
			 */
		}
	}

	public class my_order_adapter extends ArrayAdapter<String> {
		private final Activity context;
		private final String[] order_ref;
		private final String[] pick_date;
		private final String[] pick_time;
		private final String[] respond;
		private final String[] cancel;
		private final String[] pick;
		private final String[] delivery_status;

		public my_order_adapter(Activity context, String[] order_ref,
				String[] pick_date, String[] pick_time, String[] respond,
				String[] cancel, String[] pick, String[] delivery_status) {
			super(context, R.layout.new_myorder, order_ref);
			// TODO Auto-generated constructor stub
			this.context = context;
			this.order_ref = order_ref;
			this.pick_date = pick_date;
			this.pick_time = pick_time;
			this.respond = respond;
			this.cancel = cancel;
			this.pick = pick;
			this.delivery_status = delivery_status;
		}

		public View getView(final int position, View view, ViewGroup parent) {
			LayoutInflater inflater = context.getLayoutInflater();
			View rowView = inflater.inflate(R.layout.new_myorder, null, true);
			LinearLayout order_section= (LinearLayout) rowView.findViewById(R.id.order_sec);
			TextView order_id = (TextView) rowView.findViewById(R.id.order_id);
			TextView pick_Date = (TextView) rowView.findViewById(R.id.pick_Date);
			TextView status = (TextView) rowView.findViewById(R.id.status);
			TextView invoice = (TextView) rowView.findViewById(R.id.invoice);
			ImageView view_det = (ImageView) rowView.findViewById(R.id.view);
			LinearLayout feedback = (LinearLayout) rowView.findViewById(R.id.feedback);

			order_id.setText(order_ref[position]);
			pick_Date.setText(pick_date[position] + "," + pick_time[position]);

			String response=respond[position];
			String cancel_status=cancel[position];
			String pick_status=pick[position];
			String delivery=delivery_status[position];

			if (delivery.equals("1")){
				status.setText("Delivered");
				order_section.setBackgroundColor(Color.parseColor("#2ecc71"));
				invoice.setVisibility(View.VISIBLE);
			}
			else if (pick_status.equals("1")){
				status.setText("Picked");
				invoice.setVisibility(View.VISIBLE);
			}
			else if (cancel_status.equals("1")){
				status.setText("Canceled");
			}
			else if (response.equals("1")){
				status.setText("Pending");
			}


			order_section.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					int pos = position;
					String orderref = order_ref[pos];
					String uid = hm_userid;
					Intent in = new Intent(getActivity(), Detail_order.class);
					in.putExtra("ODR_REF", orderref);
					in.putExtra("U_ID_O", uid);
					startActivity(in);

				}
			});
			invoice.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					int pos = position;
					String orderref = order_ref[pos];
					String pickdate = pick_date[pos];
					String picktime = pick_time[pos];
					//String deldate = order_ref[pos];
					//String deltime = order_ref[pos];
					//String del_date = order_ref[pos];
					String uid = hm_userid;
					Intent in = new Intent(getActivity(), Invoice.class);
					in.putExtra("ODR_REF", orderref);
					in.putExtra("U_ID_O", uid);
					startActivity(in);

				}
			});
			feedback.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					int pos = position;
					String orderref = order_ref[pos];
					String uid = hm_userid;
					Intent in = new Intent(getActivity(), Detail_order.class);
					in.putExtra("ODR_REF", orderref);
					in.putExtra("U_ID_O", uid);
					startActivity(in);

				}
			});
			return rowView;
		}
	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	@Override
	public void onAttach(Activity activity) {
		myorderContext = (MainActivity) activity;
		super.onAttach(activity);
	}

	public void onBackPressed() {
		//FragmentManager manager = getActivity().getSupportFragmentManager();
		/*mFragmentManager = getFragmentManager();
		mFragmentTransaction = mFragmentManager.beginTransaction();
		mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();*/

		/*FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Homefragment llf = new Homefragment();
		ft.replace(R.id.containerView, llf);
		ft.commit();*/
		/*mFragmentManager =getFragmentManager();
		mFragmentTransaction = mFragmentManager.beginTransaction();
		mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();*/
	}

}


package com.superdhobi.navigation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;
import com.superdhobi.supperdhobi.Addlocationnew;
import com.superdhobi.supperdhobi.R;
import com.superdhobi.supperdhobi.RateThisApp;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Homefragment extends Fragment {
android.support.v4.app.FragmentManager mFragmentManager;
	android.support.v4.app.FragmentTransaction mFragmentTransaction;
	Toolbar toolbar;
	/* Spinner location,pick_time, del_time; */
	Dialog promo_dialog;
	static String orderid_promo;
	static String id;
	private SharedPreferences shp;
	static MainActivity homeContext;
static String promo;
	static String hm_userid;
	ImageView addlocation;
	LinearLayout s_ll1, s_ll2, s_ll3, s_ll4;
	ImageView im_w,im_d,im_i,im_sp;
	LinearLayout ll_time_slot_pick, ll_time_slot_del, ll_pick, ll_del;
	public static String slot, shif_id, slot1, shif_id1;
	LinearLayout ly_timeslot, ly_dltime_slot;

	boolean isFirstViewClick = false;
	boolean isSecondViewClick = false;
	boolean isThirdViewClick = false;
	boolean isFourthViewClick = false;
	boolean isFifthViewClick = false;

	boolean adviewone = false;
	boolean adviewtwo = false;
	boolean adviewthree = false;
	boolean adviewfour = false;
	boolean adviewfive = false;

	private SimpleDateFormat dateFormatter, dateFormatter2;
	private DatePickerDialog fromDatePickerDialog, fromDatePickerDialog2;
	Calendar mcurrentTime, mcurrentTime2;
	Spinner time_slot, dl_time_slot;
	TextView pick_date, del_date;
	ArrayList<String> slot_time, pick_shift, dl_slot_time, dl_pick_shift;
	ArrayList<String> addressid, tag_addressname;
	TextView final_date, final_time, final_dl_date, final_dl_time;

	static String date1 = "", time1 = "", dl_date1 = "", dl_time1 = "";
	// EditText pick_date,del_date;
	TextView tv, tv1;
	static String date, dl_date;
	static String dld;
	static int year1, monthOfYear1, dayOfMonth1;
	static long mills;
	static String s, p, q, r;
	LinearLayout address1, address2, address3, address4, address5;
	TextView tag, tag2, tag3, tag4, tag5, tv_set_add;
	TextView tv_show_tag, tv_show_plot, tv_show_landmark, tv_show_location,
			tv_show_circle, tv_show_center, tv_show_city;
	static String addr1, addr2, addr3, addr4, addr5;
	public final static int REQUEST_CODE = 1;
	public static String tagg;
	static Button save, save2;
	FrameLayout progress, progress1, progress_ad;
	static ArrayList<String> washtype,wash_type_name;
	static String o_adr_id = null, o_wash_id;
	Button order;
	static String adcolor;
	ImageView home,myorder,profile,price;
	TextView promo_details;
	String fnl_promocode,m_status;
	LinearLayout show_pdate,show_ddate;
	ImageView loc1,loc2,loc3,loc4,loc5;
	static String w="";


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		shp = homeContext.getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor shpEdit = shp.edit();
		shpEdit.putString("start_home", "fromHome");
		shpEdit.commit();

		SharedPreferenceClass sharedPreferenceClass=new SharedPreferenceClass(getActivity());
		if(sharedPreferenceClass.getValue_int("Rate")==0){
			RateThisApp.init(new RateThisApp.Config(3, 5));
			RateThisApp.showRateDialog(getActivity());
		}
		//sharedPreferenceClass.setValue_int("FRG",1);
		//AppRater.app_launched(getActivity());


		View rootView = inflater.inflate(R.layout.design2, container, false);
		shp = getActivity().getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);

		/*sharedPreferenceClass.setValue_string("NAV", "FRAG");
		sharedPreferenceClass.setValue_boolean("FRAG", true);*/
		hm_userid = shp.getString("USER_UID", null);
		// new Time_slotAsyncTask().execute(ServerLinks.slot_time);
		progress = (FrameLayout) rootView.findViewById(R.id.frame_progress);
		((MainActivity) getActivity()).toolbar.setSubtitle("Order");
		s_ll1 = (LinearLayout) rootView.findViewById(R.id.ll_wash);
		s_ll2 = (LinearLayout) rootView.findViewById(R.id.ll_dry);
		s_ll3 = (LinearLayout) rootView.findViewById(R.id.ll_iron);
		s_ll4 = (LinearLayout) rootView.findViewById(R.id.ll_super);
		show_pdate = (LinearLayout) rootView.findViewById(R.id.shw_pdate);
		show_ddate = (LinearLayout) rootView.findViewById(R.id.shw_ddate);
		pick_date = (TextView) rootView.findViewById(R.id.pdate);
		del_date = (TextView) rootView.findViewById(R.id.ddate);
		time_slot = (Spinner) rootView.findViewById(R.id.p_slot);
		dl_time_slot = (Spinner) rootView.findViewById(R.id.d_slot);
		im_w= (ImageView) rootView.findViewById(R.id.iv_wash);
		im_d= (ImageView) rootView.findViewById(R.id.iv_dry);
		im_i= (ImageView) rootView.findViewById(R.id.iv_iron);
		im_sp= (ImageView) rootView.findViewById(R.id.iv_super);

		ll_pick = (LinearLayout) rootView.findViewById(R.id.ll_spin);
		// ll_del=(LinearLayout) rootView.findViewById(R.id.ll_spin_del);
		home= (ImageView) rootView.findViewById(R.id.home);
		myorder= (ImageView) rootView.findViewById(R.id.morder);
		profile= (ImageView) rootView.findViewById(R.id.profile);
		price= (ImageView) rootView.findViewById(R.id.price);

		ll_time_slot_pick = (LinearLayout) rootView.findViewById(R.id.ll_time_slot_pick);
		ll_time_slot_del = (LinearLayout) rootView.findViewById(R.id.ll_time_slot_del);
		order = (Button) rootView.findViewById(R.id.order);

		address1 = (LinearLayout) rootView.findViewById(R.id.add1);
		address2 = (LinearLayout) rootView.findViewById(R.id.add2);
		address3 = (LinearLayout) rootView.findViewById(R.id.add3);
		address4 = (LinearLayout) rootView.findViewById(R.id.add4);
		address5 = (LinearLayout) rootView.findViewById(R.id.add5);

		tag = (TextView) rootView.findViewById(R.id.tv_tag);
		tag2 = (TextView) rootView.findViewById(R.id.tv_tag1);
		tag3 = (TextView) rootView.findViewById(R.id.tv_tag2);
		tag4 = (TextView) rootView.findViewById(R.id.tv_tag3);
		tag5 = (TextView) rootView.findViewById(R.id.tv_tag4);

		loc1= (ImageView) rootView.findViewById(R.id.loc1);
		loc2= (ImageView) rootView.findViewById(R.id.loc2);
		loc3= (ImageView) rootView.findViewById(R.id.loc3);
		loc4= (ImageView) rootView.findViewById(R.id.loc4);
		loc5= (ImageView) rootView.findViewById(R.id.loc5);

		DatendTimeActivity();


		new display_tagAsyncTask().execute(ServerLinks.view_tag);






		washtype = new ArrayList<String>();
		wash_type_name = new ArrayList<String>();
		// for wash nd iron
		s_ll1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (isFirstViewClick == false) {
					im_w.setImageResource(R.drawable.hm_wash_actv);

					s = '"' + "12" + '"';
					washtype.add(s);
					wash_type_name.add("Normal Wash");
					isFirstViewClick = true;
				} else {
					im_w.setImageResource(R.drawable.hm_wash);
					if (washtype.isEmpty()) {

					} else {
						washtype.remove(s);
						wash_type_name.remove("Normal Wash");
					}

					isFirstViewClick = false;
				}
			}
		});
		// for dry clean
		s_ll2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (isSecondViewClick == false) {
					im_d.setImageResource(R.drawable.hm_drws_actv);
					p = '"' + "13" + '"';
					washtype.add(p);
					wash_type_name.add("Dry Clean");
					isSecondViewClick = true;
				}

				else {
					im_d.setImageResource(R.drawable.hm_drws);
					if (washtype.isEmpty()) {

					} else {
						washtype.remove(p);
						wash_type_name.remove("Dry Clean");
					}
					isSecondViewClick = false;
				}
			}
		});
		// for iron
		s_ll3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (isThirdViewClick == false) {
					im_i.setImageResource(R.drawable.hm_iron_actv);
					q = '"' + "11" + '"';
					washtype.add(q);
					wash_type_name.add("Iron");
					isThirdViewClick = true;
				}

				else {
					im_i.setImageResource(R.drawable.hm_iron);
					if (washtype.isEmpty()) {

					} else {
						washtype.remove(q);
						wash_type_name.remove("Iron");
					}
					isThirdViewClick = false;
				}
			}
		});
		s_ll4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (isFourthViewClick == false) {
					im_sp.setImageResource(R.drawable.hm_spws_actv);
					r = '"' + "14" + '"';
					washtype.add(r);
					wash_type_name.add("Super wash");
					isFourthViewClick = true;
				}

				else {
					im_sp.setImageResource(R.drawable.hm_spws);
					if (washtype.isEmpty()) {

					} else {
						washtype.remove(r);
						wash_type_name.remove("Super wash");
					}
					isFourthViewClick = false;
				}
			}
		});
		// for fetching adress


		address1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				addr1 = tag.getText().toString();
				if (addr1.equals("Add")) {

					throwActivity();

				} else {

					int pos = tag_addressname.indexOf(addr1);
					id = addressid.get(pos);
					adcolor = "r1";
					Dialog_class();

					/*
					 * if(adviewone==false){
					 * address1.setBackgroundResource(R.drawable.appform);
					 * 
					 * adviewone=true; }
					 * 
					 * else{
					 * s_ll4.setBackgroundResource(R.drawable.appformbackgrnd);
					 * adviewone=false; }
					 */
				}
			}
		});
		address2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				addr1 = tag2.getText().toString();
				if (addr1.equals("Add")) {
					throwActivity();
				} else {

					int pos = tag_addressname.indexOf(addr1);
					id = addressid.get(pos);
					adcolor = "r2";
					Dialog_class();

				}
			}
		});
		address3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				addr1 = tag3.getText().toString();
				if (addr1.equals("Add")) {
					throwActivity();

				} else {

					int pos = tag_addressname.indexOf(addr1);
					id = addressid.get(pos);
					adcolor = "r3";
					Dialog_class();

				}
			}
		});
		address4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				addr1 = tag4.getText().toString();
				if (addr1.equals("Add")) {
					throwActivity();

				} else {

					int pos = tag_addressname.indexOf(addr1);
					id = addressid.get(pos);
					adcolor = "r4";
					Dialog_class();

				}
			}
		});
		address5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				addr1 = tag5.getText().toString();
				if (addr1.equals("Add")) {
					throwActivity();
				} else {
					int pos = tag_addressname.indexOf(addr1);
					id = addressid.get(pos);
					adcolor = "r5";
					Dialog_class();
				}
			}
		});

		order.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				o_wash_id = washtype.toString();

				String newaddid = o_adr_id;
				if (date == null || dld == null || shif_id == null
						|| shif_id1 == null || newaddid == null) {
					Toast.makeText(getActivity(), "Choose All the field", Toast.LENGTH_SHORT)
							.show();
				} else if (o_wash_id.equals("[]")) {
					Toast.makeText(getActivity(), "Choose All the field", Toast.LENGTH_SHORT)
							.show();
				} else {
					//new Wash_orderAsyncTask().execute(ServerLinks.wash_order);
					new Check_memberAsyncTask().execute(ServerLinks.check_member);
				}

			}
		});

		home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();
			}
		});

		myorder.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Myorderfragment()).commit();
			}
		});
		price.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Pricingfragment()).commit();

			}
		});
		profile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Profilefragment()).commit();
			}
		});

		return rootView;
	}

	private class Time_slotAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(getActivity());
			 * 
			 * progressDialog.setMessage("Loding In...");
			 * progressDialog.setIndeterminate(false);
			 * 
			 * progressDialog.show();
			 */
		//	progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("p_date", date);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {

			slot_time = new ArrayList<String>();
			pick_shift = new ArrayList<String>();
			// String OutputData = "";
			JSONObject jsonResponse;

			try {
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {

					JSONArray jArr = jsonObject.getJSONArray("slot");
					if (jArr != null) {
						/*
						 * ly_timeslot.setVisibility(View.VISIBLE);
						 * tv.setVisibility(View.VISIBLE);
						 * time_slot.setVisibility(View.VISIBLE);
						 */
						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);
							String shift = jsonResponse.optString("shift_id")
									.toString();
							String time = jsonResponse.optString("time")
									.toString();
							slot_time.add(time);
							pick_shift.add(shift);
						}
						loadSpinnerData();

					}
					new Time_slotAsyncTask1().execute(ServerLinks.slot_time);
				} else {
					/*
					 * ly_timeslot.setVisibility(View.GONE);
					 * tv.setVisibility(View.GONE);
					 * time_slot.setVisibility(View.GONE);
					 */
					Toast.makeText(getActivity(), "Sorry No Slot For Today.",
							Toast.LENGTH_SHORT).show();
					final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("Sorry!!");
					builder.setIcon(R.drawable.warn);
					builder.setMessage("There is no slot for today. Choose tomorrow.")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											time_slot.setAdapter(null);
											dl_time_slot.setAdapter(null);

											dialog.dismiss();
										}
									});

					AlertDialog alert = builder.create();
					alert.show();

				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}
			//progress.setVisibility(View.GONE);

			super.onPostExecute(result);
		}

	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	private void loadSpinnerData() {
		// TODO Auto-generated method stub

		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_spinner_item, slot_time);

		// Drop down layout style - list view with radio button
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		time_slot.setAdapter(dataAdapter);

		time_slot.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				slot = slot_time.get(position);
				shif_id = pick_shift.get(position);
				// then return selectedValue to the web service
				new Time_slotAsyncTask1().execute(ServerLinks.slot_time);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});
	}

	private class Time_slotAsyncTask1 extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("p_date", date);
				jsonObject.accumulate("d_date", dld);
				jsonObject.accumulate("shift_id", shif_id);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {

			dl_slot_time = new ArrayList<String>();
			dl_pick_shift = new ArrayList<String>();
			// String OutputData = "";
			JSONObject jsonResponse;

			try {

				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {

					JSONArray jArr = jsonObject.getJSONArray("slot");
					if (jArr != null) {
						/*
						 * ly_dltime_slot.setVisibility(View.VISIBLE);
						 * tv1.setVisibility(View.VISIBLE);
						 * dl_time_slot.setVisibility(View.VISIBLE);
						 */
						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);
							String shift1 = jsonResponse.optString("shift_id")
									.toString();
							String time1 = jsonResponse.optString("time")
									.toString();
							dl_slot_time.add(time1);
							dl_pick_shift.add(shift1);
						}
						loadSpinnerData1();

					}

				} else {
					/*
					 * ly_dltime_slot.setVisibility(View.GONE);
					 * tv.setVisibility(View.GONE);
					 * time_slot.setVisibility(View.GONE);
					 */
					/*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("Sorry!!");
					builder.setIcon(R.drawable.warn);
					builder.setMessage("There is no slot for today. Choose tomorrow.")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											getActivity().finish();
										}
									});

					AlertDialog alert = builder.create();
					alert.show();*/

				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}
		//	progress.setVisibility(View.GONE);

			super.onPostExecute(result);
		}

	}

	private void loadSpinnerData1() {
		// TODO Auto-generated method stub

		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_spinner_item,
				dl_slot_time);

		// Drop down layout style - list view with radio button
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		dl_time_slot.setAdapter(dataAdapter);

		dl_time_slot.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				slot1 = dl_slot_time.get(position);
				shif_id1 = dl_pick_shift.get(position);
				// then return selectedValue to the web service
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});
	}

	private class display_tagAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
			getActivity().getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("userid", hm_userid);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
			getActivity().getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
			addressid = new ArrayList<String>();
			tag_addressname = new ArrayList<String>();

			JSONObject jsonResponse;

			try {
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					JSONArray jArr = jsonObject.getJSONArray("circle");
					if (jArr != null) {

						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);
							String add_id = jsonResponse.optString("addressid")
									.toString();
							String ad_name = jsonResponse.optString(
									"tag_addressname").toString();
							addressid.add(add_id);
							tag_addressname.add(ad_name);


						}

						// Toast.makeText(getActivity(),
						// "Please Check Your Internet Connection.",
						// Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(getActivity(), "You have no adress",
							Toast.LENGTH_SHORT).show();
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}

			for (int j = 0; j < tag_addressname.size(); j++) {
				String pos = tag_addressname.get(j);

				if (j == 0) {
					// ethi 1st textview re ad_name set karibu

					tag.setText(pos);
					loc1.setImageResource(R.drawable.location);
					address2.setVisibility(View.VISIBLE);

				} else if (j == 1) {
					// ethi 2nd ta
					tag2.setText(pos);
					loc2.setImageResource(R.drawable.location);
					address3.setVisibility(View.VISIBLE);
				} else if (j == 2) {
					// ethi 3rd ta
					tag3.setText(pos);
					loc3.setImageResource(R.drawable.location);
					address4.setVisibility(View.VISIBLE);
				} else if (j == 3) {
					tag4.setText(pos);
					loc4.setImageResource(R.drawable.location);
					address5.setVisibility(View.VISIBLE);

				} else {
					tag5.setText(pos);
					loc5.setImageResource(R.drawable.location);

				}

			}
		}

	}

	private class tag_addressAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress_ad.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("userid", hm_userid);
				jsonObject.accumulate("addressid", id);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("unused")
		@Override
		protected void onPostExecute(String result) {
			/*
			 * super.onPostExecute(result); progressDialog.dismiss();
			 */

			JSONObject jsonResponse;

			try {
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {

					JSONObject jOb = jsonObject.getJSONObject("addressdetai");
					// JSONObject jOb = new JSONObject(result);
					/*
					 * result { "addressid": "2", "userid":
					 * "SDU201501","tag_addressname": "home", "cityname":
					 * "cuttack","cityid": "2", "centrename":
					 * "sailashree vihar", "centerid": "2", "circlename":
					 * "bigbazar","circleid": "1", "locationname":
					 * "patia college","locationid": "3","plot_detail":
					 * "vim-92 saliasheee vihar groundfloor bhubaneswar",
					 * "landmark_detail": "sbi bank atm" }
					 */
					if (jOb != null) {

						String tag = jOb.getString("tag_addressname");
						String city = jOb.getString("cityname");
						String center = jOb.getString("centrename");
						String circle = jOb.getString("circlename");
						String location = jOb.getString("locationname");
						String plot = jOb.getString("plot_detail");
						String landmark = jOb.getString("landmark_detail");
						tv_show_tag.setText(tag);
						tv_show_plot.setText(plot + ",");
						tv_show_landmark.setText("Near:" + landmark);
						tv_show_location.setText(location + ",");
						tv_show_circle.setText(circle + ",");
						tv_show_center.setText(center);
						tv_show_city.setText(city);
						// tv_set_add.setText(plot+","+location+"\n"+circle+","+center+","+city+"\n"+"LandMark:"+landmark);

						// Toast.makeText(getActivity(),
						// "Please Check Your Internet Connection.",
						// Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(getActivity(),
								"Please Check Your Internet Connection.",
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(getActivity(),
							"something is going wrong !!!", Toast.LENGTH_LONG)
							.show();
				}

			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}

			progress_ad.setVisibility(View.GONE);

			super.onPostExecute(result);
		}

	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void Dialog_class() {
		o_adr_id = id;
		if (adcolor == "r1") {
			address1.setBackgroundResource(R.drawable.appform);
			address2.setBackgroundResource(R.drawable.appformbackgrnd);
			address3.setBackgroundResource(R.drawable.appformbackgrnd);
			address4.setBackgroundResource(R.drawable.appformbackgrnd);
			address5.setBackgroundResource(R.drawable.appformbackgrnd);
			tag.setTextColor(getResources().getColor(R.color.white));
			tag2.setTextColor(getResources().getColor(R.color.text));
			tag3.setTextColor(getResources().getColor(R.color.text));
			tag4.setTextColor(getResources().getColor(R.color.text));
			tag5.setTextColor(getResources().getColor(R.color.text));
			adviewone = true;
		} else if (adcolor == "r2") {
			address1.setBackgroundResource(R.drawable.appformbackgrnd);
			address2.setBackgroundResource(R.drawable.appform);
			address3.setBackgroundResource(R.drawable.appformbackgrnd);
			address4.setBackgroundResource(R.drawable.appformbackgrnd);
			address5.setBackgroundResource(R.drawable.appformbackgrnd);
			tag.setTextColor(getResources().getColor(R.color.text));
			tag2.setTextColor(getResources().getColor(R.color.white));
			tag3.setTextColor(getResources().getColor(R.color.text));
			tag4.setTextColor(getResources().getColor(R.color.text));
			tag5.setTextColor(getResources().getColor(R.color.text));
			adviewtwo = true;
		} else if (adcolor == "r3") {
			address1.setBackgroundResource(R.drawable.appformbackgrnd);
			address2.setBackgroundResource(R.drawable.appformbackgrnd);
			address3.setBackgroundResource(R.drawable.appform);
			address4.setBackgroundResource(R.drawable.appformbackgrnd);
			address5.setBackgroundResource(R.drawable.appformbackgrnd);
			tag.setTextColor(getResources().getColor(R.color.text));
			tag2.setTextColor(getResources().getColor(R.color.text));
			tag3.setTextColor(getResources().getColor(R.color.white));
			tag4.setTextColor(getResources().getColor(R.color.text));
			tag5.setTextColor(getResources().getColor(R.color.text));
			adviewthree = true;
		} else if (adcolor == "r4") {
			address1.setBackgroundResource(R.drawable.appformbackgrnd);
			address2.setBackgroundResource(R.drawable.appformbackgrnd);
			address3.setBackgroundResource(R.drawable.appformbackgrnd);
			address4.setBackgroundResource(R.drawable.appform);
			address5.setBackgroundResource(R.drawable.appformbackgrnd);
			tag.setTextColor(getResources().getColor(R.color.text));
			tag2.setTextColor(getResources().getColor(R.color.text));
			tag3.setTextColor(getResources().getColor(R.color.text));
			tag4.setTextColor(getResources().getColor(R.color.white));
			tag5.setTextColor(getResources().getColor(R.color.text));
			adviewfour = true;
		} else {
			address1.setBackgroundResource(R.drawable.appformbackgrnd);
			address2.setBackgroundResource(R.drawable.appformbackgrnd);
			address3.setBackgroundResource(R.drawable.appformbackgrnd);
			address4.setBackgroundResource(R.drawable.appformbackgrnd);
			address5.setBackgroundResource(R.drawable.appform);
			tag.setTextColor(getResources().getColor(R.color.text));
			tag2.setTextColor(getResources().getColor(R.color.text));
			tag3.setTextColor(getResources().getColor(R.color.text));
			tag4.setTextColor(getResources().getColor(R.color.text));
			tag5.setTextColor(getResources().getColor(R.color.white));
			adviewfive = true;
		}
		
	}

	private class del_addressAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("addressid", id);
				jsonObject.accumulate("userid", hm_userid);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject jObject = new JSONObject(result);
				String status = jObject.getString("status");
				if (status.equals("0")) {

					Toast.makeText(getActivity(), "Try Again!",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getActivity(), "Successfully deleted!",
							Toast.LENGTH_LONG).show();
					// new display_tagAsyncTask().execute(ServerLinks.view_tag);
					refresh_fragment();

				}
			} catch (Exception e) {

			}
			new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					// progress.setVisibility(View.GONE);
				}
			};

			super.onPostExecute(result);

		}
	}

	private class Wash_orderAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			progress1.setVisibility(View.VISIBLE);

			/*
			 * progressDialog = new ProgressDialog(getActivity());
			 * progressDialog.setMessage("Loding In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
			// progress1.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("p_date", date);
				jsonObject.accumulate("d_date", dld);
				jsonObject.accumulate("p_shiftid", shif_id);
				jsonObject.accumulate("d_shiftid", shif_id1);
				jsonObject.accumulate("addressid", o_adr_id);
				jsonObject.accumulate("washtype", o_wash_id);
				jsonObject.accumulate("userid", hm_userid);
				jsonObject.accumulate("status", m_status);
				jsonObject.accumulate("promo", fnl_promocode);
				json = jsonObject.toString();
//{"p_date":"06-11-2015",
// "d_date":"07-11-2015",
// "p_shiftid":"6",
// "d_shiftid":"6",
// "addressid":"23",
// "washtype":"[\"12\", \"13\"]",
// "userid":"SDU201505","status":"1",
// "promo":"qwerty"}
				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
//{"status":"1","order_id":"SDR04111501","msg":"data is  saved"}
			progress1.setVisibility(View.GONE);

			try {
				JSONObject jObject = new JSONObject(result);
				String status = jObject.getString("status");
				if (status.equals("0")) {

					Toast.makeText(getActivity(), "Try Again!", Toast.LENGTH_SHORT).show();
				}
				else if (status.equals("1")){
					//orderid_promo = jObject.getString("order_id");
					Toast.makeText(getActivity(), "Successfully Ordered!", Toast.LENGTH_SHORT)
							.show();
					promo_dialog.dismiss();
					mFragmentManager = getFragmentManager();
					mFragmentTransaction = mFragmentManager.beginTransaction();
					mFragmentTransaction.replace(R.id.containerView,new Myorderfragment()).commit();

					/*Fragment fragment = new Myorderfragment();
					FragmentManager fragmentManager = getActivity()
							.getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();*/
					//final Dialog promo_dialog = new Dialog(getActivity());
					/* promo_dialog = new Dialog(getActivity());
					LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View view = inflater.inflate(R.layout.promocode, null, false);
					promo_dialog.setCanceledOnTouchOutside(false);
					promo_dialog.setContentView(view);

					promo_dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					promo_dialog.setTitle("Enter Promocode");
					promo_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
					final EditText promocode=(EditText) promo_dialog.findViewById(R.id.et_promo);
					Button send_promo= (Button) promo_dialog.findViewById(R.id.promo_send);
					send_promo.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							promo=promocode.getText().toString();
							if (promo.equals("")){
								Toast.makeText(getActivity(),"Enter promocode",Toast.LENGTH_SHORT).show();
								promo_dialog.show();
							}
							else{
								new promo_entry().execute(ServerLinks.promo_code_);
								promo_dialog.dismiss();
							}

						}
					});
					promo_dialog.show();*/

				}
				else if(status.equals("4")) {
					Toast.makeText(getActivity(), "Successfully Ordered!", Toast.LENGTH_SHORT)
							.show();
					promo_dialog.dismiss();
					mFragmentManager = getFragmentManager();
					mFragmentTransaction = mFragmentManager.beginTransaction();
					mFragmentTransaction.replace(R.id.containerView,new Myorderfragment()).commit();
					/*Fragment fragment = new Myorderfragment();
					FragmentManager fragmentManager = getActivity()
							.getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();*/

				}
			} catch (Exception e) {

			}
			new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					// progress.setVisibility(View.GONE);
				}
			};

			super.onPostExecute(result);

		}
	}

	// for validate promo_code
	private class promo_entry extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress1.setVisibility(View.VISIBLE);

		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				//jsonObject.accumulate("order_id", orderid_promo);
				jsonObject.accumulate("customerid", hm_userid);
				jsonObject.accumulate("promo", promo);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			progress1.setVisibility(View.GONE);
			try {
				JSONObject jObject = new JSONObject(result);
				String status = jObject.getString("status");
				if (status.equals("0")) {

					Toast.makeText(getActivity(), "information is incomplete",
							Toast.LENGTH_LONG).show();
				}
				else if (status.equals("1")){
					//msg Here is the Promocode used success
					Toast.makeText(getActivity(), "PromoCode Successfully Applied!",
							Toast.LENGTH_LONG).show();
					fnl_promocode=promo;
					/*Fragment fragment = new Myorderfragment();
					FragmentManager fragmentManager = getActivity()
							.getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
*/
				}
				else if (status.equals("2")){
					//msg Here is the Promocode Already  used by you success
					Toast.makeText(getActivity(), "This promocode already used!! Enter another.",
							Toast.LENGTH_LONG).show();
					promo_details.setText("This Promo Code is already being used!!");
					promo_dialog.show();
				}
				else if (status.equals("3")){
					//msg Here is the Promo code is Invalid
					Toast.makeText(getActivity(), "Invlid Promocode",
							Toast.LENGTH_LONG).show();
					promo_details.setText("Invalid PromoCode!!");
					promo_dialog.show();
				}
				else if (status.equals("4")){
					//msg Here is the promocode blank
					//gt-555
					Toast.makeText(getActivity(), "Enter promocode",
							Toast.LENGTH_LONG).show();
					promo_dialog.show();
				}
			} catch (Exception e) {

			}
			new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					// progress.setVisibility(View.GONE);
				}
			};

			super.onPostExecute(result);

		}
	}
// for checking membership or not
	private class Check_memberAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);

		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				//jsonObject.accumulate("order_id", orderid_promo);
				jsonObject.accumulate("userid", hm_userid);//{"userid":"SDU201505"}
				//jsonObject.accumulate("promo", promo);
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {

				progress.setVisibility(View.GONE);
			try {
				JSONObject jObject = new JSONObject(result);
				//{"status": "1",
				// "msg": "Non - membership"}


				String status = jObject.getString("status");
				if (status.equals("0")) {

					Toast.makeText(getActivity(), "information is incomplete",
							Toast.LENGTH_LONG).show();
				}
				else if (status.equals("1")){
					//msg Here is the Promocode used success
					Toast.makeText(getActivity(), "Dont have any membership plan",
							Toast.LENGTH_LONG).show();
					m_status="1";

					promo_dialog = new Dialog(getActivity());
					LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View view = inflater.inflate(R.layout.promocode, null, false);
					promo_dialog.setCanceledOnTouchOutside(false);
					promo_dialog.setContentView(view);

					promo_dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					promo_dialog.setTitle("Order Summary");
					promo_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

					progress1 = (FrameLayout) promo_dialog.findViewById(R.id.frame_progress1);
					final EditText promocode=(EditText) promo_dialog.findViewById(R.id.et_promo);
					TextView apply=(TextView) promo_dialog.findViewById(R.id.apply_code);
					promo_details=(TextView) promo_dialog.findViewById(R.id.promo_details);
					TextView pr_pick_date=(TextView) promo_dialog.findViewById(R.id.prm_pick_Date);
					TextView prm_wash_type=(TextView) promo_dialog.findViewById(R.id.prm_wash_type);
					TextView pr_pick_time=(TextView) promo_dialog.findViewById(R.id.prm_pick_Time);
					TextView pr_del_date=(TextView) promo_dialog.findViewById(R.id.prm_del_Date);
					TextView pr_del_time=(TextView) promo_dialog.findViewById(R.id.prm_del_time);
					pr_pick_time.setText(slot);
					pr_pick_date.setText(date);
					pr_del_time.setText(slot1);
					pr_del_date.setText(dld);

					for (int j=0;j<wash_type_name.size();j++){

						if (j==0){
							 w= wash_type_name.get(j);
						}
						else{
							w=w+","+wash_type_name.get(j);
						}

					}
					prm_wash_type.setText(w);
					Button send_promo= (Button) promo_dialog.findViewById(R.id.promo_send);
					Button cancel_promo= (Button) promo_dialog.findViewById(R.id.promo_cancel);

					apply.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							promo=promocode.getText().toString();
							if (promo.equals("")){
								Toast.makeText(getActivity(),"Enter promocode",Toast.LENGTH_SHORT).show();
								//promo_dialog.show();
							}
							else{
								new promo_entry().execute(ServerLinks.promo_code_);

							}

						}
					});

					send_promo.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							new Wash_orderAsyncTask().execute(ServerLinks.wash_order);
							/*promo=promocode.getText().toString();
							if (promo.equals("")){
								Toast.makeText(getActivity(),"Enter promocode",Toast.LENGTH_SHORT).show();
								promo_dialog.show();
							}
							else{
								new promo_entry().execute(ServerLinks.promo_code_);
								promo_dialog.dismiss();
							}*/

						}
					});
					cancel_promo.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							refresh_fragment();
							promo_dialog.dismiss();
							/*promo=promocode.getText().toString();
							if (promo.equals("")){
								Toast.makeText(getActivity(),"Enter promocode",Toast.LENGTH_SHORT).show();
								promo_dialog.show();
							}
							else{
								new promo_entry().execute(ServerLinks.promo_code_);
								promo_dialog.dismiss();
							}*/

						}
					});
					promo_dialog.show();


					/*Fragment fragment = new Myorderfragment();
					FragmentManager fragmentManager = getActivity()
							.getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, fragment);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();*/

				}
				else if (status.equals("2")){
					//msg Here is the Promocode Already  used by you success
					Toast.makeText(getActivity(), "having membership plan",
							Toast.LENGTH_LONG).show();
					m_status="2";
					promo_dialog.show();
				}
				/*else if (status.equals("3")){
					//msg Here is the Promo code is Invalid
					Toast.makeText(getActivity(), "Invlid Promocode",
							Toast.LENGTH_LONG).show();
					promo_dialog.show();
				}
				else if (status.equals("4")){
					//msg Here is the promocode blank
					//gt-555
					Toast.makeText(getActivity(), "Enter promocode",
							Toast.LENGTH_LONG).show();
					promo_dialog.show();
				}*/
			} catch (Exception e) {

			}
			new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					// progress.setVisibility(View.GONE);
				}
			};

			super.onPostExecute(result);

		}
	}

	public void DatendTimeActivity() {
		Date now = new Date();
		Date alsoNow = Calendar.getInstance().getTime();
		date = new SimpleDateFormat("dd-MM-yyyy").format(now);
		int month = now.getMonth();
		int d = now.getDate();
		int yr = now.getYear();
		pick_date.setText(date);

		final Calendar c = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		c.add(Calendar.DAY_OF_YEAR, 2);
		Date tomorrow = c.getTime();
		dld = dateFormat.format(tomorrow);
		del_date.setText(dld);

		new Time_slotAsyncTask().execute(ServerLinks.slot_time);
		new Time_slotAsyncTask1().execute(ServerLinks.slot_time);

		mcurrentTime = Calendar.getInstance();
		dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

		show_pdate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				fromDatePickerDialog = new DatePickerDialog(getActivity(),
						new OnDateSetListener() {

							public void onDateSet(DatePicker view, int year,
									int monthOfYear, int dayOfMonth) {
								year1 = view.getYear();
								monthOfYear1 = view.getMonth();
								dayOfMonth1 = view.getDayOfMonth() + 02;
								Calendar newDate = Calendar.getInstance();
								newDate.set(year, monthOfYear, dayOfMonth);

								Calendar n = Calendar.getInstance();
								n.set(year, monthOfYear, dayOfMonth + 02);

								date = dateFormatter.format(newDate.getTime());
								dld = dateFormatter.format(n.getTime());
								pick_date.setText(date);
								del_date.setText(dld);

								if (date != "")
									new Time_slotAsyncTask()
											.execute(ServerLinks.slot_time);
								if (dld != "")
									new Time_slotAsyncTask1()
											.execute(ServerLinks.slot_time);
								// save.setEnabled(true);
							}

						}, mcurrentTime.get(Calendar.YEAR), mcurrentTime
								.get(Calendar.MONTH), mcurrentTime
								.get(Calendar.DAY_OF_MONTH));
				// fromDatePickerDialog.getDatePicker();

				fromDatePickerDialog.getDatePicker().setMinDate(
						System.currentTimeMillis() - 1000);

				fromDatePickerDialog.show();

			}
		});

		// del_date.setText(dld);

		mcurrentTime2 = Calendar.getInstance();
		dateFormatter2 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

		show_ddate.setOnClickListener(new View.OnClickListener() {

			@TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Calendar c = Calendar.getInstance();
				c.set(year1, monthOfYear1, dayOfMonth1, 0, 0);
				long mili = c.getTimeInMillis();
				fromDatePickerDialog2 = new DatePickerDialog(getActivity(),
						new OnDateSetListener() {

							public void onDateSet(DatePicker view, int year,
									int monthOfYear, int dayOfMonth) {

								Calendar newDate1 = Calendar.getInstance();
								newDate1.set(year, monthOfYear, dayOfMonth);
								mills = newDate1.getTimeInMillis();
								dld = dateFormatter2.format(newDate1.getTime());
								// dld=dateFormatter.format(n.getTime());
								del_date.setText(dld);

								if (dld != "")
									new Time_slotAsyncTask1()
											.execute(ServerLinks.slot_time);
								// save2.setEnabled(true);
							}
						}, mcurrentTime2.get(Calendar.YEAR), mcurrentTime2
								.get(Calendar.MONTH), dayOfMonth1);

				fromDatePickerDialog2.getDatePicker().setMinDate(mili - 1000);
				fromDatePickerDialog2.show();
			}
		});
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void throwActivity() {
		Intent in = new Intent(getActivity(), Addlocationnew.class);
		startActivityForResult(in, REQUEST_CODE);
	}

	public void refresh_fragment() {
		Fragment frg = null;
		mFragmentManager = getFragmentManager();
		mFragmentTransaction = mFragmentManager.beginTransaction();
		mFragmentTransaction.detach(frg);
		mFragmentTransaction.attach(frg);
		mFragmentTransaction.commit();
		//mFragmentTransaction.replace(R.id.containerView,new Myorderfragment()).commit();
		/*frg = getFragmentManager().findFragmentById(R.id.content_frame);
		final FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.detach(frg);
		ft.attach(frg);
		ft.commit();*/
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

	//if (resultCode == getActivity().RESULT_OK) {
		//	if (requestCode == REQUEST_CODE)
				if(resultCode==6 && requestCode == REQUEST_CODE ){
				try {

					new display_tagAsyncTask().execute(ServerLinks.view_tag);
				} catch (Exception e) {



				}

			}
		}
	//}

	@Override
	public void onAttach(Activity activity) {
		homeContext = (MainActivity) activity;
		super.onAttach(activity);
	}

	/*public void onBackPressed() {

		FragmentManager fm = homeContext.getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Mainhomefragment llf = new Mainhomefragment();
		ft.replace(R.id.content_frame, llf);
		ft.commit();
	}*/
}

package com.superdhobi.navigation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.superdhobi.AdapterPackage.My_plan_adapter;
import com.superdhobi.AdapterPackage.PickSlot_recycler_adapter;
import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 26-04-2016.
 */
public class Membership_myplan extends Fragment {
    RecyclerView my_plan_name;

    String[] my_plan_list={"Super Wash Monthly","Super Wash Family","Dry Clean Monthly","Dry Clean Family"};
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.membership_myplan, container, false);
        my_plan_name= (RecyclerView) rootView.findViewById(R.id.recycler_view_myplan);

        My_plan_adapter my_plan_adapter= new My_plan_adapter(getActivity(),my_plan_list);

// Setup layout manager for items
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

// Control orientation of the items
// also supports LinearLayoutManager.HORIZONTAL
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
// Optionally customize the position you want to default scroll to
        layoutManager.scrollToPosition(0);
// Attach layout manager to the RecyclerView
        my_plan_name.setLayoutManager(layoutManager);
        my_plan_name.setAdapter(my_plan_adapter);

        return rootView;
    }
}

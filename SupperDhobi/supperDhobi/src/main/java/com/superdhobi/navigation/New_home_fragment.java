package com.superdhobi.navigation;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdhobi.supperdhobi.DividerItemDecoration;
import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 03-03-2016.
 */
public class New_home_fragment extends Fragment {
    String[] cloth={"Shirt", "Pant", "T_shirt","Shirt", "Pant", "T_shirt"};
    RecyclerView men_cloth,women_cloth,house_cloth;
    LinearLayout men_lay,women_lay,house_lay,men_head,women_head,house_head,m_select,w_select,h_select;
    ImageView men_icon,women_icon,house_icon, men_up,men_d,women_up,women_d,hs_up,hs_d;
    TextView men,women,house;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.new_home, container, false);
        men_lay= (LinearLayout) rootView.findViewById(R.id.men_list);
        women_lay= (LinearLayout) rootView.findViewById(R.id.women_list);
        house_lay= (LinearLayout) rootView.findViewById(R.id.house_list);
        men_head= (LinearLayout) rootView.findViewById(R.id.men_header);
        women_head= (LinearLayout) rootView.findViewById(R.id.women_header);
        house_head= (LinearLayout) rootView.findViewById(R.id.house_header);


        men_icon= (ImageView) rootView.findViewById(R.id.men_image);
        women_icon= (ImageView) rootView.findViewById(R.id.women_image);
        house_icon= (ImageView) rootView.findViewById(R.id.house_image);
        men= (TextView) rootView.findViewById(R.id.m);
        women= (TextView) rootView.findViewById(R.id.wm);
        house= (TextView) rootView.findViewById(R.id.hs);



        men_cloth= (RecyclerView) rootView.findViewById(R.id.recycler_view_men);
        women_cloth= (RecyclerView) rootView.findViewById(R.id.recycler_view_women);
        house_cloth= (RecyclerView) rootView.findViewById(R.id.recycler_view_house);

        men_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                men_icon.setImageResource(R.drawable.men_act);
                men_head.setBackgroundColor(getResources().getColor(R.color.accordion_header_select));
                men_lay.setVisibility(View.VISIBLE);
                men.setTextColor(getResources().getColor(R.color.white));

                women_icon.setImageResource(R.drawable.wmn);
                women_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                women.setTextColor(getResources().getColor(R.color.accordion_header_select));
                women_lay.setVisibility(View.GONE);

                house_icon.setImageResource(R.drawable.household);
                house_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                house.setTextColor(getResources().getColor(R.color.accordion_header_select));
                house_lay.setVisibility(View.GONE);
            }
        });

        women_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                women_icon.setImageResource(R.drawable.wmn_act);
                women_head.setBackgroundColor(getResources().getColor(R.color.accordion_header_select));
                women.setTextColor(getResources().getColor(R.color.white));
                women_lay.setVisibility(View.VISIBLE);

                men_icon.setImageResource(R.drawable.men);
                men_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                men.setTextColor(getResources().getColor(R.color.accordion_header_select));
                men_lay.setVisibility(View.GONE);

                house_icon.setImageResource(R.drawable.household);
                house_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                house.setTextColor(getResources().getColor(R.color.accordion_header_select));
                house_lay.setVisibility(View.GONE);
            }
        });

        house_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                men_icon.setImageResource(R.drawable.men);
                men_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                men.setTextColor(getResources().getColor(R.color.accordion_header_select));
                men_lay.setVisibility(View.GONE);

                women_icon.setImageResource(R.drawable.wmn);
                women_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                women.setTextColor(getResources().getColor(R.color.accordion_header_select));
                women_lay.setVisibility(View.GONE);

                house_icon.setImageResource(R.drawable.household_act);
                house_head.setBackgroundColor(getResources().getColor(R.color.accordion_header_select));
                house.setTextColor(getResources().getColor(R.color.white));
                house_lay.setVisibility(View.VISIBLE);

            }
        });

//adapter set into recyclerview
        Recycler_adapter_hme recycler_adapter_hme= new Recycler_adapter_hme(getActivity(),cloth);

        men_cloth.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        men_cloth.setLayoutManager(mLayoutManager);
        men_cloth.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        men_cloth.setItemAnimator(new DefaultItemAnimator());

        women_cloth.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        women_cloth.setLayoutManager(mLayoutManager1);
        women_cloth.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        women_cloth.setItemAnimator(new DefaultItemAnimator());

        house_cloth.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getActivity());
        house_cloth.setLayoutManager(mLayoutManager2);
        house_cloth.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        house_cloth.setItemAnimator(new DefaultItemAnimator());

        men_cloth.setAdapter(recycler_adapter_hme);// set adapter
        women_cloth.setAdapter(recycler_adapter_hme);
        house_cloth.setAdapter(recycler_adapter_hme);
        return rootView;
    }

    public class Recycler_adapter_hme extends RecyclerView.Adapter<Recycler_adapter_hme.MyViewHolder> {
        private String[] cloth;


        Context context;
        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, year, genre;

            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.cloth_name);

            }
        }

        public Recycler_adapter_hme( Context context,String[] cloth) {
            this.cloth = cloth;
            this.context = context;
        }



        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.home_custom, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.title.setText(cloth[position]);
        }





        @Override
        public int getItemCount() {
            return cloth.length;
        }
    }
}

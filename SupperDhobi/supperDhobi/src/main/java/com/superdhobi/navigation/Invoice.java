package com.superdhobi.navigation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by PCIS-ANDROID on 10-02-2016.
 */
public class Invoice extends Activity {
    static String order_ref,user_id,invoiceno_;
    TextView orderef,invoiceno,picktnd,deltnd,del_status,totalprice;
    ListView invoicelist;
    String[] clothes, prices,quantity,total;
    ImageView back_in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.invoice);
        order_ref=getIntent().getStringExtra("ODR_REF");
        user_id=getIntent().getStringExtra("U_ID_O");

        orderef= (TextView) findViewById(R.id.de_order_id);
        invoiceno= (TextView) findViewById(R.id.invoice_no);
        picktnd= (TextView) findViewById(R.id.de_pick_Date);
        deltnd= (TextView) findViewById(R.id.de_dlvr_Date);
       // del_status= (TextView) findViewById(R.id.del_status);
        invoicelist= (ListView) findViewById(R.id.invoice_list);
       // back_in= (ImageView) findViewById(R.id.back_in);
        //userid/orderids
       /* totalcloth= (TextView) findViewById(R.id.cloth_status);
        */

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        totalprice= (TextView) findViewById(R.id.show_total);
        orderef.setText(order_ref);

        new invoice_DetailsAsyntask().execute(ServerLinks.user_invoice);
       /* back_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
    }

    private class invoice_DetailsAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog ;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("userid", user_id);
                jsonObject.accumulate("orderre", order_ref);
                //userid/orderids


                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //progress.setVisibility(View.GONE);
            JSONObject jsonResponse;

            try {
                if (!result.equals("")){
                JSONObject jsonObject=new JSONObject(result);
               /*
    {
    {
        "status": "1",
        "data": "{  "data":[{"cloth":"SHIRT / T-SHIRT(Iron)","price":"4","quantity":"6","subtotal":24},
                            {"cloth":"JEANS / PANT / TROUSER(Iron)","price":"4","quantity":"6","subtotal":24}],
                  "InvoiceNo":"SDI14121506",
                   "OrderRequest":"SDR141215038",
                   "OrderNo":"SDO141215020",
                   "PickDate":"14-Dec-2015",
                  "DeliveryDate":"15-Dec-2015",
                  "CustomerId":"SDU00350",
                  "CustomerName":"R.SG",
                  "Emailid":"",
                  "Mobile No":"9672978942",
                  "Total":48}"
    }
*/
                String status=jsonObject.optString("status");

                if(status.equals("1")) {
                    JSONObject jo = jsonObject.getJSONObject("data");

                    if(jo.equals("")){

                    }
                    else{

                        String invo=jo.optString("InvoiceNo");
                        String order_req=jo.optString("OrderRequest");
                        String order_no=jo.optString("OrderNo");
                        String pdate=jo.optString("PickDate");
                        String ddate=jo.optString("DeliveryDate");
                        String cid=jo.optString("CustomerId");
                        String cname=jo.optString("CustomerName");
                        String cemail=jo.optString("Emailid");
                        String cmbl=jo.optString("Mobile No");
                        String invo_tot=jo.optString("Total");
                    JSONArray jArr = jo.getJSONArray("data");
                    if (jArr != null) {
                       // invoiceno_=jArr
                        clothes = new String[jArr.length()];
                        prices = new String[jArr.length()];
                        quantity = new String[jArr.length()];
                        total = new String[jArr.length()];
                        for (int i = 0; i < jArr.length(); i++) {
                            jsonResponse = jArr.getJSONObject(i);

                            clothes[i] = jsonResponse.getString("cloth");
                            prices[i] = jsonResponse.getString("price");
                            quantity[i] = jsonResponse.getString("quantity");
                            total[i] = jsonResponse.getString("subtotal");

                        }
                    } else {
                        Toast.makeText(Invoice.this, "You have no adress",
                                Toast.LENGTH_SHORT).show();
                    }
                       // invoiceno,picktnd,deltnd
                        invoiceno.setText(invo);
                        picktnd.setText(pdate);
                        deltnd.setText(ddate);
                        totalprice.setText("Total: ₹"+invo_tot);
                        // android:text="Total: ₹ 450.00"
                    }
                }
                    Invoice_list_adapter adapter = new Invoice_list_adapter(Invoice.this, clothes, prices,quantity,total);

                    invoicelist.setAdapter(adapter);
                }
                else{
                    Toast.makeText(Invoice.this, "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            catch(Exception e){
                Log.e("String " , e.toString());
            }

        }
    }

    public class Invoice_list_adapter extends ArrayAdapter<String> {
        private final Activity context;
        private final String[] clothes;
        private final String[] prices;
        private final String[] quantity;
        private final String[] total;
        //clothes, prices,quantity,total

        public Invoice_list_adapter(Activity context, String[] clothes,
                                  String[] prices, String[] quantity, String[] total) {
            super(context, R.layout.invoice_custom, clothes);
            // TODO Auto-generated constructor stub
            this.context = context;
            this.clothes = clothes;
            this.prices = prices;
            this.quantity = quantity;
            this.total = total;
        }

        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.invoice_custom, null, true);
            TextView item = (TextView) rowView.findViewById(R.id.item);
            TextView item_price = (TextView) rowView.findViewById(R.id.item_price);
            TextView item_quantity = (TextView) rowView.findViewById(R.id.item_qt);
            TextView item_total = (TextView) rowView.findViewById(R.id.item_total);

            item.setText(clothes[position]);
            item_price.setText(prices[position]);
            item_quantity.setText(quantity[position]);
            item_total.setText(total[position]);

            return rowView;
        }
    }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

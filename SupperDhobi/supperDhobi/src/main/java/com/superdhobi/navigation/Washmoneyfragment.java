package com.superdhobi.navigation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.All_plan_adapter;
import com.superdhobi.AdapterPackage.All_plan_order_adapter;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Washmoneyfragment extends Fragment{
	
	private SharedPreferences shp;
	static MainActivity washmoneyContext;
	TextView membershipplan, myplan,morder;
	TextView t_plan,t_limit,t_category,t_total,t_left,t_used,t_dis;
	TextView plan_name_,membership_type,membership_category,discount_on,discoumt_per,maxm_order,valid_plan;
	LinearLayout my_plan,planlist,plan_ord;
	ListView plan_name,plan_order_list;
	static  String custmer_id,wallet_amount;
	FrameLayout progress,progress1;
	static  String m_id,cancel_id;
	String[] discount;
	String[] cate_discount;
	String[] plan_list,plan_list_id,plan_orderid,plan_quant;
	static String p_name,p_m_id,p_m_type,p_m_cat,p_m_dispr,p_m_price,p_m_valid;
	String str="";
	Dialog buy_dialog;
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		shp=washmoneyContext.getSharedPreferences("AuthToken",Context.MODE_PRIVATE);
		SharedPreferences.Editor shpEdit = shp.edit();
    	shpEdit.putString("start_home", "from_washmoney");
    	shpEdit.commit();
		
		View rootView=inflater.inflate(R.layout.washmoney, container, false);

		shp = getActivity().getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);
		//progress = (FrameLayout) rootView.findViewById(R.id.frame_progress);
		custmer_id = shp.getString("USER_UID", null);
		wallet_amount = shp.getString("WALLET", null);
        progress= (FrameLayout) rootView.findViewById(R.id.frame_progress);
		membershipplan= (TextView) rootView.findViewById(R.id.m_plan);
		myplan= (TextView) rootView.findViewById(R.id.my_plan);
		morder= (TextView) rootView.findViewById(R.id.view_m_order);
		my_plan= (LinearLayout) rootView.findViewById(R.id.my_membership_plan);
		planlist= (LinearLayout) rootView.findViewById(R.id.list_plan);
		plan_ord= (LinearLayout) rootView.findViewById(R.id.list_order_plan);
		plan_name= (ListView) rootView.findViewById(R.id.list_plan_name);
		plan_order_list= (ListView) rootView.findViewById(R.id.list_order_p);
		t_plan= (TextView) rootView.findViewById(R.id.plan);
		t_limit= (TextView) rootView.findViewById(R.id.limit);
		t_category= (TextView) rootView.findViewById(R.id.category);
		t_total= (TextView) rootView.findViewById(R.id.total);
		t_left= (TextView) rootView.findViewById(R.id.left);
		t_used= (TextView) rootView.findViewById(R.id.used);
		t_dis= (TextView) rootView.findViewById(R.id.dis);
		//setListViewHeightBasedOnChildren(plan_name);
	//	new allplan_tagAsyncTask().execute(ServerLinks.num_plan);

		myplan.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				my_plan.setVisibility(View.VISIBLE);
				planlist.setVisibility(View.GONE);
				plan_ord.setVisibility(View.GONE);




				myplan.setBackgroundColor(getResources().getColor(R.color.app_color));
				membershipplan.setBackgroundColor(getResources().getColor(R.color.text));
				morder.setBackgroundColor(getResources().getColor(R.color.text));
				new myplan_tagAsyncTask().execute(ServerLinks.my_plan);

			}
		});
		membershipplan.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				my_plan.setVisibility(View.GONE);
				planlist.setVisibility(View.VISIBLE);
				plan_ord.setVisibility(View.GONE);

				myplan.setBackgroundColor(getResources().getColor(R.color.text));
				membershipplan.setBackgroundColor(getResources().getColor(R.color.app_color));
				morder.setBackgroundColor(getResources().getColor(R.color.text));
			}
		});
		plan_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				m_id = plan_list_id[position];
				dialog_planActivity();
				//new deatailplan_tagAsyncTask().execute(ServerLinks.num_plan);
			}
		});
		morder.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				my_plan.setVisibility(View.GONE);
				planlist.setVisibility(View.GONE);
				plan_ord.setVisibility(View.VISIBLE);

				myplan.setBackgroundColor(getResources().getColor(R.color.text));
				membershipplan.setBackgroundColor(getResources().getColor(R.color.text));
				morder.setBackgroundColor(getResources().getColor(R.color.app_color));
				new myplan_order_tagAsyncTask().execute(ServerLinks.my_plan_order);
			}
		});

		return rootView;
	}


	private class allplan_tagAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
			getActivity().getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("customerid", custmer_id);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
			getActivity().getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

			JSONObject jsonResponse;

			try {
				if (!result.equals("")){
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					//planlist.setVisibility(View.VISIBLE);
					JSONArray jArr = jsonObject.getJSONArray("membership_plan");
					if (jArr != null) {
						plan_list = new String[jArr.length()];
						plan_list_id = new String[jArr.length()];

						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);

							plan_list[i] = jsonResponse.getString("plan_name");
							plan_list_id[i] = jsonResponse.getString("membership_plan_ids");

						}
					}

					/*All_plan_adapter all_plan_adapter =new All_plan_adapter(getActivity(),plan_list,plan_list_id);

					plan_name.setAdapter(all_plan_adapter);*/
				} else {
					Toast.makeText(getActivity(), "There is no membership plan.",Toast.LENGTH_SHORT).show();
				}
				}
				else{
					Toast.makeText(getActivity(), "Check your Internet  Connection!!",
							Toast.LENGTH_SHORT).show();
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}


		}

	}

	private class myplan_order_tagAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
			getActivity().getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("customerid", custmer_id);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
			getActivity().getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

			JSONObject jsonResponse;
//{"status":"1","order_id":[{"order":"0","quantityPlaced":"0"}]}
			try {
				if (!result.equals("")){
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					//planlist.setVisibility(View.VISIBLE);
					JSONArray jArr = jsonObject.getJSONArray("order_id");
					if (jArr != null) {
						plan_orderid = new String[jArr.length()];
						plan_quant = new String[jArr.length()];

						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);

							plan_orderid[i] = jsonResponse.getString("order");
							plan_quant[i] = jsonResponse.getString("quantityPlaced");

						}
					}

					All_plan_order_adapter all_plan_order_adapter =new All_plan_order_adapter(getActivity(),plan_orderid,plan_quant);

					plan_order_list.setAdapter(all_plan_order_adapter);
				} else {
					//Toast.makeText(getActivity(), "There is no order using membership plan.",Toast.LENGTH_SHORT).show();
				}

			}
			else{
				Toast.makeText(getActivity(), "Check your Internet  Connection!!",
						Toast.LENGTH_SHORT).show();
			}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}


		}

	}

	private class deatailplan_tagAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress1.setVisibility(View.VISIBLE);
			getActivity().getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("customerid", custmer_id);
				jsonObject.accumulate("membership_plan_ids", m_id);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress1.setVisibility(View.GONE);
			getActivity().getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

			JSONObject jsonResponse;
//{"status":"1","msg":"Here is the plan for membership",
// "membership_plan":[
// {"plan_name":"New deluxplan",
// "membership_plan_ids":"SDMI201501",
// "Membership_Type":"Family",
// "Membership_Category":"Normal wash",
// "discout_on":["Dry Wash","Super Wash"],
// "discount_per":"20",
// "price_of_membership":"600",
// "validation_membership_days":"30"}
// ]}
			try {
				if (!result.equals("")){
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					JSONArray jo = jsonObject.getJSONArray("membership_plan");

					for (int i = 0; i < jo.length(); i++) {

						JSONObject jobj = jo.getJSONObject(i);
						p_name=jobj.getString("plan_name");
						p_m_id=jobj.getString("membership_plan_ids");
						p_m_type=jobj.getString("Membership_Type");
						p_m_cat=jobj.getString("Membership_Category");
						p_m_dispr=jobj.getString("discount_per");
						p_m_price=jobj.getString("price_of_membership");
						p_m_valid=jobj.getString("validation_membership_days");


						JSONArray jArr=jobj.getJSONArray("discout_on");
						discount = new String[jArr.length()];
						for(int j = 0; j < jArr.length(); j++){




								discount[j] = jArr.getString(j);

								discount_on.setText(discount_on.getText().toString()+","+discount[j]);



						}

					}


					plan_name_.setText(p_name);
					membership_type.setText(p_m_type);
					membership_category.setText(p_m_cat);
					discoumt_per.setText(p_m_dispr);
					maxm_order.setText("Rs."+p_m_price);
					valid_plan.setText(p_m_valid + "Days");

				} else {
					//Toast.makeText(getActivity(), "There is no membership plan.",Toast.LENGTH_SHORT).show();
				}
				}
				else{
					Toast.makeText(getActivity(), "Check your Internet  Connection!!",
							Toast.LENGTH_SHORT).show();
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}


		}

	}
	private class myplan_tagAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog ;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("customerid", custmer_id);
//{"customerid":"SDU2015019"}

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if(inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}
		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("unused")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);

        	/*washtype=new ArrayList<String>();
            wash_id=new ArrayList<String>();*/

			JSONObject jsonResponse;

//{"status":"2","msg":"information is Ur membership is Expired"}
			try {
				if (!result.equals("")){
				JSONObject jsonObject=new JSONObject(result);

//{"status":"1",
// "TotalClothLimit":"60",
// "LeftClothLimit":60,
// "membershipPlan":"New deluxplan",
// "Startdate":"2015-10-05",
// "Enddate":"2015-11-04",
// "discount":"20",
// "category_wash":"Normal wash",
// "category_wash_discount":["Dry Wash","Super Wash"],
// "membership_id":"SDAM201502",
// "used_clothlimit":0}


				String status=jsonObject.optString("status");

				if(status.equals("1")){

					String planname=jsonObject.optString("membershipPlan");
					String startdate=jsonObject.optString("Startdate");
					String enddate=jsonObject.optString("Enddate");
					String offer=jsonObject.optString("category_wash");
					String clothlimit=jsonObject.optString("TotalClothLimit");
					String leftcloth=jsonObject.optString("LeftClothLimit");
					String used_clothlimit=jsonObject.optString("used_clothlimit");
					String membership_id=jsonObject.optString("membership_id");
                    String discount0nplan=jsonObject.optString("discount");
					JSONArray jArr=jsonObject.getJSONArray("category_wash_discount");
					cate_discount = new String[jArr.length()];
					for(int j = 0; j < jArr.length(); j++){
						//cate_discount[j] = jArr.getString(j);

						//t_category.setText(t_category.getText().toString()+","+cate_discount[j]);
						if(str.equals("")){
							str=jArr.getString(j);
						}
						else {
							str = str + "," + jArr.getString(j);;
						}

					}
					t_dis.setText("Discount on "+str+" is "+discount0nplan+"%");
					// t_plan,t_limit,t_category,t_total,t_left,t_used,t_dis;

					t_plan.setText(planname);
					t_limit.setText("Valid Through "+startdate+" - "+enddate);
					t_category.setText("Offer in "+offer);
					t_total.setText("Cloth Limit for "+offer+":"+clothlimit);
					t_left.setText("Cloth Left for "+offer+":"+leftcloth);
					t_used.setText("Cloth used for "+offer+":"+used_clothlimit);


				}
				else if (status.equals("2")){
					Toast.makeText(getActivity(),"Your Membership plan is expired!!!",Toast.LENGTH_SHORT).show();
				}
				else{

				}

			}
			else{
				Toast.makeText(getActivity(), "Check your Internet  Connection!!",
						Toast.LENGTH_SHORT).show();
			}
			}

			catch(Exception e){
				Log.e("String " , e.toString());
			}

		}
	}


	public void dialog_planActivity() {


		final Dialog dialog = new Dialog(getActivity());
		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.plan_details, null, false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(view);
		dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		dialog.setTitle("Plan Details");
		dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
		//progress_ad = (FrameLayout) dialog.findViewById(R.id.frame_progress_ad);
		// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		//new tag_addressAsyncTask().execute(ServerLinks.tag_address);
        plan_name_= (TextView) dialog.findViewById(R.id.plan_n);
		membership_type=(TextView) dialog.findViewById(R.id.member_type);
		membership_category=(TextView) dialog.findViewById(R.id.member_cat);
		discount_on=(TextView) dialog.findViewById(R.id.discount);
		discoumt_per=(TextView) dialog.findViewById(R.id.discount_per);
		maxm_order=(TextView) dialog.findViewById(R.id.mxm_order);
		valid_plan=(TextView) dialog.findViewById(R.id.valid);
		Button ok= (Button) dialog.findViewById(R.id.m_ok);
		Button buy= (Button) dialog.findViewById(R.id.m_buy);
		progress1= (FrameLayout) dialog.findViewById(R.id.frame_progress_);

		new deatailplan_tagAsyncTask().execute(ServerLinks.num_plan);

		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});
		buy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				 buy_dialog = new Dialog(getActivity());
				LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View view = inflater.inflate(R.layout.membership_buy_dialog, null, false);
				buy_dialog.setCanceledOnTouchOutside(false);
				buy_dialog.setContentView(view);
				buy_dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				buy_dialog.setTitle("Plan Details");
				buy_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
				//progress_ad = (FrameLayout) dialog.findViewById(R.id.frame_progress_ad);
				// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

				//new tag_addressAsyncTask().execute(ServerLinks.tag_address);
				TextView wallet= (TextView) buy_dialog.findViewById(R.id.wallet);
				TextView plan_name_b=(TextView) buy_dialog.findViewById(R.id.plan_name);
				TextView plan_price_b=(TextView) buy_dialog.findViewById(R.id.plan_price);
				TextView back=(TextView) buy_dialog.findViewById(R.id.back);
				TextView buy_f=(TextView) buy_dialog.findViewById(R.id.buy_f);

				plan_name_b.setText("Plan Name: "+p_name);
				plan_price_b.setText("Price: "+"Rs."+p_m_price);
				wallet.setText("Rs."+wallet_amount);


				back.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// Close dialog
						buy_dialog.dismiss();
					}
				});
				buy_f.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						/*if (wallet_amount.equals("0")){
							Toast.makeText(getActivity(),"You don't have any wallet balance.",Toast.LENGTH_LONG).show();
						}
						// Close dialog
						else {*/
							new membership_buyAsyncTask().execute(ServerLinks.buy_membership);
						//}

					}
				});


				// if decline button is clicked, close the custom dialog
				buy_dialog.show();
			}
		});


		// if decline button is clicked, close the custom dialog
		dialog.show();


	}
	private class membership_buyAsyncTask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
			getActivity().getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("userid", custmer_id);
				jsonObject.accumulate("membership_plan_ids", m_id);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
			getActivity().getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

			JSONObject jsonResponse;
//{"status":"1","order_id":[{"order":"0","quantityPlaced":"0"}]}
			try {
				if (!result.equals("")){
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					//planlist.setVisibility(View.VISIBLE);
					Toast.makeText(getActivity(), "You have successfuly apllied for membership plan.",Toast.LENGTH_SHORT).show();
					buy_dialog.dismiss();
				}
				else if (status.equals("2")){
					Toast.makeText(getActivity(), "please try again !!", Toast.LENGTH_SHORT).show();
				}
				else if (status.equals("3")){
					 cancel_id = jsonObject.optString("cancel_id");
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("Membership Plan");
					builder.setIcon(R.drawable.warn);
					builder.setMessage("You have already requested for this Membership Plan. Do you want to cancel this request??")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											new membership_cancel().execute(ServerLinks.member_cancel);
											dialog.cancel();
										}
									})
							.setNegativeButton("No", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});
					AlertDialog alert = builder.create();
					alert.show();
				}
				else if (status.equals("4")){
					Toast.makeText(getActivity(), "You have already purchased this Membership.", Toast.LENGTH_SHORT).show();
				}
				else {
					//Toast.makeText(getActivity(), "Incomplete data!!",Toast.LENGTH_SHORT).show();
				}
				}
				else{
					Toast.makeText(getActivity(), "Check your Internet  Connection!!",
							Toast.LENGTH_SHORT).show();
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}


		}

	}

	private class membership_cancel extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
			getActivity().getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("userid", custmer_id);
				jsonObject.accumulate("cancel_id", cancel_id);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
			getActivity().getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

			JSONObject jsonResponse;
//{"status":"1","order_id":[{"order":"0","quantityPlaced":"0"}]}
			try {
				if (!result.equals("")){
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					//planlist.setVisibility(View.VISIBLE);
					Toast.makeText(getActivity(), "You have successfuly cancelled request for this membership plan.",
							Toast.LENGTH_SHORT).show();
				}
				else if (status.equals("0")){
					Toast.makeText(getActivity(), "please try again !!",Toast.LENGTH_SHORT).show();
				}
				else if (status.equals("2")){
					Toast.makeText(getActivity(), "Membership Mismatches. Please contact your Superdhobi customer service!!!",
							Toast.LENGTH_SHORT).show();
				}
				/*else if (status.equals("2")){
					Toast.makeText(getActivity(), "Membership is not found.",
							Toast.LENGTH_SHORT).show();
				}*/
				else {
					Toast.makeText(getActivity(), "Incomplete data!!", Toast.LENGTH_SHORT).show();
				}
			}
			else{
				Toast.makeText(getActivity(), "Check your Internet  Connection!!",
						Toast.LENGTH_SHORT).show();
			}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}


		}

	}

	private static String convertInputStreamToString(InputStream inputStream)
					throws IOException {
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(inputStream));
				String line = "";
				String result = "";
				while ((line = bufferedReader.readLine()) != null)
					result += line;

				inputStream.close();
				return result;
			}
	/*public static void setListViewHeightBasedOnChildren(ListView listView)
	{
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
		int totalHeight=0;
		View view = null;

		for (int i = 0; i < listAdapter.getCount(); i++)
		{
			view = listAdapter.getView(i, view, listView);

			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.MATCH_PARENT));

			view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();

		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));

		listView.setLayoutParams(params);
		listView.requestLayout();

	}*/

			@Override
			public void onAttach(Activity activity) {
				washmoneyContext = (MainActivity) activity;
				super.onAttach(activity);
			}

			public void onBackPressed() {

				/*mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();*/

				/*FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				Homefragment llf = new Homefragment();
				ft.replace(R.id.containerView, llf);
				ft.commit();*/
			}


		}

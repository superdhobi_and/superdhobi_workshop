package com.superdhobi.navigation;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.Order_history_adapter;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.A;
import com.superdhobi.supperdhobi.DividerItemDecoration;
import com.superdhobi.supperdhobi.New_offer;
import com.superdhobi.supperdhobi.New_order_details;
import com.superdhobi.supperdhobi.R;
import com.superdhobi.supperdhobi.Sd_Order;
import com.superdhobi.supperdhobi.super_dhobi_order;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by PCIS-ANDROID on 15-03-2016.
 */
public class New_sd_home extends Fragment {
    LinearLayout order_lay,offer_section,Service_Execute;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    private SharedPreferences shp;
    String _userid="";
    static String json_orderId;
    int json_orderstatus;
    ImageView lastorder,nextorder;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.new_sd_home, container, false);
        order_lay= (LinearLayout) rootView.findViewById(R.id.order_lay);
        offer_section= (LinearLayout) rootView.findViewById(R.id.offer_section);
        Service_Execute= (LinearLayout) rootView.findViewById(R.id.services_execute);
        nextorder=(ImageView) rootView.findViewById(R.id.next_order);
        lastorder=(ImageView) rootView.findViewById(R.id.lastimage);

        shp = getActivity().getSharedPreferences("AuthToken", Context.MODE_PRIVATE);

        _userid=shp.getString("USER_UID", null);
        String _username=shp.getString("UNAME", null);

        order_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* mFragmentManager = getFragmentManager();
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.containerView,new New_home_fragment()).commit();*/
                Intent i= new Intent(getActivity(),Sd_Order.class);
                i.putExtra("userId", _userid);
                getActivity().startActivity(i);
            }
        });
        offer_section.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentManager = getFragmentManager();
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.containerView,new New_offer()).addToBackStack(null).commit();
                MainActivity.viewIsAtHome=true;
            }
        });
        Service_Execute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentManager = getFragmentManager();
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.containerView,new New_Services()).addToBackStack(null).commit();
                MainActivity.viewIsAtHome=true;
            }
        });
        nextorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(getContext(), New_order_details.class);
                i.putExtra("jsonorderId",json_orderId);
                getContext().startActivity(i);
            }
        });

        new lastorder_listAsyntask().execute(ServerLinks.lastorder);
        return rootView;


    }
    private class lastorder_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", _userid);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //progress.setVisibility(View.GONE);

            JSONObject jsonResponse;

            try {
                if (!result.equals("")){

                    JSONObject jObject = new JSONObject(result);
                    if(jObject!=null){
                        int status = jObject.getInt("status");
                        String data=jObject.getString("data");
                        //Create a jsonObject of String data
                        JSONObject dataObject = new JSONObject(data);

                        if (status == 1) {
                            json_orderId 	    =dataObject.getString("order_id");
                            json_orderstatus    =dataObject.getInt("orderStatus");

                            if(json_orderstatus >=5 && json_orderstatus !=6 && json_orderstatus < 9){
                                lastorder.setImageResource(R.drawable.stat_1);
                            }else if(json_orderstatus >=9  && json_orderstatus < 11 ){
                                lastorder.setImageResource(R.drawable.stat_2);
                            }else if(json_orderstatus >=11 && json_orderstatus !=12){
                                lastorder.setImageResource(R.drawable.stat_3);
                            }else {
                                lastorder.setImageResource(R.drawable.stat_def);
                            }

                        }
                    }
                    else {
                        Toast.makeText(getActivity(), "NetWork Error", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(getActivity(), "Bad Network Connection !!",Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e) {
                Log.e("String ", e.toString());
            }
        }
    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

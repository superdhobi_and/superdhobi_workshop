package com.superdhobi.navigation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;
import com.superdhobi.supperdhobi.InputValidation;
import com.superdhobi.supperdhobi.R;
import com.superdhobi.supperdhobi.TelephonyInfo;
import com.superdhobi.supperdhobi.Validation;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Mobile_num extends Activity {
	private SharedPreferences shp;
	EditText mobile, otp_mobile,pasword,refer;
	static String mbl, otp_mbl, mb_id,_pas,refer_code;
	Button submit_mbl, submit_otp;
	FrameLayout mbl_frm, mbl_otp,progress;
	static String f_uid_o,fc_uid;
	static String f_name, f_mbl, f_uid, f_email;
	static  String imsiSIM1,imsiSIM2;
	ImageView back;
	SharedPreferenceClass sharedPreferenceClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mobile_number);
		sharedPreferenceClass=new SharedPreferenceClass(getApplicationContext());
		progress= (FrameLayout) findViewById(R.id.frame_progress);
		shp = getSharedPreferences("AuthToken", MODE_PRIVATE);
		f_uid_o = getIntent().getStringExtra("uid");
		fc_uid = getIntent().getStringExtra("fuid");
		mbl_frm = (FrameLayout) findViewById(R.id.otp_frame1);
		mbl_otp = (FrameLayout) findViewById(R.id.otp_frame2);
		mobile = (EditText) findViewById(R.id.et_mbl_fb);
		otp_mobile = (EditText) findViewById(R.id.et_otp2);
		pasword = (EditText) findViewById(R.id.et_up);
		refer = (EditText) findViewById(R.id.et_ref_);
		//back = (ImageView) findViewById(R.id.back);

		submit_mbl = (Button) findViewById(R.id.ok_otp_fb);
		submit_otp = (Button) findViewById(R.id.ok_otp2);
		refer_code = refer.getText().toString();
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent bc = new Intent(Mobile_num.this, Login_Activity.class);
				startActivity(bc);
				overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();
			}
		});
		/*back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent bc = new Intent(Mobile_num.this, Login_Activity.class);
				startActivity(bc);
				overridePendingTransition(R.anim.left_in, R.anim.right_out);
				finish();

			}
		});*/

		mobile.setInputType(InputType.TYPE_CLASS_NUMBER);
		mobile.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				Validation.isPhoneNumber(mobile, false);
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
									  int count) {
			}
		});

		/*TelephonyManager tm=(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		imsiSIM1=tm.getDeviceId();*/

// actual code
		/*TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);

		imsiSIM1 = telephonyInfo.getImsiSIM1();
		imsiSIM2 = telephonyInfo.getImsiSIM2();

		boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
		boolean isSIM2Ready = telephonyInfo.isSIM2Ready();

		boolean isDualSIM = telephonyInfo.isDualSIM();
		*//*Toast.makeText(this," IME1 : " + imsiSIM1 + "\n" +
				" IME2 : " + imsiSIM2 + "\n" +
				" IS DUAL SIM : " + isDualSIM + "\n" +
				" IS SIM1 READY : " + isSIM1Ready + "\n" +
				" IS SIM2 READY : " + isSIM2Ready + "\n",Toast.LENGTH_LONG).show();*/

		submit_mbl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!InputValidation.isEditTextHasvalue(mobile)) {
					mobile.setError("Enter your mobile number");
				}
				else if (!InputValidation.isNumberselected(mobile)) {
					mobile.setError("Enter a valid mobile number");
				}
				else if (!InputValidation.isEditTextHasvalue(pasword)) {
					pasword.setError("Enter password");
				}
				else if (!InputValidation.isPasswordLengthCheck(pasword)) {
					pasword.setError("Password length should be greater than 6");
				}
				else {
					mbl = mobile.getText().toString();
					_pas=pasword.getText().toString();
					refer_code = refer.getText().toString();
					new Fbsignup_mobile_HttpAsyncTask()
							.execute(ServerLinks.f_otp);
				}

			}
		});

		submit_otp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				otp_mbl = otp_mobile.getText().toString();
				if (otp_mbl.equalsIgnoreCase("")) {
					Toast.makeText(Mobile_num.this, " Enter your OTP ",
							Toast.LENGTH_LONG).show();
				} else {
					//new otp_verification_HttpAsyncTask().execute(ServerLinks.otp);
				}
			}
		});
	}

	private class Fbsignup_mobile_HttpAsyncTask extends
			AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);

		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("custmerid", f_uid_o);
				jsonObject.accumulate("mobile", mbl);
				jsonObject.accumulate("pass", _pas);
				jsonObject.accumulate("deviceid1",imsiSIM1 );
				jsonObject.accumulate("deviceid2", imsiSIM2);
				jsonObject.accumulate("senderreferral", refer_code);
				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {
//{"status":"1",
// "email":"priyadarshisantanu@gmail.com",
// "mobile":"7205344985",
// "name":"PriyadarshiSantanu",
// "custmerid":"SDU201507",
// "msg":"moble is update"}
			progress.setVisibility(View.GONE);
			try {
				if (!result.equals("")){
				JSONObject jObject = new JSONObject(result);
				int status = jObject.getInt("status");
				if (status == 0) {

					Toast.makeText(getBaseContext(), "Enter a Valid mobile number", Toast.LENGTH_SHORT).show();
				} else if (status == 1) {
					//mb_id = jObject.getString("mid");
					f_name = jObject.getString("name");
					f_email = jObject.getString("email");
					f_mbl = jObject.getString("mobile");
					f_uid = jObject.getString("custmerid");
					sharedPreferenceClass.setValue_string("Who_Login","USER");
					sharedPreferenceClass.setValue_boolean("USER", true);
					submitForm();
					Toast.makeText(getApplication(), "Successfull", Toast.LENGTH_SHORT).show();
					Intent intent4 = new Intent(Mobile_num.this,MainActivity.class);
					startActivity(intent4);
					finish();

					/*
					 * Intent intent4 = new Intent(SignupActivity.this,
					 * MainActivity.class); startActivity(intent4);
					 */

				}
				else if (status==2){
					Toast.makeText(getApplication(), "Invalid", Toast.LENGTH_SHORT).show();
				}
				else if (status==3){
					Toast.makeText(getApplication(), "Mobile Number already in used", Toast.LENGTH_SHORT).show();
				}
				}
				else{
					Toast.makeText( Mobile_num.this, "Bad Network Connection !!",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {

			}

			// progressDialog.dismiss();

			super.onPostExecute(result);

		}
		private void submitForm() {

			try {

				SharedPreferences.Editor shpEdit = shp.edit();
				shpEdit.putString("AuthKey", f_email);
				shpEdit.putString("USER_UID", f_uid);
				shpEdit.putString("UNAME", f_name);
				shpEdit.putString("MBL_NUM", f_mbl);
				shpEdit.putString("UID", fc_uid);
				shpEdit.commit();
				finish();
			} catch (Exception e) {
				e.printStackTrace();

				finish();

			}
		}

	}

	private class otp_verification_HttpAsyncTask extends
			AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("otp", otp_mbl);
				jsonObject.accumulate("mid", f_uid_o);
				jsonObject.accumulate("mobile", mbl);
				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {

			// progress.setVisibility(View.GONE);
			try {
				JSONObject jObject = new JSONObject(result);
				int status = jObject.getInt("status");
				if (status == 0) {

					Toast.makeText(getBaseContext(), "Try Again!", Toast.LENGTH_SHORT).show();
				} else if (status == 1) {
					f_name = jObject.getString("name");
					f_email = jObject.getString("email");
					f_mbl = jObject.getString("mobile");
					f_uid = jObject.getString("userid");
					submitForm();
					Toast.makeText(getApplication(), "Successfull", Toast.LENGTH_SHORT).show();

					Intent intent4 = new Intent(Mobile_num.this,
							MainActivity.class);
					startActivity(intent4);
					finish();

				} else if (status == 2) {
					Toast.makeText(getApplication(), "OTP did not match.", Toast.LENGTH_SHORT)
							.show();
				}
			} catch (Exception e) {

			}

			// progressDialog.dismiss();

			super.onPostExecute(result);

		}

		private void submitForm() {

			try {

				SharedPreferences.Editor shpEdit = shp.edit();
				shpEdit.putString("AuthKey", f_email);
				shpEdit.putString("USER_UID", f_uid);
				shpEdit.putString("UNAME", f_name);
				shpEdit.putString("MBL_NUM", f_mbl);
				shpEdit.commit();
				finish();
			} catch (Exception e) {
				e.printStackTrace();

				finish();

			}
		}
	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}
	@Override
	public void onBackPressed() {
		//Display alert message when back button has been pressed
		backButtonHandler();
		return;
	}

	public void backButtonHandler() {
		Intent bc = new Intent(Mobile_num.this, Login_Activity.class);
		startActivity(bc);
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
		finish();
	}
}

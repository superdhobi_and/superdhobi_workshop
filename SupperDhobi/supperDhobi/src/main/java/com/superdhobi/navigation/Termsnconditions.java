package com.superdhobi.navigation;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.AllStaticVariables;
import com.superdhobi.navigation.webservices.AsynkTaskCommunicationInterface;
import com.superdhobi.navigation.webservices.AsynkTaskForServerCommunication;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.NointernetActivity;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Termsnconditions extends AppCompatActivity implements AsynkTaskCommunicationInterface {

	private SharedPreferences shp;
	static MainActivity reportContext;
	LinearLayout call,mail;
	TextView tnc;
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;
	AsynkTaskForServerCommunication asynkTaskForServerCommunication;
	TextView termsanduseq,termsanduseA,policyq,policyA,Licesnseq,LicenseA;
	ImageView termsanduseadd,termsandusesub,policyadd,policysub,licenseadd,licensesub;
	LinearLayout termsanduse_linear,privacypolicy_linear,license_linear;
	String Data;
	String Termscond,privacy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//shp = reportContext.getSharedPreferences("AuthToken", Context.MODE_PRIVATE);
		/*SharedPreferences.Editor shpEdit = shp.edit();
		shpEdit.putString("start_home", "from_report");
		shpEdit.commit();*/
        setContentView(R.layout.report_pg);
		//View rootView = inflater.inflate(R.layout.report_pg, container, false);
		//tnc = (TextView) findViewById(R.id.show_tnc);
		termsanduse_linear = (LinearLayout) findViewById(R.id.termsanduse_linear);
		privacypolicy_linear = (LinearLayout) findViewById(R.id.privacypolicy_linear);
		termsanduseq = (TextView) findViewById(R.id.question_term);
		termsanduseA = (TextView) findViewById(R.id.answer_term1);
		policyq = (TextView) findViewById(R.id.question_term1);
		policyA = (TextView) findViewById(R.id.answer_term3);


		termsanduseadd = (ImageView) findViewById(R.id.terms_use_add);
		termsandusesub = (ImageView) findViewById(R.id.terms_use_sub);
		policyadd = (ImageView) findViewById(R.id.policy_add);
		policysub = (ImageView) findViewById(R.id.policy_sub);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);


		termsanduse_linear.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (termsanduseA.getVisibility()==View.GONE)
				{
					termsanduseA.setVisibility(View.VISIBLE);
					termsanduseadd.setVisibility(View.GONE);
					termsandusesub.setVisibility(View.VISIBLE);
				}else
				{
					termsanduseA.setVisibility(View.GONE);
					termsanduseadd.setVisibility(View.VISIBLE);
					termsandusesub.setVisibility(View.GONE);
				}
			}
		});


		privacypolicy_linear.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (policyA.getVisibility()==View.GONE)
				{
					policyA.setVisibility(View.VISIBLE);
					policyadd.setVisibility(View.GONE);
					policysub.setVisibility(View.VISIBLE);
				}else
				{
					policyA.setVisibility(View.GONE);
					policyadd.setVisibility(View.VISIBLE);
					policysub.setVisibility(View.GONE);
				}

			}
		});
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("data",Data);
		HashMap<String, String> hashMapLink = new HashMap<String, String>();
		hashMapLink.put(AllStaticVariables.link, ServerLinks.termsnconditn);
		asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Termsnconditions.this);
		asynkTaskForServerCommunication.execute(parameters, hashMapLink);

		//new Tnc_HttpAsyncTask().execute(ServerLinks.tnc);

	}


	@Override
	public void doInBackgroundForComun(int progress) {

	}

	@Override
	public void doPostExecuteForCommu(JSONObject jsonOb) {
		try {
			if (!jsonOb.equals("")){
				//JSONObject jObject = new JSONObject(jsonOb);
				int status = jsonOb.getInt("status");
				String data=jsonOb.getString("data");
				//Create a jsonObject of String data
				JSONObject dataObject = new JSONObject(data);

				if (status == 0) {
					//Toast.makeText(getBaseContext(), "Please Enter valid user id and password!!", Toast.LENGTH_SHORT).show();
				} else if (status == 1) {

					Termscond=dataObject.getString("termscond");
					privacy=dataObject.getString("privacy");





					termsanduseA.setText(Html.fromHtml(Termscond));
					policyA.setText(Html.fromHtml(privacy));




					//Toast.makeText(getApplication(), "You Are Logged In...", Toast.LENGTH_SHORT).show();


				}
			}
			else{
				//This part is call when internet connection not found..
				Intent i = new Intent(Termsnconditions.this,NointernetActivity.class);
				startActivity(i);
				Termsnconditions.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
				finish();
			}
		} catch (Exception e) {

		}
	}

	/*@Override
	public void onAttach(Activity activity) {
		reportContext = (MainActivity) activity;
		super.onAttach(activity);
	}
*/
	public void onBackPressed() {
		finish();
	}

}

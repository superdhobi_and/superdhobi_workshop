package com.superdhobi.navigation;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdhobi.supperdhobi.CustomLinearLayoutManager;
import com.superdhobi.supperdhobi.DividerItemDecoration;
import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 19-03-2016.
 */
public class New_home_order extends AppCompatActivity {
    String[] cloth={"Shirt", "Pant", "Suits"};
    RecyclerView men_cloth,women_cloth,house_cloth;
    LinearLayout men_lay,women_lay,house_lay,men_head,women_head,house_head,m_select,w_select,h_select;
    ImageView men_icon,women_icon,house_icon, men_up,men_d,women_up,women_d,hs_up,hs_d;
    TextView men,women,house;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_home);
        men_lay= (LinearLayout) findViewById(R.id.men_list);
        women_lay= (LinearLayout) findViewById(R.id.women_list);
        house_lay= (LinearLayout) findViewById(R.id.house_list);
        men_head= (LinearLayout) findViewById(R.id.men_header);
        women_head= (LinearLayout) findViewById(R.id.women_header);
        house_head= (LinearLayout) findViewById(R.id.house_header);


        men_icon= (ImageView) findViewById(R.id.men_image);
        women_icon= (ImageView) findViewById(R.id.women_image);
        house_icon= (ImageView) findViewById(R.id.house_image);
        men= (TextView) findViewById(R.id.m);
        women= (TextView) findViewById(R.id.wm);
        house= (TextView) findViewById(R.id.hs);



        men_cloth= (RecyclerView) findViewById(R.id.recycler_view_men);
        women_cloth= (RecyclerView) findViewById(R.id.recycler_view_women);
        house_cloth= (RecyclerView) findViewById(R.id.recycler_view_house);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        men_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                men_icon.setImageResource(R.drawable.men_act);
                men_head.setBackgroundColor(getResources().getColor(R.color.accordion_header_select));
                men_lay.setVisibility(View.VISIBLE);
                men.setTextColor(getResources().getColor(R.color.white));

                women_icon.setImageResource(R.drawable.wmn);
                women_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                women.setTextColor(getResources().getColor(R.color.accordion_header_select));
                women_lay.setVisibility(View.GONE);

                house_icon.setImageResource(R.drawable.household);
                house_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                house.setTextColor(getResources().getColor(R.color.accordion_header_select));
                house_lay.setVisibility(View.GONE);
            }
        });

        women_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                women_icon.setImageResource(R.drawable.wmn_act);
                women_head.setBackgroundColor(getResources().getColor(R.color.accordion_header_select));
                women.setTextColor(getResources().getColor(R.color.white));
                women_lay.setVisibility(View.VISIBLE);

                men_icon.setImageResource(R.drawable.men);
                men_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                men.setTextColor(getResources().getColor(R.color.accordion_header_select));
                men_lay.setVisibility(View.GONE);

                house_icon.setImageResource(R.drawable.household);
                house_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                house.setTextColor(getResources().getColor(R.color.accordion_header_select));
                house_lay.setVisibility(View.GONE);
            }
        });

        house_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                men_icon.setImageResource(R.drawable.men);
                men_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                men.setTextColor(getResources().getColor(R.color.accordion_header_select));
                men_lay.setVisibility(View.GONE);

                women_icon.setImageResource(R.drawable.wmn);
                women_head.setBackgroundColor(getResources().getColor(R.color.accordion_header));
                women.setTextColor(getResources().getColor(R.color.accordion_header_select));
                women_lay.setVisibility(View.GONE);

                house_icon.setImageResource(R.drawable.household_act);
                house_head.setBackgroundColor(getResources().getColor(R.color.accordion_header_select));
                house.setTextColor(getResources().getColor(R.color.white));
                house_lay.setVisibility(View.VISIBLE);

            }
        });

//adapter set into recyclerview
        Recycler_adapter_hme recycler_adapter_hme= new Recycler_adapter_hme(New_home_order.this,cloth);

        men_cloth.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(New_home_order.this);
        men_cloth.setLayoutManager(mLayoutManager);
        men_cloth.addItemDecoration(new DividerItemDecoration(New_home_order.this, LinearLayoutManager.VERTICAL));
        men_cloth.setItemAnimator(new DefaultItemAnimator());

        women_cloth.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(New_home_order.this);
        women_cloth.setLayoutManager(mLayoutManager1);
        women_cloth.addItemDecoration(new DividerItemDecoration(New_home_order.this, LinearLayoutManager.VERTICAL));
        women_cloth.setItemAnimator(new DefaultItemAnimator());

        house_cloth.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(New_home_order.this);
        house_cloth.setLayoutManager(mLayoutManager2);
        house_cloth.addItemDecoration(new DividerItemDecoration(New_home_order.this, LinearLayoutManager.VERTICAL));
        house_cloth.setItemAnimator(new DefaultItemAnimator());

        men_cloth.setAdapter(recycler_adapter_hme);// set adapter
        women_cloth.setAdapter(recycler_adapter_hme);
        house_cloth.setAdapter(recycler_adapter_hme);
    }

    // recycler view adapter for list of cloth
    public class Recycler_adapter_hme extends RecyclerView.Adapter<Recycler_adapter_hme.MyViewHolder> {
        private String[] cloth;

        Recycler_adapter_sub recycler_adapter_sub;

        Context context;
        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, year, genre;
            public RecyclerView sub_list;
            public LinearLayout list_cloth;
            String[] cloth_list={"Shirt", "t_shirt", "kurta"};

            public MyViewHolder(View view1) {
                super(view1);
                title = (TextView) view1.findViewById(R.id.cloth_name);
                sub_list=(RecyclerView)view1.findViewById(R.id.sub_list);
               // sub_list.setNestedScrollingEnabled(false);
                list_cloth=(LinearLayout)view1.findViewById(R.id.list_cloth);

            }
        }

        public Recycler_adapter_hme( Context context,String[] cloth) {
            this.cloth = cloth;
            this.context = context;
        }



        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view1 = LayoutInflater.from(context).inflate(R.layout.home_custom, parent, false);
            MyViewHolder holder = new MyViewHolder(view1);
            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            holder.title.setText(cloth[position]);
            holder.list_cloth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                     recycler_adapter_sub= new Recycler_adapter_sub(context,holder.cloth_list);

                    holder.sub_list.setVisibility(View.VISIBLE);
                    holder.sub_list.setHasFixedSize(true);
                     RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                   /* holder.sub_list.setLayoutManager(mLayoutManager);
                    holder.sub_list.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
                    holder.sub_list.setItemAnimator(new DefaultItemAnimator());*/
                    RecyclerView.LayoutManager layoutManager = new CustomLinearLayoutManager(context);
                    holder.sub_list.setLayoutManager(layoutManager);
                    holder.sub_list.setAdapter(recycler_adapter_sub);
                }
            });
        }
      @Override
        public int getItemCount() {
            return cloth.length;
        }
    }

    // recycler view adapter for sub_cloth_list under the cloth list element
    public class Recycler_adapter_sub extends RecyclerView.Adapter<Recycler_adapter_sub.MyViewHolder> {
        private String[] cloth_list;


        Context context1;
        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView sub_title, year, genre;

            public MyViewHolder(View view) {
                super(view);
                sub_title = (TextView) view.findViewById(R.id.sub_cloth_name);

            }
        }

        public Recycler_adapter_sub( Context context1,String[] cloth_list) {
            this.cloth_list = cloth_list;
            this.context1 = context1;
        }



        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context1).inflate(R.layout.sub_list_custom, parent, false);
            MyViewHolder holder1 = new MyViewHolder(view);
            return holder1;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder1, int position) {
            holder1.sub_title.setText(cloth_list[position]);
            String item=cloth_list[position].toString();
            //Recycler_adapter_sub recycler_adapter_sub= new Recycler_adapter_sub(context,cloth_list);
        }
        @Override
        public int getItemCount() {
            return cloth_list.length;
        }
    }
}

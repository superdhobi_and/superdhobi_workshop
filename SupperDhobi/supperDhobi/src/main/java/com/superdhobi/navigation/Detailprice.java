package com.superdhobi.navigation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Detailprice extends Activity {
	FrameLayout progress;
	ListView pricelist;
	TextView type;
	static String wash_id, w_type;
	String[] clothes, prices;
	ImageView cross;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_price);
		progress = (FrameLayout) findViewById(R.id.frame_progress);

		wash_id = getIntent().getStringExtra("WASH_ID");

		cross = (ImageView) findViewById(R.id.back_price);
		type = (TextView) findViewById(R.id.type);
		pricelist = (ListView) findViewById(R.id.price_list);
		w_type = getIntent().getStringExtra("WASH_TYPE");
		type.setText(w_type);
		new price_listAsyntask().execute(ServerLinks.view_pricelist);
		cross.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Detailprice.this.finish();

			}
		});
	}

	private class price_listAsyntask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(getActivity()); // Set
			 * progressdialog title
			 * //mProgressDialog.setTitle("Android JSON Parse Tutorial"); // Set
			 * progressdialog message progressDialog.setMessage("Loading...");
			 * progressDialog.setIndeterminate(false); // Show progressdialog
			 * progressDialog.show();
			 */
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("wash_id", wash_id);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("unused")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);

			/*
			 * washtype=new ArrayList<String>(); wash_id=new
			 * ArrayList<String>();
			 */

			JSONObject jsonResponse;

			try {
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					JSONArray jArr = jsonObject.getJSONArray("price");
					if (jArr != null) {
						clothes = new String[jArr.length()];
						prices = new String[jArr.length()];
						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);

							clothes[i] = jsonResponse.getString("clothes");
							prices[i] = jsonResponse.getString("price");

						}
					} else {
						Toast.makeText(Detailprice.this, "You have no adress",
								Toast.LENGTH_SHORT).show();
					}
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}

			Price_list_adapter adapter = new Price_list_adapter(
					Detailprice.this, clothes, prices);

			pricelist.setAdapter(adapter);
		}
	}

	public class Price_list_adapter extends ArrayAdapter<String> {
		private final Activity context;
		private final String[] clothes;
		private final String[] prices;

		public Price_list_adapter(Activity context, String[] clothes,
				String[] prices) {
			super(context, R.layout.list_price, clothes);
			// TODO Auto-generated constructor stub
			this.context = context;
			this.clothes = clothes;
			this.prices = prices;
		}

		public View getView(int position, View view, ViewGroup parent) {
			LayoutInflater inflater = context.getLayoutInflater();
			View rowView = inflater.inflate(R.layout.list_price, null, true);
			TextView item = (TextView) rowView.findViewById(R.id.item_name);
			TextView item_price = (TextView) rowView
					.findViewById(R.id.item_price);

			item.setText(clothes[position]);
			item_price.setText(prices[position]);

			return rowView;
		}
	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}
}

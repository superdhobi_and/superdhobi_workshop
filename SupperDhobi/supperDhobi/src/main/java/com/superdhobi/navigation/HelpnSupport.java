package com.superdhobi.navigation;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdhobi.supperdhobi.R;
import com.superdhobi.supperdhobi.SimpleTabsActivity;

public class HelpnSupport extends AppCompatActivity{
	
	private SharedPreferences shp;
	static MainActivity helpContext;
	LinearLayout support,faq,term,s_call,mail;

	TextView sup;
	boolean isTextViewClick = false;
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.support);
		support= (LinearLayout) findViewById(R.id.tv_call);
		faq= (LinearLayout) findViewById(R.id.tv_qust);
		//term= (LinearLayout) rootView.findViewById(R.id.tv_term);
		s_call= (LinearLayout) findViewById(R.id.call_lay_sup);
		mail = (LinearLayout) findViewById(R.id.mail_lay);
		sup= (TextView) findViewById(R.id.sup);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		support.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isTextViewClick == false) {
					s_call.setVisibility(View.VISIBLE);
					sup.setTextColor(getResources().getColor(R.color.app_color));
					isTextViewClick = true;
				} else {
					s_call.setVisibility(View.GONE);

					sup.setTextColor(getResources().getColor(R.color.text));

					isTextViewClick = false;
				}
			}
		});
		/*term.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent t=new Intent(getActivity(),TNC.class);
				startActivity(t);
			}
		});*/
		faq.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent f=new Intent(HelpnSupport.this,SimpleTabsActivity.class);
				startActivity(f);
			}
		});

		s_call.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent callIntent = new Intent(Intent.ACTION_DIAL);
				callIntent.setData(Uri.parse("tel:09549005123"));
				startActivity(callIntent);
			}
		});

		mail.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String recepientEmail = "info@superdhobi.in"; // either set to destination email or leave empty
				Intent intent = new Intent(Intent.ACTION_SENDTO);
				intent.setData(Uri.parse("mailto:" + recepientEmail));
				intent.putExtra(Intent.EXTRA_SUBJECT, "Query");
				startActivity(intent);

				/*Intent emailIntent = new Intent(Intent.ACTION_SEND);
				emailIntent.putExtra(Intent.EXTRA_EMAIL,
						new String[] { "priyadarshisantanu@gmail.com" });
				emailIntent.setType("text/plain");
				startActivity(emailIntent);*/
			}
		});
	}
	
	/*@Override
	public void onAttach(Activity activity) {
		helpContext=(MainActivity) activity;
		super.onAttach(activity);
	}*/
	    public void onBackPressed()
	{
finish();

	}
}

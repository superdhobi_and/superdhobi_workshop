package com.superdhobi.navigation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Feedback extends Activity{
	private SharedPreferences shp;
	Button submit;
	static String emailid,name,order_ref,userid,msg;
	TextView orderid,uname,email,feedback;
	FrameLayout progress;
	ImageView back_feed;
	Spinner feedback_cat;
	static  String category;
	List<String> list_category = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fedback);
		shp = getSharedPreferences("AuthToken", Context.MODE_PRIVATE);
		emailid = shp.getString("AuthKey", null);
		name = shp.getString("UNAME", null);
		userid = shp.getString("USER_UID", null);
		order_ref=getIntent().getStringExtra("orderid");
		progress = (FrameLayout)findViewById(R.id.frame_progress);
		orderid= (TextView) findViewById(R.id.orderid_fd);
		uname=(TextView) findViewById(R.id.name_fd);
		email=(TextView) findViewById(R.id.email_fd);
		feedback=(EditText) findViewById(R.id.fd_edit);
        //back_feed= (ImageView) findViewById(R.id.back_fd);
		feedback_cat= (Spinner) findViewById(R.id.feedback_cat);
		loadSpinnerData();
		if (emailid.equals("")){
			email.setText("Not available");
		}
		else{
			email.setText(emailid);
		}
		uname.setText(name);
		//email.setText(emailid);
		orderid.setText(order_ref);

		submit=(Button) findViewById(R.id.btn_smt);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_36dp);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				msg=feedback.getText().toString();
				category=feedback_cat.getSelectedItem().toString();
				if (msg.equals("")){
					Toast.makeText(Feedback.this,"Feedback field should not aa blank!!",Toast.LENGTH_SHORT).show();
				}
				else if (category.equals("Select Feedback Category")){
					Toast.makeText(Feedback.this,"Select Category!!",Toast.LENGTH_SHORT).show();
				}
				else {
					new Feedback_HttpAsyncTask().execute(ServerLinks.feedback);
				}
			}
		});
		/*back_feed.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});*/
	}

	private void loadSpinnerData() {
		// TODO Auto-generated method stub

		list_category.add("Select Feedback Category");
		list_category.add("Suggestion");
		list_category.add("Query");
		list_category.add("Problem");


		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Feedback.this, android.R.layout.simple_spinner_item,
				list_category);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		feedback_cat.setAdapter(dataAdapter);


	}

	private class Feedback_HttpAsyncTask extends
			AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);

		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("userid", userid);
				jsonObject.accumulate("orderid", order_ref);
				/*jsonObject.accumulate("name", name);
				jsonObject.accumulate("email", emailid);*/
				jsonObject.accumulate("messages", msg);
				jsonObject.accumulate("category", category);
				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {
//{"status":"1","error":"Your feed back is saved success ."}
			progress.setVisibility(View.GONE);
			try {
				if (!result.equals("")){
				JSONObject jObject = new JSONObject(result);
				int status = jObject.getInt("status");
				if (status == 0) {

					Toast.makeText(getBaseContext(), "Try Again!", Toast.LENGTH_SHORT).show();
				} else {

					Toast.makeText(getApplication(), "Successful Submitted", Toast.LENGTH_SHORT).show();
					finish();


				}
			}
			else{
				Toast.makeText( Feedback.this, "Bad Network Connection !!",
						Toast.LENGTH_SHORT).show();
			}
			} catch (Exception e) {

			}

			// progressDialog.dismiss();

			super.onPostExecute(result);

		}
	}
	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

}

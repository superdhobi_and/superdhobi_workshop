package com.superdhobi.navigation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.Toast;

import com.superdhobi.AdapterPackage.All_plan_adapter;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by PCIS-ANDROID on 26-04-2016.
 */
public class Membership_Allplan_frag extends Fragment {
    private SharedPreferences shp;
    ListView plan_name;
    static  String custmer_id;
    String[] plan_list={"Super Wash Monthly","Super Wash Family","Dry Clean Monthly","Dry Clean Family"};
    String[] plan_list_id;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.membership_allplan, container, false);
        shp = getActivity().getSharedPreferences("AuthToken",
                Context.MODE_PRIVATE);
        //progress = (FrameLayout) rootView.findViewById(R.id.frame_progress);
        custmer_id = shp.getString("USER_UID", null);
        plan_name= (ListView) rootView.findViewById(R.id.list_plan_name);
       // new allplan_tagAsyncTask().execute(ServerLinks.num_plan);
        All_plan_adapter all_plan_adapter =new All_plan_adapter(getActivity(),plan_list);

        plan_name.setAdapter(all_plan_adapter);
        return rootView;
    }

    private class allplan_tagAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progress.setVisibility(View.VISIBLE);
            getActivity().getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("customerid", custmer_id);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //progress.setVisibility(View.GONE);
            getActivity().getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            JSONObject jsonResponse;

            try {
                if (!result.equals("")){
                    JSONObject jsonObject = new JSONObject(result);

                    String status = jsonObject.optString("status");

                    if (status.equals("1")) {
                        //planlist.setVisibility(View.VISIBLE);
                        JSONArray jArr = jsonObject.getJSONArray("membership_plan");
                        if (jArr != null) {
                            plan_list = new String[jArr.length()];
                            plan_list_id = new String[jArr.length()];

                            for (int i = 0; i < jArr.length(); i++) {
                                jsonResponse = jArr.getJSONObject(i);

                                plan_list[i] = jsonResponse.getString("plan_name");
                                plan_list_id[i] = jsonResponse.getString("membership_plan_ids");

                            }
                        }

                        /*All_plan_adapter all_plan_adapter =new All_plan_adapter(getActivity(),plan_list,plan_list_id);

                        plan_name.setAdapter(all_plan_adapter);*/
                    } else {
                        Toast.makeText(getActivity(), "There is no membership plan.", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(getActivity(), "Check your Internet  Connection!!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }


        }

    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

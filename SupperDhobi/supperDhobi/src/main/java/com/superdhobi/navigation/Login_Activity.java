package com.superdhobi.navigation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.NewPermissionsRequest;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;
import com.superdhobi.supperdhobi.InputValidation;
import com.superdhobi.supperdhobi.NointernetActivity;
import com.superdhobi.supperdhobi.Otp_Activity;
import com.superdhobi.supperdhobi.R;
import com.superdhobi.supperdhobi.SignupActivity;
import com.superdhobi.vo.LoginRequestVO;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class Login_Activity extends Activity implements
        GoogleApiClient.OnConnectionFailedListener{
    Boolean isInternetPresent = false;
    private SharedPreferences shp;
    //AIzaSyByfZEYYcAc3kLqeYssHvTv368SlaIy49s
    private String userId;
    static String fb_user_name, fb_user_mail;
    static String f_name1, f_email1, f_mbl1, f_referal;

    ImageView backlog;
    TextView fb, google_log;
    TextView newuser, forget_pwd;
    ImageView login;
    EditText uid, pwd;
    String pasword;
    Animation shake;
    static String u_id, u_userid, u_username, u_name, u_mobile, Email, u_refcode, mobile_varify;
    private HashMap<String, String> userHashmap;
    private ArrayList<HashMap<String, String>> friendList;

    private ProgressDialog pd;
    FrameLayout progress;
    public static final String USER_MAP = "userHashmap";
    public static final String FRIEND_LIST = "friendList";

    public static final String USER_ID = "userId";
    public static final String NAME = "name";
    public static final String USER_NAME = "userName";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String BIRTHDAY = "birthday";
    public static final String GENDER = "gender";
    public static final String EMAIL_ID = "emailId";
    public static final String IMAGE_URL = "imageUrl";

    public static final String webClientID="1014938659944-csg2ps1466bo6ppqdrlue5241g042tle.apps.googleusercontent.com";

    public static final String androidClientID="1014938659944-6hs977rlecb9dtsmf3d1ekrr31fpjru6.apps.googleusercontent.com";

    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "Login_Activity";
    private static final int RC_SIGN_IN = 9001;

    SharedPreferenceClass sharedPreferenceClass;
    static String imsiSIM1, imsiSIM2;

    private String mobileNumber;
    private LoginRequestVO loginRequestVO;
    GoogleCloudMessaging gcm;
    SharedPreferences preferences;


    private String reg_cgm_id;
    public static final String PROPERTY_REG_ID = "notifyId";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    //for fb
    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (pd != null && pd.isShowing())
                pd.dismiss();

            if (msg.what == 1) {
                new User_signup_fb_HttpAsyncTask().execute(ServerLinks.user_social_Signup);
            } else if (msg.what == 2) {
                if (friendList.size() > 0) {
                    //intent
                } else {
                    Toast.makeText(Login_Activity.this, "No friends found.",
                            Toast.LENGTH_SHORT).show();
                }
            }
            return false;
        }
    });


    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        } else {
//            callbackmanager.onActivityResult(requestCode, responseCode, data);
            Session.getActiveSession().onActivityResult(this, requestCode,
                    resultCode, data);
        }

    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            loginRequestVO =new LoginRequestVO();
            loginRequestVO.setEmail(acct.getEmail());
            loginRequestVO.setName(acct.getDisplayName());
            loginRequestVO.setGoogle_id(acct.getId());
            loginRequestVO.setLogin_type("2"); //2 for google

            Log.d(TAG,loginRequestVO.toString());

            new AuthLoginAsyncTask().execute(ServerLinks.authLogin);


        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.v2_login);
        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        progress = (FrameLayout) findViewById(R.id.frame_progress);

        getKeyHash();
        shp = getSharedPreferences("AuthToken", MODE_PRIVATE);
        fb = (TextView) findViewById(R.id.iv_fb);
        google_log = (TextView) findViewById(R.id.google_log);
        newuser = (TextView) findViewById(R.id.new_user);
        forget_pwd = (TextView) findViewById(R.id.forget_pass);

        login = (ImageView) findViewById(R.id.btn_login);
        uid = (EditText) findViewById(R.id.et_userid);
        pwd = (EditText) findViewById(R.id.et_pw);

        // GCM
//        if (checkPlayServices()) {
//            gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
//            String reg_cgm_id = getRegistrationId(getApplicationContext());
//            Log.i(TAG, "Play Services Ok.");
//            if (reg_cgm_id.isEmpty()) {
//                Log.i(TAG, "Find Register ID.");
//                registerInBackground();
//            }
//        } else {
//            Log.i(TAG, "No valid Google Play Services APK found.");
//        }




        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(androidClientID)
                .requestEmail()
                .build();





        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();




        fb.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getFacebookUserInfo();
            }
        });
        google_log.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                googleSignIn();

            }
        });

        shake = AnimationUtils.loadAnimation(this, R.anim.shake);
        newuser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Login_Activity.this,
                        SignupActivity.class);
                startActivity(intent);
                SharedPreferences.Editor shpEdit = shp.edit();
                shpEdit.putString("start_sign", "from_log");
                shpEdit.commit();
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                finish();

            }
        });
        forget_pwd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent1 = new Intent(Login_Activity.this, Forgot_pw.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                finish();

            }
        });
		/*backlog.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent bck= new Intent(Login_Activity.this,First.class);
				startActivity(bck);
				finish();

			}
		});*/
        login.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Email = uid.getText().toString();
                pasword = pwd.getText().toString();
                if (!InputValidation.isEditTextHasvalue(uid)) {
                    uid.setError("Enter email id or mobile number");
                }
				/*else if (!InputValidation.isEditTextContainEmail(uid)) {
					uid.setError("Enter a valid email id");
				}*/
                else if (!InputValidation.isEditTextHasvalue(pwd)) {
                    pwd.setError("Enter password");
                }
				/*else if (!InputValidation.isPasswordLengthCheck(pwd)) {
					pwd.setError("Enter a valid password");
				}*/
                else {
                    new LoginAsyncTask().execute(ServerLinks.user_Login);
                }

				/*if (Email.equalsIgnoreCase("") || pasword.equalsIgnoreCase("")) {
					findViewById(R.id.et_pw).startAnimation(shake);
					findViewById(R.id.et_userid).startAnimation(shake);
					Toast toast = Toast.makeText(Login_Activity.this,
							"Fill all the field", Toast.LENGTH_LONG);
					toast.show();
				} else
					new LoginAsyncTask().execute(ServerLinks.user_Login);*/
            }
        });

    }



    private void getUserInfoFromFacebook(final GraphUser user) {


        loginRequestVO =new LoginRequestVO();
        loginRequestVO.setEmail(user.asMap().get("email").toString());
        loginRequestVO.setName(user.getName());
        loginRequestVO.setFb_id(user.getId());
        loginRequestVO.setLogin_type("1"); //1 for facebook

        Log.d(TAG,loginRequestVO.toString());

        new AuthLoginAsyncTask().execute(ServerLinks.authLogin);



    }


    public void showMobileNumberDialog()
    {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_custom_mobile_number, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                // edit text
                                mobileNumber=userInput.getText().toString();
                                Log.d(TAG,"Mobile Number " +mobileNumber);
                                loginRequestVO.setMobile(mobileNumber);
                                new AuthLoginAsyncTask().execute(ServerLinks.authLogin);

                                //result.setText(userInput.getText());
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
//    private void getUserInfoFromWithMobileNumber(LoginRequestVO loginRequestVO) {
//        if(mobileNumber!="")
//        {
//            loginRequestVO.setMobile(mobileNumber);
//            Call<LoginResponseVO> call =backendService.saveUser(loginRequestVO);
//            call.enqueue(new Callback<LoginResponseVO>() {
//
//                @Override
//                public void onResponse(Call<LoginResponseVO> call, retrofit2.Response<LoginResponseVO> response) {
//                    Log.d(TAG,"getUserInfoFromWithMobileNumber"+response.body().toString());
//                    LoginResponseVO loginResponseVO=response.body();
//                    Log.d(TAG,loginResponseVO.toString());
//
//                    if(loginResponseVO.getStatus()==400)
//                    {
//                        Log.d(TAG,"Asking for mobile number");
//                    }
//                    else if(loginResponseVO.getStatus()==1)
//                    {
////                        LoginResponseDataVO data=(LoginResponseDataVO)loginResponseVO.getData();
//                        Log.d(TAG,loginResponseVO.getData().toString());
//                    }
//                    else
//                    {
//                        Toast.makeText(Login_Activity.this,(String)loginResponseVO.getData().toString(),Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<LoginResponseVO> call, Throwable t) {
//                    Log.d(TAG, "Failure");
//                    t.printStackTrace();
//                }
//            });
//        }
//
//    }

        @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }



    private class AuthLoginAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        private Activity context;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

//                // 3. build jsonObject
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.accumulate("username", Email);
//                jsonObject.accumulate("password", pasword);
//                // 4. convert JSONObject to JSON to String
//                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                ObjectMapper mapper = new ObjectMapper();
                json = mapper.writeValueAsString(loginRequestVO);
                Log.d(TAG,json);
                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {

            Log.d(TAG,"AuthLoginTask result = "+result);
            try {
                if (!result.equals("")) {
                    JSONObject jObject = new JSONObject(result);
                    String status = jObject.optString("status");
                    String data = jObject.getString("data");
                    if (status != null) {
                        if (status.equals("1")) {
                            //Create a jsonObject of String data
                            JSONObject dataObject = new JSONObject(data);
                            u_userid = dataObject.getString("userid");
                            u_name = dataObject.getString("name");
                            u_mobile = dataObject.getString("mobile");
                            Email = dataObject.getString("email");
                            u_refcode = dataObject.getString("ref_code");
                            mobile_varify = dataObject.getString("mobile_varified");

                            submitForm_();
                            //Toast.makeText(getApplication(), "You Are Logged In...", Toast.LENGTH_SHORT).show();
                            sharedPreferenceClass.setValue_string("Who_Login", "USER");
                            sharedPreferenceClass.setValue_boolean("USER", true);
                            sharedPreferenceClass.setValue_string("MobileVerify", mobile_varify);

                            if (mobile_varify.equals("0")) {
                                Intent intent4 = new Intent(Login_Activity.this, Otp_Activity.class);
                                startActivity(intent4);
                                finish();
                            }
                            else {
                                Intent intent4 = new Intent(Login_Activity.this, MainActivity.class);
                                startActivity(intent4);
                                finish();
                            }
                        }
                        else if(status.equals("400"))
                        {
                            Log.d(TAG,"Asking for mobile number");
                            if (pd != null && pd.isShowing())
                                pd.dismiss();
                            showMobileNumberDialog();
                        }
                        else {
                            Toast.makeText(Login_Activity.this, data, Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    //This part is call when internet connection not found..
                    Intent i = new Intent(Login_Activity.this, NointernetActivity.class);
                    startActivity(i);
                    Login_Activity.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
                    finish();
                }
            } catch (Exception e) {

            }
            progress.setVisibility(View.GONE);
            super.onPostExecute(result);

        }

        private void submitForm_() {
            try {
                // save auth key
                SharedPreferences.Editor shpEdit = shp.edit();
                shpEdit.putString("AuthKey", Email);
                shpEdit.putString("USER_UID", u_userid);
                shpEdit.putString("UNAME", u_name);
                shpEdit.putString("MBL_NUM", u_mobile);
                shpEdit.putString("USER_REFERAL", u_refcode);
                shpEdit.commit();
                finish();
            } catch (Exception e) {
                e.printStackTrace();
                finish();

            }
        }
    }


        private class LoginAsyncTask extends AsyncTask<String, Void, String> {
            ProgressDialog progressDialog;
            private Activity context;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... urls) {

                InputStream inputStream = null;
                String result = "";
                try {

                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(urls[0]);

                    String json = "";

                    // 3. build jsonObject
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.accumulate("username", Email);
                    jsonObject.accumulate("password", pasword);
                    // 4. convert JSONObject to JSON to String
                    json = jsonObject.toString();

                    // ** Alternative way to convert Person object to JSON string
                    // usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(json);

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the
                    // content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if (inputStream != null)
                        result = convertInputStreamToString(inputStream);
                    else
                        result = "Did not work!";

                } catch (Exception e) {
                    Log.d("InputStream", e.toString());
                }

                // 11. return result
                return result;

                // return POST(urls[0]);
            }

            // onPostExecute displays the results of the AsyncTask.
            @SuppressWarnings("deprecation")
            @Override
            protected void onPostExecute(String result) {

                try {
                    if (!result.equals("")) {
                        JSONObject jObject = new JSONObject(result);
                        String status = jObject.optString("status");
                        String data = jObject.getString("data");
                        if (status != null) {
                            if (status.equals("1")) {
                                //Create a jsonObject of String data
                                JSONObject dataObject = new JSONObject(data);
                                u_userid = dataObject.getString("userid");
                                u_name = dataObject.getString("name");
                                u_mobile = dataObject.getString("mobile");
                                Email = dataObject.getString("email");
                                u_refcode = dataObject.getString("ref_code");
                                mobile_varify = dataObject.getString("mobile_varified");

                                submitForm_();
                                //Toast.makeText(getApplication(), "You Are Logged In...", Toast.LENGTH_SHORT).show();
                                sharedPreferenceClass.setValue_string("Who_Login", "USER");
                                sharedPreferenceClass.setValue_boolean("USER", true);
                                sharedPreferenceClass.setValue_string("MobileVerify", mobile_varify);

                                if (mobile_varify.equals("0")) {
                                    Intent intent4 = new Intent(Login_Activity.this, Otp_Activity.class);
                                    startActivity(intent4);
                                    finish();
                                } else {
                                    Intent intent4 = new Intent(Login_Activity.this, MainActivity.class);
                                    startActivity(intent4);
                                    finish();
                                }
                            } else {
                                Toast.makeText(Login_Activity.this, data, Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        //This part is call when internet connection not found..
                        Intent i = new Intent(Login_Activity.this, NointernetActivity.class);
                        startActivity(i);
                        Login_Activity.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
                        finish();
                    }
                } catch (Exception e) {

                }
                progress.setVisibility(View.GONE);
                super.onPostExecute(result);

            }


            private void submitForm_() {
                try {
                    // save auth key
                    SharedPreferences.Editor shpEdit = shp.edit();
                    shpEdit.putString("AuthKey", Email);
                    shpEdit.putString("USER_UID", u_userid);
                    shpEdit.putString("UNAME", u_name);
                    shpEdit.putString("MBL_NUM", u_mobile);
                    shpEdit.putString("USER_REFERAL", u_refcode);
                    shpEdit.commit();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    finish();

                }
            }

        }


    private void getKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("Keyhash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }

    private void getFacebookUserInfo() {
        Session.openActiveSession(this, true, new Session.StatusCallback() {

            @Override
            public void call(Session session, SessionState state,
                             Exception exception) {

                if (session.isOpened()) {
                    boolean isPermissionAvailable = false;
                    for (int i = 0; i < session.getPermissions().size(); i++) {
                        if (session.getPermissions().get(i).contains("email")) {
                            pd = ProgressDialog.show(Login_Activity.this, "",
                                    "");
                            isPermissionAvailable = true;

                            Request.newMeRequest(session,
                                    new GraphUserCallback() {
                                        @Override
                                        public void onCompleted(
                                                final GraphUser user,
                                                Response response) {

                                            if (user != null) {
                                                Log.d(TAG, user.toString());
                                                getUserInfoFromFacebook(user);
                                            }
                                        }
                                    }).executeAsync();
                        }
                    }
                    if (!isPermissionAvailable)
                        getPermissionUserInfo();
                }
            }
        });
    }



    private void getPermissionUserInfo() {
        String[] permissions = {"basic_info", "email"};
        Session.getActiveSession().requestNewReadPermissions(
                new NewPermissionsRequest(Login_Activity.this, Arrays
                        .asList(permissions)));
    }



    private class User_signup_fb_HttpAsyncTask extends
            AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("username", fb_user_name);
                jsonObject.accumulate("email", fb_user_mail);
                jsonObject.accumulate("deviceid1", imsiSIM1);
                jsonObject.accumulate("deviceid2", imsiSIM2);
                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {
//{"status":"1",
// "name":"PriyadarshiSantanu",
// "custmerid":"SDU201502",
// "email":"priyadarshisantanu@gmail.com",
// "msg":"new user"}

//{"status":"2",
// "name":"PriyadarshiSantanu",
// "custmerid":"SDU201501",
// "mobile":null,
// "email":"priyadarshisantanu@gmail.com",
// "msg":"already exits"}

            try {
                if (!result.equals("")) {
                    JSONObject jObject = new JSONObject(result);
                    //	{"status":"1",
                    // "name":"Durga","custmerid":"SDU001070","email":"panigrahi.durga9@gmail.com","referalid":"oFSNv1HAL0","msg":"new user"}
                    int status = jObject.getInt("status");
                    if (status == 0) {

                        Toast.makeText(getBaseContext(), "TRy Again!", Toast.LENGTH_SHORT).show();
                    } else if (status == 1) {
                        u_userid = jObject.getString("custmerid");
                        f_name1 = jObject.getString("name");
                        f_email1 = jObject.getString("email");
                        f_referal = jObject.getString("referalid");
						/*sharedPreferenceClass.setValue_string("Who_Login","USER");
						sharedPreferenceClass.setValue_boolean("USER", true);*/
                        //submitForm();
                        //ethi karibini, ethi intent re patheibi sethi get kari sethi set karibi
                        //Toast.makeText(Login_Activity.this," Sign in successfuly ", Toast.LENGTH_LONG).show();
                        Intent intent4 = new Intent(Login_Activity.this, Mobile_num.class);
                        intent4.putExtra("uid", u_userid);
                        intent4.putExtra("fuid", userId);
                        intent4.putExtra("uname", f_name1);
                        intent4.putExtra("uemail", f_email1);
                        intent4.putExtra("urefer", f_referal);
                        startActivity(intent4);
                        finish();
                    } else if (status == 2) {
                        //{"status":"2",
                        // "name":"Durga","custmerid":"SDU001070",
                        // "mobile":null,"email":"panigrahi.durga9@gmail.com","referalid":"oFSNv1HAL0","msg":"already exits"}
                        u_userid = jObject.getString("custmerid");
                        f_name1 = jObject.getString("name");
                        f_email1 = jObject.getString("email");
                        f_mbl1 = jObject.getString("mobile");
                        f_referal = jObject.getString("referalid");

                        if (f_mbl1 != "null") {

                            sharedPreferenceClass.setValue_string("Who_Login", "USER");
                            sharedPreferenceClass.setValue_boolean("USER", true);
                            submitForm1();
                            //Toast.makeText(Login_Activity.this," Sign in successfuly ", Toast.LENGTH_LONG).show();

                            Intent intent5 = new Intent(Login_Activity.this, MainActivity.class);
                            intent5.putExtra("uid", u_userid);
                            intent5.putExtra("uname", f_name1);
                            intent5.putExtra("uemail", f_email1);
                            intent5.putExtra("urefer", f_referal);
                            startActivity(intent5);
                            finish();
                        } else {
                            //ethi karibini, ethi intent re patheibi sethi get kari sethi set karibi
                            //submitForm();
                            Intent intent67 = new Intent(Login_Activity.this, Mobile_num.class);
                            intent67.putExtra("uid", u_userid);
                            intent67.putExtra("fuid", userId);
                            intent67.putExtra("uname", f_name1);
                            intent67.putExtra("uemail", f_email1);
                            intent67.putExtra("urefer", f_referal);
                            startActivity(intent67);
                            finish();
                        }


                    }
                } else {
                    Toast.makeText(Login_Activity.this, "Bad Network Connection !!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {

            }

            progress.setVisibility(View.GONE);

            super.onPostExecute(result);

        }

        private void submitForm() {

            try {

                SharedPreferences.Editor shpEdit = shp.edit();
                shpEdit.putString("AuthKey", fb_user_mail);
                shpEdit.putString("UID", userId);
                // shpEdit.putString("USER_UID", u_userid);
                shpEdit.putString("UNAME", fb_user_name);
                shpEdit.putString("USER_REFERAL", f_referal);

                shpEdit.commit();

            } catch (Exception e) {
                e.printStackTrace();


            }
        }

        private void submitForm1() {

            try {

                SharedPreferences.Editor shpEdit = shp.edit();
                shpEdit.putString("AuthKey", f_email1);
                shpEdit.putString("UID", userId);
                shpEdit.putString("USER_UID", u_userid);
                shpEdit.putString("UNAME", f_name1);
                shpEdit.putString("MBL_NUM", f_mbl1);
                shpEdit.putString("USER_REFERAL", f_referal);
                shpEdit.commit();
                finish();
            } catch (Exception e) {
                e.printStackTrace();

                finish();

            }
        }

    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, 9000).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    private void storeRegistrationId(Context context, String regId) {
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private String getRegistrationId(Context context) {
        String registrationId = preferences.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = preferences.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "Analytics version changed.");
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void registerInBackground() {
            String msg="";
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(Login_Activity.this);
                    }
//                    reg_cgm_id = gcm.register(getString(R.string.google_api_sender_id));
                    msg = "Device registered, registration ID=" + reg_cgm_id;
                    Log.d(TAG, "ID GCM: " + reg_cgm_id);
                    //sendRegistrationIdToBackend();
                    storeRegistrationId(Login_Activity.this, reg_cgm_id);

    }


}

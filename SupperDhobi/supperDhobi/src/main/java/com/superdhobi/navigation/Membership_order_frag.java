package com.superdhobi.navigation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superdhobi.AdapterPackage.Membership_order_adapter;
import com.superdhobi.AdapterPackage.Txn_recycler_adapter;
import com.superdhobi.supperdhobi.DividerItemDecoration;
import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 26-04-2016.
 */
public class Membership_order_frag extends Fragment {
    RecyclerView membership_order;
    String[] my_order_list={"SD2016wsh234","SD2016wsh235","SD2016wsh236","SD2016wsh237","SD2016wsh238"};
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.membership_orderhistory, container, false);
        membership_order= (RecyclerView) rootView.findViewById(R.id.recycler_view_order);
        Membership_order_adapter membership_order_adapter= new Membership_order_adapter(getActivity(),my_order_list);

        membership_order.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getActivity());
        membership_order.setLayoutManager(mLayoutManager2);
        membership_order.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        membership_order.setItemAnimator(new DefaultItemAnimator());
        membership_order.setAdapter(membership_order_adapter);
        return rootView;
    }
}

package com.superdhobi.navigation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.superdhobi.supperdhobi.R;

/**
 * Created by PCIS-ANDROID on 15-02-2016.
 */
public class New_frag extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.new_frag,null);

        return rootView;
    }
}

package com.superdhobi.navigation;

import android.app.Activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.navigation.webservices.SharedPreferenceClass;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Pricingfragment extends Fragment {
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;
	Toolbar toolbar;
	public static TabLayout tabLayout;
	public static ViewPager viewPager;
	public static int int_items = 3;
	ListView pricelist;
	TextView type_;
	private SharedPreferences shp;
	static MainActivity priceContext;

	String Cloth, Iron, NormalWash, DryWash, SuperWash, Clothes;
	TextView tv_normal, tv_super, tv_iron, tv_dryclean;
	ArrayList<String> washtype;
	ArrayList<String> wash_id;
	FrameLayout progress;
	static String wash_name;
	static String id, type;
	ImageView pr_home, pr_myorder, pr_profile, pr_price;
	static String newwash_id = "12";
	static String s_wash_id = "14";
	static String d_wash_id = "13";
	static String i_wash_id = "11";
	ImageView nwash_image, swash_image, dwash_image, iwash_image;
	String[] clothes, prices;
	SharedPreferenceClass sharedPreferenceClass;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		shp = priceContext.getSharedPreferences("AuthToken",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor shpEdit = shp.edit();
		shpEdit.putString("start_home", "from_price");
		shpEdit.commit();



		View rootView = inflater.inflate(R.layout.pricepage, container, false);

		sharedPreferenceClass=new SharedPreferenceClass(getActivity());
		/*sharedPreferenceClass.setValue_string("NAV", "FRAG");
		sharedPreferenceClass.setValue_boolean("FRAG", false);*/
		//pricepage
		((MainActivity) getActivity()).toolbar.setSubtitle("Pricing");
		tv_iron = (TextView) rootView.findViewById(R.id.wt_iron);
		tv_normal = (TextView) rootView.findViewById(R.id.wt_nowmal);
		tv_dryclean = (TextView) rootView.findViewById(R.id.wt_dry);
		tv_super = (TextView) rootView.findViewById(R.id.wt_super);
		progress = (FrameLayout) rootView.findViewById(R.id.frame_progress);

		nwash_image = (ImageView) rootView.findViewById(R.id.iv_nwash);
		swash_image = (ImageView) rootView.findViewById(R.id.iv_swash);
		dwash_image = (ImageView) rootView.findViewById(R.id.iv_dwash);
		iwash_image = (ImageView) rootView.findViewById(R.id.iv_iwash);
		LinearLayout nwash_lay = (LinearLayout) rootView.findViewById(R.id.ll_nwash);
		LinearLayout swash_lay = (LinearLayout) rootView.findViewById(R.id.ll_swash);
		LinearLayout dwash_lay = (LinearLayout) rootView.findViewById(R.id.ll_dwash);
		LinearLayout iwash_lay = (LinearLayout) rootView.findViewById(R.id.ll_iwash);

		pr_home = (ImageView) rootView.findViewById(R.id.pr_home);
		pr_myorder = (ImageView) rootView.findViewById(R.id.pr_morder);
		pr_profile = (ImageView) rootView.findViewById(R.id.pr_profile);
		pr_price = (ImageView) rootView.findViewById(R.id.pr_price);
		type_ = (TextView) rootView.findViewById(R.id.type);
		pricelist = (ListView) rootView.findViewById(R.id.price_list);
		//new display_pricetagAsynTask().execute(ServerLinks.view_pricelist);
		type_.setText("NormalWash");
		new price_listAsyntask().execute(ServerLinks.view_pricelist);
		nwash_lay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				newwash_id = "12";
				type_.setText("NormalWash");
				nwash_image.setImageResource(R.drawable.hm_wash_actv);
				swash_image.setImageResource(R.drawable.hm_spws);
				dwash_image.setImageResource(R.drawable.hm_drws);
				iwash_image.setImageResource(R.drawable.hm_iron);

				tv_normal.setTextColor(getResources().getColor(R.color.app_color));
				tv_super.setTextColor(getResources().getColor(R.color.text));
				tv_dryclean.setTextColor(getResources().getColor(R.color.text));
				tv_iron.setTextColor(getResources().getColor(R.color.text));
				new price_listAsyntask().execute(ServerLinks.view_pricelist);
				/*wash_name = tv_iron.getText().toString();
				int pos = washtype.indexOf(wash_name);
				id = wash_id.get(pos);
				type = washtype.get(pos);
				Details_class();*/
			}
		});

		swash_lay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				newwash_id = "14";
				type_.setText("SuperWash");
				nwash_image.setImageResource(R.drawable.hm_wash);
				swash_image.setImageResource(R.drawable.hm_spws_actv);
				dwash_image.setImageResource(R.drawable.hm_drws);
				iwash_image.setImageResource(R.drawable.hm_iron);

				tv_normal.setTextColor(getResources().getColor(R.color.text));
				tv_super.setTextColor(getResources().getColor(R.color.app_color));
				tv_dryclean.setTextColor(getResources().getColor(R.color.text));
				tv_iron.setTextColor(getResources().getColor(R.color.text));
				new price_listAsyntask().execute(ServerLinks.view_pricelist);
				/*wash_name = tv_normal.getText().toString();
				int pos = washtype.indexOf(wash_name);
				id = wash_id.get(pos);
				type = washtype.get(pos);
				Details_class();*/

			}
		});

		dwash_lay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				newwash_id = "13";
				type_.setText("DryClean");
				nwash_image.setImageResource(R.drawable.hm_wash);
				swash_image.setImageResource(R.drawable.hm_spws);
				dwash_image.setImageResource(R.drawable.hm_drws_actv);
				iwash_image.setImageResource(R.drawable.hm_iron);

				tv_normal.setTextColor(getResources().getColor(R.color.text));
				tv_super.setTextColor(getResources().getColor(R.color.text));
				tv_dryclean.setTextColor(getResources().getColor(R.color.app_color));
				tv_iron.setTextColor(getResources().getColor(R.color.text));
				new price_listAsyntask().execute(ServerLinks.view_pricelist);
				/*wash_name = tv_dryclean.getText().toString();
				int pos = washtype.indexOf(wash_name);
				id = wash_id.get(pos);
				type = washtype.get(pos);
				Details_class();*/
			}
		});

		iwash_lay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				newwash_id = "11";
				type_.setText("Iron");
				nwash_image.setImageResource(R.drawable.hm_wash);
				swash_image.setImageResource(R.drawable.hm_spws);
				dwash_image.setImageResource(R.drawable.hm_drws);
				iwash_image.setImageResource(R.drawable.hm_iron_actv);

				tv_normal.setTextColor(getResources().getColor(R.color.text));
				tv_super.setTextColor(getResources().getColor(R.color.text));
				tv_dryclean.setTextColor(getResources().getColor(R.color.text));
				tv_iron.setTextColor(getResources().getColor(R.color.app_color));
				new price_listAsyntask().execute(ServerLinks.view_pricelist);
				/*wash_name = tv_super.getText().toString();
				int pos = washtype.indexOf(wash_name);
				id = wash_id.get(pos);
				type = washtype.get(pos);
				Details_class();*/
			}
		});

		pr_home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();

			}
		});

		pr_myorder.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Myorderfragment()).commit();

			}
		});
		pr_price.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Pricingfragment()).commit();

			}
		});
		pr_profile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFragmentManager = getFragmentManager();
				mFragmentTransaction = mFragmentManager.beginTransaction();
				mFragmentTransaction.replace(R.id.containerView,new Profilefragment()).commit();

			}
		});

		return rootView;
	}

	public void Details_class() {
		Intent normal = new Intent(getActivity(), Detailprice.class);
		normal.putExtra("WASH_ID", id);
		normal.putExtra("WASH_TYPE", type);
		getActivity().overridePendingTransition(R.anim.right_in,
				R.anim.left_out);
		startActivity(normal);
	}

	/*private class display_pricetagAsynTask extends
			AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			*//*
			 * progressDialog = new ProgressDialog(getActivity()); // Set
			 * progressdialog title
			 * //mProgressDialog.setTitle("Android JSON Parse Tutorial"); // Set
			 * progressdialog message progressDialog.setMessage("Loading...");
			 * progressDialog.setIndeterminate(false); // Show progressdialog
			 * progressDialog.show();
			 *//*
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				// jsonObject.accumulate("userid", hm_userid);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("unused")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
//{"status":"1",
// "washtype":
// [{"washtype":"Iron","wash_id":"11"},
// {"washtype":"Normal wash","wash_id":"12"},
// {"washtype":"Dry Wash","wash_id":"13"},
// {"washtype":"Super Wash","wash_id":"14"}]}

			washtype = new ArrayList<String>();
			wash_id = new ArrayList<String>();

			JSONObject jsonResponse;

			try {
				JSONObject jsonObject = new JSONObject(result);

				String status = jsonObject.optString("status");

				if (status.equals("1")) {
					JSONArray jArr = jsonObject.getJSONArray("washtype");
					if (jArr != null) {

						for (int i = 0; i < jArr.length(); i++) {
							jsonResponse = jArr.getJSONObject(i);
							String wtype = jsonResponse.optString("washtype")
									.toString();
							String wid = jsonResponse.optString("wash_id")
									.toString();
							washtype.add(wtype);
							wash_id.add(wid);
							// Toast.makeText(getActivity(),
							// "Please Check Your Internet Connection.",
							// Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(getActivity(), "You have no adress",
								Toast.LENGTH_SHORT).show();
					}
				}
			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}

			for (int j = 0; j < washtype.size(); j++) {
				String pos = washtype.get(j);

				if (j == 0) {
					// ethi 1st textview re ad_name set karibu

					tv_iron.setText(pos);

				} else if (j == 1) {
					// ethi 2nd ta
					tv_normal.setText(pos);

				} else if (j == 2) {
					// ethi 3rd ta
					tv_dryclean.setText(pos);

				} else {
					tv_super.setText(pos);

				}
			}
		}
	}*/

	private class price_listAsyntask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(getActivity()); // Set
			 * progressdialog title
			 * //mProgressDialog.setTitle("Android JSON Parse Tutorial"); // Set
			 * progressdialog message progressDialog.setMessage("Loading...");
			 * progressDialog.setIndeterminate(false); // Show progressdialog
			 * progressDialog.show();
			 */
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {

			InputStream inputStream = null;
			String result = "";
			try {

				// 1. create HttpClient
				HttpClient httpclient = new DefaultHttpClient();

				// 2. make POST request to the given URL
				HttpPost httpPost = new HttpPost(urls[0]);

				String json = "";

				// 3. build jsonObject
				JSONObject jsonObject = new JSONObject();
				jsonObject.accumulate("wash_id", newwash_id);

				// 4. convert JSONObject to JSON to String
				json = jsonObject.toString();

				// ** Alternative way to convert Person object to JSON string
				// usin Jackson Lib
				// ObjectMapper mapper = new ObjectMapper();
				// json = mapper.writeValueAsString(person);

				// 5. set json to StringEntity
				StringEntity se = new StringEntity(json);

				// 6. set httpPost Entity
				httpPost.setEntity(se);

				// 7. Set some headers to inform server about the type of the
				// content
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// 8. Execute POST request to the given URL
				HttpResponse httpResponse = httpclient.execute(httpPost);

				// 9. receive response as inputStream
				inputStream = httpResponse.getEntity().getContent();

				// 10. convert inputstream to string
				if (inputStream != null)
					result = convertInputStreamToString(inputStream);
				else
					result = "Did not work!";

			} catch (Exception e) {
				Log.d("InputStream", e.toString());
			}

			// 11. return result
			return result;

			// return POST(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("unused")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);

			/*
			 * washtype=new ArrayList<String>(); wash_id=new
			 * ArrayList<String>();
			 */

			JSONObject jsonResponse;

			try {
				if (!result.equals("")){
				JSONObject jsonObject = new JSONObject(result);
//{"status":"1","price":[{"clothes":"SHIRT \/ T-SHIRT","price":"10"},
// {"clothes":"JEANS \/ PANT \/ TROUSER","price":"10"},
// {"clothes":"LOWER","price":"10"},
// {"clothes":"BEDSHEET SINGLE","price":"15"},
// {"clothes":"PILLOW COVER","price":"8"},
// {"clothes":"BEDSHEET DOUBLE","price":"20"},
// {"clothes":"TOPS \/KURTI","price":"10"},
// {"clothes":"LEGGING \/ STOCKINGS \/ SALWAR","price":"10"},
// {"clothes":"KURTA(SYNTHETIC\/COTTON)","price":"15"},
// {"clothes":"KURTA(PRINTED simple)","price":"10"},{"clothes":"BEDCOVER COTTON PREMIUM","price":"25"},{"clothes":"TOWEL BATHING","price":"10"},{"clothes":"DUPATTA","price":"10"},{"clothes":"Jacket","price":"40"},{"clothes":"PYAJAMA SIMPLE","price":"10"},{"clothes":"Boxer\/Shorts","price":"10"}]}
					String status = jsonObject.optString("status");

					if (status.equals("1")) {
						JSONArray jArr = jsonObject.getJSONArray("price");
						if (jArr != null) {
							clothes = new String[jArr.length()];
							prices = new String[jArr.length()];
							for (int i = 0; i < jArr.length(); i++) {
								jsonResponse = jArr.getJSONObject(i);

								clothes[i] = jsonResponse.getString("clothes");
								prices[i] = jsonResponse.getString("price");

							}
						} else {
							//Toast.makeText(getActivity(), "You have no adress",Toast.LENGTH_SHORT).show();
						}
					}
					Price_list_adapter adapter = new Price_list_adapter(getActivity(), clothes, prices);

					pricelist.setAdapter(adapter);
				}
            else{
					Toast.makeText(getActivity(), "Bad Network Connection !!",
							Toast.LENGTH_SHORT).show();
				}

			}

			catch (Exception e) {
				Log.e("String ", e.toString());
			}


		}
	}

	public class Price_list_adapter extends ArrayAdapter<String> {
		private final Activity context;
		private final String[] clothes;
		private final String[] prices;

		public Price_list_adapter(Activity context, String[] clothes,
								  String[] prices) {
			super(context, R.layout.list_price, clothes);
			// TODO Auto-generated constructor stub
			this.context = context;
			this.clothes = clothes;
			this.prices = prices;
		}

		public View getView(int position, View view, ViewGroup parent) {
			LayoutInflater inflater = context.getLayoutInflater();
			View rowView = inflater.inflate(R.layout.list_price, null, true);
			TextView item = (TextView) rowView.findViewById(R.id.item_name);
			TextView item_price = (TextView) rowView
					.findViewById(R.id.item_price);

			item.setText(clothes[position]);
			item_price.setText(prices[position]);

			return rowView;
		}
}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	@Override
	public void onAttach(Activity activity) {
		priceContext = (MainActivity) activity;
		super.onAttach(activity);
	}

	public void onBackPressed() {
/*
		mFragmentManager = getFragmentManager();
		mFragmentTransaction = mFragmentManager.beginTransaction();
		mFragmentTransaction.replace(R.id.containerView,new Homefragment()).commit();*/

		/*FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Homefragment llf = new Homefragment();
		ft.replace(R.id.containerView, llf);
		ft.commit();*/
	}



}

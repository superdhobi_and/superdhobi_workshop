package com.superdhobi.navigation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.superdhobi.navigation.webservices.ServerLinks;
import com.superdhobi.supperdhobi.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Pick_order_del_details extends Activity {
	String order_ref,user_id;
	FrameLayout progress;
	TextView orderef,washtp,picktnd,deltnd,add,del_status;
	String[] wtype;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pick_order_details);
		progress=(FrameLayout)findViewById(R.id.frame_progress);
		order_ref=getIntent().getStringExtra("ODR_RF");
		user_id=getIntent().getStringExtra("EMP_ID_O");
		
		orderef=(TextView) findViewById(R.id.pc_order_id);
		washtp=(TextView)findViewById(R.id.pc_wash_type);
		picktnd=(TextView) findViewById(R.id.pc_pick_Date);
		//deltnd=(TextView) findViewById(R.id.de_dlvr_Date);
		add=(TextView) findViewById(R.id.pc_address_tag);
		//del_status=(TextView) findViewById(R.id.del_status);
		
		orderef.setText(order_ref);
		new pickup_view_DetailsAsyntask().execute(ServerLinks.pickup_view);
		
		
	}
	private class pickup_view_DetailsAsyntask extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog ;
		@Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }
		
		@Override
        protected String doInBackground(String... urls) {
 
			InputStream inputStream = null;
	        String result = "";
	        try {
	 
	            // 1. create HttpClient
	            HttpClient httpclient = new DefaultHttpClient();
	 
	            // 2. make POST request to the given URL
	            HttpPost httpPost = new HttpPost(urls[0]);
	 
	            String json = "";
	 
	            // 3. build jsonObject
	            JSONObject jsonObject = new JSONObject();
	            jsonObject.accumulate("empid", user_id);
	            jsonObject.accumulate("order_ref", order_ref);
	            
	            
	            // 4. convert JSONObject to JSON to String
	            json = jsonObject.toString();
	 
	            // ** Alternative way to convert Person object to JSON string usin Jackson Lib 
	            // ObjectMapper mapper = new ObjectMapper();
	            // json = mapper.writeValueAsString(person); 
	 
	            // 5. set json to StringEntity
	            StringEntity se = new StringEntity(json);
	 
	            // 6. set httpPost Entity
	            httpPost.setEntity(se);
	 
	            // 7. Set some headers to inform server about the type of the content   
	            httpPost.setHeader("Accept", "application/json");
	            httpPost.setHeader("Content-type", "application/json");
	 
	            // 8. Execute POST request to the given URL
	            HttpResponse httpResponse = httpclient.execute(httpPost);
	 
	            // 9. receive response as inputStream
	            inputStream = httpResponse.getEntity().getContent();
	 
	            // 10. convert inputstream to string
	            if(inputStream != null)
	                result = convertInputStreamToString(inputStream);
	            else
	                result = "Did not work!";
	 
	        } catch (Exception e) {
	            Log.d("InputStream", e.toString());
	        }
	 
	        // 11. return result
	        return result;
 
           // return POST(urls[0]);
        }
		// onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
		@Override
        protected void onPostExecute(String result) {
        	super.onPostExecute(result);
        	progress.setVisibility(View.GONE);
        	 
        	/*washtype=new ArrayList<String>();
            wash_id=new ArrayList<String>();*/
        	
        	JSONObject jsonResponse;
            
            try {
            	JSONObject jsonObject=new JSONObject(result);
            	
            	/*{"status":"1",
            	"myorder_detail":
            	        {"washtype":"[\\\"12\\\"]","pick_date":"2015-08-11","delivery_date":"2015-08-12",
            	"address_detail":
            	    {"tag_addressname":"fhh",
            	    "cityname":"Khorda",
            	    "centrename":"crpf",
            	    "circlename":"office",
            	    "locationname":"vim",
            	    "plot_detail":"fghv",
            	    "landmark_detail":"fgughsf"},
            	"respond_status":"0","pick_status":"0","deli_status":"0","pick_time":"6:00 pm-7:30 pm",
            	"ddelivery_time":"6:00 pm-7:30 pm"}}
            	*/
            	String status=jsonObject.optString("status");
            	
            	if(status.equals("1")){
            		JSONObject jo = jsonObject.getJSONObject("myorder_detail");
            		 //String wtype= jo.optString("washtype").toString();
            		JSONArray jArr=jo.getJSONArray("washtype");
            		if(jArr!=null)
                	{
            			wtype = new String[jArr.length()];
                    	for(int i=0;i<jArr.length();i++){
                    	jsonResponse = jArr.getJSONObject(i);
                    	
                    	wtype[i] = jsonResponse.getString("wash");    
                    		
                    	washtp.setText(washtp.getText().toString()+","+wtype[i]);    
                	}
                	}
            		
            		//String strArray[] = wtype.split(" ");
                    
                   // ArrayList<String> wash=new ArrayList<String>();
                   
                    //print elements of String array
                    /*for(int i=0; i < strArray.length; i++){
                            System.out.println(strArray[i]);
                            
                            String str = strArray[i].replaceAll("\\[", "").replaceAll("\\]","");
                            str=str.replace("\"", "");
                            wash.add(str);
                    }*/
            		 
            		 JSONObject joad=jo.getJSONObject("address_detail");
            		 String tag_d=joad.optString("tag_addressname").toString();
            		 String plot=joad.optString("plot_detail").toString();
            		 String landmark=joad.optString("landmark_detail").toString();
            		 String loc=joad.optString("locationname").toString();
            		 String circle=joad.optString("circlename").toString();
            		 String center=joad.optString("centrename").toString();
            		 String city=joad.optString("cityname").toString();
            		
            		 
            		 String pick_d= jo.optString("pick_date").toString();
            		 String pickt_d= jo.optString("pick_time").toString();
            		/* String del_d= jo.optString("delivery_date").toString();
            		 String delt_d= jo.optString("ddelivery_time").toString();*/
            		 
            		 /*for(int i=0;i<wash.size();i++){
            			 
            		 washtp.setText(washtp.getText().toString()+","+wash.get(i));
            		 }*/
            		 picktnd.setText(pick_d+","+pickt_d);
            		//deltnd.setText(del_d+","+delt_d);
            		add.setText(tag_d+"\n"+plot+","+landmark+"\n"+loc+","+circle+"\n"+center+","+city);
            	
            }
            	}
        	
        	catch(Exception e){
        	Log.e("String " , e.toString());	
	        }
           
            }
        }
	private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	    BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	    String line = "";
	    String result = "";
	    while((line = bufferedReader.readLine()) != null)
	        result += line;

	    inputStream.close();
	    return result;
	     }
}
